package org.evolizer.ontology.java.builder;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.IJavaProject;
import org.evolizer.ontology.java.builder.model.IJavaModelEntities;
import org.evolizer.ontology.java.builder.model.JavaOntModel;

/**
 * Stores an instance of {@link JavaOntModel} for {@link IJavaProject}s.
 * 
 * Uses a singleton pattern.
 * 
 * @author wuersch
 * 
 */
public enum JavaOntModelStore {
	INSTANCE;
	
	private Map<IJavaProject, JavaOntModel> models;
	private Map<JavaOntModel, IJavaProject> projects;

	private JavaOntModelStore() {
		models = new HashMap<IJavaProject, JavaOntModel>();
		projects = new HashMap<JavaOntModel, IJavaProject>();
	}
	
	/**
	 * Clears the model store. Useful for cleanup.
	 */
	public void shutdown() {
		models.clear();
		models = null;
		projects.clear();
		projects = null;

	}

	/**
	 * Gets the model for a given instance of {@link IJavaProject}. Creates a
	 * new one, if there wasn't a model stored for the project before.
	 * 
	 * @param javaProject
	 *            the {@link IJavaProject} that we want to retrieve the
	 *            {@link JavaOntModel} of.
	 * @return an instance of {@link JavaOntModel} that is associated to
	 *         the project, or <code>null</code> if no model has been stored for
	 *         the project so far.
	 */
	public JavaOntModel getModel(IJavaProject javaProject) {
		return models.get(javaProject);
	}
	
	public boolean hasModel(IJavaProject javaProject) {
		return models.containsKey(javaProject);
	}
	
	public IJavaProject getProject(IJavaModelEntities model) {
		return projects.get(model);
	}

	/**
	 * Convenience method to store a newly created, empty model for a project.
	 * 
	 * @param javaProject
	 *            an instance of {@link IJavaProject}.
	 * @return the stored model.
	 */
	public JavaOntModel storeModel(IJavaProject javaProject) {
		return storeModel(javaProject, new JavaOntModel(javaProject.getElementName()));
	}
	
	/**
	 * Stores a {@link JavaOntModel} for a {@link IJavaProject}. An existing
	 * model will be replaced.
	 * 
	 * @param javaProject
	 *            an instance of {@link IJavaProject}.
	 * @param model
	 *            an instance of {@link JavaOntModel} that should be associated
	 *            with the project. No assumptions are made on whether the model
	 *            is complete or consistent.
	 * @return the stored model for convenience.
	 */
	public JavaOntModel storeModel(IJavaProject javaProject, JavaOntModel model) {
		models.put(javaProject, model);
		projects.put(model, javaProject);
		
		return model;
	}
}
