package org.evolizer.ontology.java.builder;

import org.eclipse.jdt.core.IJavaProject;

// TODO move to a separate folder remove visibility of builder folder?
public interface IBuildProgressListener {
	public void projectComplete(IJavaProject javaProject);
	
}
