package org.evolizer.ontology.java.builder;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

public class JavaOntModelBuilder extends IncrementalProjectBuilder {
	public static final String BUILDER_ID = "org.evolizer.ontology.java.javaOntModelBuilder";
	
	// TODO create notifier class (extend LinkedList?)
	private static List<IBuildProgressListener> progressListeners = new LinkedList<IBuildProgressListener>();
	
	private ProjectAnalyzer fAnalyzer;
	
	public JavaOntModelBuilder() {
		fAnalyzer = new ProjectAnalyzer();
	}
	
	public static void addProgressListener(IBuildProgressListener listener) {
		if(listener != null && !progressListeners.contains(listener)) {
			progressListeners.add(listener);
		}
	}
	
	public static void removeProgressListener(IBuildProgressListener listener) {
		// TODO Auto-generated method stub
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.internal.events.InternalBuilder#build(int,
	 *      java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
	 */	
	@Override @SuppressWarnings("rawtypes")
	protected IProject[] build(int kind, Map args, IProgressMonitor monitor)
			throws CoreException {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		
		if (kind == FULL_BUILD) {
			fullBuild(subMonitor);
		} else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(subMonitor);
			} else {
				incrementalBuild(delta, subMonitor);
			}
		}
		
		notifyListeners(getJavaProject());
		
		return null; // we're not interested in the deltas of other projects - the current one is implicitly included anyway.
	}

	// TODO param needed?
	private void notifyListeners(IJavaProject project) {
		for(IBuildProgressListener listener : progressListeners) {
			listener.projectComplete(project);
		}
	}

	private void fullBuild(final IProgressMonitor monitor)
			throws CoreException {	
		SubMonitor subMonitor = SubMonitor.convert(monitor, 1);
		
		IJavaProject theJavaProject = getJavaProject();
		if(theJavaProject != null) { // there will be no analysis if project is not an IJavaProject.
			fAnalyzer.performFullAnalysis(theJavaProject, subMonitor.newChild(1));
		}
	}

	private void incrementalBuild(IResourceDelta delta, IProgressMonitor monitor) throws CoreException {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
	}

	private IJavaProject getJavaProject() {
		IJavaProject theJavaProject = JavaCore.create(getProject());
		return theJavaProject;
	}
}
