package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

public class MethodParameterHandle extends AbstractSourceCodeEntityHandle {

	public MethodParameterHandle(Individual theEntity, JavaOntModel model) {
		super(theEntity, model);
	}
	
	public void setType(AbstractTypeHandle typeHandle) {
		addRelation(IJavaModelEntities.HAS_DATA_TYPE, typeHandle);
	}
	
	public void setPosition(int number) {
		addLiteral(IJavaModelEntities.HAS_POSITION, number);
	}
}
