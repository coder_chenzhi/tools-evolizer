package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java packages.
 * 
 * @author wuersch
 *
 */
public class PackageHandle extends AbstractSourceCodeEntityHandle {
	/**
	 * Constructor.
	 * 
	 * @param thePackage
	 *            the package to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public PackageHandle(Individual thePackage, JavaOntModel model) {
		super(thePackage, model);
	}
	
	/**
	 * Adds an interface to the underlying package.
	 * 
	 * @param qualifiedInterfaceName
	 *            the qualified interface name
	 * @param localInterfaceName
	 *            the local identifier of the interface, used as human-readable
	 *            label.
	 * @param location
	 *            the source code location of the interface declaration.
	 * 
	 * @return a handle to the newly added interface.
	 */
	public InterfaceHandle addInterface(String qualifiedInterfaceName, String localInterfaceName, SourceCodeLocationHelper location) {
		InterfaceHandle interfaceHandle = model().getInterfaceHandleFor(qualifiedInterfaceName, localInterfaceName);
	
		addType(interfaceHandle, location);
		
		return interfaceHandle;
	}
	
	/**
	 * Adds an interface to the underlying package.
	 * 
	 * @param qualifiedClassName
	 *            the qualified class name
	 * @param localClassName
	 *            the local identifier of the class, used as human-readable
	 *            label.
	 * @param location
	 *            the source code location of the class declaration.
	 * 
	 * @return a handle to the newly added interface.
	 */
	public ClassHandle addClass(String qualifiedClassName, String localClassName, SourceCodeLocationHelper location) {
		ClassHandle classHandle = model().getClassHandleFor(qualifiedClassName, localClassName);
		
		addType(classHandle, location);
		
		return classHandle;
	}
	
	/**
	 * Helper method for
	 * {@link #addInterface(String, String, SourceCodeLocationHelper)} and
	 * {@link #addClass(String, String, SourceCodeLocationHelper)}.
	 * 
	 * @param typeHandle
	 *            the handle of the type that should be added to the underlying
	 *            package.
	 * @param localName
	 *            the local identifier of the type.
	 * @param location
	 *            the source code location of the type declaration.
	 */
	private void addType(AbstractTypeHandle typeHandle, SourceCodeLocationHelper location) {
		typeHandle.addRelation(IJavaModelEntities.IS_PACKAGE_MEMBER_OF , this);
				
		location.applyTo(typeHandle);
	}
}
