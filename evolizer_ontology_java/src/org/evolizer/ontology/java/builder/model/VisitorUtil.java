package org.evolizer.ontology.java.builder.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.evolizer.core.exceptions.EvolizerRuntimeException;

// TODO write a test.
public class VisitorUtil {

	static boolean isInterface(TypeDeclaration node) {
		return node.isInterface();
	}

	static boolean isInterface(Type type) {
		return type.resolveBinding().isInterface();
	}

	static boolean isConstructor(MethodDeclaration node) {
		return node.isConstructor();
	}

	static String resolveAndEncode(TypeDeclaration node) {		
		return transformAndEncode(qualifierFor(node.resolveBinding()));
	}

	static String resolveAndEncode(Type type) {
		return transformAndEncode(qualifierFor(type.resolveBinding()));
	}

	static String resolveAndEncode(MethodDeclaration node) {
		return resolveAndEncode(node.resolveBinding());
	}

	static String resolveAndEncode(IMethodBinding binding) {
		String methodIdentifier = binding.getName();
		String encodedParentTypeQualifier = transformAndEncode(qualifierFor(binding.getDeclaringClass()));
		
		StringBuilder sb = new StringBuilder(methodIdentifier).append('(');		
		
		// add fully qualified param type names, delimited by '+'
		ITypeBinding[] params = binding.getParameterTypes();
		for(int i = 0; i < params.length; i++) {
			if(i > 0) { // separate params
				sb.append('+');
			}
			
			String parameterTypeQualifier = params[i].getQualifiedName();
			sb.append(parameterTypeQualifier);
		}
				
		sb.append(')');
		
		return encodedParentTypeQualifier + '/' + encode(sb.toString());
	}

	static String resolveAndEncodeCallee(MethodInvocation node) {
		IMethodBinding resolvedBinding = node.resolveMethodBinding();
		IMethodBinding callee = resolvedBinding.getMethodDeclaration();
		
		return resolveAndEncode(callee);
	}

	static String resolveAndEncodeCallee(ConstructorInvocation node) {
		IMethodBinding resolvedBinding = node.resolveConstructorBinding();
		IMethodBinding callee = resolvedBinding.getMethodDeclaration();
		
		return resolveAndEncode(callee);
	}

	static String qualifierFor(ITypeBinding binding) { // if(binding.isPrimitive())
		return binding.getQualifiedName();
	}

	static String identifierFor(AbstractTypeDeclaration node) {
		return node.getName().getIdentifier();
	}

	static String identifierFor(Type type) { // ArrayType, ParameterizedType, PrimitiveType, QualifiedType, SimpleType, WildcardType
		return type.resolveBinding().getName();
	}

	static String identifierFor(VariableDeclarationFragment fragment) {
		return fragment.getName().getIdentifier();
	}

	static String identifierFor(SingleVariableDeclaration svd) {
		return svd.getName().getIdentifier();
	}

	static String identifierFor(IVariableBinding fieldBinding) {
		return fieldBinding.getName();
	}

	static String identifierFor(MethodDeclaration node) {
		return node.getName().getIdentifier();
	}

	static String identifierFor(MethodInvocation node) {
		return node.getName().getIdentifier();
	}

	static String identifierFor(ConstructorInvocation node) {
		return node.resolveConstructorBinding().getName();
	}

	static String encode(String uniqueName) throws EvolizerRuntimeException {
		try {
			return URLEncoder.encode(uniqueName, "UTF-8");
		} catch (UnsupportedEncodingException ex) {
			// Should never happen.
			throw new EvolizerRuntimeException("UTF-8 encoding is not supported for identifier/URI.", ex);
		}
	}

	static String transformAndEncode(String qualifiedTypeName) {
	    int pos = qualifiedTypeName.lastIndexOf('.');
	    if (pos > -1) {
	    	return new StringBuilder().append(encode(qualifiedTypeName.substring(0, pos)))
	    							  .append('/')
	    							  .append(encode(qualifiedTypeName.substring(pos + 1, qualifiedTypeName.length())))
	    							  .toString();
	    } else {
	        return encode(qualifiedTypeName);
	    }
	}

}
