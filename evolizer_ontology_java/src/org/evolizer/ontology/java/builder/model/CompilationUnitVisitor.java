package org.evolizer.ontology.java.builder.model;

import java.util.List;
import java.util.Stack;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class CompilationUnitVisitor extends ASTVisitor {
	private JavaOntModel model;
	
	private ICompilationUnit cu;
	
	private PackageHandle packageHandle;

	private Stack<AbstractTypeHandle> typeStack;
	private Stack<AbstractCallableHandle> callableStack;
	
	public CompilationUnitVisitor(ICompilationUnit cu, String qualifiedPackageName, JavaOntModel model) {
		this.cu = cu;
		this.model = model;

		typeStack = new Stack<AbstractTypeHandle>();
		callableStack = new Stack<AbstractCallableHandle>();
		
		if(qualifiedPackageName.length() == 0) {
			qualifiedPackageName = "<default package>";
		}
	
		packageHandle = model.getPackageHandleFor(VisitorUtil.encode(qualifiedPackageName), qualifiedPackageName);
	}
	
	@Override
	public boolean visit(AnonymousClassDeclaration node) {
		return false; // FIXME not yet supported.
	}
	
	@Override
	public boolean visit(Initializer node) {
		return false; // FIXME not yet supported.
	}
		
	@Override
	public boolean visit(TypeDeclaration node) {
		if(noCurrentType()) {
			AbstractTypeHandle typeHandle = createTypeFrom(node);
			addInterfaces(typeHandle, node.superInterfaceTypes());
			remember(typeHandle);
		} else {
			// typeStack.peek().addClass
			return false; // TODO inner classes shall be skipped for now
		}
		
		return super.visit(node);
	}
	
	@Override
	public void endVisit(TypeDeclaration node) {
		if(!node.isLocalTypeDeclaration() && !node.isMemberTypeDeclaration()) { // FIXME local inner and member types *sigh*
			forgetCurrentType();
		}
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		AbstractTypeHandle currentTypeHandle = currentType();
		Type fieldType = node.getType();
		
		List<?> fragments = node.fragments();
		for(Object o : fragments) {
			if(o instanceof VariableDeclarationFragment) {
				VariableDeclarationFragment fragment = (VariableDeclarationFragment) o;
				
				String fieldIdentifier = VisitorUtil.identifierFor(fragment);
				String encodedParentTypeQualifier = VisitorUtil.resolveAndEncode((TypeDeclaration) node.getParent()); // FIXME fails for anonymous class declarations
				String encodedFieldQualfier = encodedParentTypeQualifier + '/' + fieldIdentifier;
				
				FieldHandle fieldHandle = currentTypeHandle.addField(encodedFieldQualfier, fieldIdentifier, new SourceCodeLocationHelper(cu, fragment));
				AbstractTypeHandle fieldTypeHandle = model.getTypeHandleFor(VisitorUtil.resolveAndEncode(fieldType), VisitorUtil.identifierFor(fieldType), VisitorUtil.isInterface(fieldType));
				fieldHandle.setType(fieldTypeHandle);
			}
		}
		
		return false; // No need to descend deeper into the subtree - var decl fragments are already handled above.
	}
	
	@Override
	public boolean visit(MethodDeclaration node) {
		String encodedCallableQualifier = VisitorUtil.resolveAndEncode(node);
		String callableIdentifier = VisitorUtil.identifierFor(node);
		
		AbstractCallableHandle callableHandle;
		if(VisitorUtil.isConstructor(node)) {
			callableHandle = currentType().addConstructor(encodedCallableQualifier, callableIdentifier, new SourceCodeLocationHelper(cu, node));
		} else {
			MethodHandle methodHandle = currentType().addMethod(encodedCallableQualifier, callableIdentifier, new SourceCodeLocationHelper(cu, node));			
			addReturnType(node, methodHandle);
			callableHandle = methodHandle;
		}
		
		
		List<?> parameters = node.parameters();
		for(int i = 0; i < parameters.size(); i++){ 
			SingleVariableDeclaration svd = (SingleVariableDeclaration) parameters.get(i);
			Type paramType = svd.getType();
						
			String encodedParamQualifier = encodedCallableQualifier + '/' + VisitorUtil.encode(VisitorUtil.identifierFor(svd));
			MethodParameterHandle paramHandle = callableHandle.addParameter(encodedParamQualifier, VisitorUtil.identifierFor(svd), VisitorUtil.identifierFor(paramType), new SourceCodeLocationHelper(cu, svd));
			AbstractTypeHandle paramTypeHandle = model.getTypeHandleFor(VisitorUtil.resolveAndEncode(paramType), VisitorUtil.identifierFor(paramType), VisitorUtil.isInterface(paramType));
			paramHandle.setType(paramTypeHandle);
			paramHandle.setPosition(i);
		}
		
		remember(callableHandle);
		
		return super.visit(node);
	}

	@Override
	public void endVisit(MethodDeclaration node) {
		forgetLastCallable();
	}
	
	@Override
	public boolean visit(FieldAccess node) {
		createAccessFrom(node.resolveFieldBinding(), new SourceCodeLocationHelper(cu, node));
			
		return super.visit(node);
	}

	@Override
    public boolean visit(QualifiedName node) {
    	createAccessFrom(node, new SourceCodeLocationHelper(cu, node));
    	
    	return super.visit(node);
    }

	@Override
	public boolean visit(SimpleName node) {
		createAccessFrom(node, new SourceCodeLocationHelper(cu, node));
		
		return super.visit(node);
	}
	
	@Override
	public boolean visit(MethodInvocation node) {
		IMethodBinding binding = node.resolveMethodBinding();
		if(binding.getDeclaringClass() != null && 
		   binding.getDeclaringClass().isTopLevel() /* hack, resolve */) {
			
			// A constructor/method can only invoke other methods - no need to check for constructors here.
			MethodHandle methodHandle = model.getMethodHandleFor(VisitorUtil.resolveAndEncodeCallee(node), VisitorUtil.identifierFor(node));

			currentCallable().addMethodInvocation(methodHandle, new SourceCodeLocationHelper(cu, node));
		}
				
		return super.visit(node);
	}
	
	@Override
	public boolean visit(ConstructorInvocation node) {
		IMethodBinding binding = node.resolveConstructorBinding();
		if (binding.getDeclaringClass() != null &&
			binding.getDeclaringClass().isTopLevel() /* hack, resolve */) {

			ConstructorHandle constructorHandle = model.getConstructorHandleFor(VisitorUtil.resolveAndEncodeCallee(node), VisitorUtil.identifierFor(node));

			currentCallable().addMethodInvocation(constructorHandle, new SourceCodeLocationHelper(cu, node));
		}

		return super.visit(node);
	}

	private AbstractTypeHandle createTypeFrom(TypeDeclaration node) {
		String encodedTypeQualifier = VisitorUtil.resolveAndEncode(node);
		String typeIdentifier = VisitorUtil.identifierFor(node);
		
		SourceCodeLocationHelper location = new SourceCodeLocationHelper(cu, node);
		
		AbstractTypeHandle typeHandle;
		if(VisitorUtil.isInterface(node)) {
			typeHandle = packageHandle.addInterface(encodedTypeQualifier, typeIdentifier, location);
		} else {
			ClassHandle classHandle = packageHandle.addClass(encodedTypeQualifier, typeIdentifier, location);
			addSuperClass(classHandle, node.getSuperclassType());
			typeHandle = classHandle;
		}
		
		return typeHandle;
	}

	private void createAccessFrom(IVariableBinding fieldBinding, SourceCodeLocationHelper location) {
		if(!noCurrentCallable() /* hack, resolve */ &&
			fieldBinding.getDeclaringClass() != null &&
			fieldBinding.getDeclaringClass().isTopLevel()) { // TODO Ignores initializers, oinit cinit.
			String fieldIdentifier = VisitorUtil.identifierFor(fieldBinding);
			String encodedParentTypeQualifier = VisitorUtil.transformAndEncode(VisitorUtil.qualifierFor(fieldBinding.getDeclaringClass()));
			String encodedFieldQualifier = encodedParentTypeQualifier + '/' + fieldIdentifier;
			
			FieldHandle fieldHandle = model.getFieldHandleFor(encodedFieldQualifier, fieldIdentifier);
			AbstractCallableHandle methodHandle = currentCallable();
			methodHandle.addAccess(fieldHandle, location);
		}
	}

	private void createAccessFrom(Name node, SourceCodeLocationHelper location) {
		IBinding binding = node.resolveBinding();
		if(binding instanceof IVariableBinding) {
			IVariableBinding varBinding = (IVariableBinding) binding;
			
			if(varBinding.isField()) {
				createAccessFrom(varBinding, location);
			}
		}
	}

	private void addSuperClass(ClassHandle classHandle, Type superClassType) {
		if(superClassType != null) {
			ClassHandle superClassHandle = model.getClassHandleFor(VisitorUtil.resolveAndEncode(superClassType), VisitorUtil.identifierFor(superClassType));
			classHandle.setSuperClass(superClassHandle);
		}
	}

	private void addInterfaces(AbstractTypeHandle typeHandle, List<?> interfaces) {
		for(Object o : interfaces) {
			if(o instanceof Type) {
				Type ift = (Type) o;
				InterfaceHandle interfaceHandle = model.getInterfaceHandleFor(VisitorUtil.resolveAndEncode(ift), VisitorUtil.identifierFor(ift));
				typeHandle.addInterface(interfaceHandle);
			}
		}
	}

	private void addReturnType(MethodDeclaration node, MethodHandle methodHandle) {
		Type returnType = node.getReturnType2();
		AbstractTypeHandle returnTypeHandle = model.getTypeHandleFor(VisitorUtil.resolveAndEncode(returnType), VisitorUtil.identifierFor(returnType), VisitorUtil.isInterface(returnType));
		methodHandle.setReturnType(returnTypeHandle);
	}

	private boolean noCurrentType() {
		return typeStack.empty();
	}

	private boolean noCurrentCallable() {
		return callableStack.empty();
	}

	private AbstractTypeHandle currentType() {
		return typeStack.peek();
	}

	private AbstractCallableHandle currentCallable() {
		return callableStack.peek();
	}

	private void remember(AbstractTypeHandle typeHandle) {
		typeStack.push(typeHandle);
	}

	private void remember(AbstractCallableHandle callableHandle) {
		callableStack.push(callableHandle);
	}

	private void forgetCurrentType() {
		typeStack.pop();
	}

	private void forgetLastCallable() {
		callableStack.pop();
	}
}
