package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java methods.
 * 
 * @author wuersch
 *
 */
public class MethodHandle extends AbstractCallableHandle {
	/**
	 * Constructor.
	 * 
	 * @param theMethod
	 *            the method to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public MethodHandle(Individual theMethod, JavaOntModel model) {
		super(theMethod, model);
	}
	
	/**
	 * Sets the return type of this method.
	 * 
	 * @param returnTypeHandle
	 *            the handle for the return type of this method.
	 */
	public void setReturnType(AbstractTypeHandle returnTypeHandle) { // TODO Interfaces?
		addRelation(IJavaModelEntities.HAS_RETURN_TYPE, returnTypeHandle);
	}
}
