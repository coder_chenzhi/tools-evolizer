package org.evolizer.ontology.java.builder.model;

import java.io.IOException;

import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.ontology.java.EvolizerOntologyJavaPlugin;
import org.evolizer.ontology.model.EvolizerOntologyModelFactory;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntDocumentManager;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.tdb.TDB;
import com.hp.hpl.jena.vocabulary.RDFS;

public class JavaOntModel {	
	private String base;
	
	private OntModel baseModel;
	private OntModel inferenceModel;
	
	public JavaOntModel(String base) {			
        // TODO Should grab the cvs/svn path or use default value instead.
		this.base = "http://evolizer.org/ontologies/seon/projects/" + base;
			
		// TODO needs refactoring - model shouldn't be created here. Wrap in OWL_MEM model to prevent reasoning.
		baseModel = EvolizerOntologyModelFactory.createDefaultOntModelWithTDB("/Users/wuersch/tdb_test/" + base);
		inferenceModel = EvolizerOntologyModelFactory.createOntModelWithPellet(baseModel);

		try {
			setUpAltEntries();
			readOntologies();
		} catch(IOException ex) {
			throw new EvolizerRuntimeException("Error in setting-up ontologies for the Java Ontology Model.", ex);
		}
	}

	public void reconcile() {
		inferenceModel.rebind();
		flush();
	}

	public void flush() {
		TDB.sync(inferenceModel);
	}
	
	public void close() {
		if(!inferenceModel.isClosed()) {
			flush();
			inferenceModel.close();
		}
	}

	public PackageHandle getPackageHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName,  IJavaModelEntities.JAVA_PACKAGE);
		
		return new PackageHandle(individual, this);
	}
	
	public InterfaceHandle getInterfaceHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName, IJavaModelEntities.JAVA_INTERFACE);
		
		return new InterfaceHandle(individual, this);
	}
	
	public ClassHandle getClassHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName, IJavaModelEntities.JAVA_CLASS);
		
		return new ClassHandle(individual, this);
	}
	
	public AbstractTypeHandle getTypeHandleFor(String uniqueName, String localName, boolean isInterface) {
		AbstractTypeHandle typeHandle;
		if(isInterface) {
			typeHandle = getInterfaceHandleFor(uniqueName, localName);
		} else {
			typeHandle = getClassHandleFor(uniqueName, localName);
		}
		
		return typeHandle;
	}
	
	public FieldHandle getFieldHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName, IJavaModelEntities.JAVA_FIELD);
		
		return new FieldHandle(individual, this);
	}
	
	public MethodHandle getMethodHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName, IJavaModelEntities.JAVA_METHOD);
		
		return new MethodHandle(individual, this);
	}
	
	public ConstructorHandle getConstructorHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName, IJavaModelEntities.JAVA_CONSTRUCTOR);
		
		return new ConstructorHandle(individual, this);
	}
	
	public MethodParameterHandle getMethodParameterHandleFor(String uniqueName, String localName, String typeName) {
		Individual individual = fetchIndividual(uniqueName, localName, typeName, IJavaModelEntities.JAVA_PARAMETER);
		
		return new MethodParameterHandle(individual, this);
	}

	public PackageHandle addPackage(String uniqueName, String localName) {
		return getPackageHandleFor(uniqueName, localName);
	}

	public void addLabelTo(AbstractSourceCodeEntityHandle handle, String label) {
		addLiteralTo(handle, RDFS.label, label);
	}

	public void addIdentifierTo(AbstractSourceCodeEntityHandle handle, String label) {
		addLiteralTo(handle, IJavaModelEntities.HAS_IDENTIFIER, label);
	}

	public void addPropertyBetween(AbstractSourceCodeEntityHandle subjectHandle, Property property, AbstractSourceCodeEntityHandle objectHandle) {
		subjectHandle.asIndividual().addProperty(property, objectHandle.asIndividual());
	}

	public void addLiteralTo(AbstractSourceCodeEntityHandle handle, Property property, int value) {
		Literal literal = baseModel.createTypedLiteral(value);
		handle.asIndividual().addLiteral(property, literal);
	}
	
	public void addLiteralTo(AbstractSourceCodeEntityHandle handle, Property property, String value) {
		Literal literal = baseModel.createTypedLiteral(value);
		handle.asIndividual().addLiteral(property, literal);
	}
	
	public OntModel getJenaOntModel() {
		return inferenceModel;
	}
	
	public long size() {
		return inferenceModel.size();
	}

	private void setUpAltEntries() throws IOException {
		OntDocumentManager documentManager = baseModel.getDocumentManager();
		documentManager.addAltEntry("http://evolizer.org/ontologies/evolizer-nl/2010/11/annotations.owl", fsPathFor("ontologies/local_evolizer_annotations_10_11"));
		documentManager.addAltEntry("http://evolizer.org/ontologies/seon/2009/06/java.owl", fsPathFor("ontologies/local_seon_java_09_06"));
		documentManager.addAltEntry("http://evolizer.org/ontologies/seon/2010/11/java-synonyms.owl", fsPathFor("ontologies/local_seon_java_synonyms_10_11"));
	}

	private void readOntologies() {
		String[] ontUris = new String[] {
				"http://evolizer.org/ontologies/evolizer-nl/2010/11/annotations.owl",
				"http://evolizer.org/ontologies/seon/2009/06/java.owl",
				"http://evolizer.org/ontologies/seon/2010/11/java-synonyms.owl",
				"http://evolizer.org/ontologies/seon/2009/06/relations.owl"
										};
		
		boolean transactionsSupported = baseModel.supportsTransactions();
		
		for(String uri : ontUris) {
			if(transactionsSupported) { baseModel.begin(); }
			
			baseModel.read(uri); // TODO should not happen here, should it?
			
			if(transactionsSupported) { baseModel.commit(); }
		}
	}

	private Individual fetchIndividual(String uniqueName, String localName, String label, Resource ontClass) throws InvalidResourceException {
		Individual individual = getIndividual(uniqueName);
		
		if(individual == null) {
			individual = createIndividual(uniqueName, ontClass);
			individual.addLiteral(RDFS.label, label)
					  .addLiteral(IJavaModelEntities.HAS_IDENTIFIER, localName);
		} else {
			checkOntClass(ontClass, individual);
		}
		
		return individual;
	}
	
	private Individual fetchIndividual(String uniqueName, String localName, Resource ontClass) throws InvalidResourceException {
		return fetchIndividual(uniqueName, localName, localName, ontClass);
	}

	private Individual getIndividual(String uniqueName) {
		return baseModel.getIndividual(uri(uniqueName));
	}
	
	private Individual createIndividual(String uniqueName, Resource ontClass) {
		return baseModel.createIndividual(uri(uniqueName), ontClass);
	}
	
	private String uri(String uniqueName) {
		return base + "#" + uniqueName;
	}

	private static void checkOntClass(Resource ontClass, Individual individual) throws InvalidResourceException {
		if(!individual.hasOntClass(ontClass)) {
			throw new InvalidResourceException(ontClass, individual.getOntClass(true));
		}
	}

	private static String fsPathFor(String relativePath) throws IOException {
		return "file:" + EvolizerOntologyJavaPlugin.getAbsoluteFSPath(relativePath);
	}
}
