package org.evolizer.ontology.java.builder.model;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;

/**
 * Parameter object for source code location information.
 * 
 * @author Michael
 *
 */
public class SourceCodeLocationHelper {
	private String path;
	private int startPosition;
	private int length;
	
	public SourceCodeLocationHelper(ICompilationUnit cu, ASTNode node) {
		this(cu.getPath().toString(), node.getStartPosition(), node.getLength());
	}
	
	public SourceCodeLocationHelper(String path, int startPosition, int length) {
		this.path = path(path);
		this.startPosition = startPosition;
		this.length = length;
	}
	
	public void applyTo(AbstractSourceCodeEntityHandle handle) {		
		handle.addLiteral(IJavaModelEntities.HAS_PATH, path);
		handle.addLiteral(IJavaModelEntities.STARTS_AT, startPosition);
		handle.addLiteral(IJavaModelEntities.HAS_LENGTH, length);
	}

	private String path(String fullPath) {
		return fullPath.substring(fullPath.indexOf("src/") + 4);
	}
}
