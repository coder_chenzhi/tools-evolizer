package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java classes.
 * 
 * @author wuersch
 *
 */
public class ClassHandle extends AbstractTypeHandle {
	/**
	 * Constructor.
	 * 
	 * @param theClass
	 *            the class to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public ClassHandle(Individual theClass, JavaOntModel model) {
		super(theClass, model);
	}
	
	/**
	 * Sets the superclass of the underlying class. Does not perform any
	 * validation - callers of the method need to ensure that they do not
	 * accidentally introduce multiple inheritance (which is not possible in
	 * Java).
	 * 
	 * @param superClassHandle
	 *            the handle of the superclass.
	 */
	public void setSuperClass(ClassHandle superClassHandle) {
		addRelation(IJavaModelEntities.HAS_SUPERCLASS, superClassHandle);
	}
	
	/**
	 * Adds an interface to the underlying class.
	 * 
	 * @param the handle of the interface.
	 */
	public void addInterface(InterfaceHandle interfaceHandle) {
		addRelation(IJavaModelEntities.IMPLEMENTS_INTERFACE, interfaceHandle);
	}
}
