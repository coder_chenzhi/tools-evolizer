package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for fields or attributes of Java classes.
 * 
 * @author wuersch
 *
 */
public class FieldHandle extends AbstractSourceCodeEntityHandle {
	/**
	 * Constructor.
	 * 
	 * @param theField
	 *            the field to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public FieldHandle(Individual theField, JavaOntModel model) {
		super(theField, model);
	}
	
	/**
	 * Sets the type (class or interface) of the field.
	 * 
	 * @param fieldTypeHandle the handle of the field's type.
	 */
	public void setType(AbstractTypeHandle fieldTypeHandle) {
		addRelation(IJavaModelEntities.HAS_DATA_TYPE, fieldTypeHandle);
	}
}
