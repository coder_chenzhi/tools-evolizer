package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.rdf.model.Property;

/**
 * Abstract base class for handles of packages, interfaces, classes, fields,
 * methods, etc.
 * 
 * Wrapps an individual from the ontology and maintains a reference to the
 * {@link JavaOntModel} it is associated with.
 * 
 * @author wuersch
 * 
 */
public abstract class AbstractSourceCodeEntityHandle {
	/**
	 * The source code entity this handle refers to.
	 */
	private Individual theEntity;
	/**
	 * The associated Java source code model.
	 */
	private JavaOntModel model;
	
	/**
	 * Constructor.
	 * 
	 * @param theEntity
	 *            the source code entity this handle refers to. Clients need to
	 *            ensure that the individual has the correct ontology class - no
	 *            additional checks are performed to keep the number of model
	 *            accesses low.
	 * @param model
	 *            the associated Java source code model.
	 */
	public AbstractSourceCodeEntityHandle(Individual theEntity, JavaOntModel model) {
		this.theEntity = theEntity;
		this.model	   = model;
	}
	
	/**
	 * Convenience method to add a literal to the entity.
	 * 
	 * @param property the datatype property.
	 * @param value the literal value
	 */
	protected void addLiteral(Property property, int value) {
		model.addLiteralTo(this, property, value);
	}
	
	/**
	 * Convenience method to add a literal to the entity.
	 * 
	 * @param property the datatype property.
	 * @param value the literal value.
	 */
	protected void addLiteral(Property property, String value) {
		model.addLiteralTo(this, property, value);
	}
	
	/**
	 * Convenience method to add a relation to the entity.
	 * 
	 * @param property the object property.
	 * @param value the object.
	 */
	protected void addRelation(Property property, AbstractSourceCodeEntityHandle otherHandle) {
		model.addPropertyBetween(this, property, otherHandle);
	}
	
	/**
	 * Converts the handle to a Jena individual and returns it.
	 * 
	 * @return the individual this handle refers to.
	 */
	protected Individual asIndividual() {
		return theEntity;
	}
		
	/**
	 * Returns the associated Java source code model.
	 * 
	 * @return the associated Java model.
	 */
	protected JavaOntModel model() {
		return model;
	}
}
