package org.evolizer.ontology.java.builder.model;

import org.evolizer.core.exceptions.EvolizerRuntimeException;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Unchecked exception that may be raised whenever an encountered Jena resources
 * does not correspond to the expected one.
 * 
 * @author wuersch
 * 
 */
public class InvalidResourceException extends EvolizerRuntimeException {
	private static final long serialVersionUID = 1234549415136886637L;
	
	private Resource expectedResource;
	private Resource actualResource;
	
	/**
	 * Constructor. 
	 * 
	 * @param expectedResource the expected resource.
	 * @param actualResource the encountered resource.
	 */
	public InvalidResourceException(Resource expectedResource, Resource actualResource) {
		super("Expected " + expectedResource.getLocalName() + " but was " + actualResource.getLocalName());
		this.expectedResource = expectedResource;
		this.actualResource   = actualResource;
	}
	
	/**
	 * Returns the resource that was expected.
	 * 
	 * @return the expected Jena resource.
	 */
	public Resource getExpectedResource() {
		return expectedResource;
	}

	/**
	 * Returns the resource that was encountered.
	 * 
	 * @return
	 */
	public Resource getActualResource() {
		return actualResource;
	}
}
