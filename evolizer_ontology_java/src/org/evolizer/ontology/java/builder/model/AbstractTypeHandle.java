package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Abstract base class for type handles, currently class and interface handles.
 * Enums are types too, but are not considered yet.
 * 
 * @author wuersch
 * 
 */
public abstract class AbstractTypeHandle extends AbstractSourceCodeEntityHandle {	
	/**
	 * Constructor.
	 * 
	 * @param theType the type to which this handle refers to. 
	 * @param isInterface denotes whether the current type is an interface or not.
	 * @param model the associated Java source code model.
	 */
	protected AbstractTypeHandle(Individual theType, JavaOntModel model) {
		super(theType, model);
	}
	
	/**
	 * Adds a new field to the underlying type and returns a handle for it.
	 * 
	 * @param qualifiedFieldName
	 *            the fully qualified name of the field, i.e., an unique
	 *            identifier composed of the qualified parent name and the local
	 *            identifier of the field.
	 * @param fieldName
	 *            the local identifier of the field, used as a human-readable
	 *            label.
	 * @param location
	 *            the source code location of the field declaration.
	 * 
	 * @return a handle to the newly added field.
	 */
	// TODO override in InterfaceHandle to set constant?
	public FieldHandle addField(String qualifiedFieldName, String fieldName, SourceCodeLocationHelper location) {
		FieldHandle handle = model().getFieldHandleFor(qualifiedFieldName, fieldName);
		
		addRelation(IJavaModelEntities.DECLARES_FIELD, handle);		
		location.applyTo(handle);
		
		return handle;
	}
	
	/**
	 * Adds a new method to the underlying type and returns a handle for it.
	 * 
	 * @param qualifiedMethodName
	 *            the fully qualified name of the method, i.e., an unique
	 *            identifier composed of the qualified parent name, the local
	 *            identifier of the method, and the qualified type names of the
	 *            method's parameters.
	 * @param methodName
	 *            the local identifier of the method, used as a human-readable
	 *            label.
	 * @param location
	 *            location the source code location of the method declaration.
	 * 
	 * @return a handle to the newly added method.
	 */
	public MethodHandle addMethod(String qualifiedMethodName, String methodName, SourceCodeLocationHelper location) {
		MethodHandle handle = model().getMethodHandleFor(qualifiedMethodName, methodName);
		
		addRelation(IJavaModelEntities.DECLARES_METHOD, handle);		
		location.applyTo(handle);
		
		return handle;
	}

	/**
	 * Adds a new constructor to the underlying type and returns a handle for it.
	 * 
	 * @param qualifiedMethodName
	 *            the fully qualified name of the constructor, i.e., an unique
	 *            identifier composed of the qualified parent name, the local
	 *            identifier of the constructor, and the qualified type names of the
	 *            constructor's parameters.
	 * @param methodName
	 *            the local identifier of the constructor, used as a human-readable
	 *            label.
	 * @param location
	 *            location the source code location of the constructor declaration.
	 * 
	 * @return a handle to the newly added constructor.
	 */
	public ConstructorHandle addConstructor(String qualifiedMethodName, String methodName, SourceCodeLocationHelper location) {
		ConstructorHandle handle = model().getConstructorHandleFor(qualifiedMethodName, methodName);
		
		addRelation(IJavaModelEntities.DECLARES_CONSTRUCTOR, handle);		
		location.applyTo(handle);
		
		return handle;
	}

	/**
	 * Adds a new interface to the underlying type. For classes, this means that
	 * an 'implements' relationship is established, while interfaces will
	 * receive a 'has super interface' relationship.
	 * 
	 * @param interfaceHandle
	 *            the handle of the interface to add.
	 */
	public abstract void addInterface(InterfaceHandle interfaceHandle);
}
