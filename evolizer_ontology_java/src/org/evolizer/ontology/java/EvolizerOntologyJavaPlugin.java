package org.evolizer.ontology.java;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.core.logging.base.PluginLogManager;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class EvolizerOntologyJavaPlugin extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.evolizer.ontology.java";

	// The shared instance
	private static EvolizerOntologyJavaPlugin plugin;

	// The path to the log4j.properties file
	private static final String LOG_XML_FILE = "config/log4j.xml";

	// The log manager
	private PluginLogManager fLogManager;

	/**
	 * The constructor
	 */
	public EvolizerOntologyJavaPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		configure();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static EvolizerOntologyJavaPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the log manager.
	 * 
	 * @return the log manager.
	 */
	public static PluginLogManager getLogManager() {
		return getDefault().fLogManager;
	}
	
	public static String getAbsoluteFSPath(String relativeFSPath) throws IOException {
		return FileLocator.resolve(getURL(relativeFSPath)).getFile();
	}
	
	public static URL getURL(String relativeFSPath) throws IOException {
		return FileLocator.find(EvolizerOntologyJavaPlugin.getDefault().getBundle(), new Path(relativeFSPath), null);
	}
	
	public static InputStream openFile(String relativeFSPath) throws IOException {
		return FileLocator.openStream(EvolizerOntologyJavaPlugin.getDefault().getBundle(), new Path(relativeFSPath), false);
	}

	private void configure() {
		try {
			fLogManager = new PluginLogManager(this, getURL(LOG_XML_FILE));
		} catch (Exception e) {
			String message = "Error while initializing log properties." + e.getMessage();

			IStatus status = new Status(IStatus.ERROR,
										getDefault().getBundle()
													.getSymbolicName(),
										IStatus.ERROR,
										message,
										e);
			getLog().log(status);

			throw new EvolizerRuntimeException(
					"Error while initializing log properties.", e);
		}
	}
}