package org.evolizer.ontology.java.facade;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.ontology.java.builder.IBuildProgressListener;
import org.evolizer.ontology.java.builder.JavaOntModelBuilder;
import org.evolizer.ontology.java.builder.JavaOntModelNature;
import org.evolizer.ontology.java.builder.JavaOntModelStore;
import org.evolizer.ontology.java.builder.model.JavaOntModel;

public class JavaModelFacade {
	// TODO rename (also unregister)
	public static void register(IBuildProgressListener listener) {
		JavaOntModelBuilder.addProgressListener(listener);
	}

	public static void unregister(IBuildProgressListener listener) {
		JavaOntModelBuilder.removeProgressListener(listener);
		
	}

	public static JavaOntModel getModelFor(IJavaProject project) {
		 return JavaOntModelStore.INSTANCE.getModel(project);
	}
	
	public static BuildStatus getBuildStatusFor(IJavaProject javaProject) {
		int statusCode = BuildStatus.NONE;
		
		if(hasNatureEnabled(javaProject)) {
			statusCode |= BuildStatus.HAS_NATURE;
		}
		
		if((hasModel(javaProject))) {
			statusCode |= BuildStatus.HAS_MODEL;
		}
		
		return new BuildStatus(javaProject, statusCode);
	}

	public static boolean hasModel(IJavaProject project) {
		 return JavaOntModelStore.INSTANCE.hasModel(project);
	}
	
	public static boolean hasNatureEnabled(IProject project) throws EvolizerRuntimeException {
		try {
			return project.isNatureEnabled(JavaOntModelNature.NATURE_ID);
		} catch (CoreException e) { 
			throw new EvolizerRuntimeException("Error while checking for the JavaOntModelNature. Project is closed or does not exist.", e); // TODO own exception?
		}
	}
	
	public static boolean hasNatureEnabled(IJavaProject javaProject) throws EvolizerRuntimeException {
		return hasNatureEnabled(javaProject.getProject());
	}
	
	public static void addNatureTo(IJavaProject javaProject, IProgressMonitor monitor) {
		JavaOntModelNature.applyTo(javaProject, monitor);
	}
	
	// TODO build stuff should go elsewhere
	public static void build(IJavaProject javaProject, IProgressMonitor monitor) {
		try {
			javaProject.getProject().build(IncrementalProjectBuilder.FULL_BUILD, monitor);
		} catch (CoreException cex) {
			throw new EvolizerRuntimeException("Build did not succeed.", cex);
		}
	}
	
	// TODO build stuff should go elsewhere
	public static void waitForBuild(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		
		IJobManager jobManager = Job.getJobManager();
		
		// we need to ensure that the build has completed:
		// for some strange reason join-thread gets interrupted
		// once in a while (at least on windows).
		boolean buildHasntFinishedYet = true;
		while(buildHasntFinishedYet && !subMonitor.isCanceled()) {
			subMonitor.setWorkRemaining(100);
			try {
				jobManager.join(ResourcesPlugin.FAMILY_AUTO_BUILD, subMonitor.newChild(50));
				jobManager.join(ResourcesPlugin.FAMILY_MANUAL_BUILD, subMonitor.newChild(50));
				
				buildHasntFinishedYet = false;
			} catch (InterruptedException e) {
				buildHasntFinishedYet = true; // 
			}
		}
	}
	
//	public static IPreferenceStore getSharedPreferenceStore() {
//
//	}
}
