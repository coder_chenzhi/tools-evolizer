package org.evolizer.ontology.java.facade;

import org.eclipse.jdt.core.IJavaProject;

public class BuildStatus {
	public static final int NONE = 0;
	public static final int HAS_NATURE = 2;
	public static final int HAS_MODEL = 4;
	
	private IJavaProject javaProject;
	private int statusCode;
	
	public BuildStatus(IJavaProject javaProject, int statusCode) {
		this.javaProject = javaProject;
		this.statusCode = statusCode;
	}
	
	public IJavaProject getJavaProject() {
		return javaProject;
	}
	
	public int getStatus() {
		return statusCode;
	}
	
	public boolean hasStatus(int otherStatusCode) {
		return (statusCode & otherStatusCode) != 0;
	}
}
