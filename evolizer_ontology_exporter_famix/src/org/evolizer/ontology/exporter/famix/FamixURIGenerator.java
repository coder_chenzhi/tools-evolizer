package org.evolizer.ontology.exporter.famix;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.evolizer.famix.model.entities.AbstractFamixEntity;
import org.evolizer.ontology.exporter.api.IURIGenerator;

public class FamixURIGenerator implements IURIGenerator {

	public FamixURIGenerator() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean canGenerate(Class<?> type) {
		Class<?> superClass = type;
		while (!superClass.equals(Object.class)) {
			if (superClass.equals(AbstractFamixEntity.class)) {
				return true;
			} else {
				superClass = superClass.getSuperclass();
			}
		}

		return false;
	}

	@Override
	public String getIdentifier(Object resource) {
		try {
			return URLEncoder.encode(((AbstractFamixEntity) resource).getUniqueName(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace(); // should not happen
			return null;
		}
	}

}
