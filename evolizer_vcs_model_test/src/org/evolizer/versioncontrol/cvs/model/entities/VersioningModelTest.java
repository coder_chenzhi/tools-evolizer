package org.evolizer.versioncontrol.cvs.model.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.versioncontrol.cvs.model.EvolizerCVSModelPlugin;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.ModificationReport;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.cvs.model.entities.VersionedFile;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class VersioningModelTest {

	private static final String DB_CONFIG_FILE = "./config/db.properties";

	protected static String fDBUrl;
	protected static String fDBDialect;
	protected static String fDBDriverName;
	protected static String fDBUser;
	protected static String fDBPasswd;

	private IEvolizerSession fEvolizerSession = null;
	private static EvolizerSessionHandler fSessionHandler;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream propertiesInputStream = EvolizerCVSModelPlugin.openFile(DB_CONFIG_FILE);

		if (propertiesInputStream != null) {
			Properties props = new Properties();
			props.load(propertiesInputStream);
			propertiesInputStream.close();

			fDBUrl = props.getProperty("dbUrl");
			fDBDialect = props.getProperty("dbDialect");
			fDBDriverName = props.getProperty("dbDriverName");
			fDBUser = props.getProperty("dbUser");
			fDBPasswd = props.getProperty("dbPasswd");
		}
		
		propertiesInputStream.close();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		try{
			fSessionHandler = EvolizerSessionHandler.getHandler();
			fSessionHandler.initSessionFactory(fDBUrl, fDBUser, fDBPasswd);
			fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
			fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		if (fEvolizerSession.isOpen()) {
			fEvolizerSession.flush();
			fEvolizerSession.close();
		}

		fSessionHandler.cleanupHibernateSessions();
		fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		fSessionHandler = null;
	}

	private <T>T loadUniqueFromDB(String reloadQuery, Class<T> resultType) {
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			T result = s.uniqueResult(reloadQuery, resultType);
			
			return result;
		} catch (EvolizerException e) {
			fail(e.getMessage());
			return null;
		}
	}

	private <T>T saveAndReloadUniqueFromDB(Object object, String reloadQuery, Class<T> resultType) {
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			s.startTransaction();
			s.saveObject(object);
			s.endTransaction();

			s.close();

			s = fSessionHandler.getCurrentSession(fDBUrl);

			T result = s.uniqueResult(reloadQuery, resultType);
			return result;
		} catch (EvolizerException e) {
			fail(e.getMessage());
			return null;
		}
	}

	@Test
	public void testFileMapping() {
		VersionedFile file = new VersionedFile("full_path/foo", null);
		assertEquals("foo", file.getName());

		VersionedFile loadedFile = saveAndReloadUniqueFromDB(file, "from File", VersionedFile.class);
		assertNotNull(loadedFile);
		assertEquals("full_path/foo", loadedFile.getPath());
		assertEquals("foo", loadedFile.getName());
	}

	@Test
	public void testDirectoryMapping() {
		Directory dir = new Directory("foo_dir/foo", null);
		Directory loadedDir = saveAndReloadUniqueFromDB(dir,
				"from Directory", Directory.class);
		assertNotNull(loadedDir);
		assertEquals("foo", loadedDir.getName());
		assertEquals("foo_dir/foo", loadedDir.getPath());
	}

	@Test
	public void testDirectoryFileMapping() {
		Directory dir1 = new Directory("/dir_one", Directory.ROOT);
		VersionedFile file1 = new VersionedFile("/dir_one/file_one", dir1);
		VersionedFile file2 = new VersionedFile("/dir_one/file_two", dir1);
		dir1.add(file1);
		dir1.add(file2);
		Directory loadedDir = saveAndReloadUniqueFromDB(Directory.ROOT,
				"from Directory as d where d.path = '/dir_one'", Directory.class);
		assertNotNull(loadedDir);
		VersionedFile loadedFile = loadUniqueFromDB("from File f where f.path = '/dir_one/file_one'", VersionedFile.class);
		assertNotNull(loadedFile);
	}

	@Test
	public void testFileRevisionMapping() throws ParseException {
		VersionedFile file1 = new VersionedFile("foobar", null);
	
		Person p1 = new Person();
		p1.setFirstName("fn1");
		p1.setLastName("ln1");
		p1.setEmail("1@1.ch");
		
		Person p2 = new Person();
		p2.setFirstName("fn2");
		p2.setLastName("ln2");
		p2.setEmail("2@2.ch");
		
		Person p3 = new Person();
		p3.setFirstName("fn3");
		p3.setLastName("ln3");
		p3.setEmail("3@3.ch");
;

		ModificationReport mr1 = new ModificationReport("2006/11/08 12:00:00");
		mr1.setAuthor(p1);
		ModificationReport mr2 = new ModificationReport("2006/11/09 12:00:00");
		mr2.setAuthor(p2);
		ModificationReport mr3 = new ModificationReport("2006/11/10 12:00:00");
		mr3.setAuthor(p3);
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(mr1.getAuthor());
		fEvolizerSession.saveObject(mr2.getAuthor());
		fEvolizerSession.saveObject(mr3.getAuthor());
		fEvolizerSession.endTransaction();
		
		Revision rev1 = new Revision("1");
		rev1.setReport(mr1);
		Revision rev2 = new Revision("2");
		rev2.setReport(mr2);
		Revision rev3 = new Revision("3");
		rev3.setReport(mr3);		
		
		file1.addRevision(rev1);
		file1.addRevision(rev2);
		file1.addRevision(rev3);
		VersionedFile loadedFile = saveAndReloadUniqueFromDB(file1,
				"from File f where f.path = 'foobar'", VersionedFile.class);
		assertNotNull(loadedFile);
		assertEquals(rev3.getId(), loadedFile.getLatestRevision().getId());
		assertEquals(rev3.getNumber(), loadedFile.getLatestRevision().getNumber());
		assertEquals(3, loadedFile.getRevisions().size());
	}

	@Test
	public void testBranchMapping() throws EvolizerException {
		Branch branch = new Branch("branchName");
		Branch firstChildBranch = new Branch("childBranch");
		Branch secondChildBranch = new Branch("childBranch");

		Set<Branch> branchChildren = new HashSet<Branch>();
		branchChildren.add(firstChildBranch);
		branchChildren.add(secondChildBranch);

		branch.setChildren(branchChildren);

		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(branch);
		fEvolizerSession.endTransaction();

		fEvolizerSession.close();
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);

		List<Branch> branchResults = fEvolizerSession.query("from Branch", Branch.class);

		assertEquals(2, branchResults.size());

		Branch branchResult = branchResults.get(0);
		assertEquals(null, branchResult.getParent());
		assertEquals("branchName", branchResult.getName());
		assertEquals(1, branchResult.getChildren().size());

		for (Iterator<Branch> iter = branchResult.getChildren().iterator(); iter
				.hasNext();) {
			Branch childBranch = iter.next();
			assertEquals("childBranch", childBranch.getName());
			assertEquals(new Long(1), childBranch.getParent().getId());
		}
	}

	@Test
	public void testRevisonAndModReportAndPersonMapping() throws ParseException, EvolizerException {

		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = df.parse("2006/01/02 01:02:03");
		Person author = new Person();
		author.setFirstName("Emanuel");
		author.setLastName("Giger");
		author.setEmail("gigs@orc-warriors.ogrimmar.horde");

		Revision oldRevision = new Revision("1.1");
		oldRevision.setState("active");

		ModificationReport oldReport = new ModificationReport();
		oldReport.setCommitMessage("old message");
		oldReport.setCreationTime(date);
		oldReport.setLinesAdd(12);
		oldReport.setLinesDel(14);
		oldReport.setAuthor(author);

		Revision currentRevision = new Revision("1.2");
		currentRevision.setState("active");
		currentRevision.setSource("source code of rev 1.2");

		ModificationReport currentReport = new ModificationReport();
		currentReport.setCommitMessage("current message");
		currentReport.setCreationTime(date);
		currentReport.setLinesAdd(16);
		currentReport.setLinesDel(18);
		currentReport.setAuthor(author);

		Revision newRevision = new Revision("1.3");
		newRevision.setState("active");

		ModificationReport newReport = new ModificationReport();
		newReport.setCommitMessage("new message");
		newReport.setCreationTime(date);
		newReport.setLinesAdd(22);
		newReport.setLinesDel(17);
		newReport.setAuthor(author);

		oldRevision.setReport(oldReport);
		currentRevision.setReport(currentReport);
		newRevision.setReport(newReport);

		currentRevision.setPreviousRevision(oldRevision);
		currentRevision.setNextRevision(newRevision);
		
		
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(author);
		fEvolizerSession.saveObject(oldRevision);
		fEvolizerSession.saveObject(newRevision);
		fEvolizerSession.saveObject(currentRevision);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();

		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);

		List<Revision> revResults = fEvolizerSession
				.query("from Revision as b where b.number = '1.2'", Revision.class);
		assertEquals(1, revResults.size());

		Revision resultCurrentRevision = (Revision) revResults.get(0);
		assertEquals("1.2", resultCurrentRevision.getNumber());
		assertEquals("source code of rev 1.2", resultCurrentRevision.getSource());
		assertEquals("1.1", resultCurrentRevision.getPreviousRevision()
				.getNumber());
		assertEquals("1.3", resultCurrentRevision.getNextRevision().getNumber());

		ModificationReport resultReport = resultCurrentRevision.getReport();
		assertEquals("current message", resultReport.getCommitMessage());
		assertEquals(date, resultReport.getCreationTime());
		assertEquals(16, resultReport.getLinesAdd());
		assertEquals(18, resultReport.getLinesDel());
		assertEquals("Emanuel", resultReport.getAuthor().getFirstName());
		assertEquals("gigs@orc-warriors.ogrimmar.horde", resultReport
				.getAuthor().getEmail());

		resultReport = resultCurrentRevision.getPreviousRevision().getReport();
		assertEquals("old message", resultReport.getCommitMessage());
		assertEquals(date, resultReport.getCreationTime());
		assertEquals(12, resultReport.getLinesAdd());
		assertEquals(14, resultReport.getLinesDel());
		assertEquals("Giger", resultReport.getAuthor().getLastName());
		assertEquals("gigs@orc-warriors.ogrimmar.horde", resultReport
				.getAuthor().getEmail());

		resultReport = resultCurrentRevision.getNextRevision().getReport();
		assertEquals("new message", resultReport.getCommitMessage());
		assertEquals(date, resultReport.getCreationTime());
		assertEquals(22, resultReport.getLinesAdd());
		assertEquals(17, resultReport.getLinesDel());
		assertEquals("Emanuel", resultReport.getAuthor().getFirstName());
		assertEquals("gigs@orc-warriors.ogrimmar.horde", resultReport
				.getAuthor().getEmail());
	}
	
	
	@Test
	public void testReleaseMapping() throws ParseException, EvolizerException{
		Release releaseOne = new Release("releaseNameOne");
		Release releaseTwo = new Release("releaseNameTwo");
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = df.parse("2006/01/02 01:02:03");
		releaseOne.setTimeStamp(date);
		releaseTwo.setTimeStamp(date);
		
		ModificationReport modReport = new ModificationReport();
		
		modReport.setAuthor(new Person());
		
		Revision revOne = new Revision("1.1");
		Revision revTwo = new Revision("1.2");
		Revision revThree= new Revision("1.3");
		Revision revFour = new Revision("1.4");
		Revision revFive = new Revision("1.5");
		
		//A dummy report otherwise Hibernate throws an eception
		revOne.setReport(modReport);
		revTwo.setReport(modReport);
		revThree.setReport(modReport);revOne.setReport(modReport);
		revFour.setReport(modReport);
		revFive.setReport(modReport);
		
		releaseOne.addRevision(revOne);
		releaseOne.addRevision(revFour);
		releaseOne.addRevision(revFive);
		

		releaseTwo.addRevision(revTwo);
		releaseTwo.addRevision(revThree);
		releaseTwo.addRevision(revOne);
		
		for(Iterator<Revision> iter = releaseOne.getRevisions().iterator();iter.hasNext();){
			iter.next().addRelease(releaseOne);
		}
		
		for(Iterator<Revision> iter = releaseTwo.getRevisions().iterator();iter.hasNext();){
			iter.next().addRelease(releaseTwo);
		}
		
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(modReport.getAuthor());
		fEvolizerSession.saveObject(releaseOne);
		fEvolizerSession.saveObject(releaseTwo);
		fEvolizerSession.saveObject(revOne);
		fEvolizerSession.saveObject(revTwo);
		fEvolizerSession.saveObject(revThree);
		fEvolizerSession.saveObject(revFour);
		fEvolizerSession.saveObject(revFive);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();
		
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		List<Release> releaseResultsList = fEvolizerSession.query("from Release", Release.class);
		assertEquals(2, releaseResultsList.size());
		assertEquals("releaseNameOne", ((Release)releaseResultsList.get(0)).getName());
		assertEquals("releaseNameTwo", ((Release)releaseResultsList.get(1)).getName());
		
		assertEquals(3, ((Release)releaseResultsList.get(0)).getRevisions().size());
		assertEquals(3, ((Release)releaseResultsList.get(1)).getRevisions().size());
		
		//Test if first Release and Revision were mapped were mapped correctly
		for(Iterator<Revision> iter = ((Release)releaseResultsList.get(0)).getRevisions().iterator();iter.hasNext();){
			Revision rev = iter.next();
			if(!(rev.getNumber().equals("1.1")|| rev.getNumber().equals("1.4") || rev.getNumber().equals("1.5"))){
				Assert.fail("Release and Revision were not properly mapped! Please check Database");
			}
		}
		//Test if second Realse and Revision were mapped were mapped correctly
		for(Iterator<Revision> iter = ((Release)releaseResultsList.get(1)).getRevisions().iterator();iter.hasNext();){
			Revision rev = iter.next();
			if(!(rev.getNumber().equals("1.2")|| rev.getNumber().equals("1.3") || rev.getNumber().equals("1.1"))){
				Assert.fail("Release and Revision were not properly mapped! Please check Database");
			}
		}
	}
	
	@Test
	public void testTransactionMapping() throws ParseException, EvolizerException{
		Transaction transaction = new Transaction();
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = df.parse("2006/01/02 01:02:03");
		transaction.setStarted(date);
		transaction.setFinished(date);
		Revision rev = new Revision("1.1");
		transaction.addRevision(rev);
		
		ModificationReport modReport = new ModificationReport();
		modReport.setAuthor(new Person());
		rev.setReport(modReport);
		
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(modReport.getAuthor());
		fEvolizerSession.saveObject(rev);
		fEvolizerSession.saveObject(transaction);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();
		
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		List<Transaction> transactionResults = fEvolizerSession.query("from Transaction", Transaction.class);
		assertEquals(1, transactionResults.size());
		
		Transaction transactionResult = (Transaction) transactionResults.get(0);
		assertEquals(date, transactionResult.getStarted());
		assertEquals(date, transactionResult.getFinished());
		assertEquals(1, transactionResult.getInvolvedRevisions().size());
		
		Revision resultInvolvedRevision = transactionResult.getInvolvedRevisions().iterator().next();
		assertEquals("1.1", resultInvolvedRevision.getNumber());
	}
	
	@Test
	public void testDirectoryEqualsAndHashCode(){
		Directory d1 = new Directory("/foo/subDir", null);
		Directory d2 = new Directory("/foo/subDir", null);
		Directory d3 = new Directory("/foo/subDir2", null);
		
		assertEquals(false, d1.equals(null));
		assertEquals(false, d1.equals(new Revision("1.1")));
		
		assertEquals(true, d1.equals(d2));
		assertEquals(false, d1.equals(d3));
		
		assertEquals(d1.hashCode(), d2.hashCode());
	}
	
	@Test
	public void testFileEqualsAndHashCode(){
		VersionedFile f1 = new VersionedFile("/foo/File.java", null);
		assertEquals(false, f1.equals(null));
		assertEquals(false, f1.equals(new Revision("1.1")));
		
		VersionedFile f2 = new VersionedFile("/foo/File.java", null);
		
		assertEquals(true, f1.equals(f2));
		assertEquals(true, f1.hashCode()==f2.hashCode());
		
		VersionedFile f3 = new VersionedFile("/foo/Test.java", null);
		assertFalse(f3.equals(f1));
	}
	
	@Test
	public void testBranchEqualsAndHashCode(){
		Branch b1 = new Branch("myName");
		assertEquals(false, b1.equals(null));
		assertEquals(false, b1.equals(new Object()));
		Branch b2 = new Branch("myName");
		assertEquals(true, b1.equals(b2));
		assertEquals(true, b1.hashCode()==b2.hashCode());
		Branch p1 = new Branch("parent");
		b1.setParent(p1);
		assertEquals(false, b1.equals(b2));
		b2.setParent(p1);
		assertEquals(true, b1.equals(b2));
		assertEquals(true, b1.hashCode()==b2.hashCode());
		
		b1.setName("myName2");
		assertEquals(false, b1.equals(b2));
		b1.setName("myName");
		Branch c1 = new Branch("child1");
		b1.addChild(c1);
		assertEquals(false, b1.equals(b2));
		b2.addChild(c1);
		assertEquals(true, b1.equals(b2));
		
		Revision rev = new Revision("1.1");
		b1.addRevision(rev);
		assertEquals(false, b1.equals(b2));
		b2.addRevision(rev);
		assertEquals(true, b1.equals(b2));
		assertEquals(true, b1.hashCode()==b2.hashCode());
	}
	
	
	@Test
	public void testModReportEqualsAndHashCode(){
		ModificationReport m1 = new ModificationReport();
		ModificationReport m2 = new ModificationReport();
		Date date = new Date();
		Person author = new Person();
		m1.setCreationTime(date);
		m2.setCreationTime(date);
		
		m1.setAuthor(author);
		m2.setAuthor(author);
		
		m1.setCommitMessage("message");
		m2.setCommitMessage("message");
		
		m1.setLinesAdd(3);
		m2.setLinesAdd(3);
		
		m1.setLinesDel(4);
		m2.setLinesDel(4);
		
		
		assertFalse(m1.equals(null));
		assertEquals(true, m1.equals(m1));
		assertTrue(m1.equals(m2));
		assertTrue(m1.hashCode()==m2.hashCode());
	}
	
	@Test
	public void testRevisionEqualsAndHashCode(){
		Revision rev1 = new Revision("1.1");
		Revision rev2 = new Revision("1.1");
		
		VersionedFile file = new VersionedFile("foo.bar", null);
		ModificationReport modRe = new ModificationReport();
		
		rev1.setFile(file);
		rev2.setFile(file);
		
		rev1.setReport(modRe);
		rev2.setReport(modRe);
		
		rev1.setState("active");
		rev2.setState("active");
		
		assertFalse(rev1.equals(null));
		assertFalse(rev1.equals(new Object()));
		assertTrue(rev1.equals(rev1));
		assertTrue(rev1.equals(rev2));
		assertTrue(rev1.hashCode()==rev2.hashCode());
		
		rev2.setNumber("1.2");
		assertFalse(rev1.equals(rev2));
	}
	
	@Test
	public void testReleaseEqualsAndHashCode(){
		Date date = new Date();
		Revision rev1 = new Revision("1.1");
		Revision rev2 = new Revision("1.2");
		Release r1 = new Release("name");
		Release r2 = new Release("name");
		
		r1.setTimeStamp(date);
		r2.setTimeStamp(date);
		
		r1.addRevision(rev1);
		r2.addRevision(rev1);
		
		assertFalse(r1.equals(null));
		assertFalse(r1.equals(new Object()));
		assertTrue(r1.equals(r1));
		assertTrue(r1.equals(r2));
		assertTrue(r1.hashCode()==r2.hashCode());
		r1.addRevision(rev2);
		assertFalse(r1.equals(r2));
	}
	
	@Test
	public void testTransactionEqualsAndHashCode(){
		Date date = new Date();
		Transaction t1 = new Transaction();
		Transaction t2 = new Transaction();
		t1.setStarted(date);
		t1.setFinished(date);
		
		t2.setStarted(date);
		t2.setFinished(date);
		
		assertFalse(t1.equals(null));
		assertFalse(t1.equals(new Object()));
		assertTrue(t1.equals(t1));
		assertTrue(t1.equals(t2));
		assertTrue(t1.hashCode()==t2.hashCode());
		
		
		t1.setStarted(null);
		assertFalse(t1.equals(t2));
	}
}
