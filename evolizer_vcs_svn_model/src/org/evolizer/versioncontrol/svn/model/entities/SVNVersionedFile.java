/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.model.entities;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.versioncontrol.cvs.model.entities.VersionedFile;

/**
 * The specification of {@link VersionedFile} for the SVN importer model. It adds just a few more properties to its
 * parent class.
 * 
 * @author ghezzi
 */
@Entity
public class SVNVersionedFile extends VersionedFile {

    /*
     * The file this one was copied from.
     */
    private SVNVersionedFile copiedFrom;
    /*
     * The file this one was copied to.
     */
    private SVNVersionedFile copiedTo;

    /**
     * A default constructor is needed for Hibernate to load instances.
     */
    public SVNVersionedFile() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param path
     *            the file path.
     * @param parent
     *            the parent directory.
     */
    public SVNVersionedFile(String path, Directory parent) {
        super(path, parent);
    }

    /**
     * Returns the {@link SVNVersionedFile} that was copied from this file.
     * 
     * @return The {@link SVNVersionedFile}.
     */
    @OneToOne
    public SVNVersionedFile getCopiedTo() {
        return copiedTo;
    }

    /**
     * Sets the {@link SVNVersionedFile} that was copied from this file.
     * 
     * @param copiedTo
     *            The {@link SVNVersionedFile} to set.
     */
    public void setCopiedTo(SVNVersionedFile copiedTo) {
        this.copiedTo = copiedTo;
    }

    /**
     * Returns the {@link SVNVersionedFile} this file was copied from.
     * 
     * @return The {@link SVNVersionedFile}.
     */
    @OneToOne
    public SVNVersionedFile getCopiedFrom() {
        return copiedFrom;
    }

    /**
     * Sets the {@link SVNVersionedFile} this file was copied from.
     * 
     * @param copiedFrom
     *            The {@link SVNVersionedFile} to set.
     */
    public void setCopiedFrom(SVNVersionedFile copiedFrom) {
        this.copiedFrom = copiedFrom;
    }
}
