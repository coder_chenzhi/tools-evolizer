/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;

/**
 * The specification of {@link Revision} for the SVN importer model. It adds just a few more properties to its parent
 * class.
 * 
 * @author ghezzi
 */
@Entity
public class SVNRevision extends Revision {

    /**
     * The ancestor represents were this SVNRevision comes from: from what other SVNRevision it was copied from. A
     * SVNRevision has an ancestor when the associated file was moved inside the repository or it was copied during a
     * tag or branch creation.
     */
    private SVNRevision ancestor;
    /**
     * The transaction on which this revision was committed.
     */
    private Transaction changeSet;

    /**
     * A default constructor is needed for Hibernate to load instances.
     */
    SVNRevision() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param revNumber
     *            The number of the new revision.
     */
    public SVNRevision(String revNumber) {
        super(revNumber);
    }

    /**
     * Returns the {@link Transaction} on which this revision was committed.
     * 
     * @return The associated transaction.
     */
    @ManyToOne(cascade = CascadeType.ALL)
    public Transaction getChangeSet() {
        return changeSet;
    }

    /**
     * Sets the associated {@link Transaction} (the one on which the revision was committed).
     * 
     * @param changeSet
     *            The new {@link Transaction} to set.
     */
    public void setChangeSet(Transaction changeSet) {
        this.changeSet = changeSet;
    }

    /**
     * Returns the {@link SVNRevision} from which this revision comes from. The ancestor represents were this
     * SVNRevision comes from: from what other SVNRevision it was copied from. A SVNRevision has an ancestor when the
     * associated file was moved inside the repository or it was copied during a tag or branch creation.
     * 
     * @return
     */
    @OneToOne(cascade = CascadeType.ALL)
    public SVNRevision getAncestor() {
        return ancestor;
    }

    /**
     * Sets this revision ancestor.
     * 
     * @param ancestor
     *            The {@link SVNRevision} to set as ancestor.
     */
    public void setAncestor(SVNRevision ancestor) {
        this.ancestor = (SVNRevision) ancestor;
    }
}
