/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;

/**
 * The specification of {@link Release} for the SVN importer model. It adds just a few more properties to its parent
 * class.
 * 
 * @author ghezzi
 */
@Entity
public class SVNRelease extends Release {

    /*
     * The repository related url of the release.
     */
    private String url;
    /*
     * The revisions associated to the release (the ones that were created when the release was created).
     */
    private Set<Revision> releaseRevisions = new HashSet<Revision>();

    /**
     * A default constructor is needed for Hibernate to load instances.
     */
    SVNRelease() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param name
     *            The name of the release.
     */
    public SVNRelease(String name) {
        super(name);
    }

    /**
     * Returns the repository related url of the release.
     * 
     * @return The string containing the url of the release
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the release url.
     * 
     * @param url
     *            The string containing the new url to set.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Removes a {@link Revision} from the release revisions.
     * 
     * @param fileVersion
     *            The associated revisions.
     */
    public void removeReleaseRevision(Revision fileVersion) {
        this.releaseRevisions.remove(fileVersion);
    }

    /**
     * Returns the revisions related to the release.
     * 
     * @return The {@link Set} containing the release revisions.
     */
    @OneToMany(cascade = CascadeType.ALL)
    public Set<Revision> getReleaseRevisions() {
        return releaseRevisions;
    }

    /**
     * Sets the release revisions.
     * 
     * @param releaseVersions
     *            The {@link Set} containing the new revisions.
     */
    public void setReleaseRevisions(Set<Revision> releaseVersions) {
        this.releaseRevisions = releaseVersions;
    }

    /**
     * Adds a {@link Revision} to the release revisions.
     * 
     * @param releaseVersion
     *            The new revision to add.
     */
    public void addReleaseRevision(Revision releaseVersion) {
        this.releaseRevisions.add(releaseVersion);
    }
}