package org.evolizer.ontology.exporter.famix.test;


import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import org.evolizer.famix.model.entities.AbstractFamixEntity;
import org.evolizer.ontology.annotations.rdf;
import org.evolizer.ontology.exporter.main.Exporter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExporterTest {
	private Collection<Object> testEntities = new ArrayList<Object>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		createModel();
	}


	@After
	public void tearDown() throws Exception {
		clearModel();
	}
	
	@Test
	public void testExporter() {
		Exporter exporter = new Exporter("http://wuersch.org/ontologies/testproject");
		
		for (Object object : testEntities) {
			exporter.export(object);
		}
		
		exporter.writeModel();
		
		fail("Hey dude, you should really implement a real test!");
	}	

	private void createModel() {
		AbstractFamixEntity e1 = new FamixDummy();
		e1.setUniqueName("org.test.Foo");
		
		testEntities.add(e1);
	}
	
	private void clearModel() {
		testEntities.clear();
	}
	
	@rdf("https://code.org/onto.owl#Dummy")
	class FamixDummy extends AbstractFamixEntity {
		
	}
}




