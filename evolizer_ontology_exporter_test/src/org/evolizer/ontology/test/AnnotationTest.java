package org.evolizer.ontology.test;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.evolizer.ontology.annotations.object;
import org.evolizer.ontology.annotations.rdf;
import org.evolizer.ontology.annotations.subject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnnotationTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testIfAnnotationPresent() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		FoafPerson michael = new FoafPerson("Michael", "Wuersch");
		
		Class<? extends FoafPerson> pClass = michael.getClass();
		rdf annoPerson = pClass.getAnnotation(rdf.class);
		assertEquals("value of @rdf for Person is unexpected", FoafPerson.foaf + "Person", annoPerson.value());
		
		Method methodGetFirstName = pClass.getMethod("getFirstName");
		Method methodGetFamilyName = pClass.getMethod("getFamilyName");
		
		assertEquals("value of first name is unexpected", "Michael", methodGetFirstName.invoke(michael));
		assertEquals("value of family name is unexpected", "Wuersch", methodGetFamilyName.invoke(michael));
		
		rdf annoName = methodGetFirstName.getAnnotation(rdf.class);
		assertEquals("value of @rdf for getFirstName is unexpected", FoafPerson.foaf + "firstName", annoName.value());
		
		rdf annoFamilyName = methodGetFamilyName.getAnnotation(rdf.class);
		assertEquals("value of @rdf for getFamilyName is unexpected", FoafPerson.foaf + "family_name", annoFamilyName.value());
		
		FoafPerson gigs = new FoafPerson("Emanuel", "Giger");
		
		PersonToPersonRelationship coworking = new PersonToPersonRelationship(michael, gigs);
		
		Class<? extends PersonToPersonRelationship> ptpClass = coworking.getClass();
		rdf annoPtP = ptpClass.getAnnotation(rdf.class);
		assertEquals("isPredicate of @rdf for PersonToPersonRelationship is unexpected", true, annoPtP.isPredicate());
	}
	
	@rdf(FoafPerson.foaf + "Person")
	private class FoafPerson {
		public static final String foaf = "http://xmlns.com/foaf/0.1/";
		
		private String firstName;
		private String familyName;
		
		public FoafPerson(String name, String familyName) {
			this.firstName = name;
			this.familyName = familyName;
		}
		
		@SuppressWarnings("unused")
        public void setFirstName(String name) {
			this.firstName = name;
		}
		
		@SuppressWarnings("unused")
        @rdf(FoafPerson.foaf + "firstName")
		public String getFirstName() {
			return firstName;
		}
		
		
		@SuppressWarnings("unused")
        public void setFamilyName(String email) {
			this.familyName = email;
		}
		
		@SuppressWarnings("unused")
        @rdf(FoafPerson.foaf + "family_name")
		public String getFamilyName() {
			return familyName;
		}
	}
	
	@rdf(isPredicate=true, value = PersonToPersonRelationship.foaf + "knows")
	private class PersonToPersonRelationship {
		public static final String foaf = "http://xmlns.com/foaf/0.1/";
		private FoafPerson subject;

		private FoafPerson object;
		
		public PersonToPersonRelationship(FoafPerson subject, FoafPerson object) {
			this.subject = subject;
			this.object = object;
		}
		
		@SuppressWarnings("unused")
        @subject
		public FoafPerson getSubject() {
			return subject;
		}
		
		@SuppressWarnings("unused")
        @object
		public FoafPerson getObject() {
			return object;
		}
	}
}

