package org.evolizer.ontology.test;


import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import org.evolizer.ontology.annotations.object;
import org.evolizer.ontology.annotations.rdf;
import org.evolizer.ontology.annotations.subject;
import org.evolizer.ontology.exporter.main.Exporter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExporterTest {
	private Collection<Object> testEntities = new ArrayList<Object>();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		createModel();
	}


	@After
	public void tearDown() throws Exception {
		clearModel();
	}
	
	@Test
	public void testExporter() {
		Exporter exporter = new Exporter("http://wuersch.org/ontologies/testproject");
		
		for (Object object : testEntities) {
			exporter.export(object);
		}
		
		exporter.writeModel();
		
		fail("Hey dude, you should really implement a real test!");
	}	

	private void createModel() {
		FoafPerson michael = new FoafPerson("Michael", "Wuersch", 1L);
		Pet murloc = new Pet("Gurgarggmlll", 1L);
		HumanPetRelationship relation = new HumanPetRelationship(michael, murloc);
		
		testEntities.add(michael);
		testEntities.add(murloc);
		testEntities.add(relation);
	}
	
	private void clearModel() {
		testEntities.clear();
	}
	
	@rdf(FoafPerson.foaf + "Person")
	private class FoafPerson {
		public static final String foaf = "http://xmlns.com/foaf/0.1/";
		
		private Long id;
		private String firstName;
		private String familyName;
		
		public FoafPerson(String name, String familyName, Long id) {
			this.id = id;
			this.firstName = name;
			this.familyName = familyName;
		}

		@SuppressWarnings("unused")
        public Long getId() {
			return id;
		}
		
		@SuppressWarnings("unused")
        public void setFirstName(String name) {
			this.firstName = name;
		}
		
		@SuppressWarnings("unused")
        @rdf(FoafPerson.foaf + "firstName")
		public String getFirstName() {
			return firstName;
		}
		
		@SuppressWarnings("unused")
        public void setFamilyName(String email) {
			this.familyName = email;
		}
		
		@SuppressWarnings("unused")
        @rdf(FoafPerson.foaf + "family_name")
		public String getFamilyName() {
			return familyName;
		}
		
		public String getLabel() {
			return firstName + " " + familyName;
		}
		
		@Override
		public String toString() {
			return getLabel();
		}
	}
	
	@rdf(Pet.pet + "Pet")
	private class Pet {
		public static final String pet = "http://www.wuersch.org/zoo/0.1/mimi.owl";
		
		private Long id;
		private String name;
		
		public Pet(String name, Long id) {
			this.id = id;
			this.name = name;
		}
		
		@SuppressWarnings("unused")
        public Long getId() {
			return id;
		}
		
		@SuppressWarnings("unused")
        public void setName(String name) {
			this.name = name;
		}
		
		@SuppressWarnings("unused")
        @rdf(Pet.pet + "name")
		public String getName() {
			return this.name;
		}
		
		public String getLabel() {
			return name;
		}
		
		@Override
		public String toString() {
			return getLabel();
		}
	}
	
	@rdf(	
			isPredicate=true,
			value = HumanPetRelationship.relation + "hasPet"
	)
	private class HumanPetRelationship {
		public static final String relation = "http://www.wuersch.org/relations/0.1/";
		
		private FoafPerson owner;
		private Pet hisPet;
		
		public HumanPetRelationship(FoafPerson owner, Pet hisPet) {
			this.owner = owner;
			this.hisPet = hisPet;
		}
		
		@SuppressWarnings("unused")
        @subject
		public FoafPerson getOwner() {
			return this.owner;
		}
		
		@SuppressWarnings("unused")
        @object
		public Pet getPet(){
			return this.hisPet;
		}
	}
}



