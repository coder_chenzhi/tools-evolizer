package org.evolizer.model.resources;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	ResourceModelTest.class
})
public class AllTests {

}
