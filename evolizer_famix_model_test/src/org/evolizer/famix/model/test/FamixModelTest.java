package org.evolizer.famix.model.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.famix.model.entities.FamixAssociation;
import org.evolizer.famix.model.entities.FamixAttribute;
import org.evolizer.famix.model.entities.FamixClass;
import org.evolizer.famix.model.entities.FamixLocalVariable;
import org.evolizer.famix.model.entities.FamixMethod;
import org.evolizer.famix.model.entities.FamixPackage;
import org.evolizer.famix.model.entities.FamixParameter;
import org.evolizer.famix.model.entities.SourceAnchor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class FamixModelTest extends AbstractEvolizerHibernateTest {
	private IEvolizerSession fEvolizerSession = null;
	private static EvolizerSessionHandler fSessionHandler;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AbstractEvolizerHibernateTest.setUpBeforeClass();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		try{
			fSessionHandler = EvolizerSessionHandler.getHandler();
			fSessionHandler.initSessionFactory(fDBUrl, fDBUser, fDBPasswd);
			fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
			fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		if (fEvolizerSession.isOpen()) {
			fEvolizerSession.flush();
			fEvolizerSession.close();
		}

		fSessionHandler.cleanupHibernateSessions();
		fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		fSessionHandler = null;
	}
	
	@Test
	public void testFamixAssociationAndSourceAnchorConnection() throws EvolizerException{
		FamixAssociation association = new FamixAssociation();
		SourceAnchor anchor = new SourceAnchor("someFile", new Integer(1),new Integer(2));
		association.setSourceAnchor(anchor);
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(anchor);
		fEvolizerSession.saveObject(association);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();
		
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		List<FamixAssociation> associationResults = fEvolizerSession.query("from FamixAssociation", FamixAssociation.class);
		assertEquals(1,associationResults.size());
		FamixAssociation associationResult = associationResults.get(0);
		SourceAnchor anchorresult = associationResult.getSourceAnchor();
		assertEquals("someFile", anchorresult.getFile());
		assertEquals(2, anchorresult.getEndPos().intValue());
		assertEquals(1, anchorresult.getStartPos().intValue());
	}
	
	@Test
	public void testAssiocationAndFamixEntityConnection() throws EvolizerException{
		FamixAssociation association = new FamixAssociation();
		FamixMethod functionTo = new FamixMethod("functionTo");
		FamixMethod functionFrom = new FamixMethod("functionFrom");
		association.setFrom(functionFrom);
		association.setTo(functionTo);
		
		
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(functionTo);
		fEvolizerSession.saveObject(functionFrom);
		fEvolizerSession.saveObject(association);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();
		
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		List<FamixAssociation> associationResults = fEvolizerSession.query("from FamixAssociation", FamixAssociation.class);
		assertEquals(1, associationResults.size());
		
		FamixAssociation associationResult = associationResults.get(0);
		assertEquals("functionTo", associationResult.getTo().getName());
		assertEquals("functionFrom", associationResult.getFrom().getName());
	}
	
	@Test
	public void testFamixEntityAndPrarentConnection() throws EvolizerException{
		//Take Famix#Function as a concrete subclass of Famix#FamixEntity
		FamixMethod method = new FamixMethod("drawCircle");
		FamixMethod parentFunction = new FamixMethod("drawVisuals");
		method.setParent(parentFunction);
		method.setModifiers(1);
		
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(method);
		fEvolizerSession.saveObject(parentFunction);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();
		
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		List<FamixMethod> results = fEvolizerSession.query("from FamixMethod as f where f.uniqueName = 'drawCircle'", FamixMethod.class);
		assertEquals(1, results.size());
		FamixMethod resultFamixMethod = results.get(0);
		assertEquals("drawCircle", resultFamixMethod.getUniqueName());
		assertEquals("drawVisuals", resultFamixMethod.getParent().getUniqueName());
		assertEquals(1,resultFamixMethod.getModifiers());
	}
	
	@Test
	public void testBehaviouralEntityProperties() throws EvolizerException{
		//Take FamixModel#FamixMethod as a concrete subclass of FamixModel#BehavioralEntity
		FamixMethod method = new FamixMethod("foo");
		
		FamixClass declaredReturnedClass = new FamixClass("someClass");
		method.setDeclaredReturnClass(declaredReturnedClass);
		
		FamixLocalVariable lVar1 = new FamixLocalVariable("counter");
		FamixLocalVariable lVar2 = new FamixLocalVariable("metricValue");
		Set<FamixLocalVariable> lVars = new HashSet<FamixLocalVariable>();
		lVars.add(lVar1);
		lVars.add(lVar2);
		method.setLocalVariables(lVars);
		lVar1.setParent(method);
		lVar2.setParent(method);
		
		FamixClass anonymClass1 = new FamixClass("anonymClass1");
		FamixClass anonymClass2 = new FamixClass("anonymClass2");
		Set<FamixClass> anonyClasses = new HashSet<FamixClass>();
		anonyClasses.add(anonymClass1);
		anonyClasses.add(anonymClass2);
		method.setAnonymClasses(anonyClasses);
		anonymClass1.setParent(method);
		anonymClass2.setParent(method);
		
		FamixParameter fPar1 = new FamixParameter("parameter1");
		FamixParameter fPar2 = new FamixParameter("parameter2");
		List<FamixParameter> fPars = new ArrayList<FamixParameter>();
		fPars.add(fPar1);
		fPars.add(fPar2);
		method.setParameters(fPars);
		fPar1.setParent(method);
		fPar2.setParent(method);
		
		
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(lVar1);
		fEvolizerSession.saveObject(lVar2);
		fEvolizerSession.saveObject(anonymClass1);
		fEvolizerSession.saveObject(anonymClass2);
		fEvolizerSession.saveObject(declaredReturnedClass);
		fEvolizerSession.saveObject(fPar1);
		fEvolizerSession.saveObject(fPar2);
		fEvolizerSession.saveObject(method);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();
		
		
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		List<FamixMethod> result = fEvolizerSession.query("from FamixMethod", FamixMethod.class);
		assertEquals(1, result.size());
		FamixMethod returnedFamixMethod = result.get(0);
		assertEquals("foo", returnedFamixMethod.getUniqueName());
		
		Set<FamixLocalVariable> returnedLVars = returnedFamixMethod.getLocalVariables();
		assertEquals(2, returnedLVars.size());
		for(Iterator<FamixLocalVariable> iter = returnedLVars.iterator();iter.hasNext();){
			FamixLocalVariable var = iter.next();
			assertTrue((var.getUniqueName().equals("counter") || var.getUniqueName().equals("metricValue")));
			assertEquals("foo", var.getParent().getUniqueName());
			assertTrue(returnedFamixMethod==var.getParent());
		}

		List<FamixParameter> returnedFormalPars = returnedFamixMethod.getParameters();
		assertEquals(2, returnedFormalPars.size());
		for(Iterator<FamixParameter> iter = returnedFamixMethod.getParameters().iterator();iter.hasNext();){
			FamixParameter par = iter.next();
			assertTrue((par.getUniqueName().equals("parameter1") || par.getUniqueName().equals("parameter2")));
			assertEquals("foo", par.getParent().getUniqueName());
			assertTrue(returnedFamixMethod==par.getParent());
		}
		
		Set<FamixClass> returnedAnonyClass = returnedFamixMethod.getAnonymClasses();
		assertEquals(2, returnedAnonyClass.size());
		for(Iterator<FamixClass> iter = returnedFamixMethod.getAnonymClasses().iterator();iter.hasNext();){
			FamixClass returnedClass = iter.next();
			assertTrue((returnedClass.getUniqueName().equals("anonymClass1") || returnedClass.getUniqueName().equals("anonymClass2")));
			assertEquals("foo", returnedClass.getParent().getUniqueName());
			assertTrue(returnedFamixMethod==returnedClass.getParent());
		}
		assertEquals("someClass", returnedFamixMethod.getDeclaredReturnClass().getUniqueName());
	}
	
	@Test
	public void testFamixClass() throws EvolizerException{
		FamixClass clazz = new FamixClass("Foo");
		
		Set<FamixClass> innerClasses = new HashSet<FamixClass>();
		FamixClass class1 = new FamixClass("innerClass1");
		FamixClass class2 = new FamixClass("innerClass2");
		class1.setParent(clazz);
		class2.setParent(clazz);
		innerClasses.add(class1);
		innerClasses.add(class2);
		clazz.setInnerClasses(innerClasses);
		
		FamixAttribute a1 = new FamixAttribute("attribute1");
		FamixAttribute a2 = new FamixAttribute("attribute2");
		a1.setParent(clazz);
		a2.setParent(clazz);
		clazz.getAttributes().add(a1);
		clazz.getAttributes().add(a2);
		
		FamixMethod m1 = new FamixMethod("method1");
		FamixMethod m2 = new FamixMethod("method2");
		m1.setParent(clazz);
		m2.setParent(clazz);
		clazz.getMethods().add(m1);
		clazz.getMethods().add(m2);
				
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(class1);
		fEvolizerSession.saveObject(class2);
		fEvolizerSession.saveObject(clazz);
		fEvolizerSession.saveObject(a1);
		fEvolizerSession.saveObject(a2);
		fEvolizerSession.saveObject(m1);
		fEvolizerSession.saveObject(m2);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();
		
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		List<FamixClass> results = fEvolizerSession.query("from FamixClass as c where c.uniqueName = 'Foo'", FamixClass.class);
		assertEquals(1, results.size());
		
		FamixClass returnedClass = results.get(0);
		assertEquals("Foo", returnedClass.getUniqueName());
		
		Set<FamixClass> returnedInnerClasses = returnedClass.getInnerClasses();
		assertEquals(2, returnedInnerClasses.size());
		for(Iterator<FamixClass> iter = returnedInnerClasses.iterator();iter.hasNext();){
			FamixClass cl = iter.next();
			assertTrue((cl.getUniqueName().equals("innerClass1") || cl.getUniqueName().equals("innerClass2")));
			assertTrue(cl.getParent()==returnedClass);
		}
		
		Set<FamixAttribute> returnedFamixAttributes = returnedClass.getAttributes();
		assertEquals(2, returnedFamixAttributes.size());
		for(Iterator<FamixAttribute> iter = returnedFamixAttributes.iterator();iter.hasNext();){
			FamixAttribute at = iter.next();
			assertTrue((at.getUniqueName().equals("attribute1") || at.getUniqueName().equals("attribute2")));
			assertTrue(at.getParent()==returnedClass);
		}
		
		Set<FamixMethod> returnedFamixMethods = returnedClass.getMethods();
		assertEquals(2, returnedFamixMethods.size());
		for(Iterator<FamixMethod> iter = returnedFamixMethods.iterator();iter.hasNext();){
			FamixMethod m = iter.next();
			assertTrue((m.getUniqueName().equals("method1") || m.getUniqueName().equals("method2")));
			assertTrue(m.getParent()==returnedClass);
		}
	}


	@Test
	public void testFile(){
		//TODO re-implement this test!
	}
	
	@Test
	public void testFamixPackage() throws EvolizerException{
		FamixPackage p = new FamixPackage("java.awt");
				
		FamixClass clazz = new FamixClass("ActionEvent");
		p.getClasses().add(clazz);
				
		fEvolizerSession.startTransaction();
		fEvolizerSession.saveObject(p);
		fEvolizerSession.saveObject(clazz);
		fEvolizerSession.endTransaction();
		fEvolizerSession.close();
		
		fEvolizerSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		List<FamixPackage> packages = fEvolizerSession.query("from FamixPackage as p where p.uniqueName = 'java.awt'", FamixPackage.class);
		assertEquals(1, packages.size());
		FamixPackage returnedFamixPackage = packages.get(0);
		assertEquals("java.awt", returnedFamixPackage.getUniqueName());
				
		assertEquals(1, returnedFamixPackage.getClasses().size());
		FamixClass returnedClass = returnedFamixPackage.getClasses().iterator().next();
		assertEquals("ActionEvent", returnedClass.getUniqueName());
	}
}
