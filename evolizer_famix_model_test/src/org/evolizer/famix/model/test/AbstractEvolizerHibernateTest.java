package org.evolizer.famix.model.test;

import java.io.InputStream;
import java.util.Properties;

import org.evolizer.famix.model.FamixModelPlugin;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public abstract class AbstractEvolizerHibernateTest {
	private static final String DB_CONFIG_FILE = "./config/db.properties";

	protected static String fDBUrl;

	protected static String fDBDialect;

	protected static String fDBDriverName;

	protected static String fDBUser;

	protected static String fDBPasswd;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream propertiesInputStream = FamixModelPlugin.openBundledFile(DB_CONFIG_FILE);

		if (propertiesInputStream != null) {
			Properties props = new Properties();
			props.load(propertiesInputStream);
			propertiesInputStream.close();

			fDBUrl = props.getProperty("dbUrl");
			fDBDialect = props.getProperty("dbDialect");
			fDBDriverName = props.getProperty("dbDriverName");
			fDBUser = props.getProperty("dbUser");
			fDBPasswd = props.getProperty("dbPasswd");
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

}
