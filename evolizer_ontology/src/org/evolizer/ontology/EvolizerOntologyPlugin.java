package org.evolizer.ontology;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class EvolizerOntologyPlugin extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.evolizer.ontology";

	// The shared instance
	private static EvolizerOntologyPlugin plugin;
	
	/**
	 * The constructor
	 */
	public EvolizerOntologyPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static EvolizerOntologyPlugin getDefault() {
		return plugin;
	}
	
	/**
	 * Opens a file located within the plug-in-bundle
	 * 
	 * @param filePath
	 *            relative path of the file starting
	 * @return an InputStream reading the specified file
	 * @throws IOException
	 *             if file could not be opened
	 */
	public static InputStream openBundledFile(String filePath)
			throws IOException {
		return EvolizerOntologyPlugin.getDefault().getBundle().getEntry(filePath)
				.openStream();
	}
	
	/**
	 * Converts a path relative to the bundle root to an absolute file system path.
	 * 
	 * @param relativeFSPath a path relative to the root of the current bundle.
	 * @return the absolute file system path for the specified relative one.
	 * @throws IOException if an error occurs during the resolution of the relative path.
	 */
	public String getAbsoluteFSPath(String relativeFSPath) throws IOException {
		URL url = getURL(relativeFSPath);
		return FileLocator.resolve(url).getFile();
	}
	
	/**
	 * Converts a relative path to an {@link URL} object.
	 * 
	 * @param relativeFSPath a path relative to the root of the current bundle.
	 * @return a {@link URL} pointing to the file/folder specified by the bundle-relative path.
	 */
	public URL getURL(String relativeFSPath) {
		return FileLocator.find(getBundle(), new Path(relativeFSPath), null);
	}
}
