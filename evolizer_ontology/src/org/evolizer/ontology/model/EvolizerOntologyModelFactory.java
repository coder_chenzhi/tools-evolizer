package org.evolizer.ontology.model;

import java.io.IOException;

import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.ontology.EvolizerOntologyPlugin;
import org.evolizer.ontology.vocabulary.DCES;
import org.evolizer.ontology.vocabulary.OWL;
import org.evolizer.ontology.vocabulary.RDF;
import org.evolizer.ontology.vocabulary.RDFS;
import org.evolizer.ontology.vocabulary.SeonRelations;
import org.mindswap.pellet.jena.PelletReasonerFactory;

import com.hp.hpl.jena.ontology.OntDocumentManager;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.tdb.TDBFactory;

public final class EvolizerOntologyModelFactory {
	private static final String ONTOLOGY_DIR = "ontologies";
	
	public static Model createEmptyModelWithTDB(String storagePath) {
		return TDBFactory.createModel(storagePath);
	}
	
	public static OntModel createDefaultOntModelWithTDB(String storagePath) {
		OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM, createEmptyModelWithTDB(storagePath));
		
		addDefaultEntriesTo(model);
		
		return model;
	}
	
	
	public static OntModel createEmptyOntModel() {
		return ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
	}
	
	public static OntModel createDefaultOntModel() {
		OntModel empty = createEmptyOntModel();

		addDefaultEntriesTo(empty);

		return empty;
	}

	public static OntModel createOntModelWithPellet(OntModel base) {
		return ModelFactory.createOntologyModel(PelletReasonerFactory.THE_SPEC, base);
	}

	private static void addDefaultEntriesTo(OntModel ontModel) {
		OntDocumentManager documentManager = ontModel.getDocumentManager();

		try {
			String dir = EvolizerOntologyPlugin.getDefault().getAbsoluteFSPath(ONTOLOGY_DIR);

			// Sem. Web defaults
			documentManager.addAltEntry(RDF.BASE, "file:" + dir + RDF.LOCAL);
			documentManager.addAltEntry(RDFS.BASE, "file:" + dir + RDFS.LOCAL);
			documentManager.addAltEntry(OWL.BASE, "file:" + dir + OWL.LOCAL);
			documentManager.addAltEntry(DCES.BASE, "file:" + dir + DCES.LOCAL);

			// Seon defaults
			documentManager.addAltEntry(SeonRelations.BASE, "file:" + dir + SeonRelations.LOCAL);
		} catch (IOException e) {
			throw new EvolizerRuntimeException("Error while resolving ontology directory.", e); // TODO own exception?
		}
	}
}
