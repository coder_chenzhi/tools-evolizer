/**
 * 
 */
package org.evolizer.ontology.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * This annotation allows to add rdf information to java objects by annotating
 * the class declaration as well as the declaration of all relevant getter
 * methods. <code>ElementType.TYPE</code> its value has to correspond to an
 * URI reference describing an OWL class. If added to a
 * <code>ElementType.METHOD</code> its value has to correspond to an URI
 * reference describing an OWL property. NOTE that no validity checks of any
 * kind are performed by Evolizer at this time, meaning that the developer is
 * responsible for annotating classes in meaningful way.
 * 
 * @author wuersch
 * 
 */
@Retention(RUNTIME)
@Target( { TYPE, METHOD })
public @interface rdf {
	/**
	 * Returns the URI describing the OWL class or one of its properties.
	 * @return an URI
	 */
	String value();
	boolean isPredicate() default false;
}
