package org.evolizer.ontology.util;

/**
 * Implementors of this interface need to provide functionality
 * to serialize the given statement.
 * 
 * @author wuersch
 *
 * @param <S> the type of the statement's subject.
 * @param <P> the type of the statement's predicate.
 * @param <O> the type of the statement's object.
 */
public interface IStatementSerializationStrategy<S, P, O> {
	public void serialize(Statement<S, P, O> statement);
}
