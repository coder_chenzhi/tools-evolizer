/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.git.importer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.git.importer.exceptions.GitImporterException;
import org.evolizer.versioncontrol.git.importer.job.GITImporterJob;
import org.evolizer.versioncontrol.git.importer.mapper.GITModelMapper;

import edu.nyu.cs.javagit.api.DotGit;
import edu.nyu.cs.javagit.api.JavaGitConfiguration;
import edu.nyu.cs.javagit.api.JavaGitException;
import edu.nyu.cs.javagit.api.commands.GitLogOptions;
import edu.nyu.cs.javagit.api.commands.GitLogResponse.Commit;
import edu.nyu.cs.javagit.api.commands.GitLogResponse.CommitFile;

/**
 * 
 * This class imports the GIT information of a given {@link IProject} into the RHDB. It extracts and parses the desired
 * information from the log entries which is then mapped to a specific Version History model by the
 * {@link GITModelMapper} ModelMapper which creates a custom model accordingly.
 * 
 * @see GITImporterJob
 * @see GITModelMapper
 * @author ghezzi
 * 
 */
public class EvolizerGitImporter {

    /**
     * The entity in charge of maping the data extracted from GIT to a Version Control model
     */
    private GITModelMapper fMapper;
    /**
     * The standard logger
     */
    private static final Logger LOGGER =
            EvolizerGITPlugin.getLogManager().getLogger(EvolizerGitImporter.class.getCanonicalName());
    /**
     * The file containing some useful importer settings: the git binaries folder location and the temporary folder in
     * which to download the git repository to be analyzed
     */
    private static final String CONFIG_FILE = "config/importer.properties";
    /**
     * The path of the project GIT repository (can be both a file system path or a URL)
     */
    private String fGitRepositoryLocation;
    /**
     * Instance of the GIT repository
     */
    private DotGit fDotGit;
    /**
     * Hibernate Session providing the connection to the database
     */
    private IEvolizerSession fSession;
    /**
     * Monitor used to track and report the progress of the import
     */
    private IProgressMonitor fMonitor;
    /**
     * The wildcard representing any type of file extension. Used when the user wants to fetch the source for every type
     * of file.
     */
    private static final String FILE_EXTENSION_WILDCARD = "*";
    /**
     * Whether or not the importer has to fetch the contents of some specific file types
     */
    private boolean fFetchSrc;
    /**
     * The list of the file types for which the importer has to fetch the contents
     */
    private ArrayList<String> fExtensions = new ArrayList<String>();

    /**
     * Construct.
     * 
     * @param projectLocation
     *            the path in which the project to be analyzed resides
     * @param session
     *            the session that is used to save the data found
     * @param fileExtensionRegex
     *            the file extension regex, telling the importer which files source to fetch. If null or empty, no file
     *            will be fetched
     * @param monitor
     *            to monitor the import process
     * @throws GitImporterException
     *             if there are some problems setting up the importer
     */
    public EvolizerGitImporter(
            String projectLocation,
            IEvolizerSession session,
            String fileExtensionRegex,
            IProgressMonitor monitor) throws GitImporterException {
        fSession = session;
        fGitRepositoryLocation = projectLocation;
        if (monitor == null) {
            fMonitor = new NullProgressMonitor();
        } else {
            fMonitor = monitor;
        }

        if (fileExtensionRegex != null && !fileExtensionRegex.equals("")) {
            fFetchSrc = true;
            if (fileExtensionRegex.equals(FILE_EXTENSION_WILDCARD)) {
                fExtensions.add(fileExtensionRegex);
            } else {
                StringTokenizer t = new StringTokenizer(fileExtensionRegex, ";");
                while (t.hasMoreTokens()) {
                    fExtensions.add(t.nextToken());
                }
            }
        }
        this.setUp();
    }

    /**
     * Sets up all the required options in the import: the git binary path and the directory in which the required
     * project will be downloaded and its history extracted in case the project needs to be downloaded (only if the
     * repository is remote).
     * 
     * @throws GitImporterException
     *             if there are some problems setting up the git library
     */
    private void setUp() throws GitImporterException {
        fMonitor.beginTask("Setting up git importer", 3);
        String gitPath;
        Properties props = null;
        fMonitor.subTask("Setting up git properties");
        try {
            props = getProperties();

            gitPath = props.getProperty("gitPath");

            if (gitPath == null || gitPath.compareTo("") == 0) {
                throw new GitImporterException("the git path was not set, check the importer settings file");
            }
        } catch (IOException e) {
            throw new GitImporterException("Error while reading the importer settings file");
        }

        // I set up the Git path right away
        try {
            JavaGitConfiguration.setGitPath(gitPath);
        } catch (IOException e) {
            throw new GitImporterException("Error while setting the git path: " + gitPath, e);
        } catch (JavaGitException e) {
            throw new GitImporterException("Error while setting the git path: " + gitPath, e);
        }
        fMonitor.worked(1);
        fMapper = new GITModelMapper(fSession);
        
        // Importing from a local (or pre-downloaded repository)
        fMonitor.subTask("Connecting to the local git repository location");
        fDotGit = DotGit.getInstance(fGitRepositoryLocation);
        fMonitor.worked(2);
        fMonitor.done();
    }

    /**
     * Method reading the configuration file, written following the java.util.Properties "standards". These properties
     * are needed to create set up the git path and the directory in which the git project will be downloaded and then
     * its history extracted.
     * 
     * @return the actual properties extracted from the config file
     * @see java.util.Properties
     * @throws IOException
     */
    private static Properties getProperties() throws IOException {
        InputStream propertiesInputStream;
        Properties props = new Properties();
        propertiesInputStream = EvolizerGITPlugin.openFile(CONFIG_FILE);
        if (propertiesInputStream != null) {
            props.load(propertiesInputStream);
        }
        propertiesInputStream.close();
        return props;
    }

    /**
     * Does the actual importing.
     * 
     * @throws GitImporterException
     *             in case some problems arise while logging to get all the tags or the data for a specific one
     */
    public void importProject() throws GitImporterException {
        ArrayList<String> tags = new ArrayList<String>();
        try {
            // I get the tags of the project. They are also known in other Version Control Systems as Releases.
            tags = (ArrayList<String>) fDotGit.getTags();
            fMonitor.beginTask("Importing revision history", tags.size() + 1);
        } catch (JavaGitException e) {
            throw new GitImporterException("Problems encountered while fetching all the git tags", e);
        } catch (IOException e) {
            throw new GitImporterException("Problems encountered while fetching all the git tags", e);
        }
        // For each tag/release I extract its history
        for (String tag : tags) {
            GitLogOptions options = new GitLogOptions();
            options.setOptSpecificTag(true, tag);
            options.setOptFileDetails(true);
            options.setOptOrderingReverse(true);
            options.setOptLimitNoMerges(true); // Don't care about merges for now
            options.setOptFileChangeType(true);
            try {
                fMonitor.subTask("Extracting history for tag " + tag);
                handleRelease(options, tag);
                fMonitor.worked(1);
            } catch (JavaGitException e) {
                throw new GitImporterException("Problems encountered while importing git history for release " + tag, e);
            } catch (IOException e) {
                throw new GitImporterException("Problems encountered while importing git history for release " + tag, e);
            }
        }
        if (!tags.isEmpty()) {
            fMonitor.subTask("Saving the data");
            fMapper.saveModel();
        } else {
            throw new GitImporterException("The GIT repository contains no tags therefore no history could be extracted");
        }
        fMonitor.worked(1);
        fMonitor.done();
        LOGGER.debug("Repository " + fGitRepositoryLocation + " imported successfully");
    }

    /**
     * Handles the extraction of the history of a given GIT tag (release).
     * 
     * @param options
     *            The GIT options required to correctly fetch the data for the tag
     * @param tag
     *            The tag name
     * @throws JavaGitException
     *             If problems arise while fetching the history from the GIT repository
     * @throws IOException
     *             If problems arise while fetching the history from the GIT repository
     * @throws GitImporterException
     *             If problems arise while creating the Version Control model out of the tag history
     */
    private void handleRelease(GitLogOptions options, String tag)
            throws JavaGitException,
            IOException,
            GitImporterException {
        LOGGER.debug("******Extracting history for tag/release " + tag + "******");
        // Creating the release
        List<Commit> commits = fDotGit.getLog(options);
        if (!commits.isEmpty()) {
            Commit lastCommit = commits.get(commits.size() - 1);
            fMapper.createRelease(tag, lastCommit.getDateString());
            // Iterating over the commits in reverse order, from the oldest to the newest.
            for (Commit commit : commits) {
                LOGGER.debug("extracting data for commit " + commit.getSha());
                if (commit.getFiles() != null) {
                    if (fFetchSrc) {
                        Map<String, String> filesContent = new HashMap<String, String>();
                        for (CommitFile f : commit.getFiles()) {
                            if (f.getChangeType() == null) {
                                throw new GitImporterException(
                                        "Found a file that had no change type (all the files must be marked as either Modified, Deleted or Added) ");
                            }
                            if (f.getChangeType().compareTo("A") == 0 || f.getChangeType().compareTo("M") == 0) {
                                
                                String fileContents = getFileContents(f.getName(), commit.getSha());
                                if (fileContents != null) {
                                    filesContent.put(f.getName(), fileContents);
                                }
                            }
                        }
                        fMapper.createTransaction(commit, filesContent);
                    } else {
                        fMapper.createTransaction(commit, null);
                    }
                }
            }
            LOGGER.info("******Finished extracting Release " + tag + " history******");

        } else {
            LOGGER.warn("Release " + tag + " is empty, it has no commits");
        }
    }

    /**
     * Fetches the contents of a specific revision of a file, only if it belongs to one of the file types that need to
     * be exported.
     * 
     * @param filePath
     *            The repository path to the entity to be extracted
     * @param sha
     *            The SHA of the specific revision
     * @return A string with all the contents of that entity
     * @throws IOException
     *             If problems arise while reading the contents of the file
     * @throws JavaGitException
     *             If problems arise while fetching the file from GIT
     */
    private String getFileContents(String filePath, String sha) throws JavaGitException, IOException {
        String content = null;
        int index = filePath.lastIndexOf(".");
        if (index != -1) {
            filePath.substring(index);
        }
        if (fExtensions.contains(FILE_EXTENSION_WILDCARD)
                || (index != 1 && fExtensions.contains(filePath.substring(index)))) {
            content = fDotGit.show(filePath, sha);
        }
        return content;
    }

}
