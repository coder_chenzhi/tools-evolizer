/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.git.importer.exceptions;

/**
 * Exception used to report problems during the import of the versioning history from a git repository.
 * 
 * @author ghezzi
 * 
 */
public class GitImporterException extends Exception {

    /**
     * The required serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * 
     * @param message
     *            The message of the newly created exception
     */
    public GitImporterException(String message) {
        super(message);
    }

    /**
     * Constructor
     * 
     * @param cause
     *            The cause of the newly created exception
     */
    public GitImporterException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor
     * 
     * @param message
     *            The message of the newly created exception
     * @param cause
     *            The cause of the newly created exception
     */
    public GitImporterException(String message, Throwable cause) {
        super(message, cause);
    }
}
