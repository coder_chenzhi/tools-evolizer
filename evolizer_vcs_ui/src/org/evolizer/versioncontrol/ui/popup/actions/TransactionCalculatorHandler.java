/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.ui.popup.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.evolizer.versioncontrol.cvs.importer.job.TransactionCalculatorJob;
import org.evolizer.versioncontrol.ui.wizards.TransactionCalculatorWizard;

/**
 * Menu action to invoke {@link TransactionCalculatorJob}.
 * 
 * @author wuersch
 */
public class TransactionCalculatorHandler extends AbstractHandler {

    private IProject getProject(ExecutionEvent event) {
        IStructuredSelection selection = null;
        if (HandlerUtil.getCurrentSelection(event) instanceof IStructuredSelection) {
            selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
        }
        Object selectedElement = selection.getFirstElement();
        IProject project;
        if (selectedElement instanceof IProject) {
            project = (IProject) selectedElement;
        } else {
            project = ((IJavaProject) selectedElement).getProject();
        }
        return project;
    }

    /**
     * {@inheritDoc}
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
        IProject project = getProject(event);
        Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

        TransactionCalculatorWizard wizard = new TransactionCalculatorWizard();

        WizardDialog dialog = new WizardDialog(activeShell, wizard);
        dialog.open();

        if (!wizard.isCanceled()) {
            Job job =
                    new TransactionCalculatorJob("Transaction Reconstruction", project, wizard.getTMax(), wizard
                            .getMaxDist());
            job.setUser(true);
            job.schedule();
        }
        return null;
    }
}
