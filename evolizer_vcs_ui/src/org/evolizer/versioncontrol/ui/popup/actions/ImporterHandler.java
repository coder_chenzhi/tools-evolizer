/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.ui.popup.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.egit.core.GitProvider;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.team.internal.ccvs.core.CVSTeamProvider;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.evolizer.versioncontrol.cvs.importer.job.CVSImporterJob;
import org.evolizer.versioncontrol.ui.wizards.CVSImporterWizard;
import org.evolizer.versioncontrol.ui.wizards.GITImporterWizard;
import org.evolizer.versioncontrol.ui.wizards.SVNImporterWizard;
import org.eclipse.team.svn.core.SVNTeamProvider;

/**
 * Menu action to invoke a specific version history importer job, depending on the versioning system the project is
 * using. Right now the available importer jobs are {@link CVSImporterJob}, {@link SVNImporterJob}.
 * 
 * @author wuersch, ghezzi
 */
@SuppressWarnings("restriction")
public class ImporterHandler extends AbstractHandler {

    private IProject getProject(ExecutionEvent event) {
        IStructuredSelection selection = null;
        if (HandlerUtil.getCurrentSelection(event) instanceof IStructuredSelection) {
            selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
        }
        Object selectedElement = selection.getFirstElement();
        IProject project;
        if (selectedElement instanceof IProject) {
            project = (IProject) selectedElement;
        } else {
            project = ((IJavaProject) selectedElement).getProject();
        }
        return project;
    }

    /**
     * {@inheritDoc}
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {

        IProject project = getProject(event);
        RepositoryProvider provider = RepositoryProvider.getProvider(project);
        if (provider instanceof CVSTeamProvider) {
            CVSImporterWizard wizard = new CVSImporterWizard(project);
            Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
            WizardDialog wizardDialog = new WizardDialog(activeShell, wizard);
            wizardDialog.open();
        } else if (provider instanceof SVNTeamProvider) {
            SVNImporterWizard wizard = new SVNImporterWizard(project);
            Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
            WizardDialog wizardDialog = new WizardDialog(activeShell, wizard);
            wizardDialog.open();
        } else if (provider instanceof GitProvider) {
            GITImporterWizard wizard = new GITImporterWizard(project);
            Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
            WizardDialog wizardDialog = new WizardDialog(activeShell, wizard);
            wizardDialog.open();
        }
        return null;
    }
}
