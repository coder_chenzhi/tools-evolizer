/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.ui.popup.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.team.internal.ccvs.core.CVSTeamProvider;
import org.eclipse.team.svn.core.SVNTeamProvider;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.cvs.importer.job.MissingFileContentImporterJob;
import org.evolizer.versioncontrol.svn.importer.job.MissingSVNFileContentImporterJob;
import org.evolizer.versioncontrol.ui.wizards.CVSMissingSourceCodeWizard;
import org.evolizer.versioncontrol.ui.wizards.SVNMissingSourceCodeWizard;

/**
 * Menu action that allows to (re-)fetch the file contents from CVS after the CVS importer has run for the first time.
 * 
 * @author wuersch
 */
@SuppressWarnings("restriction")
public class FetchMissingSourceCodeHandler extends AbstractHandler {

    private IProject getProject(ExecutionEvent event) {
        IStructuredSelection selection = null;
        if (HandlerUtil.getCurrentSelection(event) instanceof IStructuredSelection) {
            selection = (IStructuredSelection) HandlerUtil.getCurrentSelection(event);
        }
        Object selectedElement = selection.getFirstElement();
        IProject project;
        if (selectedElement instanceof IProject) {
            project = (IProject) selectedElement;
        } else {
            project = ((IJavaProject) selectedElement).getProject();
        }
        return project;
    }

    /**
     * {@inheritDoc}
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
        IProject project = getProject(event);

        Shell activeShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
        
        //Before I even open the wizard, I check that some history was already imported, otherwise I won't proceed.
        EvolizerSessionHandler handler = EvolizerSessionHandler.getHandler();
        try {
            IEvolizerSession persistenceProvider = handler.getCurrentSession(project);
            Long res = persistenceProvider.uniqueResult("select count (*) from Release", Long.class);
            if (res == null || res <= 0) {
                MessageDialog.openError(activeShell, "Missing File Content Importer Error", "No history was imported yet.\nFile content cannot be imported if no history was imported beforehand.");
                return null;
            }
        } catch (EvolizerException e) {
            MessageDialog.openError(activeShell, "Missing File Content Importer SetUp Error", "Problems encountered while connecting to the release history DB while setting up the importer wizard.");
            return null;
        }
        
        RepositoryProvider provider = RepositoryProvider.getProvider(project);
        if (provider instanceof CVSTeamProvider) {
            CVSMissingSourceCodeWizard wizard = new CVSMissingSourceCodeWizard();

            WizardDialog dialog = new WizardDialog(activeShell, wizard);
            dialog.open();

            if (!wizard.isCanceled()) {
                Job job =
                        new MissingFileContentImporterJob("Import Missing File Content", project, wizard
                                .getFileExtensionRegEx(), wizard.isReImportEnabled());
                job.setUser(true);
                job.schedule();
            }
        } else if (provider instanceof SVNTeamProvider) {
            SVNMissingSourceCodeWizard wizard = new SVNMissingSourceCodeWizard(project);
            
            WizardDialog dialog = new WizardDialog(activeShell, wizard);
            dialog.open();

            if (!wizard.isCanceled()) {
                Job job =
                        new MissingSVNFileContentImporterJob("Import Missing File Content", project, wizard
                                .getFileExtensionRegEx(), wizard.getFetchType(), wizard.getStart(), wizard.getEnd(), wizard.getSelectedReleases());
                job.setUser(true);
                job.schedule();
            }
        }
        
        return null;
    }
}
