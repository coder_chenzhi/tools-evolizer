/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.ui.wizards;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.evolizer.versioncontrol.ui.EvolizerVersioningUIPlugin;

/**
 * Wizard to guide the user through the process of importing contents from CVS into the RHDB.
 * 
 * @author giger, ghezzi
 */
public class CVSMissingSourceCodeWizard extends Wizard {

    private MissingFileContentPage fPage;
    private boolean fCanceled;
    private boolean fReImport;
    private String fFileExtensionRegEx;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performFinish() {
        fReImport = fPage.isReImportEnabled();
        fFileExtensionRegEx = fPage.getFileExtensionRegEx();
        return true;
    }

    /**
     * Returns the file extension regex.
     * 
     * @return the file extension regex
     */
    public String getFileExtensionRegEx() {
        return fFileExtensionRegEx;
    }

    /**
     * Checks if is re import enabled.
     * 
     * @return <code>true</code> if is re import enabled, <code>false</code> otherwise
     */
    public boolean isReImportEnabled() {
        return fReImport;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPages() {
        fPage = new MissingFileContentPage("File Content Importer");
        fPage.setTitle("Import File Content for Versioning Model");
        fPage.setDescription("Import file content for an existing versioning model.");
        fPage
        .setImageDescriptor(ImageDescriptor.createFromFile(
                EvolizerVersioningUIPlugin.class,
                "/icons/versioncontrol_background.png"));
        addPage(fPage);
    }

    /**
     * Checks if is canceled.
     * 
     * @return <code>true</code> if is canceled, <code>false</code> otherwise
     */
    public boolean isCanceled() {
        return fCanceled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performCancel() {
        fCanceled = true;
        return super.performCancel();
    }

    private class MissingFileContentPage extends WizardPage {

        private Button fReImportButton;
        private Label fRegExLabel;
        private Text fFileExtensionText;
        private String fFileExtensionsRegEx = "(\\*)|(\\.\\w+)(;\\.\\w+)*";

        protected MissingFileContentPage(String pageName) {
            super(pageName);
        }

        public boolean isReImportEnabled() {
            return fReImportButton.getSelection();
        }

        public String getFileExtensionRegEx() {
            return fFileExtensionText.getText();
        }

        public void createControl(Composite parent) {

            Composite mainComposite = new Composite(parent, SWT.NONE);

            GridLayout mainGridLayout = new GridLayout();
            mainComposite.setLayout(mainGridLayout);
            mainGridLayout.numColumns = 2;

            fReImportButton = new Button(mainComposite, SWT.CHECK);

            GridData statusGridData = new GridData(GridData.FILL_HORIZONTAL);
            statusGridData.horizontalSpan = 2;
            fReImportButton.setLayoutData(statusGridData);
            fReImportButton.setText("Re-Import any existing content");
            fReImportButton.setSelection(false);

            fRegExLabel = new Label(mainComposite, SWT.NONE);
            fRegExLabel.setText("Filter the extensions you want to fetch, for example \".java\".\nSeparate very extension with a \";\". Use * to fetch all types of files");

            fFileExtensionText = new Text(mainComposite, SWT.BORDER);
            GridData fileExtensionGridData = new GridData(GridData.FILL_HORIZONTAL);
            fFileExtensionText.setLayoutData(fileExtensionGridData);
            fFileExtensionText.setText("*");
            fFileExtensionText.addKeyListener(new KeyListener() {

                public void keyReleased(KeyEvent e) {
                    MissingFileContentPage.this.fileExtensionsChanged(((Text) e.getSource()).getText());
                }

                public void keyPressed(KeyEvent e) {}
            });

            setControl(mainComposite);
        }
        
        /**
         * Checks that the given string is a valid instance of the file type regular expression (a semi colon separated
         * list of file extensions).
         * 
         * @param fileExtensions
         *            The string to check.
         */
        private void fileExtensionsChanged(String fileExtensions) {
            Pattern p = Pattern.compile(fFileExtensionsRegEx);
            Matcher m = p.matcher(fileExtensions);
            if (!m.matches()) {
                this.setErrorMessage("Separate every extension with a \";\" or use * to fetch all types of files");
                this.setPageComplete(false);
            } else {
                this.setErrorMessage(null);
                this.setPageComplete(true);
            }
        }
    }
}
