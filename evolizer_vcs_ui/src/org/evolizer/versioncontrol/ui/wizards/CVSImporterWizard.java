/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.ui.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSImporter;
import org.evolizer.versioncontrol.cvs.importer.job.CVSImporterJob;
import org.evolizer.versioncontrol.ui.EvolizerVersioningUIPlugin;

/**
 * The Wizard to specify all the import settings required to run an {@link CVSImporterJob}, which takes then care to
 * execute the {@link EvolizerCVSImporter}.
 * 
 * @author ghezzi
 * 
 */
public class CVSImporterWizard extends Wizard {

    private CVSImporterWizardPage fPage;
    private boolean fCanceled;
    private IProject fProject;

    /**
     * Constructor.
     * 
     * @param project
     *            The project target of the SVN history extraction
     */
    public CVSImporterWizard(IProject project) {
        super();
        fProject = project;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public boolean performFinish() {
        String srcFilter = "";
        if (fPage.importSrc()) {
            srcFilter = fPage.getSourceFilter();
        }

        CVSImporterJob importerJob = new CVSImporterJob(fProject, srcFilter, fPage.diff());
        importerJob.setUser(true);
        importerJob.schedule();
        return true;
    }

    /**
     * Checks if is canceled.
     * 
     * @return <code>true</code> if is canceled, <code>false</code> otherwise
     */
    public boolean isCanceled() {
        return fCanceled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performCancel() {
        fCanceled = true;
        return super.performCancel();
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void addPages() {
        this.fPage = new CVSImporterWizardPage("CVS Importer");
        this.fPage.setTitle("CVS Importer settings");
        this.fPage.setDescription("Required settings for a correct CVS history import");
        this.fPage
        .setImageDescriptor(ImageDescriptor.createFromFile(
                EvolizerVersioningUIPlugin.class,
                "/icons/versioncontrol_background.png"));
        this.addPage(this.fPage);
    }

    /**
     * The {@link WizardPage} associated to the {@link CVSImporterWizard} to set all the required importer settings.
     * 
     * @author ghezzi
     * 
     */
    private class CVSImporterWizardPage extends WizardPage {

        private Text fSrcText;
        private Label fSrcTextLabel;
        private boolean fImportSrc;
        private boolean fDiff;

        protected CVSImporterWizardPage(String pageName) {
            super(pageName);
        }

        public boolean importSrc() {
            return this.fImportSrc;
        }

        public boolean diff() {
            return this.fDiff;
        }

        public String getSourceFilter() {
            return this.fSrcText.getText();
        }

        // public void performHelp() {
        // PlatformUI.getWorkbench().getHelpSystem().displayHelp("CONTEXT_ID");
        // }

        /**
         * 
         * {@inheritDoc}
         */
        public void createControl(Composite parent) {

            Composite mainComposite = new Composite(parent, SWT.NONE);

            GridLayout mainGridLayout = new GridLayout();
            mainComposite.setLayout(mainGridLayout);
            mainGridLayout.numColumns = 1;

            Group importSettingsGroup = new Group(mainComposite, SWT.SHADOW_ETCHED_IN);
            importSettingsGroup.setText("Import settings");
            GridLayout importGroupGridLayout = new GridLayout();
            importGroupGridLayout.numColumns = 2;
            importSettingsGroup.setLayout(importGroupGridLayout);
            GridData groupGridData = new GridData(GridData.FILL_HORIZONTAL);
            groupGridData.horizontalSpan = 2;
            importSettingsGroup.setLayoutData(groupGridData);

            Button diffCheck = new Button(importSettingsGroup, SWT.CHECK);
            GridData diffCheckGridData = new GridData(GridData.FILL_HORIZONTAL);
            diffCheckGridData.horizontalSpan = 2;
            diffCheck.setLayoutData(diffCheckGridData);
            diffCheck.setText("Calculate diff between subsequent file versions");
            diffCheck.addSelectionListener(new SelectionAdapter() {

                public void widgetSelected(SelectionEvent e) {
                    CVSImporterWizardPage.this.diffSelectionChanged();
                }
            });

            Button importSrcCheck = new Button(importSettingsGroup, SWT.CHECK);
            GridData importSrcGridData = new GridData(GridData.FILL_HORIZONTAL);
            importSrcGridData.horizontalSpan = 2;
            importSrcCheck.setLayoutData(importSrcGridData);
            importSrcCheck.setText("Import file contents");
            importSrcCheck.addSelectionListener(new SelectionAdapter() {

                public void widgetSelected(SelectionEvent e) {
                    CVSImporterWizardPage.this.importSelectionChanged();
                }
            });

            this.fSrcTextLabel = new Label(importSettingsGroup, SWT.NONE);
            GridData srcTextGridData = new GridData(GridData.FILL_HORIZONTAL);
            this.fSrcTextLabel.setLayoutData(srcTextGridData);
            this.fSrcTextLabel
                    .setText("Filter the extension you want to import, for example \".java\".\nSeparate very extension with a \";\". Use * to import all types of files");
            this.fSrcTextLabel.setVisible(false);

            this.fSrcText = new Text(importSettingsGroup, SWT.BORDER);
            GridData srcGridData = new GridData(GridData.FILL_HORIZONTAL);
            this.fSrcText.setLayoutData(srcGridData);
            this.fSrcText.setEnabled(false);
            this.fSrcText.setText("*");
            this.fSrcText.setVisible(false);
            setControl(mainComposite);
        }

        /**
         * Modifies the Page according the new selection
         * 
         * @param newSelection
         *            The selection driving the change
         */
        protected void diffSelectionChanged() {
            this.fDiff = !this.fDiff;
        }

        /**
         * Modifies the Page according the new selection
         * 
         * @param newSelection
         *            The selection driving the change
         */
        protected void importSelectionChanged() {
            this.fImportSrc = !this.fImportSrc;
            this.fDiff = !this.fDiff;
            this.fSrcText.setEnabled(!this.fSrcText.isEnabled());
            this.fSrcText.setVisible(this.fSrcText.isEnabled());
            this.fSrcTextLabel.setVisible(this.fSrcText.isEnabled());
        }
    }
}
