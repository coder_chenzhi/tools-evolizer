/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.ui.wizards;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.evolizer.versioncontrol.cvs.importer.transactions.TransactionReconstructor;
import org.evolizer.versioncontrol.ui.EvolizerVersioningUIPlugin;

/**
 * Wizard that guides the user through the process of reconstructing transactions.
 * 
 * @see TransactionReconstructor
 * 
 * @author wuersch
 */
public class TransactionCalculatorWizard extends Wizard {

    private TransactionCalculatorContentPage fPage;
    private long fTMax;
    private long fMaxDist;
    private boolean fCanceled;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performFinish() {
        fTMax = fPage.getTMax();
        fMaxDist = fPage.getMaxDist();
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPages() {
        fPage = new TransactionCalculatorContentPage("Transaction Calculator");
        fPage.setTitle("Calculate Transaction for Versioning Model");
        fPage.setDescription("Calculate transaction from the saved revision information.");
        fPage
        .setImageDescriptor(ImageDescriptor.createFromFile(
                EvolizerVersioningUIPlugin.class,
                "/icons/versioncontrol_background.png"));
        addPage(fPage);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performCancel() {
        fCanceled = true;
        return super.performCancel();
    }

    /**
     * Returns the t max.
     * 
     * @return the t max
     */
    public long getTMax() {
        return fTMax;
    }

    /**
     * Returns the max distance.
     * 
     * @return the max distance
     */
    public long getMaxDist() {
        return fMaxDist;
    }

    /**
     * Checks if is canceled.
     * 
     * @return <code>true</code> if is canceled, <code>false</code> otherwise
     */
    public boolean isCanceled() {
        return fCanceled;
    }

    private class TransactionCalculatorContentPage extends WizardPage {

        private Label fTMaxLabel;
        private Text fTMaxText;

        private Label fMaxDistLabel;
        private Text fMaxDistText;

        protected TransactionCalculatorContentPage(String pageName) {
            super(pageName);
        }

        public void createControl(Composite parent) {
            Composite mainComposite = new Composite(parent, SWT.NONE);
            GridLayout mainGridLayout = new GridLayout();
            mainComposite.setLayout(mainGridLayout);
            mainGridLayout.numColumns = 2;

            fTMaxLabel = new Label(mainComposite, SWT.NONE);
            fTMaxLabel.setText("Transaction Length (s)");
            fTMaxLabel.setToolTipText("Maximum length of time in seconds that a transaction can last");
            fTMaxText = new Text(mainComposite, SWT.BORDER);
            fTMaxText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            fTMaxText.setText("600");

            fMaxDistLabel = new Label(mainComposite, SWT.NONE);
            fMaxDistLabel.setText("Time between two MR (s)");
            fMaxDistLabel.setToolTipText("Maximum distance in seconds between two subsequent modification reports");
            fMaxDistText = new Text(mainComposite, SWT.BORDER);
            fMaxDistText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            fMaxDistText.setText("10");

            setControl(mainComposite);
        }

        public long getTMax() {
            return Long.parseLong(fTMaxText.getText());
        }

        public long getMaxDist() {
            return Long.parseLong(fMaxDistText.getText());
        }
    }
}
