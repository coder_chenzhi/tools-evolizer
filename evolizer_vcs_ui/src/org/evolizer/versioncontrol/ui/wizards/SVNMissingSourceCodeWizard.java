/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.ui.wizards;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.ui.EvolizerVersioningUIPlugin;

/**
 * Wizard to guide the user through the process of fetching the missing file content of an existing SVN history.
 * 
 * @author ghezzi
 */
public class SVNMissingSourceCodeWizard extends Wizard {

    /*
     * The actual page.
     */
    private MissingFileContentPage page;
    /*
     * What type of file fetch was selected.
     * 0 for fetching the whole history file content.
     * 1 for fetching the file content for all the releases.
     * 2 for fetching the file content for a given revision range.
     * 3 for fetching the file content for a given set of releases.
     */
    private int fetchType;
    /*
     * The revision to start the import from.
     */
    private long start = 0;
    /*
     * The revision to stop the import at.
     */
    private long end = -1;
    /*
     * Whether or not the wizard was canceled.
     */
    private boolean canceled;
    /*
     * Whether or not the import is from scratch or an update of an existing one.
     */
    private boolean reImport;
    /*
     * The filetypes to import (their extensions in a semi-colon separated list regular expression).
     */
    private String fileExtensionRegEx;
    /*
     * The project to import the historical file content from.
     */
    private IProject project;
    /*
     * The names of the releases to fetch the file content of. 
     */
    private ArrayList<String> selectedReleases = new ArrayList<String>();

    /**
     * Constructor.
     * 
     * @param project
     *            The project to import the historical file content from.
     */
    public SVNMissingSourceCodeWizard(IProject project) {
        this.project = project;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performFinish() {
        this.fileExtensionRegEx = this.page.getFileExtensionRegEx();
        this.fetchType = this.page.fetchTypeCombo.getSelectionIndex();

        if (this.fetchType == 2) {
            String[] range = this.page.revisionRangeText.getText().split(":");
            this.start = Integer.parseInt(range[0]);
            this.end = Integer.parseInt(range[1]);
        } else if (this.fetchType == 3) {
            for (String n : this.page.selectedReleasesScrollList.getItems()) {
                selectedReleases.add(n.trim());
            }
        }
        return true;
    }

    /**
     * Returns the starting revision selected, the one the missing file content fetching will start from.
     * 
     * @return The selected starting revision.
     */
    public long getStart() {
        return this.start;
    }

    /**
     * Returns the end revision selected, the one the missing file content fetching will stop at.
     * 
     * @return The selected end revision.
     */
    public long getEnd() {
        return this.end;
    }

    /**
     * Returns the file fetching type selected. 0 for fetching the whole history file content. 1 for fetching the file
     * content for all the releases. 2 for fetching the file content for a given revision range. 3 for fetching the file
     * content for a given set of releases.
     * 
     * @return The selected file fetching strategy.
     */
    public int getFetchType() {
        return this.fetchType;
    }

    /**
     * Returns the names of the releases to fetch the content of.
     * 
     * @return An {@link ArrayList} of the names of those releases.
     */
    public ArrayList<String> getSelectedReleases() {
        return this.selectedReleases;
    }

    /**
     * Returns the file extension regular expression: the filetypes to import, their extensions in a semi-colon
     * separated list regular expression.
     * 
     * @return the file extension regular expression.
     */
    public String getFileExtensionRegEx() {
        return this.fileExtensionRegEx;
    }

    /**
     * Checks if the re import option is enabled.
     * 
     * @return <code>true</code> if is re import enabled, <code>false</code> otherwise
     */
    public boolean isReImportEnabled() {
        return this.reImport;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addPages() {
        this.page = new MissingFileContentPage("File Content Importer", this.project);
        this.page.setTitle("Import File Content for Versioning Model");
        this.page.setDescription("Import file content for specific releases, revision range or the complete history.");
        this.page
        .setImageDescriptor(ImageDescriptor.createFromFile(
                EvolizerVersioningUIPlugin.class,
                "/icons/versioncontrol_background.png"));
        addPage(this.page);
    }

    /**
     * Checks if the wizard was canceled.
     * 
     * @return <code>true</code> if is canceled, <code>false</code> otherwise
     */
    public boolean isCanceled() {
        return this.canceled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performCancel() {
        this.canceled = true;
        return super.performCancel();
    }

    /**
     * The actual page used by the wizard.
     * 
     * @author ghezzi
     * 
     */
    private class MissingFileContentPage extends WizardPage {

        /*
         * The different file content fetching strategies
         */
        private String[] fetchTypes = {"All history", "All releases", "Revision range", "Selected releases"};
        /*
         * All the contents: error messages, labels, etc. used in the page.
         */
        private String fetchTypeSelectionMsg = "Select what you want to import";
        private String revisionRangeMsg = "Enter the revision range you want to import <start revision>:<end revision>";
        private String revisionRangeErrorMsg = "Enter a valid revision range: <start revision>:<end revision>";
        private String fileExtensionMsg =
                "Filter the extension you want to import, for example \".java\".\nSeparate very extension with a \";\". Use * to import all types of files";
        private String fileExtensionErrorMsg =
                "Separate every extension with a \";\" or use * to import all types of files";
        private String noEndZeroValuesError = "Can't have zero as an end revision value";
        private String endSmallerThenStartError = "End revision can't be smaller or equal to the starting one";
        private String releaseSelectionMsg = "Select the releases you want to fetch";
        private String releaseSelectionError = "Select at least one release";
        private String releaseNamesFetchError =
                "Could not fetch the names of the project releases, check that its hisotry was already imported";
        private String addReleaseButtonText = "Add Release(s) >>";
        private String removeReleaseButtonText = "<< Remove Release(s)";
        /*
         * Regular expressions used.
         */
        private String fileExtensionsRegEx = "(\\*)|(\\.\\w+)(;\\.\\w+)*";
        private String fileExtensionsWildCard = "*";
        private String revisionRangeRegEx = "(\\d)+:(\\d)+";
        /*
         * The error messages related to all the errors that prevent the wizard page from being complete.
         */
        private Set<String> errorMessages = new HashSet<String>();
        /*
         * The SWT related stuff:labels, buttons, combos, text, etc.
         */
        private Label regExLabel;
        private Label fetchTypeLabel;
        private Label revisionRangeLabel;
        private Label selectRevisionLabel;
        private Text revisionRangeText;
        private Combo fetchTypeCombo;
        private Text fileExtensionText;
        private List releasesScrollList;
        private List selectedReleasesScrollList;
        private Composite mainComposite;
        private Button selectReleaseButton;
        private Button deselectReleaseButton;
        /*
         * Flag indicating if the selected file extensions are valid
         */
        private boolean isFileExtensionValid;
        /*
         * Flag indicating if the revision range selected is valid.
         */
        private boolean isRevisionRangeValid;
        /*
         * Flag indicating if the releases selected are valid.
         */
        private boolean isReleaseSelectionValid;
        /*
         * Flag indicating if the release names were correctly fetch from the DB.
         */
        private boolean releaseNamesFetched = false;
        /*
         * The project to import the historical file content from.
         */
        private IProject project;

        /**
         * Constructor.
         * 
         * @param pageName
         *            The name of the page.
         * @param project
         *            The project to associate to this page.
         */
        protected MissingFileContentPage(String pageName, IProject project) {
            super(pageName);
            this.project = project;
        }

        /**
         * Returns the selected file types regular expression.
         * 
         * @return The regular expression.
         */
        public String getFileExtensionRegEx() {
            return this.fileExtensionText.getText();
        }

        /**
         * {@inheritDoc}
         */
        public void createControl(Composite parent) {

            // The main component
            this.mainComposite = new Composite(parent, SWT.NONE);
            GridLayout mainGridLayout = new GridLayout();
            this.mainComposite.setLayout(mainGridLayout);
            mainGridLayout.numColumns = 2;

            // The fetch type selection components.
            this.fetchTypeLabel = new Label(this.mainComposite, SWT.NONE);
            this.fetchTypeLabel.setText(this.fetchTypeSelectionMsg);

            this.fetchTypeCombo = new Combo(this.mainComposite, SWT.READ_ONLY);
            this.fetchTypeCombo.setItems(this.fetchTypes);
            this.fetchTypeCombo.select(0);
            this.fetchTypeCombo.addSelectionListener(new SelectionAdapter() {

                public void widgetSelected(SelectionEvent e) {
                    MissingFileContentPage.this.updateSelectionChanged();
                }
            });

            // The revision range selection components.
            this.revisionRangeLabel = new Label(this.mainComposite, SWT.NONE);
            this.revisionRangeLabel.setText(this.revisionRangeMsg);
            this.revisionRangeLabel.setVisible(false);

            this.revisionRangeText = new Text(this.mainComposite, SWT.BORDER);
            this.revisionRangeText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            this.revisionRangeText.setVisible(false);
            this.revisionRangeText.addKeyListener(new KeyAdapter() {

                public void keyReleased(KeyEvent e) {
                    MissingFileContentPage.this.revisionRangeChanged();
                }
            });

            // The file types selection components.
            this.regExLabel = new Label(this.mainComposite, SWT.NONE);
            this.regExLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            this.regExLabel.setText(this.fileExtensionMsg);

            this.fileExtensionText = new Text(this.mainComposite, SWT.BORDER);
            this.fileExtensionText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            this.fileExtensionText.setText(this.fileExtensionsWildCard);
            this.fileExtensionText.addKeyListener(new KeyListener() {

                public void keyReleased(KeyEvent e) {
                    MissingFileContentPage.this.fileExtensionsChanged(((Text) e.getSource()).getText());
                }

                public void keyPressed(KeyEvent e) {}
            });
            this.isFileExtensionValid = true;

            // The revision selection components.
            this.selectRevisionLabel = new Label(this.mainComposite, SWT.NONE);
            this.selectRevisionLabel.setLayoutData(new GridData(SWT.NONE, SWT.NONE, false, false, 2, 1));
            this.selectRevisionLabel.setText(this.releaseSelectionMsg);
            this.selectRevisionLabel.setVisible(false);

            this.releasesScrollList =
                    new List(this.mainComposite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
            this.releasesScrollList.setVisible(false);
            GridData data = new GridData();
            data.heightHint = 200;
            data.widthHint = 200;
            this.releasesScrollList.setLayoutData(data);

            this.selectedReleasesScrollList =
                    new List(this.mainComposite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
            this.selectedReleasesScrollList.setVisible(false);
            data = new GridData();
            data.heightHint = 200;
            data.widthHint = 200;
            this.selectedReleasesScrollList.setLayoutData(data);

            this.selectReleaseButton = new Button(this.mainComposite, SWT.NONE);
            this.selectReleaseButton.setText(this.addReleaseButtonText);
            this.selectReleaseButton.setVisible(false);
            this.selectReleaseButton.addSelectionListener(new SelectionListener() {

                public void widgetSelected(SelectionEvent e) {
                    MissingFileContentPage.this.selectReleases();
                }

                public void widgetDefaultSelected(SelectionEvent e) {}
            });

            this.deselectReleaseButton = new Button(this.mainComposite, SWT.NONE);
            this.deselectReleaseButton.setText(this.removeReleaseButtonText);
            this.deselectReleaseButton.setVisible(false);
            this.deselectReleaseButton.addSelectionListener(new SelectionListener() {

                public void widgetSelected(SelectionEvent e) {
                    MissingFileContentPage.this.deSelectReleases();
                }

                public void widgetDefaultSelected(SelectionEvent e) {}
            });

            setControl(this.mainComposite);
        }

        /**
         * Adds the releases selected in the releasesScrollList to the selectedReleasesScrollList and removes them from
         * releasesScrollList.
         */
        private void selectReleases() {
            for (String toRemove : this.releasesScrollList.getSelection()) {
                this.releasesScrollList.remove(toRemove);
                this.selectedReleasesScrollList.add(toRemove);
            }
            this.isReleaseSelectionValid = this.selectedReleasesScrollList.getItemCount() > 0;
            if (this.isReleaseSelectionValid) {
                this.errorMessages.remove(this.releaseSelectionError);
            } else {
                this.errorMessages.add(this.releaseSelectionError);
            }
            this.selectedReleasesScrollList.redraw();
            this.releasesScrollList.redraw();
            this.mainComposite.update();
            this.tryPageComplete();
        }

        /**
         * Adds the releases selected in the selectedReleasesScrollList to the releasesScrollList and removes them from
         * selectedReleasesScrollList.
         */
        private void deSelectReleases() {
            for (String toRemove : this.selectedReleasesScrollList.getSelection()) {
                this.selectedReleasesScrollList.remove(toRemove);
                this.releasesScrollList.add(toRemove);
            }
            this.isReleaseSelectionValid = this.selectedReleasesScrollList.getItemCount() > 0;
            if (this.isReleaseSelectionValid) {
                this.errorMessages.remove(this.releaseSelectionError);
            } else {
                this.errorMessages.add(this.releaseSelectionError);
            }
            this.tryPageComplete();
        }

        /**
         * Checks that the revision range in the revisionRange is valid.
         */
        private void revisionRangeChanged() {
            this.isRevisionRangeValid = Pattern.matches(this.revisionRangeRegEx, this.revisionRangeText.getText());
            if (this.isRevisionRangeValid) {
                this.errorMessages.remove(this.revisionRangeErrorMsg);
                long start =
                        Long.parseLong(this.revisionRangeText.getText().substring(
                                0,
                                this.revisionRangeText.getText().indexOf(":")));
                long end =
                        Long.parseLong(this.revisionRangeText.getText().substring(
                                this.revisionRangeText.getText().indexOf(":") + 1));
                if (end == 0) {
                    this.errorMessages.add(this.noEndZeroValuesError);
                    this.errorMessages.remove(this.endSmallerThenStartError);
                    this.isRevisionRangeValid = false;
                } else if (start >= end) {
                    this.errorMessages.add(this.endSmallerThenStartError);
                    this.errorMessages.remove(this.noEndZeroValuesError);
                    this.isRevisionRangeValid = false;
                } else {
                    this.errorMessages.remove(this.noEndZeroValuesError);
                    this.errorMessages.remove(this.endSmallerThenStartError);
                    this.isRevisionRangeValid = true;
                }
            } else {
                this.errorMessages.add(this.revisionRangeErrorMsg);
            }
            this.tryPageComplete();
        }

        /**
         * Checks that the given string is a valid instance of the file type regular expression (a semi colon separated
         * list of file extensions).
         * 
         * @param fileExtensions
         *            The string to check.
         */
        private void fileExtensionsChanged(String fileExtensions) {
            Pattern p = Pattern.compile(this.fileExtensionsRegEx);
            Matcher m = p.matcher(fileExtensions);
            if (!m.matches()) {
                this.errorMessages.add(this.fileExtensionErrorMsg);
                this.isFileExtensionValid = false;
            } else {
                this.errorMessages.remove(this.fileExtensionErrorMsg);
                this.isFileExtensionValid = true;
            }
            this.tryPageComplete();
        }

        /**
         * Checks if the page can be set as complete.
         */
        private void tryPageComplete() {
            if (!this.errorMessages.isEmpty()) {
                this.setErrorMessage(this.errorMessages.iterator().next());
            } else {
                this.setErrorMessage(null);
            }
            this
                    .setPageComplete(this.isFileExtensionValid && this.isRevisionRangeValid
                            && this.isReleaseSelectionValid);
        }

        /**
         * Updates the page layout: the labels, the input fields, the buttons, etc. depending on the fetching strategy
         * picked through the fetchTypeCombo.
         */
        private void updateSelectionChanged() {
            int selection = this.fetchTypeCombo.getSelectionIndex();

            switch (selection) {
                // Fetching the whole history file content
                case 0:
                    this.revisionRangeLabel.setVisible(false);
                    this.revisionRangeText.setVisible(false);
                    this.revisionRangeText.setText("");
                    this.isRevisionRangeValid = true;
                    this.isReleaseSelectionValid = true;
                    this.releasesScrollList.setVisible(false);
                    this.selectedReleasesScrollList.setVisible(false);
                    this.selectRevisionLabel.setVisible(false);
                    this.selectReleaseButton.setVisible(false);
                    this.deselectReleaseButton.setVisible(false);
                    this.errorMessages.remove(this.revisionRangeErrorMsg);
                    this.errorMessages.remove(this.noEndZeroValuesError);
                    this.errorMessages.remove(this.endSmallerThenStartError);
                    this.errorMessages.remove(this.releaseSelectionError);
                    this.errorMessages.remove(this.releaseNamesFetchError);
                    break;
                // Fetching the file content for all the releases.
                case 1:
                    this.revisionRangeLabel.setVisible(false);
                    this.revisionRangeText.setVisible(false);
                    this.revisionRangeText.setText("");
                    this.isRevisionRangeValid = true;
                    this.isReleaseSelectionValid = true;
                    this.releasesScrollList.setVisible(false);
                    this.selectedReleasesScrollList.setVisible(false);
                    this.selectRevisionLabel.setVisible(false);
                    this.selectReleaseButton.setVisible(false);
                    this.deselectReleaseButton.setVisible(false);
                    this.errorMessages.remove(this.revisionRangeErrorMsg);
                    this.errorMessages.remove(this.noEndZeroValuesError);
                    this.errorMessages.remove(this.endSmallerThenStartError);
                    this.errorMessages.remove(this.releaseSelectionError);
                    this.errorMessages.remove(this.releaseNamesFetchError);
                    break;
                // Fetching the file content for a given revision range.
                case 2:
                    this.revisionRangeLabel.setVisible(true);
                    this.revisionRangeText.setVisible(true);
                    this.releasesScrollList.setVisible(false);
                    this.selectedReleasesScrollList.setVisible(false);
                    this.selectRevisionLabel.setVisible(false);
                    this.selectReleaseButton.setVisible(false);
                    this.deselectReleaseButton.setVisible(false);
                    this.isReleaseSelectionValid = true;
                    this.revisionRangeChanged();
                    this.errorMessages.remove(this.releaseSelectionError);
                    this.errorMessages.remove(this.releaseNamesFetchError);
                    break;
                // Fetching the file content for a given set of releases.
                case 3:
                    this.selectRevisionLabel.setVisible(true);
                    if (!this.releaseNamesFetched) {
                        this.releaseNamesFetched = true;
                        try {
                            EvolizerSessionHandler handler = EvolizerSessionHandler.getHandler();
                            handler.updateSchema(this.project);
                            IEvolizerSession persistenceProvider = handler.getCurrentSession(this.project);
                            for (String r : persistenceProvider.query("select name from Release", String.class)) {
                                this.releasesScrollList.add(r + " ");
                            }

                        } catch (EvolizerException e) {
                            this.releasesScrollList.removeAll();
                        }
                    }
                    if (this.releasesScrollList.getItemCount() <= 0) {
                        this.errorMessages.add(this.releaseNamesFetchError);
                        this.isReleaseSelectionValid = false;
                    } else {
                        this.releasesScrollList.setVisible(true);
                        this.selectedReleasesScrollList.setVisible(true);
                        this.selectReleaseButton.setVisible(true);
                        this.deselectReleaseButton.setVisible(true);
                        this.isReleaseSelectionValid = this.selectedReleasesScrollList.getItemCount() > 0;
                        if (!this.isReleaseSelectionValid) {
                            this.errorMessages.add(this.releaseSelectionError);
                        }
                    }
                    this.revisionRangeLabel.setVisible(false);
                    this.revisionRangeText.setVisible(false);
                    this.revisionRangeText.setText("");
                    this.isRevisionRangeValid = true;
                    this.errorMessages.remove(this.revisionRangeErrorMsg);
                    this.errorMessages.remove(this.noEndZeroValuesError);
                    this.errorMessages.remove(this.endSmallerThenStartError);
                    break;
                default:
                    break;
            }
            this.tryPageComplete();
        }
    }
}
