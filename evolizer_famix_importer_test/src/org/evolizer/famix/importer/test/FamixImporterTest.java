/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.famix.importer.test;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.core.IJavaElement;
import org.evolizer.famix.importer.ProjectParser;
import org.junit.BeforeClass;

/**
 * Main FAMIX Importer test class providing methods to test the parsing functionality.
 * 
 * @author pinzger 
 */
public class FamixImporterTest extends AbstractEvolizerFamixImporterTest{
	
	/**
	 * Test setup method called before starting testing. Initializes the workspace with the temporary
	 * Java project, configures the Java parsers, and executes the parsing.
	 * 
	 * @throws Exception
	 * @throws AssertionError
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception, AssertionError {
		AbstractEvolizerFamixImporterTest.setUpBeforeClass();

		List<IJavaElement> selection = new LinkedList<IJavaElement>();
		selection.add(project);
		ProjectParser parser = new ProjectParser(selection);
		parser.parse(null);

		aModel = parser.getModel();
		
		// output all entities and source anchors
//		for (AbstractFamixEntity entity : aModel.getAllElements()) {
//			System.out.print(entity.getUniqueName());
//			if (entity.getSourceAnchor() != null) {
//				System.out.print(": " + entity.getSourceAnchor().toString());
//			}
//			System.out.println();
//		}
	}
}
