package org.evolizer.core.hibernate;

import org.evolizer.core.hibernate.model.api.IEvolizerModelProvider;

public class ModelProviderStub implements IEvolizerModelProvider{

	public Class<?>[] getAnnotatedClasses() {
		Class<?>[] annotatedClasses = { DummyBusinessEntity.class };
		return annotatedClasses;
	}

}
