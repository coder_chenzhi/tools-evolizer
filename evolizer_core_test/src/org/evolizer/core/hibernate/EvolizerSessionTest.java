package org.evolizer.core.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.evolizer.core.EvolizerCorePlugin;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EvolizerSessionTest {
	private static final String DB_CONFIG_FILE = "./config/db.properties";

	protected static String fDBUrl;

	protected static String fDBDialect;

	protected static String fDBDriverName;

	protected static String fDBUser;

	protected static String fDBPasswd;
	
	private EvolizerSessionHandler fSessionHandler;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream propertiesInputStream = EvolizerCorePlugin.openFile(DB_CONFIG_FILE);

		if (propertiesInputStream != null) {
			Properties props = new Properties();
			props.load(propertiesInputStream);
			propertiesInputStream.close();

			fDBUrl = props.getProperty("dbUrl");
			fDBDialect = props.getProperty("dbDialect");
			fDBDriverName = props.getProperty("dbDriverName");
			fDBUser = props.getProperty("dbUser");
			fDBPasswd = props.getProperty("dbPasswd");
		}
		
		propertiesInputStream.close();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		fSessionHandler = EvolizerSessionHandler.getHandler();
		fSessionHandler.initSessionFactory(fDBUrl, fDBUser, fDBPasswd);
	}

	@After
	public void tearDown() throws Exception {
		fSessionHandler.cleanupHibernateSessions();
		fSessionHandler = null;
	}

	@Test
	public void testEvolizerSession() {
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			assertNotNull(s);
			assertTrue(s.isOpen());
			s.close();
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}

	public void testClose() {
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			s.close();
			assertFalse(s.isOpen());
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testSaveObject() {
		try {
			fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			
		
			DummyBusinessEntity entity = new DummyBusinessEntity();
			entity.setAString("This is a test");
		
			s.startTransaction();
			s.saveObject(entity);
			s.endTransaction();
		
			s.close();
		
			fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testQuery() {
		try {
			fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
		
			DummyBusinessEntity entity = new DummyBusinessEntity();
			entity.setAString("This is also a test");
		
			s.startTransaction();
			s.saveObject(entity);
			s.endTransaction();
		

			List<DummyBusinessEntity> lgList = (List<DummyBusinessEntity>) s.query("from " + DummyBusinessEntity.class.getName(), DummyBusinessEntity.class);
		
			assertNotNull(lgList);
			DummyBusinessEntity savedEntity = lgList.get(0);
			assertNotNull(savedEntity);
			assertEquals(new Long(1), savedEntity.getId());
			assertEquals("This is also a test", savedEntity.getAString());
		
			fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
			
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}
}
