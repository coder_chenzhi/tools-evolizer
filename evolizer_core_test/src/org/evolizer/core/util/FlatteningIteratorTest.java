package org.evolizer.core.util;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.evolizer.core.util.collections.FlatteningIterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FlatteningIteratorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = NoSuchElementException.class)
	public void testIterate() {
		/*
		 List topLevel
		 |
		 |-- String "a"
		 |
		 |-- List secondLevel1
		 |	 |
		 |	 |- Integer "10"
		 |
		 |-- List secondLevel2
		 |	 |
		 |	 |- String "hi"
		 |	 |
		 |	 |- boolean "you"
		 */
		
		
		List<Object> topLevel = new ArrayList<Object>();
		topLevel.add(new String("a"));
		
		List<Integer> secondLevel1 = new ArrayList<Integer>();
		secondLevel1.add(new Integer(10));
		topLevel.add(secondLevel1);
		
		List<String> secondLevel2 = new LinkedList<String>();
		secondLevel2.add("hi");
		secondLevel2.add("you");
		topLevel.add(secondLevel2);
		
		FlatteningIterator iter = new FlatteningIterator(topLevel, "b");
		assertEquals("a", iter.next());
		assertEquals(new Integer(10), iter.next());
		assertEquals("hi", iter.next());
		assertEquals("you", iter.next());
		assertEquals("b", iter.next());
		assertFalse(iter.hasNext());
		
		iter.next(); // should throw a NoSuchElementException
	}
}
