package org.evolizer.core.util;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.evolizer.core.util.collections.ArrayIterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ArrayIteratorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIterator() {
		String[] strings = {"first", "second", "third"};
		ArrayIterator<String> iter = new ArrayIterator<String>(strings);
		assertTrue(iter.hasNext());
		assertEquals(0, iter.nextIndex());
		assertFalse(iter.hasPrevious());
		assertEquals("first", iter.next());
		assertEquals(1, iter.nextIndex());
		assertEquals("second", iter.next());
		assertTrue(iter.hasPrevious());
		assertEquals(0, iter.previousIndex());
		assertEquals(2, iter.nextIndex());
		assertEquals("third", iter.next());
		assertFalse(iter.hasNext());
		assertEquals(1, iter.previousIndex());
		
		assertEquals("second", iter.previous());
		assertEquals("first", iter.previous());
		assertFalse(iter.hasPrevious());
		assertEquals(-1, iter.previousIndex());
	}
}
