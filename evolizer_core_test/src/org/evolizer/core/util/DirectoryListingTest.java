package org.evolizer.core.util;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.evolizer.core.EvolizerCorePlugin;
import org.evolizer.core.util.collections.IPredicate;
import org.evolizer.core.util.resourcehandling.DirectoryListing;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DirectoryListingTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDirectoryListing() {
		try {
			String path = EvolizerCorePlugin.getAbsoluteFSPath("test_data/folder");
			DirectoryListing content = new DirectoryListing(new File(path), new IPredicate<File>() {
	
				public boolean evaluate(File f) {
					return f.getName().endsWith(".txt");
				}
				
			});
			
			Iterator<File> fileIterator = content.iterator();
			assertEquals(path + "afile.txt", fileIterator.next().getAbsolutePath());
			assertEquals(path + "subfolder/cfile.txt", fileIterator.next().getAbsolutePath());
			assertFalse(fileIterator.hasNext());
			
			content = new DirectoryListing(new File(path), new IPredicate<File>() {
				
				public boolean evaluate(File f) {
					return f.getName().endsWith(".egf");
				}
				
			});
			
			fileIterator = content.iterator();
			assertEquals(path + "subfolder/bfile.egf", fileIterator.next().getAbsolutePath());
			assertFalse(fileIterator.hasNext());
		} catch (IOException e) {
			fail();
			e.printStackTrace();
		}
	}
}
