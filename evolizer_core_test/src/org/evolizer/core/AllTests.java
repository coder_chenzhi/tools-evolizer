package org.evolizer.core;

import org.evolizer.core.hibernate.EvolizerSessionTest;
import org.evolizer.core.util.ArrayIteratorTest;
import org.evolizer.core.util.DirectoryListingTest;
import org.evolizer.core.util.FilteringIteratorTest;
import org.evolizer.core.util.FlatteningIteratorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	EvolizerSessionTest.class,
	ArrayIteratorTest.class,
	DirectoryListingTest.class,
	FilteringIteratorTest.class,
	FlatteningIteratorTest.class
})
public class AllTests {

}
