/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.misc.Content;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;

/**
 * This class provides convenience methods to work with files under version control. Its main purpose is to fetch
 * contents from the CVS server and to store them in the RHDB.
 * 
 * @author wuersch
 */
public final class FileManager {

    private FileManager() {
        super();
    }

    private static Logger sLogger = EvolizerCVSPlugin.getLogManager().getLogger(FileManager.class.getCanonicalName());

    public static final String FILE_EXTENSION_WILDCARD = "*";

    /**
     * Fetch source for all files.
     * 
     * @param persistenceProvider
     *            the persistence provider
     * @param repositoryProvider
     *            the repository provider
     * @param monitor
     *            the monitor
     * @throws EvolizerException
     *             the evolizer exception
     */
    public static void fetchSourceForAllFiles(
            IEvolizerSession persistenceProvider,
            CVSRepositoryProvider repositoryProvider,
            IProgressMonitor monitor) throws EvolizerException {

        fetchSourceForAllFiles(persistenceProvider, repositoryProvider, "*", monitor);
    }

    private static String inputStreamAsString(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line = null;

        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }

        br.close();
        return sb.toString();
    }

    /**
     * Fetch source for files.
     * 
     * @param persistenceProvider
     *            the persistence provider
     * @param repositoryProvider
     *            the repository provider
     * @param fileExtensionRegEx
     *            the file extension regex
     * @param monitor
     *            the monitor
     * @param reImport
     *            the reimport
     * @throws EvolizerException
     *             the evolizer exception
     */
    public static void fetchSourceForFiles(
            IEvolizerSession persistenceProvider,
            CVSRepositoryProvider repositoryProvider,
            String fileExtensionRegEx,
            IProgressMonitor monitor,
            boolean reImport) throws EvolizerException {

        String whereClause = assembleWhereClause(fileExtensionRegEx);
        if (!reImport) {
            if (whereClause.equals("")) {
                whereClause =
                        whereClause
                                + "as r where (r.sourceCodeWrapper.source is null or r.sourceCodeWrapper.source='') and r.state!='dead'";
            } else {
                whereClause =
                        whereClause
                                + " and (r.sourceCodeWrapper.source is null or r.sourceCodeWrapper.source='') and r.state!='dead'";
            }
        }
        // Optimizations: Attach source code to each revision in the list will
        // lead to memory problems.
        // We cannot remove elements from the list while iterating over it...
        String query = "from Revision " + whereClause;
        List<Revision> revisions = persistenceProvider.query(query, Revision.class);

        int size = revisions.size();

        // ...therefore, we copy all elements into an array and clear the list.
        Revision[] revisionsArray = revisions.toArray(new Revision[]{});

        revisions.clear();
        revisions = null;

        persistenceProvider.startTransaction();

        repositoryProvider.cleanup();
        repositoryProvider.open();
        monitor.beginTask("Fetching source files content", revisionsArray.length);
        for (int i = 0; i < revisionsArray.length; i++) {
            Revision revision = revisionsArray[i];

            try {
                monitor.setTaskName("Fetching source files content (" + (i + 1) + " out of " + revisionsArray.length + ")");
                monitor.subTask("Fetching revision " + revision.getNumber() + " of file " + revision.getFile().getPath());
                InputStream stream = repositoryProvider.checkout(revision, new NullProgressMonitor());

                String source = inputStreamAsString(stream);

                stream.close();

                Content sw = persistenceProvider.load(Content.class, revision.getSourceCodeWrapper().getId());
                sw.setSource(source);

                persistenceProvider.update(sw);

                // now we can release the revision to the garbage collector,
                // since it was made persistent.
                revisionsArray[i] = null;

                if ((i % 24 == 0) && (i != 0)) {
                    persistenceProvider.flush();
                    persistenceProvider.clear();
                }

                sLogger.debug("Saved src for " + revision.getFile().getPath() + "- " + revision.getNumber() + " (" + i
                        + "/" + size + ").");

            } catch (IOException e) {
                sLogger.warn("IOException: Cannot save source of " + revision.getFile().getPath() + " - "
                        + revision.getNumber());
                throw new EvolizerException(e);
            } catch (EvolizerException e) {
                sLogger.warn("EvolizerException: Cannot save source of " + revision.getFile().getPath() + " - "
                        + revision.getNumber());
            }
            monitor.worked(1);
        }

        repositoryProvider.close();
        persistenceProvider.endTransaction();
        monitor.done();
        sLogger.info("Finished importing sources from CVS.");

    }

    /**
     * Fetch source for all files.
     * 
     * @param persistenceProvider
     *            the persistence provider
     * @param repositoryProvider
     *            the repository provider
     * @param fileExtensionRegEx
     *            the file extension regex
     * @param monitor
     *            the monitor
     * @throws EvolizerException
     *             the Evolizer exception
     */
    public static void fetchSourceForAllFiles(
            IEvolizerSession persistenceProvider,
            CVSRepositoryProvider repositoryProvider,
            String fileExtensionRegEx,
            IProgressMonitor monitor) throws EvolizerException {

        fetchSourceForFiles(persistenceProvider, repositoryProvider, fileExtensionRegEx, monitor, false);
    }

    private static String assembleWhereClause(String fileExtensionRegEx) {
        String whereClause = "";

        if (fileExtensionRegEx.equals(FILE_EXTENSION_WILDCARD)) {
            return whereClause;
        }

        whereClause = whereClause + " as r where ";

        String[] components = fileExtensionRegEx.split(";");

        for (int i = 0; i < components.length; i++) {
            whereClause = whereClause + "(";

            whereClause = whereClause + "r.file.path like '%" + components[i] + "'";

            whereClause = whereClause + ") ";

            if (i <= components.length - 2) {
                whereClause = whereClause + "or ";
            }
        }
        return whereClause;
    }
}
