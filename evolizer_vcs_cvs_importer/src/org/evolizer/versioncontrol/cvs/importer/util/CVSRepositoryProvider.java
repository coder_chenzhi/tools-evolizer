/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.util;

import java.io.File;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.team.core.TeamException;
import org.eclipse.team.core.variants.IResourceVariant;
import org.eclipse.team.internal.ccvs.core.CVSException;
import org.eclipse.team.internal.ccvs.core.CVSTag;
import org.eclipse.team.internal.ccvs.core.ICVSFolder;
import org.eclipse.team.internal.ccvs.core.ICVSRemoteFile;
import org.eclipse.team.internal.ccvs.core.ICVSRemoteFolder;
import org.eclipse.team.internal.ccvs.core.ICVSRepositoryLocation;
import org.eclipse.team.internal.ccvs.core.ICVSResource;
import org.eclipse.team.internal.ccvs.core.client.Command;
import org.eclipse.team.internal.ccvs.core.client.RLog;
import org.eclipse.team.internal.ccvs.core.client.Session;
import org.eclipse.team.internal.ccvs.core.client.listeners.ICommandOutputListener;
import org.eclipse.team.internal.ccvs.core.connection.CVSServerException;
import org.eclipse.team.internal.ccvs.core.resources.CVSWorkspaceRoot;
import org.eclipse.team.internal.ccvs.core.util.KnownRepositories;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;

/**
 * Repository Provider for IProjects under CVS control.
 * 
 * @author fluri
 */
@SuppressWarnings("restriction")
public class CVSRepositoryProvider {

    private static Logger sLogger =
            EvolizerCVSPlugin.getLogManager().getLogger(CVSRepositoryProvider.class.getCanonicalName());
    // system wide temp folder
    private static final String SYS_TMP_DIR = System.getProperty("java.io.tmpdir");

    // checkout temp folder of evolizer
    private static final String TMP_DIR = ".evolizer.tmp";

    // project under CVS control
    private IProject fProject;

    // CVS project folder
    private ICVSFolder fCVSProjectFolder;

    // CVS project repository location
    private ICVSRepositoryLocation fCVSRepositoryLocation;

    // CVS session
    private Session fCVSSession;

    private String fRemoteModulePath;

    /**
     * Initialize a CVS repository connection.
     * 
     * @param project
     *            under CVS control
     * 
     * @throws EvolizerException
     *             the evolizer exception
     */
    public CVSRepositoryProvider(IProject project) throws EvolizerException {
        try {
            fProject = project;

            // Create CVS connection
            fCVSProjectFolder = CVSWorkspaceRoot.getCVSFolderFor(fProject);
            String root = fCVSProjectFolder.getFolderSyncInfo().getRoot();
            fRemoteModulePath = fCVSProjectFolder.getRemoteLocation(fCVSProjectFolder);
            fCVSRepositoryLocation = KnownRepositories.getInstance().getRepository(root);

            fCVSSession = new Session(fCVSRepositoryLocation, fCVSProjectFolder);

            // Create temp folder structure
            File actualTmp = new File(new File(SYS_TMP_DIR), "evolizer");
            actualTmp.mkdir();

            IFolder evoTmp = fProject.getFolder(TMP_DIR);
            if (!evoTmp.exists()) {
                evoTmp.createLink(new Path(actualTmp.getAbsolutePath()), 0, null);
            }

            if (!evoTmp.exists()) {
                evoTmp.create(true /*force*/, true /*local*/, null);
            }
        } catch (CVSException e) {
            sLogger.error(e.getMessage(), e);
            throw new EvolizerException(e);
        } catch (CoreException e) {
            sLogger.error(e.getMessage(), e);
            throw new EvolizerException(e);
        }
    }

    /**
     * CVS log for project.
     * 
     * @param listener
     *            the listener
     * @param monitor
     *            the monitor
     * @throws EvolizerException
     *             the evolizer exception
     */
    public void cvsLogForProject(ICommandOutputListener listener, IProgressMonitor monitor) throws EvolizerException {
        try {
            fCVSSession.open(null);
            RLog rLog = new RLog();
            ICVSRemoteFolder f = fCVSRepositoryLocation.getRemoteFolder(fRemoteModulePath, CVSTag.DEFAULT);
            /*Command.LOG*/rLog.execute(
                    fCVSSession,
                    Command.NO_GLOBAL_OPTIONS,
                    Command.NO_LOCAL_OPTIONS,
                    new ICVSRemoteFolder[]{f},
                    listener,
                    monitor);

            fCVSSession.close();
        } catch (CVSException e) {
            sLogger.error(e.getMessage(), e);
            throw new EvolizerException(e);
        }
    }

    /**
     * Checkout.
     * 
     * @param revision
     *            the revision
     * @param monitor
     *            the monitor
     * @return the input stream
     * @throws EvolizerException
     *             the evolizer exception
     */
    public InputStream checkout(Revision revision, IProgressMonitor monitor) throws EvolizerException {
        try {

            String fullPath = revision.getFile().getPath();
            String repositoryBasePath = fCVSProjectFolder.getRepositoryRelativePath();
            String remotePath = repositoryBasePath + "/" + fullPath;

            ICVSRemoteFile rfile =
                    fCVSRepositoryLocation.getRemoteFile(remotePath, new CVSTag(revision.getNumber(), CVSTag.VERSION));
            IResourceVariant variant = (IResourceVariant) rfile;
            IStorage storage = variant.getStorage(monitor);
            InputStream inputStream = storage.getContents();

            return inputStream;
        } catch (CVSServerException e) {
            // Do not print stack trace
            throw new EvolizerException(e);
        } catch (CVSException e) {
            e.printStackTrace();
            throw new EvolizerException(e);
        } catch (TeamException e) {
            e.printStackTrace();
            throw new EvolizerException(e);
        } catch (CoreException e) {
            e.printStackTrace();
            throw new EvolizerException(e);
        }
    }

    /**
     * Unshare resource.
     * 
     * @param resource
     *            the resource
     * @throws EvolizerException
     *             the evolizer exception
     */
    public void unshareResource(IResource resource) throws EvolizerException {
        ICVSResource cvsResource = CVSWorkspaceRoot.getCVSResourceFor(resource);
        try {
            cvsResource.unmanage(new NullProgressMonitor());
        } catch (CVSException e) {
            throw new EvolizerException("Cannot unshare resource", e);
        }
    }

    /**
     * Opens the CVS session.
     * 
     * @throws EvolizerException
     *             the evolizer exception
     */
    public void open() throws EvolizerException {
        try {
            fCVSSession.open(new NullProgressMonitor());
        } catch (CVSException e) {
            throw new EvolizerException("Cannot connect to repository", e);
        }
    }

    /**
     * Close the CVS session.
     */
    public void close() {
        fCVSSession.close();
        cleanup();
    }

    /**
     * Cleanup.
     */
    public void cleanup() {
        IFolder evoTmp = fProject.getFolder(TMP_DIR);
        if (evoTmp.exists()) {
            try {
                unshareResource(evoTmp);
            } catch (EvolizerException e) {
                sLogger.error("Error while cleaning up", e);
            }
            try {
                evoTmp.delete(true /* force */, false /* no history */, null /* progress monitor */);
            } catch (CoreException e) {
                sLogger.error("Error while cleaning up", e);
            }
        }
        File actualTmp = new File(new File(SYS_TMP_DIR), "evolizer");
        if (actualTmp.exists()) {
            deleteAll(actualTmp);
        }
    }

    private void deleteAll(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String element : children) {
                deleteAll(new File(dir, element));
            }
        }
        dir.delete();
    }

    /**
     * Returns the remote module path.
     * 
     * @return the remote module path
     */
    public String getRemoteModulePath() {
        return fRemoteModulePath;
    }
}
