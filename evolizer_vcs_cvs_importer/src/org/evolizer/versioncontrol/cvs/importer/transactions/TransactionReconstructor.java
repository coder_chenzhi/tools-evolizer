/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.transactions;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;

/**
 * Calculates change couplings of files by analyzing their revisions (respectively the modification reports of their
 * revisions).
 * 
 * @author wuersch, jetter
 */
// TODO: Use M.Fischer's sophisticated algorithm
// TODO: Add user-interface for configuration
public final class TransactionReconstructor {

    private TransactionReconstructor() {
        super();
    }

    private static final Logger LOGGER =
            EvolizerCVSPlugin.getLogManager().getLogger(TransactionReconstructor.class.getCanonicalName());

    /**
     * Calculates change couplings of files by analyzing their revisions (respectively the modification reports of their
     * revisions). Change couplings are encapsuled within {@link Transaction}-objects.
     * 
     * @param persistenceProvider
     *            the persistence provider
     * @param tMax
     *            maximum length of time in milliseconds that a transaction can last.
     * @param maxDist
     *            maximum in milliseconds distance between two subsequent modification reports.
     * @param monitor
     *            the monitor
     */
    public static void calculateCouplings(
            IEvolizerSession persistenceProvider,
            long tMax,
            long maxDist,
            IProgressMonitor monitor) {
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }

        LOGGER.debug("Querying database for Revisions.");
        monitor.beginTask("Reconstructing transactions from CVS information.", 100000);
        monitor.subTask("Fetching revisions from database");
        List<Revision> revisionsOrderedByCreationTimeAsc =
                persistenceProvider.query("select revision from Revision as revision "
                        + "order by revision.report.creationTime", Revision.class);

        LOGGER.debug("Revisions loaded.");

        List<Transaction> transactions = new Vector<Transaction>();

        Person precedingAuthor = null;
        long precedingCheckInTime = 0;
        long precedingTransactionStart = 0;
        String precedingLogMessage = null;

        Transaction transaction = null;

        LOGGER.debug("Reconstructing transactions from CVS information.");
        int revTotal = revisionsOrderedByCreationTimeAsc.size();
        for (int i = 0; (i < revisionsOrderedByCreationTimeAsc.size()) && !monitor.isCanceled(); i++) {
            Revision revision = revisionsOrderedByCreationTimeAsc.get(i);
            monitor.subTask("Examining revision " + i + " / " + revTotal);

            Person currentAuthor = revision.getAuthor();
            long currentCheckInTime = revision.getCreationTime().getTime();
            String currentLogMessage = revision.getCommitMessage();

            // Two authors (or log messages) a1 and a2 are equal if both are null or if a1 equals a2
            boolean authorsAreEqual =
                    (precedingAuthor == null ? currentAuthor == null : precedingAuthor.equals(currentAuthor));
            boolean logMessagesAreEqual =
                    (precedingLogMessage == null ? currentLogMessage == null : precedingLogMessage
                            .equals(currentLogMessage));

            // A new Transaction has to be created if current ModificationReport has another author, log message than
            // the preceding mr or if maxDist or tMax has been exeeded.
            if (!authorsAreEqual || !logMessagesAreEqual || (currentCheckInTime > (precedingCheckInTime + maxDist))
                    || (currentCheckInTime > (precedingTransactionStart + tMax))) {
                LOGGER.debug("New transaction found. Check-in time was: " + currentCheckInTime + ".");

                transaction = new Transaction();
                transactions.add(transaction);
                transaction.setStarted(new Date(currentCheckInTime));

                precedingTransactionStart = currentCheckInTime;
            }

            precedingAuthor = currentAuthor;
            precedingCheckInTime = currentCheckInTime;
            precedingLogMessage = currentLogMessage;
            // checkInTime of the current revision is temporarly the point in time where the current transaction
            // finished until we find a newer revision.
            transaction.setFinished(new Date(currentCheckInTime));
            transaction.addRevision(revision);

            // The revision.getFile() == null check is needed for testcases --> we did not want to initialize the whole
            // object tree, so file can be null.
            LOGGER.debug("Added revision " + ((revision.getFile() == null) ? "null" : revision.getFile().getName())
                    + " - " + revision.getNumber() + " to transaction.");
            monitor.worked(100000 / revTotal);
        }

        LOGGER.debug("Starting to make transactions persistent.");
        // TODO: If we notify the listener(s)/save the transaction to db immediately after creating the instance then we
        // run into problems while flushing the session. Here's the nasty workaround:
        monitor.subTask("Saving transactions to database");
        persistenceProvider.startTransaction();
        for (Transaction ta : transactions) {
            persistenceProvider.saveObject(ta);
        }
        persistenceProvider.endTransaction();
        LOGGER.debug("Finished making transactions persistent");

        LOGGER.info("Transactions were reconstructed and made persistent.");
        monitor.done();
    }
}
