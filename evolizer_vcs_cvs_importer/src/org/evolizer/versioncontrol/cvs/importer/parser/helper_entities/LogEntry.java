/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.parser.helper_entities;

import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Auxiliary class/intermediate log representation, used while building the cvs model.
 * 
 * @author wuersch
 */
public class LogEntry {

    private Date fetched;
    private String rcsFile;
    private String workingFile;
    private String head;
    private String branch;
    private List<SymbolicName> symbolicNames = new Vector<SymbolicName>();
    private int totalRevisions;
    private int selectedRevisions;
    private List<RevisionEntry> revisionEntries = new Vector<RevisionEntry>();

    /**
     * Instantiates a new log entry.
     */
    public LogEntry() {
        fetched = new Date();
    }

    /**
     * Returns the branch.
     * 
     * @return the branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * Sets the branch.
     * 
     * @param branch
     *            the new branch
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * Returns the fetched.
     * 
     * @return the fetched
     */
    public Date getFetched() {
        return fetched;
    }

    /**
     * Sets the fetched.
     * 
     * @param fetched
     *            the new fetched
     */
    public void setFetched(Date fetched) {
        this.fetched = fetched;
    }

    /**
     * Returns the head.
     * 
     * @return the head
     */
    public String getHead() {
        return head;
    }

    /**
     * Sets the head.
     * 
     * @param head
     *            the new head
     */
    public void setHead(String head) {
        this.head = head;
    }

    /**
     * Returns the RCS file.
     * 
     * @return the RCS file
     */
    public String getRcsFile() {
        return rcsFile;
    }

    /**
     * Sets the RCS file.
     * 
     * @param rcsFile
     *            the new RCS file
     */
    public void setRcsFile(String rcsFile) {
        this.rcsFile = rcsFile;
    }

    /**
     * Returns the revision entries.
     * 
     * @return the revision entries
     */
    public List<RevisionEntry> getRevisionEntries() {
        return revisionEntries;
    }

    /**
     * Sets the revision entries.
     * 
     * @param revisionEntries
     *            the new revision entries
     */
    public void setRevisionEntries(List<RevisionEntry> revisionEntries) {
        this.revisionEntries = revisionEntries;
    }

    /**
     * Returns the selected revisions.
     * 
     * @return the selected revisions
     */
    public int getSelectedRevisions() {
        return selectedRevisions;
    }

    /**
     * Sets the selected revisions.
     * 
     * @param selectedRevisions
     *            the new selected revisions
     */
    public void setSelectedRevisions(int selectedRevisions) {
        this.selectedRevisions = selectedRevisions;
    }

    /**
     * Returns the symbolic names.
     * 
     * @return the symbolic names
     */
    public List<SymbolicName> getSymbolicNames() {
        return symbolicNames;
    }

    /**
     * Sets the symbolic names.
     * 
     * @param symbolicNames
     *            the new symbolic names
     */
    public void setSymbolicNames(List<SymbolicName> symbolicNames) {
        this.symbolicNames = symbolicNames;
    }

    /**
     * Returns the total revisions.
     * 
     * @return the total revisions
     */
    public int getTotalRevisions() {
        return totalRevisions;
    }

    /**
     * Sets the total revisions.
     * 
     * @param totalRevisions
     *            the new total revisions
     */
    public void setTotalRevisions(int totalRevisions) {
        this.totalRevisions = totalRevisions;
    }

    /**
     * Returns the working file.
     * 
     * @return the working file
     */
    public String getWorkingFile() {
        return workingFile;
    }

    /**
     * Sets the working file.
     * 
     * @param workingFile
     *            the new working file
     */
    public void setWorkingFile(String workingFile) {
        this.workingFile = workingFile;
    }

    /**
     * Adds the revision entry.
     * 
     * @param currentRevEntry
     *            the current rev entry
     */
    public void addRevisionEntry(RevisionEntry currentRevEntry) {
        revisionEntries.add(currentRevEntry);
    }

    /**
     * Adds the symbolic name.
     * 
     * @param releaseTag
     *            the release tag
     * @param revisionTag
     *            the revision tag
     */
    public void addSymbolicName(String releaseTag, String revisionTag) {
        symbolicNames.add(new SymbolicName(releaseTag, revisionTag));

    }

    /**
     * Returns the revision entries ordered asc.
     * 
     * @return the revision entries ordered asc
     */
    public List<RevisionEntry> getRevisionEntriesOrderedAsc() {
        List<RevisionEntry> result = new Vector<RevisionEntry>();

        for (int i = revisionEntries.size() - 1; i >= 0; i--) {
            result.add(revisionEntries.get(i));
        }
        return result;
    }
}
