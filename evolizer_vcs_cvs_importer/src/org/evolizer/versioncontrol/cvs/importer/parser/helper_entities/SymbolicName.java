/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.parser.helper_entities;

/**
 * Helper class, used while building the version control model.
 * 
 * @author wuersch
 * @see LogEntry
 */
public class SymbolicName {

    private String releaseOrBranchTag;
    private String revisionOrBranchNumber;

    /**
     * Instantiates a new symbolic name.
     * 
     * @param releaseTag
     *            the release tag
     * @param revisionTag
     *            the revision tag
     */
    public SymbolicName(String releaseTag, String revisionTag) {
        releaseOrBranchTag = releaseTag;
        revisionOrBranchNumber = revisionTag;
    }

    /**
     * Returns the release or branch tag.
     * 
     * @return the release or branch tag
     */
    public String getReleaseOrBranchTag() {
        return releaseOrBranchTag;
    }

    /**
     * Sets the release or branch tag.
     * 
     * @param releaseOrBranchTag
     *            the new release or branch tag
     */
    public void setReleaseOrBranchTag(String releaseOrBranchTag) {
        this.releaseOrBranchTag = releaseOrBranchTag;
    }

    /**
     * Returns the revision or branch number.
     * 
     * @return the revision or branch number
     */
    public String getRevisionOrBranchNumber() {
        return revisionOrBranchNumber;
    }

    /**
     * Sets the revision or branch number.
     * 
     * @param revisionOrBranchNumber
     *            the new revision or branch number
     */
    public void setRevisionOrBranchNumber(String revisionOrBranchNumber) {
        this.revisionOrBranchNumber = revisionOrBranchNumber;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof SymbolicName) {
            SymbolicName other = (SymbolicName) object;
            return (releaseOrBranchTag == null ? other.getReleaseOrBranchTag() == null : releaseOrBranchTag
                    .equals(other.getReleaseOrBranchTag()))
                    && (revisionOrBranchNumber == null
                            ? other.getRevisionOrBranchNumber() == null
                            : revisionOrBranchNumber.equals(other.getRevisionOrBranchNumber()));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        final int PRIME = 31;

        int result = 1;

        result = PRIME * result + ((releaseOrBranchTag == null) ? 0 : releaseOrBranchTag.hashCode());
        result = PRIME * result + ((revisionOrBranchNumber == null) ? 0 : revisionOrBranchNumber.hashCode());

        return result;
    }
}
