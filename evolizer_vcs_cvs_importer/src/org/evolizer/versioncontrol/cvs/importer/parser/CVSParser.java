/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.parser;

import java.text.ParseException;
import java.util.regex.Matcher;

import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.LogEntry;
import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.RevisionEntry;

/**
 * The Class CVSParser.
 * 
 * @author wuersch, jetter
 */
public class CVSParser extends AbstractParser {

    /**
     * This parser can have one of the following states:
     * 
     * PROCESSING - Normal operation, recognizing structural elements of the CVS log. MESSAGE_CAPTURING - While
     * capturing messages the parser ignores structural elements which trigger creation of e.g {@link RevisionEntry}s
     * BRANCH_CAPTURING - Special state when capturing the information about branches in each revision entry.
     */
    private enum ParserState {
        PROCESSING,
        MESSAGE_CAPTURING,
        BRANCH_CAPTURING
    };

    /**
     * Saves the state of the parser.
     */
    private ParserState fState = ParserState.PROCESSING;

    /**
     * Working copy of the current <code>LogEntry</code>-object.
     */
    private LogEntry fCurrentLogEntry;

    /**
     * Working copy of the current <code>RevisionEntry</code>-object.
     */
    private RevisionEntry fCurrentRevEntry;

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseLine(String line) {
        switch (fState) {
            case PROCESSING:
                // workaround if log entry contains no revision
                if (line.equals("=============================================================================")) {
                    finishLogEntry();
                } else {
                    process(line);
                }

                break;
            case BRANCH_CAPTURING: // We're not using this information for mapping at the moment, but save it for each
                // LogEntry.
                if (line.startsWith("branches:")) {
                    checkBranch(line);
                    fState = ParserState.MESSAGE_CAPTURING;
                    break;
                } else {
                    fState = ParserState.MESSAGE_CAPTURING;
                } // fall through is intended!
            case MESSAGE_CAPTURING:
                if (line.equals("----------------------------")) {
                    finishRevisionEntry();
                    fState = ParserState.PROCESSING;
                } else if (line.equals("=============================================================================")) {
                    finishLogEntry();
                    fState = ParserState.PROCESSING;
                } else { // if we haven't found a revision or log entry delimiter, we threat the current line as element
                    // of the commit message.
                    fCurrentRevEntry.appendCommitMessage(line);
                    break;
                }
                break;
            default:
                break;
        }
    }

    /**
     * Passes the currentLogEntry to the registered mappers and resets several temporary datastructures if necessary.
     */
    private void finishLogEntry() {
        if (fCurrentRevEntry != null) {
            fCurrentLogEntry.addRevisionEntry(fCurrentRevEntry);
        }
        notifyLogEntryListeners(fCurrentLogEntry);
        fCurrentRevEntry = null;
        fCurrentLogEntry = null;
    }

    /**
     * Performs some clean-up and adds the finished <code>RevisionEntry</code> to the current <code>LogEntry</code>.
     */
    private void finishRevisionEntry() {
        fCurrentLogEntry.addRevisionEntry(fCurrentRevEntry);
        fCurrentRevEntry = null;
    }

    /**
     * Processes one cvs log line per invocation.
     * 
     * @param line
     *            the line to process
     */
    private void process(String line) {
        checkRCSFile(line);
        checkWorkingFile(line);
        checkHead(line);
        checkBranch(line);
        checkSymbolicNames(line);
        checkNrOfRevisions(line);
        checkRevision(line);
        checkRevisionStats(line); // changes the state to BRANCH_CAPTURING or to MESSAGE_CAPTURING
    }

    /**
     * Checks if the line contains the stats of a revision (Head and Tail)
     * 
     * @param line
     *            the line
     */
    private void checkRevisionStats(String line) {
        Matcher matcher;
        matcher = RegexLibrary.REVISION_STATS_HEAD.matcher(line);
        if (matcher.matches()) {
            checkRevisionStatsHead(line);
            checkRevisionStatsTail(line);
            fState = ParserState.BRANCH_CAPTURING;
        }
    }

    /**
     * Checks if the line contains the stats of a revision (covers the tail of the stats line)
     * 
     * @param line
     *            the line
     */
    private void checkRevisionStatsTail(String line) {
        Matcher matcher;
        matcher = RegexLibrary.REVISION_STATS_TAIL.matcher(line);
        if (matcher.matches()) {
            fCurrentRevEntry.setLinesAdd(Integer.parseInt(matcher.group(1)));
            fCurrentRevEntry.setLinesDel(Integer.parseInt(matcher.group(2)));
        }
    }

    /**
     * Checks if the line contains the stats of a revision (covers the head of the stats line)
     * 
     * @param line
     *            the line
     */
    private void checkRevisionStatsHead(String line) {
        Matcher matcher;
        matcher = RegexLibrary.REVISION_STATS_HEAD.matcher(line);
        if (matcher.matches()) {
            try {
                fCurrentRevEntry.setCreationDate(matcher.group(1));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            fCurrentRevEntry.setAuthor(matcher.group(2));
            fCurrentRevEntry.setState(matcher.group(3));
        }
    }

    /**
     * Checks if the line contains the the number of the revision.
     * 
     * @param line
     *            the line
     */
    private void checkRevision(String line) {
        Matcher matcher = RegexLibrary.REVISION.matcher(line);
        if (matcher.matches()) {
            fCurrentRevEntry = new RevisionEntry(matcher.group(1));
        }
    }

    /**
     * Checks the line to get the quantity of revisions the current file has in total and how many are selected.
     * 
     * @param line
     *            the line
     */
    private void checkNrOfRevisions(String line) {
        Matcher matcher = RegexLibrary.NO_OF_REVISIONS.matcher(line);
        if (matcher.matches()) {
            fCurrentLogEntry.setTotalRevisions(Integer.parseInt(matcher.group(1)));
            fCurrentLogEntry.setSelectedRevisions(Integer.parseInt(matcher.group(2)));
        }
    }

    /**
     * Checks if the line contains a symbolic name and saves it to the currentLogEntry.
     * 
     * @param line
     *            the line
     */
    private void checkSymbolicNames(String line) {
        Matcher matcher = RegexLibrary.SYMBOLIC_REL_REV.matcher(line);
        if (matcher.matches()) {
            fCurrentLogEntry.addSymbolicName(matcher.group(1), matcher.group(2));
        }
    }

    /**
     * Checks if the current line contains a branch tag
     * 
     * @param line
     *            the line
     */
    private void checkBranch(String line) {
        Matcher matcher = RegexLibrary.BRANCH.matcher(line);
        if (matcher.matches()) {
            fCurrentLogEntry.setBranch(matcher.group(1));
        }
    }

    /**
     * Checks if the current line contains a head tag.
     * 
     * @param line
     *            the line
     */
    private void checkHead(String line) {
        Matcher matcher = RegexLibrary.HEAD.matcher(line);
        if (matcher.matches()) {
            fCurrentLogEntry.setHead(matcher.group(1));
        }
    }

    /**
     * Checks if the current line contains a working file tag.
     * 
     * @param line
     *            the line
     */
    private void checkWorkingFile(String line) {
        Matcher matcher = RegexLibrary.WORKING_FILE.matcher(line);
        if (matcher.matches()) {
            fCurrentLogEntry.setWorkingFile(matcher.group(1));
        }
    }

    /**
     * Checks if the current line contains a RCS-file tag.
     * 
     * @param line
     *            the line
     */
    private void checkRCSFile(String line) {
        Matcher matcher = RegexLibrary.RCS_FILE.matcher(line);
        if (matcher.matches()) {
            fCurrentLogEntry = new LogEntry();
            fCurrentLogEntry.setRcsFile(matcher.group(1));
        }
    }
}
