/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.parser;

import java.util.regex.Pattern;

/**
 * Library with regular expressions which are usefull for working with cvs logs.
 * 
 * @author wuersch, jetter
 */
public final class RegexLibrary {

    private RegexLibrary() {
        super();
    }

    /** The Constant RCS_FILE. */
    public static final Pattern RCS_FILE = Pattern.compile("^RCS file: (.+),v", Pattern.CASE_INSENSITIVE);

    /** The Constant WORKING_FILE. */
    public static final Pattern WORKING_FILE = Pattern.compile("^Working file: (.*)", Pattern.CASE_INSENSITIVE);

    /** The Constant HEAD. */
    public static final Pattern HEAD = Pattern.compile("^head: (\\S+)", Pattern.CASE_INSENSITIVE);

    /** The Constant BRANCH. */
    public static final Pattern BRANCH = Pattern.compile("^branch: (\\S+)", Pattern.CASE_INSENSITIVE);

    /** The Constant LOCKS. */
    public static final Pattern LOCKS = Pattern.compile("^locks: (\\S+)", Pattern.CASE_INSENSITIVE);

    /** The Constant ACCESS_LIST. */
    public static final Pattern ACCESS_LIST = Pattern.compile("^access list: (\\S+)", Pattern.CASE_INSENSITIVE);

    /** The Constant SYMBOLIC. */
    public static final Pattern SYMBOLIC = Pattern.compile("^symbolic names:", Pattern.CASE_INSENSITIVE);

    /** The Constant SYMBOLIC_REL_REV. */
    public static final Pattern SYMBOLIC_REL_REV = Pattern.compile("^\\s+(\\S+): (\\S+)", Pattern.CASE_INSENSITIVE);

    /** The Constant KEYWORD_SUBSTITUTION. */
    public static final Pattern KEYWORD_SUBSTITUTION =
            Pattern.compile("^keyword substitution: (\\S+)", Pattern.CASE_INSENSITIVE);

    /** The Constant NO_OF_REVISIONS. */
    public static final Pattern NO_OF_REVISIONS =
            Pattern.compile("^total revisions: (\\d+);\\s+selected revisions: (\\d+)", Pattern.CASE_INSENSITIVE);

    /** The Constant REVISION_START. */
    public static final Pattern REVISION_START =
            Pattern.compile("^----------------------------", Pattern.CASE_INSENSITIVE);

    /** The Constant REVISION. */
    public static final Pattern REVISION = Pattern.compile("^revision (.+)", Pattern.CASE_INSENSITIVE);

    /** The Constant REVISION_STATS_HEAD. */
    public static final Pattern REVISION_STATS_HEAD =
            Pattern.compile("^date: (.+);  author: (.+);  state: (.+?);.*", Pattern.CASE_INSENSITIVE);

    /** The Constant REVISION_STATS_TAIL. */
    public static final Pattern REVISION_STATS_TAIL =
            Pattern.compile(".*lines: \\+(\\w+) \\-(\\w+);?$", Pattern.CASE_INSENSITIVE);

    /** The Constant DESCRIPTION. */
    public static final Pattern DESCRIPTION = Pattern.compile("^description:", Pattern.CASE_INSENSITIVE);

    /** The Constant FILE_DELIMITER. */
    public static final Pattern FILE_DELIMITER =
            Pattern.compile(
                    "^=============================================================================",
                    Pattern.CASE_INSENSITIVE);
}
