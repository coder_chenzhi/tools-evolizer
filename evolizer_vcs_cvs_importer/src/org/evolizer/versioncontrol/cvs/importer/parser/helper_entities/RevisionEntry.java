/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.parser.helper_entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Helper class, used while building the version control model.
 * 
 * @author wuersch
 * @see LogEntry
 */
public class RevisionEntry {

    private String revisionNumber;

    private Date creationDate;

    private String author;

    private String state;

    private int linesAdd;

    private int linesDel;

    private String branch;

    private String commitMessage;

    /**
     * Constructor.
     * 
     * @param revisionNumber
     *            the revision number
     */
    public RevisionEntry(String revisionNumber) {
        this.revisionNumber = revisionNumber;
    }

    /**
     * Returns the author.
     * 
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets the author.
     * 
     * @param author
     *            the new author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Returns the branch.
     * 
     * @return the branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * Sets the branch.
     * 
     * @param branch
     *            the new branch
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * Returns the commit message.
     * 
     * @return the commit message
     */
    public String getCommitMessage() {
        return commitMessage;
    }

    /**
     * Sets the commit message.
     * 
     * @param commitMessage
     *            the new commit message
     */
    public void setCommitMessage(String commitMessage) {
        this.commitMessage = commitMessage;
    }

    /**
     * Append commit message.
     * 
     * @param commitMessageLine
     *            the commit message line
     */
    public void appendCommitMessage(String commitMessageLine) {
        if (commitMessage == null) {
            commitMessage = commitMessageLine;
        } else {
            commitMessage = commitMessage.concat("\n".concat(commitMessageLine));
        }
    }

    /**
     * Returns the creation date.
     * 
     * @return the creation date
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the creation date.
     * 
     * @param creationDate
     *            the new creation date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Sets the creation date.
     * 
     * @param creationDate
     *            the new creation date
     * @throws ParseException
     *             the parse exception
     */
    public void setCreationDate(String creationDate) throws ParseException {
        // Handle different data formats
        SimpleDateFormat df;
        if (creationDate.indexOf("-") != -1) {
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else {
            df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        }
        Date date = df.parse(creationDate);

        setCreationDate(date);
    }

    /**
     * Returns the lines add.
     * 
     * @return the lines add
     */
    public int getLinesAdd() {
        return linesAdd;
    }

    /**
     * Sets the lines add.
     * 
     * @param linesAdd
     *            the new lines add
     */
    public void setLinesAdd(int linesAdd) {
        this.linesAdd = linesAdd;
    }

    /**
     * Returns the lines del.
     * 
     * @return the lines del
     */
    public int getLinesDel() {
        return linesDel;
    }

    /**
     * Sets the lines del.
     * 
     * @param linesDel
     *            the new lines del
     */
    public void setLinesDel(int linesDel) {
        this.linesDel = linesDel;
    }

    /**
     * Returns the revision number.
     * 
     * @return the revision number
     */
    public String getRevisionNumber() {
        return revisionNumber;
    }

    /**
     * Sets the revision number.
     * 
     * @param revisionNumber
     *            the new revision number
     */
    public void setRevisionNumber(String revisionNumber) {
        this.revisionNumber = revisionNumber;
    }

    /**
     * Returns the state.
     * 
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state.
     * 
     * @param state
     *            the new state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof RevisionEntry) {
            RevisionEntry other = (RevisionEntry) object;

            return ((revisionNumber == null ? other.getRevisionNumber() == null : revisionNumber.equals(other
                    .getRevisionNumber()))
                    && (creationDate == null ? other.getCreationDate() == null : creationDate.equals(other
                            .getCreationDate()))
                    && (author == null ? other.getAuthor() == null : author.equals(other.getAuthor()))
                    && (state == null ? other.getState() == null : state.equals(other.getState()))
                    && (linesAdd == other.getLinesAdd())
                    && (linesDel == other.getLinesDel())
                    && (branch == null ? other.getBranch() == null : branch.equals(other.getBranch())) && (commitMessage == null
                    ? other.getCommitMessage() == null
                    : commitMessage.equals(other.getCommitMessage())));
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        final int PRIME = 31;
        int result = 1;

        result = PRIME * result + ((revisionNumber == null) ? 0 : revisionNumber.hashCode());
        result = PRIME * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = PRIME * result + ((author == null) ? 0 : author.hashCode());
        result = PRIME * result + ((state == null) ? 0 : state.hashCode());
        result = PRIME * result + (new Integer(linesAdd).hashCode());
        result = PRIME * result + (new Integer(linesDel).hashCode());
        result = PRIME * result + ((branch == null) ? 0 : branch.hashCode());
        result = PRIME * result + ((commitMessage == null) ? 0 : commitMessage.hashCode());

        return result;
    }
}
