/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer;

import java.text.ParseException;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.team.internal.ccvs.core.ICVSFolder;
import org.eclipse.team.internal.ccvs.core.ICVSRepositoryLocation;
import org.eclipse.team.internal.ccvs.core.client.Command;
import org.eclipse.team.internal.ccvs.core.client.listeners.ICommandOutputListener;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.cvs.importer.mapper.VersioningModelBuilder;
import org.evolizer.versioncontrol.cvs.importer.parser.CVSParser;

/**
 * Instances of this class can be passed to the {@link Command#execute} methods and will receive notification when
 * normal messages or error messages are received from the server. This implementation uses the messages to build a
 * versioning model and then maps it to a relational database using the controller plug-in.
 * 
 * @author wuersch, jetter
 */
@SuppressWarnings("restriction")
public class CVSLogInterceptor implements ICommandOutputListener {

    private Logger fLogger = EvolizerCVSPlugin.getLogManager().getLogger(CVSLogInterceptor.class.getCanonicalName());

    private CVSParser fCVSParser;

    /**
     * The Constructor.
     * 
     * @param persistenceProvider
     *            the persistence provider
     * @param monitor
     *            the monitor
     * @param remoteModulePath
     *            the remote module path
     */
    public CVSLogInterceptor(IEvolizerSession persistenceProvider, IProgressMonitor monitor, String remoteModulePath) {
        fCVSParser = new CVSParser();
        fCVSParser.addLogEntryListener(new VersioningModelBuilder(persistenceProvider, monitor, remoteModulePath));
    }

    /**
     * Message line.
     * 
     * @param line
     *            the line
     * @param location
     *            the location
     * @param commandRoot
     *            the command root
     * @param monitor
     *            the monitor
     * @return the status
     * @throws ParseException
     * @see ICommandOutputListener#messageLine(String, ICVSRepositoryLocation, ICVSFolder, IProgressMonitor)
     */
    public IStatus messageLine(
            String line,
            ICVSRepositoryLocation location,
            ICVSFolder commandRoot,
            IProgressMonitor monitor) {

        fCVSParser.parseLine(line);

        return ICommandOutputListener.OK;
    }

    /**
     * {@inheritDoc}
     */
    public IStatus errorLine(
            String line,
            ICVSRepositoryLocation location,
            ICVSFolder commandRoot,
            IProgressMonitor monitor) {

        if (!line.startsWith("cvs log: Logging")) {
            fLogger.warn("The Evolizer CVS Importer received an error line from cvs-plugin: " + line);
        }

        return ICommandOutputListener.OK;
    }

    /**
     * Returns the CVS parser.
     * 
     * @return the CVS parser
     */
    public CVSParser getCvsParser() {
        return fCVSParser;
    }
}
