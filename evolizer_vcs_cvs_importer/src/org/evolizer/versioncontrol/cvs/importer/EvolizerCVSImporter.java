/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.team.internal.ccvs.core.client.listeners.ICommandOutputListener;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.cvs.importer.job.CVSImporterJob;
import org.evolizer.versioncontrol.cvs.importer.util.CVSRepositoryProvider;
import org.evolizer.versioncontrol.cvs.importer.util.FileManager;

/**
 * This class imports the CVS information of a given {@link IProject} into the RHDB. The import steps are: - parsing of
 * the CVS log (mandatory). - downloading the content of all files that match a given regular expression (optional).
 * 
 * @see CVSImporterJob
 * @author wuersch
 */
@SuppressWarnings("restriction")
public final class EvolizerCVSImporter {

    private EvolizerCVSImporter() {
        super();
    }

    /**
     * Import the given {@link IProject}.
     * 
     * @param project
     *            the project to import
     * @param persistenceProvider
     *            the persistence provider to store the imported data
     * @param monitor
     *            the monitor
     * @throws EvolizerException
     *             the evolizer exception
     */
    public static void importProject(IProject project, IEvolizerSession persistenceProvider, IProgressMonitor monitor)
            throws EvolizerException {
        importProject(project, persistenceProvider, "*", monitor);
    }

    /**
     * Import project the given {@link IProject}.
     * 
     * @param project
     *            the project to import
     * @param persistenceProvider
     *            the persistence provider to store the imported data
     * @param fileExtensionRegEx
     *            the file extension regex
     * @param monitor
     *            the monitor
     * @throws EvolizerException
     *             the evolizer exception
     */
    public static void importProject(
            IProject project,
            IEvolizerSession persistenceProvider,
            String fileExtensionRegEx,
            IProgressMonitor monitor) throws EvolizerException {
        importProject(project, persistenceProvider, fileExtensionRegEx, monitor, true);
    }

    /**
     * Import project the given {@link IProject}.
     * 
     * @param project
     *            the project to import
     * @param persistenceProvider
     *            the persistence provider to store the imported data
     * @param fileExtensionRegEx
     *            the file extension regex
     * @param monitor
     *            the monitor
     * @param isFileContentImportEnabled
     *            the is file content import enabled
     * @throws EvolizerException
     *             the evolizer exception
     */
    public static void importProject(
            IProject project,
            IEvolizerSession persistenceProvider,
            String fileExtensionRegEx,
            IProgressMonitor monitor,
            boolean isFileContentImportEnabled) throws EvolizerException {
        CVSRepositoryProvider repositoryProvider = new CVSRepositoryProvider(project);
        ICommandOutputListener logInterceptor =
                new CVSLogInterceptor(persistenceProvider, monitor, repositoryProvider.getRemoteModulePath());

        repositoryProvider.cvsLogForProject(logInterceptor, monitor);

        if (isFileContentImportEnabled) {
            FileManager.fetchSourceForAllFiles(persistenceProvider, repositoryProvider, fileExtensionRegEx, monitor);
        }
    }

    /**
     * Import missing file content.
     * 
     * @param project
     *            the project to import missing file content for
     * @param persistenceProvider
     *            the persistence provider to store the imported data
     * @param fileExtensionRegEx
     *            the file extension regex
     * @param monitor
     *            the monitor
     * @param reImport
     *            the reimport
     * @throws EvolizerException
     *             the evolizer exception
     */
    public static void importMissingFileContent(
            IProject project,
            IEvolizerSession persistenceProvider,
            String fileExtensionRegEx,
            IProgressMonitor monitor,
            boolean reImport) throws EvolizerException {
        CVSRepositoryProvider repositoryProvider = new CVSRepositoryProvider(project);
        FileManager.fetchSourceForFiles(persistenceProvider, repositoryProvider, fileExtensionRegEx, monitor, reImport);
    }
}
