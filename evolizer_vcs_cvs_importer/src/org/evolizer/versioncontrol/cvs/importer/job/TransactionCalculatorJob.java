/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.job;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;
import org.evolizer.versioncontrol.cvs.importer.transactions.TransactionReconstructor;

/**
 * Job that wrapps {@link TransactionReconstructor}.
 * 
 * @author wuersch
 */
public class TransactionCalculatorJob extends Job {

    private static Logger sLogger =
            EvolizerCVSPlugin.getLogManager().getLogger(TransactionCalculatorJob.class.getName());

    private IProject fProject;
    private long fTMax;
    private long fDistMax;

    /**
     * Instantiates a new transaction calculator job.
     * 
     * @param name
     *            the name
     * @param project
     *            the project
     * @param tMax
     *            the t max
     * @param distMax
     *            the distance max
     */
    public TransactionCalculatorJob(String name, IProject project, long tMax, long distMax) {
        super(name);
        this.fProject = project;
        fTMax = tMax;
        fDistMax = distMax;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            EvolizerSessionHandler handler = EvolizerSessionHandler.getHandler();
            handler.updateSchema(fProject);
            IEvolizerSession persistenceProvider = handler.getCurrentSession(fProject);
            TransactionReconstructor.calculateCouplings(persistenceProvider, 1000 * fTMax, 1000 * fDistMax, monitor);
            persistenceProvider.close();
            sLogger.debug("Transaction reconstruction finished");
        } catch (EvolizerException e) {
            sLogger.error("Transaction reconstruction failed:", e);
        }
        return Status.OK_STATUS;
    }
}
