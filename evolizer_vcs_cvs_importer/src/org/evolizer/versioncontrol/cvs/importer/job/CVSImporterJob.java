/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.job;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.core.natures.EvolizerNatureManager;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSImporter;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;

/**
 * Job that wraps {@link EvolizerCVSImporter} to import the CVS information of a given {@link IProject} into the
 * RHDB. The import steps are:
 * - parsing of the CVS log (mandatory).
 * - downloading the content of all files that match a given regular expression (optional).
 * 
 * @see EvolizerCVSImporter
 * @author wuersch
 */
public class CVSImporterJob extends Job {

    private static final Logger LOGGER =
            EvolizerCVSPlugin.getLogManager().getLogger(CVSImporterJob.class.getCanonicalName());

    private IProject fProject;
    private String fFileExtensionRegEx;
    private boolean fFileContentImportEnabled;

    /**
     * Instantiates a new CVS importer job.
     * 
     * @param project
     *            the project
     */
    public CVSImporterJob(IProject project) {
        super("Import CVS Version Control Information");
        fProject = project;
        fFileExtensionRegEx = "*";
        fFileContentImportEnabled = true;
    }

    /**
     * Instantiates a new CVS importer job.
     * 
     * @param project
     *            the project
     * @param fileExtensionRegEx
     *            the file extension regex
     * @param fileContentImportEnabled
     *            the file content import enabled
     */
    public CVSImporterJob(IProject project, String fileExtensionRegEx, boolean fileContentImportEnabled) {
        super("Import CVS Version Control Information");
        fProject = project;
        this.fFileExtensionRegEx = fileExtensionRegEx;
        this.fFileContentImportEnabled = fileContentImportEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            EvolizerSessionHandler handler = EvolizerSessionHandler.getHandler();
            handler.updateSchema(fProject);
            IEvolizerSession persistenceProvider = handler.getCurrentSession(fProject);

            EvolizerCVSImporter.importProject(
                    fProject,
                    persistenceProvider,
                    fFileExtensionRegEx,
                    monitor,
                    fFileContentImportEnabled);

            EvolizerNatureManager.applyEvolizerNature(fProject, monitor);

            persistenceProvider.close();

            LOGGER.info("Versioning Information has been imported for project '" + fProject.getName() + "'");
        } catch (EvolizerException e) {
            LOGGER.error("Error while performing import from versioning system", e);
        }
        return Status.OK_STATUS;
    }
}
