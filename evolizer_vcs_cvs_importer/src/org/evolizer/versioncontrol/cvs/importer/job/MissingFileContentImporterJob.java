/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.job;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSImporter;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;

/**
 * The Class MissingFileContentImporterJob.
 * 
 * @author wuersch
 */
public class MissingFileContentImporterJob extends Job {

    private static Logger sLogger =
            EvolizerCVSPlugin.getLogManager().getLogger(MissingFileContentImporterJob.class.getName());

    private IProject fProject;
    private String fFileExtensionRegEx;
    private boolean fReImport;

    /**
     * Instantiates a new missing file content importer job.
     * 
     * @param name
     *            the name
     * @param project
     *            the project
     * @param fileExtensionRegEx
     *            the file extension regex
     * @param reImport
     *            the re-import
     */
    public MissingFileContentImporterJob(String name, IProject project, String fileExtensionRegEx, boolean reImport) {
        super(name);
        this.fProject = project;
        this.fFileExtensionRegEx = fileExtensionRegEx;
        this.fReImport = reImport;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            EvolizerSessionHandler handler = EvolizerSessionHandler.getHandler();
            handler.updateSchema(fProject);
            IEvolizerSession persistenceProvider = handler.getCurrentSession(fProject);

            EvolizerCVSImporter.importMissingFileContent(
                    fProject,
                    persistenceProvider,
                    fFileExtensionRegEx,
                    monitor,
                    fReImport);
            persistenceProvider.close();
            sLogger.debug("Import of missing file content completed");
        } catch (EvolizerException e) {
            sLogger.error("Import of missing file content failed:", e);
        }
        return Status.OK_STATUS;
    }
}
