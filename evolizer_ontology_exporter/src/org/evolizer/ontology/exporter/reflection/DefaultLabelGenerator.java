package org.evolizer.ontology.exporter.reflection;


/**
 * Simple strategy for generating a label. Just calls the toString()-method,
 * which is adequate as long as it returns a short but concise name.
 * 
 * @author wuersch
 *
 */
public class DefaultLabelGenerator implements ILabelGenerator {
	public String getLabelFor(Object resource) {	
		return resource.toString();
	}
}
