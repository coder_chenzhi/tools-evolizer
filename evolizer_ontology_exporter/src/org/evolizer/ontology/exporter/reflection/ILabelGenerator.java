package org.evolizer.ontology.exporter.reflection;

/**
 * A strategy for generating meaningful labels for a given resource.
 * 
 * @author wuersch
 *
 * @param <T> The type of resource that the label generator applies to.
 */
public interface ILabelGenerator {
	public String getLabelFor(Object resource);
}
