package org.evolizer.ontology.exporter.reflection.converter;

import java.util.LinkedList;
import java.util.List;

import org.evolizer.ontology.exporter.api.IRDFPropertyConverter;

/**
 * Registers all converters necessary to export different datatypes to rdf. The
 * order of registration is important: Converters registered first have a higher
 * priority over converters registered later.
 * 
 * @author wuersch
 * 
 */
public class ConverterRegistry {
	private static final IRDFPropertyConverter DEFAULT_CONVERTER = new DefaultConverter();
	private static List<IRDFPropertyConverter> converters = new LinkedList<IRDFPropertyConverter>();

//	static {
//		ConverterRegistry.register(new EnumConverter());
//		ConverterRegistry.register(new CollectionConverter());
//	}

	public static IRDFPropertyConverter getConverterFor(Class<?> type) {
		for (IRDFPropertyConverter converter : converters) {
			if (converter.canConvert(type)) {
				return converter;
			}
		}

		return DEFAULT_CONVERTER;
	}

	public static void register(IRDFPropertyConverter converter) {
		if (!converters.contains(converter)) {
			converters.add(0, converter);
		}
	}
}
