package org.evolizer.ontology.exporter.reflection.converter;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.evolizer.ontology.exporter.EvolizerOntologyExporterPlugin;
import org.evolizer.ontology.exporter.api.IRDFPropertyConverter;
import org.evolizer.ontology.provider.util.Statement;

/** 
 * 
 * @author wuersch
 *
 */
public class EnumConverter implements IRDFPropertyConverter{
	private final static Logger logger = EvolizerOntologyExporterPlugin.getLogManager().getLogger(EnumConverter.class.getName());

	public boolean canConvert(Class<?> type) {
		return type.isEnum();
	}

	public Collection<Statement<?, ?>> convert(Object subject, String propertyURI, Object object) {
		Collection<Statement<?, ?>> statements = new ArrayList<Statement<?, ?>>();
		
		try {
			Statement<?, ?> statement = new Statement<Object, Object>(subject, propertyURI, ((Enum<?>)object).name());
			statements.add(statement);
		} catch (URISyntaxException e) {
			logger.error("Encountered an invalid uri for a property.", e);
		}
		
		return statements;
	}
}
