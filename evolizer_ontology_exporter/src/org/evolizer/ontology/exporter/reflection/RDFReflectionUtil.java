package org.evolizer.ontology.exporter.reflection;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;
import org.evolizer.core.util.reflection.api.impl.ClassRegistry;
import org.evolizer.ontology.annotations.rdf;
import org.evolizer.ontology.exporter.api.IURIGenerator;
import org.evolizer.ontology.provider.OntologyProvider;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Utility class for working with reflection and rdf annotations.
 *
 * @author wuersch
 *
 */
public class RDFReflectionUtil {
	// TODO make this configurable
	private static ILabelGenerator defaultLabelGenerator = new DefaultLabelGenerator();
	private static ILabelGenerator evolizerEntityLabelGenerator = new EvolizerEntityLabelGenerator();
	//private static IIdentifierGenerator uriGenerator = new DefaultURIGenerator();
	
	public static String fragmentIdentifier(Object object) {
		IURIGenerator uriGenerator = URIGeneratorRegistry.getConverterFor(object.getClass());
		return uriGenerator.getIdentifier(object);
	}
		
	public static String getLabelFor(Object resource) { // TODO label generator registry
		if(resource instanceof IEvolizerModelEntity) {
			return evolizerEntityLabelGenerator.getLabelFor((IEvolizerModelEntity) resource);
		} else {
			return defaultLabelGenerator.getLabelFor(resource);
		}
	}
	
	public static Resource rdfClassOf(Model model, Object object) {
		return model.createResource(uriReferenceOf(object));
	}
	
	public static Resource rdfClassOf(Model model, Class<?> clazz) {
		return model.createResource(uriReferenceOf(clazz));
	}
	
	public static Resource rdfClassOf(Model model, Method method) {
		return model.createResource(uriReferenceOf(method));
	}
	
	public static String uriReferenceOf(Class<?> clazz) {
		rdf annotation = clazz.getAnnotation(rdf.class);
		
		return annotation.value();
	}
	
	public static String uriReferenceOf(Method method) {
		rdf annotation = method.getAnnotation(rdf.class);
		
		return annotation.value();
	}
	
	public static String uriReferenceOf(Object object) {
		Class<?> type = object.getClass();
		
		return uriReferenceOf(type);
	}
	
	public static Resource rdfInstanceOf(Model model, Object object) {
		return OntologyProvider.rdfInstanceOf(model, fragmentIdentifier(object), rdfClassOf(model, object));
	}
	
	public static boolean isPredicate(Class<?> clazz) {
		rdf annotation = clazz.getAnnotation(rdf.class);
		return annotation.isPredicate();
	}

	public static List<Method> findContainment(Object object) {
		Class<?> type = object.getClass();
		return findContainment(type);
	}

	/**
	 * TODO create some kind of plug-in registry for different relations
	 * 
	 * @param type
	 * @return
	 */
	public static List<Method> findContainment(Class<?> type) {
		List<Method> result = new ArrayList<Method>();
		
		Iterator<Method> iter = ClassRegistry.methodsFor(type);
		while(iter.hasNext()) {
			Method method = iter.next();
			rdf annotation = method.getAnnotation(rdf.class);
			if(annotation != null) {
				String uriReference = annotation.value();
				boolean isContainment = OntologyProvider.hasSuperProperty(uriReference, "http://www.evolizer.org.com/ontology/relations.owl#contains");
				if(isContainment) {
					result.add(method);
				}
			}
		}
		return result;
	}
}
