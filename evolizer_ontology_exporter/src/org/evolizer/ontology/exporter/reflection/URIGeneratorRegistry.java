package org.evolizer.ontology.exporter.reflection;

import java.util.LinkedList;
import java.util.List;

import org.evolizer.ontology.exporter.api.IURIGenerator;

public class URIGeneratorRegistry {
	private static final IURIGenerator DEFAULT_GENERATOR = new DefaultURIGenerator();
	private static List<IURIGenerator> generators = new LinkedList<IURIGenerator>();

	public static IURIGenerator getConverterFor(Class<?> type) {
		for (IURIGenerator converter : generators) {
			if (converter.canGenerate(type)) {
				return converter;
			}
		}

		return DEFAULT_GENERATOR;
	}

	public static void register(IURIGenerator converter) {
		if (!generators.contains(converter)) {
			generators.add(0, converter);
		}
	}
}
