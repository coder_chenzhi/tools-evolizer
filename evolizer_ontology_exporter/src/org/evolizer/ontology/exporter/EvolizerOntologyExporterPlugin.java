package org.evolizer.ontology.exporter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.core.logging.base.PluginLogManager;
import org.evolizer.ontology.exporter.api.IURIGenerator;
import org.evolizer.ontology.exporter.api.IRDFPropertyConverter;
import org.evolizer.ontology.exporter.reflection.URIGeneratorRegistry;
import org.evolizer.ontology.exporter.reflection.converter.ConverterRegistry;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class EvolizerOntologyExporterPlugin extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.evolizer.ontology";

	// The shared instance
	private static EvolizerOntologyExporterPlugin plugin;

	// The path to the log4j.properties file
	private static final String LOG_PROPERTIES_FILE = "config/log4j.properties";

	// The log manager
	private PluginLogManager fLogManager;

	/**
	 * The constructor
	 */
	public EvolizerOntologyExporterPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		configureLogger();
		initPropertyConverters();
		initURIGenerators();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static EvolizerOntologyExporterPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the log manager.
	 * 
	 * @return the log manager.
	 */
	public static PluginLogManager getLogManager() {
		return getDefault().fLogManager;
	}

	/**
	 * Opens a file located within the plugin-bundle
	 * 
	 * @param filePath
	 *            relative path of the file starting
	 * @return an InputStream reading the specifed file
	 * @throws IOException
	 *             if file could not be opened
	 */
	public static InputStream openBundledFile(String filePath)
			throws IOException {
		return EvolizerOntologyExporterPlugin.getDefault().getBundle().getEntry(filePath)
				.openStream();
	}

	/**
	 * Configures logging
	 * 
	 */
	private void configureLogger() {
		try {
			InputStream propertiesInputStream = openBundledFile(LOG_PROPERTIES_FILE);

			if (propertiesInputStream != null) {
				Properties props = new Properties();
				props.load(propertiesInputStream);
				propertiesInputStream.close();

				// Hack: Allows us, to configure hibernate logging independently
				// from other stuff.
				PropertyConfigurator.configure(props);

				fLogManager = new PluginLogManager(this, props);
			}

			propertiesInputStream.close();
		}

		catch (Exception e) {
			String message = "Error while initializing log properties."
					+ e.getMessage();

			IStatus status = new Status(IStatus.ERROR, getDefault().getBundle()
					.getSymbolicName(), IStatus.ERROR, message, e);
			getLog().log(status);

			throw new EvolizerRuntimeException(
					"Error while initializing log properties.", e);
		}
	}
	
	private void initPropertyConverters() throws CoreException {
		for (final IRDFPropertyConverter converter : collectClassesFor("org.evolizer.ontology.exporter.propertyConverter", "converter_class", IRDFPropertyConverter.class)) {
//			ISafeRunnable runnable = new ISafeRunnable() {
//
//				public void handleException(Throwable exception) {
//					// TODO exception handling
//				}
//
//				public void run() throws Exception {
					ConverterRegistry.register(converter);
//				}
//
//			};
//
//			SafeRunner.run(runnable);
		}
	}
	
	private void initURIGenerators() throws CoreException {
		for (final IURIGenerator generator : collectClassesFor("org.evolizer.ontology.exporter.uriGenerator", "generator_class", IURIGenerator.class)) {
//			ISafeRunnable runnable = new ISafeRunnable() {
//
//				public void handleException(Throwable exception) {
//					// TODO exception handling
//				}
//
//				public void run() throws Exception {
					URIGeneratorRegistry.register(generator);
//				}
//
//			};
//
//			SafeRunner.run(runnable);
		}
	}
	
	@SuppressWarnings("unchecked")
	private <T> List<T> collectClassesFor(String strategyExtensionId, String propertyName, Class<T> clazz) throws CoreException {
        List<T> extensions = new ArrayList<T>();
		
        IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(strategyExtensionId);
        
        Arrays.sort(config, new Comparator<IConfigurationElement>(){
			@Override
			public int compare(IConfigurationElement e1, IConfigurationElement e2) {
				String order1 = ((IConfigurationElement) e1).getAttribute("order");
				String order2 = ((IConfigurationElement) e1).getAttribute("order");
				
				return order1.compareTo(order2);
			}
		});
        
        for (IConfigurationElement configElement : config) {
            Object obj = configElement.createExecutableExtension(propertyName);
            extensions.add((T)obj);
        }
        
        return extensions;
    }
}