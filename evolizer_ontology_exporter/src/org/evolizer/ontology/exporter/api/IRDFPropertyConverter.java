package org.evolizer.ontology.exporter.api;

import java.util.Collection;

import org.evolizer.ontology.provider.util.Statement;

/**
 * @author wuersch
 *
 */
public interface IRDFPropertyConverter {
	public boolean canConvert(Class<?> type);
	public Collection<Statement<?, ?>> convert(Object instance, String propertyURI, Object value);
}
