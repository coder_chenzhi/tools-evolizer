package org.evolizer.ontology.provider;

import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * Basically, this class wrapps a Jena OntModel. It loads all Evolizer Ontologies and provides
 * several convenience methods working with them.
 * 
 * TODO Refactor to factory or at least singleton.
 * 
 * @author wuersch
 *
 */
public class OntologyProvider {
//	private static String RELATION_ONTOLOGY_PATH = "./ontologies/relations.owl";
//	private static String CVS_ONTOLOGY_PATH = "./ontologies/cvs.owl";
//	
	private static OntModel model;
	
	// TODO shouldn't be hard coded.
	static {
//		OntDocumentManager docManager = OntDocumentManager.getInstance();
//
//		// Do not check url, but use local copy of ontologies instead:
//		docManager.addAltEntry("http://www.evolizer.org/ontology/relations.owl", RELATION_ONTOLOGY_PATH);
//		docManager.addAltEntry("http://www.evolizer.org/ontology/cvs.owl", CVS_ONTOLOGY_PATH);
		
		OntModelSpec s = new OntModelSpec(OntModelSpec.OWL_DL_MEM);
		model = ModelFactory.createOntologyModel(s, null);
		
//		initModels();
	}
	
	/**
	 * Checks whether <code>childURI</code> has a super property <code>ancestorURI</code>.
	 * If <code>direct == false</code>, this can be any ancestor, otherwhise <code>ancestorURI</code>
	 * has to be the direct super property of <code>childURI</code>.
	 * 
	 * @param childURI
	 * @param ancestorURI
	 * @param direct
	 * @return
	 */
	public static boolean hasSuperProperty(String childURI, String ancestorURI, boolean direct) {
		OntProperty childProperty = asProperty(childURI);
		OntProperty ancestorProperty =  asProperty(ancestorURI);
		
		if(childProperty == null || ancestorProperty == null) {
			return false;
		}
		
		return childProperty.hasSubProperty(ancestorProperty, direct);
	}
	
	/**
	 * Checks whether <code>childURI</code> has a super property <code>ancestorURI</code> somewhere
	 * along the inheritance hierarchy.
	 * 
	 * @param childURI
	 * @param ancestorURI
	 * @return
	 */
	public static boolean hasSuperProperty(String childURI, String ancestorURI) {
		return hasSuperProperty(childURI, ancestorURI, false);
	}
	
	/**
	 * Finds all the subclasses for a given superclass in an ontology. The superclass is specified by
	 * an uri.
	 * 
	 * @param uriReference
	 * @return
	 */
	public static List<String> getURIsOfSubClassesFor(String uriReference) {
		List<String> result = new ArrayList<String>();
		
		ExtendedIterator<OntClass> iter = model.getOntClass(uriReference).listSubClasses();
		while (iter.hasNext()) {
			OntClass c = iter.next();
			result.add(c.getURI());
		}
		
		return result;
	}
	
	/**
	 * Converts an uri into a property.
	 * 
	 * @param uri
	 * @return
	 */
	private static OntProperty asProperty(String uri) {
		return model.getOntProperty(uri);
	}
	
//	/**
//	 * 
//	 */
//	private static void initModels() {
//		readModel(RELATION_ONTOLOGY_PATH);
//		readModel(CVS_ONTOLOGY_PATH);
//	}
	
//	/**
//	 * Loads an existing ontology from a file into a Jena model.
//	 * 
//	 * @param path
//	 */
//	private static void readModel(String path) {
//		try {
//			InputStream in = EvolizerOntologyExporterPlugin.openBundledFile(path);
//			model.read(in, "");
//			in.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	/**
	 * Returns the last segment of an uri, e.g. http://evolizer.org/ontology#[myresource].
	 * 
	 * @param uriReference
	 * @return
	 */
	public static String fragmentIdentifier(String uriReference) {
		String fragmentIdentifier = uriReference.substring(uriReference.lastIndexOf("#") + 1, uriReference.length());
		
		return fragmentIdentifier;
	}
	
	/**
	 * Adds a resource to the model. The resource is specified by an fragmentIdentifier and a type (usually the resource is a class).
	 * 
	 * @param model
	 * @param fragmentIdentifier
	 * @param type
	 * @return
	 */
	public static Resource rdfInstanceOf(Model model, String fragmentIdentifier, Resource type) {
		return model.createResource(fragmentIdentifier, type);
	}
}
