package org.evolizer.ontology.provider.util;

import java.net.URI;
import java.net.URISyntaxException;

public class Statement<S, O>{
	private S subject;
	private URI predicate;
	private O object;
	
	private ISerializationStrategy<Statement<?, ?>> serializer;
	

	public Statement(S subject, URI predicate, O object) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}
	
	public Statement(S subject, String predicateString, O object) throws URISyntaxException {
		this.subject = subject;
		this.predicate = new URI(predicateString);
		this.object = object;
	}
	
	public S getSubject() {
		return subject;
	}

	public URI getPredicate() {
		return predicate;
	}

	public O getObject() {
		return object;
	}

	public void setSubject(S subject) {
		this.subject = subject;
	}
	
	public void setPredicate(URI predicate) {
		this.predicate = predicate;
	}

	public void setObject(O object) {
		this.object = object;
	}
	
	public void setSerializer(ISerializationStrategy<Statement<?, ?>> serializer) {
		this.serializer = serializer;
	}

	public boolean isComplete() {
		return subject != null && predicate != null && object != null;
	}
	
	public void serialize() {
		serializer.serialize(this);
	}

	@Override
	public String toString() {
		return "<" + subject.getClass().getSimpleName() + ", " + predicate.toString() + ", " + object.getClass().getSimpleName() + ">";
	}
}
