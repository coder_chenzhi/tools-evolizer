package org.evolizer.ontology.provider.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.evolizer.ontology.annotations.rdf;
import org.evolizer.ontology.exporter.EvolizerOntologyExporterPlugin;
import org.evolizer.ontology.exporter.reflection.RDFReflectionUtil;

import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

public class RDFStatementSerializer implements ISerializationStrategy<Statement<?, ?>>{
	private final static Logger logger = EvolizerOntologyExporterPlugin.getLogManager().getLogger(RDFStatementSerializer.class.getName());
	
	private Model model;
		
	public RDFStatementSerializer() {
		OntModelSpec s = new OntModelSpec(OntModelSpec.OWL_DL_MEM);
		model = ModelFactory.createOntologyModel(s, null);
		// TODO Prefix handling?
		
		// TODO replace this later:
//		OntDocumentManager docManager = OntDocumentManager.getInstance();
//
//		docManager.addAltEntry("http://www.evolizer.org/ontology/java_model.owl", "./ontologies/java_model.owl");
//		docManager.addAltEntry("http://www.christiankaiser.ch/2005/ginseng-def.owl", "./ontologies/local_ginseng.owl");
//		try {
//			loadOntologies("./ontologies/java_model.owl");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		// end of snippet that needs to be replaced
	}

	public void loadOntologies(String... paths) throws IOException {
			for (int i = 0; i < paths.length; i++) {
				loadOntology(paths[i]);	
			}
	}
	
	public void loadOntology(String path) throws IOException {
				// TODO Only works for ontologies within the exporter's plug-in for now.
				InputStream in = EvolizerOntologyExporterPlugin.openBundledFile(path);
				model.read(in, "");
				in.close();				
	}
	
	public void serialize(Statement<?, ?> statement) {
		if(statement.isComplete()) {
			Resource subject = createSubject(statement);
			RDFNode object = createObject(statement);
			Property predicate = createPredicate(statement);
			subject.addProperty(predicate, object);
			
		} else {
			logger.error("Found incomplete statement while serializing: " + statement.toString());
		}
	}
	
	public Model getModel() {
		return model;
	}

	private Property createPredicate(Statement<?, ?> statement) {
		Property predicate = model.createProperty(statement.getPredicate().toString());
		return predicate;
	}

	private Resource createSubject(Statement<?, ?> statement) {
		Resource subject = RDFReflectionUtil.rdfInstanceOf(model, statement.getSubject());
		return subject;
	}

	private RDFNode createObject(Statement<?, ?> statement) {
		RDFNode result;
		
		Object objInstance = statement.getObject();
		Class<?> classOfObject = objInstance.getClass();
		
		if(classOfObject.isAnnotationPresent(rdf.class)) {
			result = RDFReflectionUtil.rdfInstanceOf(model, statement.getObject());
		} else {
			result = model.createLiteral(objInstance.toString());
		}
		
		return result;
	}
}
