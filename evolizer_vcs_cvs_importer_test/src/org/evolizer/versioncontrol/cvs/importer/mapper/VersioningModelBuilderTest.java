package org.evolizer.versioncontrol.cvs.importer.mapper;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.fs.File;
import org.evolizer.versioncontrol.cvs.importer.AbstractEvolizerCVSImporterTest;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;
import org.evolizer.versioncontrol.cvs.importer.mapper.VersioningModelBuilder;
import org.evolizer.versioncontrol.cvs.importer.parser.CVSParser;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * @author Michael Wuersch, Andreas Jetter
 *
 */
public class VersioningModelBuilderTest extends AbstractEvolizerCVSImporterTest{
	private static EvolizerSessionHandler fSessionHandler;
	private static IEvolizerSession fSession;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		AbstractEvolizerCVSImporterTest.setUpBeforeClass();
		fSessionHandler = EvolizerSessionHandler.getHandler();
		fSessionHandler.initSessionFactory(fDBUrl, fDBUser, fDBPasswd);
		fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		fSession = fSessionHandler.getCurrentSession(fDBUrl);
		
		try{
			mapTestLogToDatabase();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws EvolizerException {
		fSession.close();
		fSessionHandler.cleanupHibernateSessions();
		fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		fSessionHandler = null;
	}
	
	/*
	 	
	@Before
	public void setUp() throws Exception {
		fSessionHandler = EvolizerSessionHandler.getHandler();
		fSessionHandler.initSessionFactory(fDBUrl, fDBUser, fDBPasswd);
		fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		createRevisions();
	}

	@After
	public void tearDown() throws Exception {
		fSessionHandler.cleanupHibernateSessions();
		fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		fSessionHandler = null;
	}
	 */

	/**
	 * Reads a sample log and maps it to the database.
	 * @throws IOException 
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws EvolizerException 
	 */
	private static void  mapTestLogToDatabase() throws IOException, EvolizerException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(EvolizerCVSPlugin.openFile("./test_data/mozilla_log_snippet.txt")));
		
		CVSParser parser = new CVSParser();
		parser.addLogEntryListener(new VersioningModelBuilder(fSession, null /* no monitor during testing*/, ""));
		
		while (reader.ready()) {
			parser.parseLine(reader.readLine());
		}
		
		reader.close();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
		
	}
	
	/**
	 * Tests if three files are mapped to db.
	 *
	 */
	public void testNoFiles() {
		int fileCount = fSession.uniqueResult("select count(*) from File as file", Integer.class);
		assertEquals(3, fileCount);
	}
	
	@Test
	public void testSampleRevision1() throws ParseException{
		Revision revision = fSession.uniqueResult("select revision from Revision as revision " +
										  "join revision.file as file " +
										  "where file.path like '%nsViewportFrame.cpp' " +
										  "and revision.number='1.63'", Revision.class);

		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = df.parse("2003/02/22 20:13:12");
		
		assertEquals(date, revision.getCreationTime());
		assertTrue(revision.getAuthor().getNickNames().contains("bzbarsky%mit.edu"));
		assertEquals("bzbarsky%mit.edu", revision.getAuthorNickName());
		assertEquals("Exp", revision.getState());
		assertEquals(39, revision.getLinesAdd());
		assertEquals(47, revision.getLinesDel());
		assertEquals("Reflow main content before reflowing fixed-pos frames so that the placeholders\nare in the right places.  Bug 90270 and dependencies.  r+sr=roc+moz", revision.getCommitMessage());	
		//TODO Add some tests for other revisions.
	}
	
	@Test
	public void testSampleRevision2() throws ParseException{
		Revision revision = fSession.uniqueResult("select revision from Revision as revision " +
										  "join revision.file as file " +
										  "where file.path like '%nsViewportFrame.cpp' " +
										  "and revision.number='1.75.6.4'", Revision.class);

		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = df.parse("2005/05/14 00:12:21");
		
		assertEquals(date, revision.getCreationTime());
		assertTrue(revision.getAuthor().getNickNames().contains("dbaron%dbaron.org"));
		assertEquals("Exp", revision.getState());
		assertEquals(18, revision.getLinesAdd());
		assertEquals(0, revision.getLinesDel());
		assertEquals("ViewportFrame does need GetMinWidth/GetPrefWidth.", revision.getCommitMessage());	
	}
	
	@Test
	public void testSampleRevision3() throws ParseException{
		Revision revision = fSession.uniqueResult("select revision from Revision as revision " +
										  "join revision.file as file " +
										  "where file.path like '%nsViewportFrame.cpp' " +
										  "and revision.number='1.1'", Revision.class);

		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = df.parse("1999/01/24 20:36:46");
		
		assertEquals(date, revision.getCreationTime());
		assertTrue(revision.getAuthor().getNickNames().contains("troy%netscape.com"));
		assertEquals("Exp", revision.getState());
		assertEquals(0, revision.getLinesAdd());
		assertEquals(0, revision.getLinesDel());
		assertEquals("Initial check-in", revision.getCommitMessage());	
	}
	
	@Test
	public void testSampleRevision4() throws ParseException{
		Revision revision = fSession.uniqueResult("select revision from Revision as revision " +
										  "join revision.file as file " +
										  "where file.path like '%punct_marks.ccmap' " +
										  "and revision.number='1.5'", Revision.class);
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = df.parse("2004/10/08 05:00:53");
		
		assertEquals(date, revision.getCreationTime());
		assertTrue(revision.getAuthor().getNickNames().contains("jshin%mailaps.org"));
		assertEquals("Exp", revision.getState());
		assertEquals(509, revision.getLinesAdd());
		assertEquals(144, revision.getLinesDel());
		assertEquals("bug 263411 : :first-letter pseudo-element doesn't include all punctuation marks in the Unicode (r/sr=dbaron)", revision.getCommitMessage());	
	}
	
	/**
	 * Tests if mapped files have the correct file names.
	 *
	 */
	
	@Test
	public void testFileNames() {
		Long fileCount = fSession.uniqueResult("select count(*) from VersionedFile as file where file.path in ('/cvsroot/mozilla/layout/generic/nsViewportFrame.cpp', '/cvsroot/mozilla/layout/generic/nsViewportFrame.h', '/cvsroot/mozilla/layout/generic/punct_marks.ccmap')", Long.class);
		assertEquals(new Long(3), fileCount);
	}
	
	/**
	 * @see VersioningModelBuilderTest#testNoRevisionsForFiles()
	 * 
	 * @param fileName
	 * @param expectedRevisionCount
	 */
	private void assertNoRevisionsForFiles(String fileName, Long expectedRevisionCount){
		Long revisionCount = fSession.uniqueResult("select count(revision) from VersionedFile as file " +
										  "join file.revisions as revision " +
										  "where file.path like '%" + fileName + "'", Long.class);

		assertEquals(expectedRevisionCount, revisionCount);
	}

	/**
	 * Tests if each of the three files is linked to the correct amount of revisions.
	 * 
	 * @param fileName
	 * @param expectedRevisionCount
	 */
	@Test
	public void testNoRevisionsForFiles(){
		assertNoRevisionsForFiles("nsViewportFrame.cpp", 85l);
		assertNoRevisionsForFiles("nsViewportFrame.h", 18l);
		assertNoRevisionsForFiles("punct_marks.ccmap", 6l);
	}
	
	@Test
	public void testNoBranches() {
		Long branchCount = fSession.uniqueResult("select count(branch.id) from Branch as branch", Long.class);
		
		assertEquals(new Long(15), branchCount);
	}
	
	@Test
	public void testBranchNames() {
		String queryString = "select distinct count(branch.id) from Branch as branch " +
							 "where branch.name in ( " +
							 	 "'DOM_AGNOSTIC_BRANCH', " +
								 "'MOZILLA_1_8_BRANCH', " +
								 "'REFLOW_20050804_BRANCH', " +
								 "'SPLITWINDOW_20050714_BRANCH', " +
								 "'BSMEDBERG_SECURITY_PLAYGROUND_20050512_BRANCH', " +
								 "'REFLOW_20050429_BRANCH', " +
								 "'SOFTWARE_UPDATE_20050428_BRANCH', " +
								 "'REFLOW_20050315_BRANCH', " +
								 "'WEBFORMS_20050202_BRANCH', " +
								 "'PREFERENCES_20050201_BRANCH', " +
								 "'REFLOW_20050111_BRANCH', " +
								 "'COMMANDLINES_20050109_BRANCH', " +
								 "'XFORMS_20050106_BRANCH', " +
								 "'PREFERENCES_20050101_BRANCH', " +
								 "'REFLOW_20041213_BRANCH'" +
							 ")";
		
		Long branchCount = fSession.uniqueResult(queryString, Long.class);
		assertEquals(new Long(15), branchCount);
	}
	
	/**
	 * Branches know which revisions from which files belong to them. This method tests whether the connection is done properly.
	 */
	@Test
	public void testConnectionsBetweenBranchesAndRevisions() {
		assertCorrectConnectionBetweenBranchesAndRevisions("REFLOW_20050804_BRANCH", "nsViewportFrame.cpp", "1.75.12.1", "1.75.12.2");
		assertCorrectConnectionBetweenBranchesAndRevisions("REFLOW_20050429_BRANCH", "nsViewportFrame.cpp", "1.75.6.1", "1.75.6.5");
		assertCorrectConnectionBetweenBranchesAndRevisions("REFLOW_20050315_BRANCH", "nsViewportFrame.cpp", "1.75.2.1", "1.75.2.1");
		assertCorrectConnectionBetweenBranchesAndRevisions("REFLOW_20050111_BRANCH", "nsViewportFrame.cpp", "1.73.6.1", "1.73.6.2");
		
		assertCorrectConnectionBetweenBranchesAndRevisions("REFLOW_20050111_BRANCH", "nsViewportFrame.h", "3.11.6.1", "3.11.6.1");
		assertCorrectConnectionBetweenBranchesAndRevisions("REFLOW_20050804_BRANCH", "nsViewportFrame.h", "3.13.12.1", "3.13.12.1");
		assertCorrectConnectionBetweenBranchesAndRevisions("REFLOW_20050429_BRANCH", "nsViewportFrame.h", "3.13.6.1", "3.13.6.2");
		assertCorrectConnectionBetweenBranchesAndRevisions("REFLOW_20050315_BRANCH", "nsViewportFrame.h", "3.13.2.1", "3.13.2.1");
	}
	
	/**
	 * @see VersioningModelBuilderTest#testConnectionsBetweenBranchesAndRevisions()
	 * 
	 * @param branchName
	 * @param fileName
	 * @param firstRevisionNumber
	 * @param lastRevisionNumber
	 */
	public void assertCorrectConnectionBetweenBranchesAndRevisions(String branchName, String fileName, String firstRevisionNumber, String lastRevisionNumber) {
		List<String> revisionNumbers = fSession.query("select revision.number from Branch as branch " +
				  "join branch.revisions as revision " +
				  "where branch.name='"+ branchName +
				  "' and revision.file.path like '%"+fileName +"'", String.class);
		String parent = firstRevisionNumber.substring(0, firstRevisionNumber.lastIndexOf('.') + 1);
		int firstRevNumber = Integer.parseInt(firstRevisionNumber.substring(firstRevisionNumber.lastIndexOf('.') + 1, firstRevisionNumber.length()));
		int lastRevNumber = Integer.parseInt(lastRevisionNumber.substring(lastRevisionNumber.lastIndexOf('.') + 1, lastRevisionNumber.length()));
		
		List<String> revNos = new Vector<String>();
		for(int i = firstRevNumber; i <= lastRevNumber; i++) {
			revNos.add(parent + i);
		}
		
		assertTrue(revisionNumbers.containsAll(revNos));
		assertTrue(revNos.containsAll(revisionNumbers));
	}
	
	@Test
	public void testNoReleases() {
		Long releaseCount = fSession.uniqueResult("select count(release.id) from Release as release", Long.class);
		
		assertEquals(new Long(23), releaseCount);
	}
	
	@Test
	public void testReleaseNames() {		
		String queryString = "select distinct count(release.id) from Release as release " +
							 "where release.name in ( " +
							 	 "'DOM_AGNOSTIC_BASE', " +
								 "'MOZILLA_1_8_BASE', " +
								 "'REFLOW_20050804_BASE', " +
								 "'SPLITWINDOW_20050714_INITIAL_TRUNK_LANDING', " +
								 "'SPLITWINDOW_20050714_BASE', " +
								 "'MOZILLA_1_8b3_RELEASE', " +
								 "'THUNDERBIRD_1_1a2_RELEASE', " +
								 "'FIREFOX_1_1a2_RELEASE', " +
								 "'THUNDERBIRD_1_1a1_RELEASE', " +
								 "'FIREFOX_1_1a1_RELEASE', " +
								 "'BSMEDBERG_SECURITY_PLAYGROUND_20050512_BASE', " +
								 "'REFLOW_20050429_BASE', " +
								 "'SOFTWARE_UPDATE_20050428_BASE', " +
								 "'REFLOW_20050315_BASE', " +
								 "'MOZILLA_1_8b1_RELEASE', " +
								 "'WEBFORMS_20050202_BASE', " +
								 "'PREFERENCES_20050201_BASE', " +
								 "'MOZILLA_1_8a6_RELEASE', " +
								 "'REFLOW_20050111_BASE', " +
								 "'COMMANDLINES_20050109_BASE', " +
								 "'XFORMS_20050106_BASE', " +
								 "'PREFERENCES_20050101_BASE', " +
								 "'REFLOW_20041213_BASE'" +
							 ")";
		
		Long releaseCount = fSession.uniqueResult(queryString, Long.class);
		assertEquals(new Long(23), releaseCount);
	}
	
	/**
	 * Releases know their revisions. This method tests whether the connection from releases to revisions is done properly.
	 */
	
	@Test
	public void testConnectionsBetweenReleasesAndRevisions() {
		assertCorrectConnectionBetweenReleasesAndRevisions("DOM_AGNOSTIC_BASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8_BASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050804_BASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("SPLITWINDOW_20050714_INITIAL_TRUNK_LANDING", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("SPLITWINDOW_20050714_BASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8b3_RELEASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("THUNDERBIRD_1_1a2_RELEASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("FIREFOX_1_1a2_RELEASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("THUNDERBIRD_1_1a1_RELEASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("FIREFOX_1_1a1_RELEASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("BSMEDBERG_SECURITY_PLAYGROUND_20050512_BASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050429_BASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("SOFTWARE_UPDATE_20050428_BASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050315_BASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8b1_RELEASE", "nsViewportFrame.cpp", "1.75");
		assertCorrectConnectionBetweenReleasesAndRevisions("WEBFORMS_20050202_BASE", "nsViewportFrame.cpp", "1.73");
		assertCorrectConnectionBetweenReleasesAndRevisions("PREFERENCES_20050201_BASE", "nsViewportFrame.cpp", "1.73");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8a6_RELEASE", "nsViewportFrame.cpp", "1.73");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050111_BASE", "nsViewportFrame.cpp", "1.73");
		assertCorrectConnectionBetweenReleasesAndRevisions("COMMANDLINES_20050109_BASE", "nsViewportFrame.cpp", "1.73");
		assertCorrectConnectionBetweenReleasesAndRevisions("XFORMS_20050106_BASE", "nsViewportFrame.cpp", "1.73");
		assertCorrectConnectionBetweenReleasesAndRevisions("PREFERENCES_20050101_BASE", "nsViewportFrame.cpp", "1.72");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20041213_BASE", "nsViewportFrame.cpp", "1.72");
		
		assertCorrectConnectionBetweenReleasesAndRevisions("DOM_AGNOSTIC_BASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8_BASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050804_BASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("SPLITWINDOW_20050714_INITIAL_TRUNK_LANDING", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("SPLITWINDOW_20050714_BASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8b3_RELEASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("THUNDERBIRD_1_1a2_RELEASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("FIREFOX_1_1a2_RELEASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("THUNDERBIRD_1_1a1_RELEASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("FIREFOX_1_1a1_RELEASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("BSMEDBERG_SECURITY_PLAYGROUND_20050512_BASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050429_BASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("SOFTWARE_UPDATE_20050428_BASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050315_BASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8b1_RELEASE", "nsViewportFrame.h", "3.13");
		assertCorrectConnectionBetweenReleasesAndRevisions("WEBFORMS_20050202_BASE", "nsViewportFrame.h", "3.11");
		assertCorrectConnectionBetweenReleasesAndRevisions("PREFERENCES_20050201_BASE", "nsViewportFrame.h", "3.11");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8a6_RELEASE", "nsViewportFrame.h", "3.11");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050111_BASE", "nsViewportFrame.h", "3.11");
		assertCorrectConnectionBetweenReleasesAndRevisions("COMMANDLINES_20050109_BASE", "nsViewportFrame.h", "3.11");
		assertCorrectConnectionBetweenReleasesAndRevisions("XFORMS_20050106_BASE", "nsViewportFrame.h", "3.11");
		assertCorrectConnectionBetweenReleasesAndRevisions("PREFERENCES_20050101_BASE", "nsViewportFrame.h", "3.10");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20041213_BASE", "nsViewportFrame.h", "3.10");
		
		assertCorrectConnectionBetweenReleasesAndRevisions("DOM_AGNOSTIC_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050804_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("SPLITWINDOW_20050714_INITIAL_TRUNK_LANDING", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("SPLITWINDOW_20050714_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8b3_RELEASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("THUNDERBIRD_1_1a2_RELEASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("FIREFOX_1_1a2_RELEASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("THUNDERBIRD_1_1a1_RELEASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("FIREFOX_1_1a1_RELEASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("BSMEDBERG_SECURITY_PLAYGROUND_20050512_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050429_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("SOFTWARE_UPDATE_20050428_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050315_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8b1_RELEASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("WEBFORMS_20050202_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("PREFERENCES_20050201_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("MOZILLA_1_8a6_RELEASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20050111_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("COMMANDLINES_20050109_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("XFORMS_20050106_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("PREFERENCES_20050101_BASE", "punct_marks.ccmap", "1.6");
		assertCorrectConnectionBetweenReleasesAndRevisions("REFLOW_20041213_BASE", "punct_marks.ccmap", "1.6");		
	}
	
	/**
	 * @see VersioningModelBuilderTest#testConnectionsBetweenReleasesAndRevisions()
	 * 
	 * @param releaseName
	 * @param fileName
	 * @param expectedRevisionNumber
	 */
	private void assertCorrectConnectionBetweenReleasesAndRevisions(String releaseName, String fileName, String expectedRevisionNumber) {
		String revisionNumber = fSession.uniqueResult("select revision.number from Release as release " +
				  "join release.revisions as revision " +
				  "where release.name='" + releaseName +
				  "' and revision.file.path like '%" + fileName + "'", String.class);
						
		assertEquals(expectedRevisionNumber, revisionNumber);
		//TODO Write test to ensure that the other direction also works (revision --> release).
	}
	
	
	/**
	 * If mapped correctly, it should be possible to use revision.getPreviousRevision (revision.getNextRevision())
	 * to get the predecessor (successor) of the current revision. If a revision has no predecessor/successor then
	 * previous-/nextRevision should return null. This test asserts this functionality for the revisions of all three files.
	 */
	
	@Test
	public void testIfRevisionNavigationWorks() {
		assertCorrectRevisionOrder("nsViewportFrame.cpp", "1.1", "1.75");
		assertCorrectRevisionOrder("nsViewportFrame.cpp", "1.73.6.1", "1.73.6.2");
		assertCorrectRevisionOrder("nsViewportFrame.cpp", "1.75.12.1", "1.75.12.2");
		assertCorrectRevisionOrder("nsViewportFrame.cpp", "1.75.6.1", "1.75.6.5");
		assertCorrectRevisionOrder("nsViewportFrame.cpp", "1.75.2.1", "1.75.2.1");
		
		assertCorrectRevisionOrder("nsViewportFrame.h", "3.1", "3.13");
		assertCorrectRevisionOrder("nsViewportFrame.h", "3.11.6.1", "3.11.6.1");
		assertCorrectRevisionOrder("nsViewportFrame.h", "3.13.12.1", "3.13.12.1");
		assertCorrectRevisionOrder("nsViewportFrame.h", "3.13.6.1", "3.13.6.2");
		assertCorrectRevisionOrder("nsViewportFrame.h", "3.13.2.1", "3.13.2.1");
		
		assertCorrectRevisionOrder("punct_marks.ccmap", "1.1", "1.6");
	}
	


	/**
	 * Convenience method for testIfRevisionNavigationWorks()
	 * 
	 * @see VersioningModelBuilderTest#testIfRevisionNavigationWorks()
	 * 
	 * @param fileName 
	 * @param firstRevisionNumber
	 * @param lastRevisionNumber
	 */
	private void assertCorrectRevisionOrder(String fileName, String firstRevisionNumber, String lastRevisionNumber) {
		String parent = firstRevisionNumber.substring(0, firstRevisionNumber.lastIndexOf('.') + 1);
		int firstRevNumber = Integer.parseInt(firstRevisionNumber.substring(firstRevisionNumber.lastIndexOf('.') + 1, firstRevisionNumber.length()));
		int lastRevNumber = Integer.parseInt(lastRevisionNumber.substring(lastRevisionNumber.lastIndexOf('.') + 1, lastRevisionNumber.length()));
		
		Revision firstRevision = getRevisionByNumber(fileName, firstRevisionNumber);
		assertNull(firstRevision.getPreviousRevision());
		Revision nextRevision = firstRevision;
		
		for(int i = firstRevNumber + 1; i <= lastRevNumber; i++) {
			nextRevision = nextRevision.getNextRevision();
			assertEquals(parent + i, nextRevision.getNumber());
		}
		
		assertNull(nextRevision.getNextRevision());
		
		Revision previousRevision = nextRevision;
		for(int i = lastRevNumber - 1; i >= firstRevNumber; i--){
			previousRevision = previousRevision.getPreviousRevision();
			assertEquals(parent + i, previousRevision.getNumber());
		}
		assertNull(previousRevision.getPreviousRevision());
	}
	
	/**
	 * Allows to specify a file name and a revision number to retrieve the corresponding <code>Revision</code>-object
	 * from database.
	 */
	private Revision getRevisionByNumber(String fileName, String revisionNumber) {
		return fSession.uniqueResult("select revision from VersionedFile as file " +
				  						  "join file.revisions as revision " +
				  						  "where file.path like '%" + fileName +
				  						  "'and revision.number='" + revisionNumber + "'", Revision.class);

	}
	
	@Test
	public void testNoSourceUnits(){
		Long k = fSession.uniqueResult("select count(source_unit.id) from File as source_unit", Long.class);
		assertEquals(new Long(8),k);
	}
	
	/**
	 * Tests if the correct number of directories has been mapped and if the directory/file-hierarchy was written
	 * correctly to the database.
	 */
	@Test
	public void testDirectories(){
		//testing quantity of directories
		Long i = fSession.uniqueResult("select count(directory) from Directory as directory", Long.class);
		assertEquals(new Long(5),i);
		//testing quantity of files
		Long j = fSession.uniqueResult("select count(file) from VersionedFile as file", Long.class);
		assertEquals(new Long(3),j);		
		//testing directory "layout/"
		Directory testDir1 = fSession.uniqueResult("from Directory as dir where dir.path='/cvsroot/mozilla/layout'", Directory.class);
		assertEquals("layout",testDir1.getName());
		assertEquals(1,testDir1.getChildren().size());
		assertEquals("", testDir1.getParentDirectory().getParentDirectory().getParentDirectory().getName());
		//testing directory "layout/generic/"
		Directory testDir2 = fSession.uniqueResult("from Directory as directory where directory.path ='/cvsroot/mozilla/layout/generic'", Directory.class);
		assertEquals("generic",testDir2.getName());
		assertEquals("layout",testDir2.getParentDirectory().getName());
		assertEquals("/cvsroot/mozilla/layout",testDir2.getParentDirectory().getPath());
		assertEquals(3,testDir2.getChildren().size());
		List<String> expectedChildrenNames = new Vector<String>();
		expectedChildrenNames.add("nsViewportFrame.cpp");
		expectedChildrenNames.add("nsViewportFrame.h");
		expectedChildrenNames.add("punct_marks.ccmap");
		List<String> childrenNames = new Vector<String>();
		for(File dir : testDir2.getChildren()){
			childrenNames.add(dir.getName());
		}
		assertTrue(childrenNames.containsAll(expectedChildrenNames));
		assertTrue(expectedChildrenNames.containsAll(childrenNames));	
	}
	
	/**
	 * Tests if each file has the correct name and parent.
	 *
	 */
	@Test
	public void testFiles(){
//		testing file layout/generic/nsViewportFrame.cpp
		File f = fSession.uniqueResult("from VersionedFile as file where file.path = '/cvsroot/mozilla/layout/generic/nsViewportFrame.cpp'", File.class);
		assertEquals("nsViewportFrame.cpp",f.getName());
		assertEquals("/cvsroot/mozilla/layout/generic",f.getParentDirectory().getPath());
		//testing file layout/generic/nsViewportFrame.h
		File f2 = fSession.uniqueResult("from VersionedFile as file where file.path = '/cvsroot/mozilla/layout/generic/nsViewportFrame.h'", File.class);
		assertEquals("nsViewportFrame.h",f2.getName());
		assertEquals("/cvsroot/mozilla/layout/generic",f2.getParentDirectory().getPath());
		//testinig file layout/generic/punct_marks.ccmap

		File f3 = fSession.uniqueResult("from VersionedFile as file where file.path = '/cvsroot/mozilla/layout/generic/punct_marks.ccmap'", File.class);
		assertEquals("punct_marks.ccmap",f3.getName());
		assertEquals("/cvsroot/mozilla/layout/generic",f3.getParentDirectory().getPath());
	}
}