package org.evolizer.versioncontrol.cvs.importer.transactions;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.cvs.importer.AbstractEvolizerCVSImporterTest;
import org.evolizer.versioncontrol.cvs.importer.transactions.TransactionReconstructor;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TransactionReconstructorTest extends AbstractEvolizerCVSImporterTest{
	private EvolizerSessionHandler fSessionHandler;
	
	private Revision rev1;
	private Revision rev2;
	private Revision rev3;
	private Revision rev4;
	private Revision rev5;
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		AbstractEvolizerCVSImporterTest.tearDownAfterClass();
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		AbstractEvolizerCVSImporterTest.setUpBeforeClass();
	}
	
	@Before
	public void setUp() throws Exception {
		fSessionHandler = EvolizerSessionHandler.getHandler();
		fSessionHandler.initSessionFactory(fDBUrl, fDBUser, fDBPasswd);
		fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		createRevisions();
	}

	@After
	public void tearDown() throws Exception {
		fSessionHandler.cleanupHibernateSessions();
		fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		fSessionHandler = null;
	}
	
	private void createRevisions() throws ParseException {
		rev1 = new Revision("1.2");
		rev1.setCreationTime("1980/10/03 12:00:00"); //yyyy/MM/dd HH:mm:ss
		rev1.addNicknameToAuthor("mwuersch");
		rev1.setCommitMessage("mimimi");
		
		rev2 = new Revision("1.3");
		rev2.setCreationTime("1980/10/03 12:02:00");
		rev2.addNicknameToAuthor("mwuersch");
		rev2.setCommitMessage("mimimi");
		
		rev3 = new Revision("1.4");
		rev3.setCreationTime("1980/10/03 12:02:01");
		rev3.addNicknameToAuthor("mwuersch");
		rev3.setCommitMessage("mimimi");
		
		rev4 = new Revision("1.44");
		rev4.setCreationTime("1980/10/03 12:04:00");
		rev4.addNicknameToAuthor("mwuersch");
		rev4.setCommitMessage("mimimi");
		
		rev5 = new Revision("1.6");
		rev5.setCreationTime("1980/10/03 12:05:00");
		rev5.addNicknameToAuthor("mwuersch");
		rev5.setCommitMessage("mimimi");
		
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			s.startTransaction();
			s.saveObject(rev1);
			s.saveObject(rev2);
			s.saveObject(rev3);
			s.saveObject(rev4);
			s.saveObject(rev5);
			s.endTransaction();
		
			s.close();
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testTransactionReconstruction1(){
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			TransactionReconstructor.calculateCouplings(s, 0, 0, null);
			
			List<Transaction> transactions = s.query("from Transaction as ta order by ta.started", Transaction.class);
			
			assertEquals(5, transactions.size());
			
			Transaction ta1 = transactions.get(0);
			Set<Revision> revisions = ta1.getInvolvedRevisions();
			assertTrue(revisions.contains(rev1));
			assertFalse(revisions.contains(rev2));
			assertFalse(revisions.contains(rev3));
			assertFalse(revisions.contains(rev4));
			assertFalse(revisions.contains(rev5));
			
			Transaction ta2 = transactions.get(1);
			revisions = ta2.getInvolvedRevisions();
			assertFalse(revisions.contains(rev1));
			assertTrue(revisions.contains(rev2));
			assertFalse(revisions.contains(rev3));
			assertFalse(revisions.contains(rev4));
			assertFalse(revisions.contains(rev5));
			
			Transaction ta3 = transactions.get(2);
			revisions = ta3.getInvolvedRevisions();
			assertFalse(revisions.contains(rev1));
			assertFalse(revisions.contains(rev2));
			assertTrue(revisions.contains(rev3));
			assertFalse(revisions.contains(rev4));
			assertFalse(revisions.contains(rev5));
			
			Transaction ta4 = transactions.get(3);
			revisions = ta4.getInvolvedRevisions();
			assertFalse(revisions.contains(rev1));
			assertFalse(revisions.contains(rev2));
			assertFalse(revisions.contains(rev3));
			assertTrue(revisions.contains(rev4));
			assertFalse(revisions.contains(rev5));
			
			Transaction ta5 = transactions.get(4);
			revisions = ta5.getInvolvedRevisions();
			assertFalse(revisions.contains(rev1));
			assertFalse(revisions.contains(rev2));
			assertFalse(revisions.contains(rev3));
			assertFalse(revisions.contains(rev4));
			assertTrue(revisions.contains(rev5));	
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testTransactionReconstruction2(){
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			TransactionReconstructor.calculateCouplings(s, 120000, 120000, null);
			
			List<Transaction> transactions = s.query("from Transaction as ta order by ta.started", Transaction.class);
			
			assertEquals(3, transactions.size());
			
			Transaction ta1 = transactions.get(0);
			Set<Revision> revisions = ta1.getInvolvedRevisions();
			assertTrue(revisions.contains(rev1));
			assertTrue(revisions.contains(rev2));
			assertFalse(revisions.contains(rev3));
			assertFalse(revisions.contains(rev4));
			assertFalse(revisions.contains(rev5));
			
			Transaction ta2 = transactions.get(1);
			revisions = ta2.getInvolvedRevisions();
			assertFalse(revisions.contains(rev1));
			assertFalse(revisions.contains(rev2));
			assertTrue(revisions.contains(rev3));
			assertTrue(revisions.contains(rev4));
			assertFalse(revisions.contains(rev5));
			
			Transaction ta3 = transactions.get(2);
			revisions = ta3.getInvolvedRevisions();
			assertFalse(revisions.contains(rev1));
			assertFalse(revisions.contains(rev2));
			assertFalse(revisions.contains(rev3));
			assertFalse(revisions.contains(rev4));
			assertTrue(revisions.contains(rev5));
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testTransactionReconstruction3(){
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			TransactionReconstructor.calculateCouplings(s, 121000, 120000, null);
			
			List<Transaction> transactions = s.query("from Transaction as ta order by ta.started", Transaction.class);
			assertEquals(2, transactions.size());
			
			Transaction ta1 = transactions.get(0);
			Set<Revision> revisions = ta1.getInvolvedRevisions();
			assertTrue(revisions.contains(rev1));
			assertTrue(revisions.contains(rev2));
			assertTrue(revisions.contains(rev3));
			assertFalse(revisions.contains(rev4));
			assertFalse(revisions.contains(rev5));
			
			Transaction ta2 = transactions.get(1);
			revisions = ta2.getInvolvedRevisions();
			assertFalse(revisions.contains(rev1));
			assertFalse(revisions.contains(rev2));
			assertFalse(revisions.contains(rev3));
			assertTrue(revisions.contains(rev4));
			assertTrue(revisions.contains(rev5));
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}
}
