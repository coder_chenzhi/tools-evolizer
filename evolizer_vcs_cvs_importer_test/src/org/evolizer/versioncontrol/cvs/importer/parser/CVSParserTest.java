package org.evolizer.versioncontrol.cvs.importer.parser;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.List;

import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;
import org.evolizer.versioncontrol.cvs.importer.mapper.api.ILogEntryListener;
import org.evolizer.versioncontrol.cvs.importer.parser.CVSParser;
import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.LogEntry;
import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.RevisionEntry;
import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.SymbolicName;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests if the CVSParser works correctly.
 * 
 * @author Michael Wuersch, Andreas Jetter
 * 
 */
public class CVSParserTest {
    private BufferedReader reader;

    /**
     * Instance of the parser we'd like to test.
     */
    private CVSParser parser;

    /**
     * There are three calls for saveObject() in our test, one for each of the
     * three log entries in our test log.
     */
    private int pass = FIRST;

    /**
     * @see CVSParserTest#pass
     */
    private static final int FIRST = 0;
    /**
     * @see CVSParserTest#pass
     */
    private static final int SECOND = 1;
    /**
     * @see CVSParserTest#pass
     */
    private static final int THIRD = 2;
    
    @Before
    public void setUp() throws IOException {
        parser = new CVSParser();
        reader = new BufferedReader(new InputStreamReader(EvolizerCVSPlugin.openFile("./test_data/files.log")));
             
    }
    
    @After
    public void tearDown() throws IOException {
            reader.close();
    }

    /**
     * Tests if the CVSParser builds LogEntry-Objects correctly.
     * @throws IOException 
     * 
     */
    @Test
    public void testCVSParser() throws IOException {
        parser.addLogEntryListener(new ILogEntryListener() {
            public void processLogEntry(LogEntry currentLogEntry) {
                checkLogEntry(currentLogEntry);
            }
        });

            while (reader.ready()) {
                parser.parseLine(reader.readLine());
            }
    }

    /**
     * Helper-method for testCVSParser()
     *
     */
    public final void checkLogEntry(LogEntry currentLogEntry) {
        switch (pass) {
        case FIRST:
            //Check if the ordinary fields are filled correctly:
            assertEquals(
                    "/home/eclipse/org.eclipse.compare/compare/org/eclipse/compare/internal/merge/LineComparator.java",
                    currentLogEntry.getRcsFile());
            assertEquals(
                    "compare/org/eclipse/compare/internal/merge/LineComparator.java",
                    currentLogEntry.getWorkingFile());
            assertEquals("1.2", currentLogEntry.getHead());
            assertNull(currentLogEntry.getBranch());
            
            //Check if the collections are filled correctly:
            assertEquals(2, currentLogEntry.getTotalRevisions());
            assertEquals(2, currentLogEntry.getSelectedRevisions());
            assertEquals(2, currentLogEntry.getSymbolicNames().size());
            
            RevisionEntry entry1 = buildRevisionEntry1OfLogEntry1();
            RevisionEntry entry2 = buildRevisionsEntry2OfLogEntry1();
            List<RevisionEntry> revisions1 = currentLogEntry.getRevisionEntries();
                        
            assertTrue(revisions1.contains(entry1));
            assertTrue(revisions1.contains(entry2));
            
            List<SymbolicName> symbolicNames1 = currentLogEntry.getSymbolicNames();
            assertTrue(symbolicNames1.contains(new SymbolicName("v20040427", "1.2")));
            assertTrue(symbolicNames1.contains(new SymbolicName("v20040420", "1.2")));
            
            pass = SECOND;
            break;
        case SECOND:
            //Check if the ordinary fields are filled correctly:
            assertEquals(
                    "/home/eclipse/org.eclipse.compare/compare/org/eclipse/compare/internal/merge/MergeMessages.java",
                    currentLogEntry.getRcsFile());
            assertEquals(
                    "compare/org/eclipse/compare/internal/merge/MergeMessages.java",
                    currentLogEntry.getWorkingFile());
            assertEquals("1.2", currentLogEntry.getHead());
            assertNull(currentLogEntry.getBranch());
           
            //Check if the collections are filled correctly:
            assertEquals(4, currentLogEntry.getTotalRevisions());
            assertEquals(4, currentLogEntry.getSelectedRevisions());
            assertEquals(7, currentLogEntry.getSymbolicNames().size());
            
            RevisionEntry entry3 = buildRevisionEntry1OfLogEntry2();
            RevisionEntry entry4 = buildRevisionEntry("1.2","2004/04/18 18:13:56","jetter","Exp","Rage against the Machine.",24,94);
            RevisionEntry entry5 = buildRevisionEntry("1.1.2.1","2004/04/18 19:13:50","someone","dead","tryed something",94,0);
            RevisionEntry entry6 = buildRevisionEntry("1.1.4.1","2004/04/18 19:23:56","tree","Exp","tryed something else",14,44);
            List<RevisionEntry> revisions2 = currentLogEntry.getRevisionEntries();
            assertTrue(revisions2.contains(entry3));
            assertTrue(revisions2.contains(entry4));
            assertTrue(revisions2.contains(entry5));
            assertTrue(revisions2.contains(entry6));
            
            List<SymbolicName> symbolicNames2 = currentLogEntry.getSymbolicNames();
            assertTrue(symbolicNames2.contains(new SymbolicName("v20040503", "1.1")));
            assertTrue(symbolicNames2.contains(new SymbolicName("v20040427", "1.1")));
            assertTrue(symbolicNames2.contains(new SymbolicName("v20040420", "1.1")));
            assertTrue(symbolicNames2.contains(new SymbolicName("Another_Branch_of_a_tree","1.2.0.4")));
            assertTrue(symbolicNames2.contains(new SymbolicName("Root_Another_Branch_of_a_tree","1.2")));
            assertTrue(symbolicNames2.contains(new SymbolicName("Branch_of_a_tree","1.2.0.2")));
            assertTrue(symbolicNames2.contains(new SymbolicName("Root_Branch_of_a_tree","1.2")));
            
            pass = THIRD;
            break;
        case THIRD:
            //Check if the ordinary fields are filled correctly:
            assertEquals(
                    "/home/eclipse/org.eclipse.compare/compare/org/eclipse/compare/internal/merge/MergeMessages.properties",
                    currentLogEntry.getRcsFile());
            assertEquals(
                    "compare/org/eclipse/compare/internal/merge/MergeMessages.properties",
                    currentLogEntry.getWorkingFile());
            assertEquals("1.3", currentLogEntry.getHead());
            assertNull(currentLogEntry.getBranch());
            
            //Check if the collections are filled correctly:
            assertEquals(5, currentLogEntry.getTotalRevisions());
            assertEquals(5, currentLogEntry.getSelectedRevisions());
            assertEquals(5, currentLogEntry.getSymbolicNames().size());
            
            RevisionEntry entry7 = buildRevisionEntry1OfLogEntry3();
            RevisionEntry entry8 = buildRevisionEntry2OfLogEntry3();
            RevisionEntry entry9 = buildRevisionEntry3OfLogEntry3();
            RevisionEntry entry10 = buildRevisionEntry("1.2.2.2","2005/03/15 18:54:22","petrov","Exp","We like to move it, move it...We like to move it, move it! Move it!\nLets start again!",25,85);
            RevisionEntry entry11 = buildRevisionEntry("1.2.2.1","2005/03/16 19:54:22","baum","Exp","Geh leveln!",65,83);
            List<RevisionEntry> revisions3 = currentLogEntry.getRevisionEntries();
            assertTrue(revisions3.contains(entry7));
            assertTrue(revisions3.contains(entry8));
            assertTrue(revisions3.contains(entry9));
            assertTrue(revisions3.contains(entry10));
            assertTrue(revisions3.contains(entry11));
            
            List<SymbolicName> symbolicNames3 = currentLogEntry.getSymbolicNames();
            assertTrue(symbolicNames3.contains(new SymbolicName("v20040503", "1.3")));
            assertTrue(symbolicNames3.contains(new SymbolicName("v20040427", "1.2")));
            assertTrue(symbolicNames3.contains(new SymbolicName("v20040420", "1.1")));
            assertTrue(symbolicNames3.contains(new SymbolicName("Branch_of_a_tree", "1.2.0.2")));
            assertTrue(symbolicNames3.contains(new SymbolicName("Root_Branch_of_a_tree","1.2")));
            break;            
        default:
            fail("Invalid test.");
        }
        assertEquals(currentLogEntry.getSelectedRevisions(), currentLogEntry
                .getRevisionEntries().size());
    }

    private RevisionEntry buildRevisionEntry(String dummyRevNr, String dummyCreationDate, String dummyAuthor, String dummyState, String dummyCommitMsg, int dummyLinesAdd, int dummyLinesDel) {
    	RevisionEntry entry = new RevisionEntry(dummyRevNr);
        try {
            entry.setCreationDate(dummyCreationDate);
        } catch (ParseException e) {
            fail("Test failed");
            e.printStackTrace();
        }
        entry.setAuthor(dummyAuthor);
        entry.setState(dummyState);
        entry.setCommitMessage(dummyCommitMsg);
        entry.setLinesAdd(dummyLinesAdd);
        entry.setLinesDel(dummyLinesDel);
        
        return entry;
	}

	/**
     * @return a dummy RevisionEntry-object
     */
    private RevisionEntry buildRevisionsEntry2OfLogEntry1() {
        RevisionEntry entry = new RevisionEntry("1.1");
        try {
            entry.setCreationDate("2004/04/18 17:13:56");
        } catch (ParseException e) {
            fail("Test failed");
            e.printStackTrace();
        }
        entry.setAuthor("wuersch");
        entry.setState("Exp");
        entry.setCommitMessage("first cut of #54091");
        
        return entry;
    }

    /**
     * @return a dummy RevisionEntry-object
     */
    private RevisionEntry buildRevisionEntry1OfLogEntry1() {
        RevisionEntry entry = new RevisionEntry("1.2");
        try {
                entry.setCreationDate("2004/04/20 09:19:29");
            } catch (ParseException e) {
                fail("Test failed");
                e.printStackTrace();
            }
        entry.setAuthor("aweinand");
        entry.setState("Exp");
        entry.setLinesAdd(4);
        entry.setLinesDel(4);
        entry.setCommitMessage("removed closing of input streams");
        
        return entry;
    }
    
    /**
     * @return a dummy RevisionEntry-object
     */
    private RevisionEntry buildRevisionEntry1OfLogEntry2() {
        RevisionEntry entry = new RevisionEntry("1.1");
        try {
                entry.setCreationDate("2004/04/18 17:13:56");
            } catch (ParseException e) {
                fail("Test failed");
                e.printStackTrace();
            }
        entry.setAuthor("jetter");
        entry.setState("dead");
        entry.setCommitMessage("first cut of #54091");
        
        return entry;
    }
    
    /**
     * @return a dummy RevisionEntry-object
     */
    private RevisionEntry buildRevisionEntry1OfLogEntry3() {
        RevisionEntry entry = new RevisionEntry("1.3");
        try {
                entry.setCreationDate("2005/04/14 13:53:03");
            } catch (ParseException e) {
                fail("Test failed");
                e.printStackTrace();
            }
        entry.setAuthor("jetter");
        entry.setState("Exp");
        entry.setLinesAdd(4);
        entry.setLinesDel(4);
        entry.setCommitMessage("converted to new NLS scheme");
        
        return entry;
    }
    
    /**
     * @return a dummy RevisionEntry-object
     */
    private RevisionEntry buildRevisionEntry2OfLogEntry3() {
        RevisionEntry entry = new RevisionEntry("1.2");
        try {
                entry.setCreationDate("2005/02/24 21:59:09");
            } catch (ParseException e) {
                fail("Test failed");
                e.printStackTrace();
            }
        entry.setAuthor("aweinand");
        entry.setState("Exp");
        entry.setLinesAdd(5);
        entry.setLinesDel(5);
        entry.setCommitMessage("changed copyright notices to epl");
        
        return entry;
    }
    
    
    
    /**
     * @return a dummy RevisionEntry-object
     */
    private RevisionEntry buildRevisionEntry3OfLogEntry3() {
        RevisionEntry entry = new RevisionEntry("1.1");
        try {
                entry.setCreationDate("2004/04/18 17:13:56");
            } catch (ParseException e) {
                fail("Test failed");
                e.printStackTrace();
            }
        entry.setAuthor("aweinand");
        entry.setState("Exp");
        entry.setCommitMessage("first cut of #54091");
        
        return entry;
    }
}
