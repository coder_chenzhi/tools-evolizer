package org.evolizer.ontology.java.builder;

import org.evolizer.core.exceptions.EvolizerRuntimeException;

public class ProjectDoesNotExistException extends EvolizerRuntimeException {
	private static final long serialVersionUID = -636100422972140289L;

	public ProjectDoesNotExistException(String message) {
		super(message);
	}
}
