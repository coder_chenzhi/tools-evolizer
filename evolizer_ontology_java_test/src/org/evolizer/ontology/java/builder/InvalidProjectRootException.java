package org.evolizer.ontology.java.builder;

import java.io.File;

public class InvalidProjectRootException extends RuntimeException {
	private static final long serialVersionUID = 8568735818010920741L;
	private File invalidProjectRoot;
	
	public InvalidProjectRootException(String message, File invalidProjectRoot) {
		super(message);
		this.invalidProjectRoot = invalidProjectRoot;
	}

	public File getInvalidProjectRoot() {
		return invalidProjectRoot;
	}
}
