package business;

import business.entities.Assistant;
import business.entities.Manager;

import java.util.List;
import org.apache.commons.collections.list.TreeList;

public class Company {
	private static List employees = new TreeList();
	
	public static void main(String[] args) {
		Manager manager = new Manager("Ospel", 1000000, 1000000000);
		Assistant assistant = new Assistant("Nobody", 10);

		employees.add(manager);
		employees.add(assistant);
		
		manager.setAssistant(assistant);
		manager.manage();
		
		System.out.println(manager.getSalary());
		System.out.println(assistant.getSalary());
		
		manager.charge();
		System.out.println(manager.getSalary());
	}
}
