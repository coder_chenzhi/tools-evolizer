package business.api;

public abstract class AbstractEmployee {
	private String name;

	private double baseSalary;
	
	public AbstractEmployee(String name, double baseSalary) {
		this.name = name;
		this.baseSalary = baseSalary;
	}
	
	protected double getBaseSalary() {
		return baseSalary;
	}

	public String getName() {
		return name;
	}

	public abstract double getSalary();
}
