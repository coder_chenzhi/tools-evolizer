package org.evolizer.core.util.reflection.api;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 
 * @author wuersch
 *
 */
public interface IReflectionProvider {
	public interface IVisitor {
		public void visit(Class<?> type, Object instance);
		public void visit(Constructor<?> constructor, Class<?> definedIn, Object instance);
		public void visit(Field field, Class<?> definedIn, Object instance);
		public void visit(Method method, Class<?> definedIn, Object instance);
		public void endVisit(Class<?> type, Object instance);
	}
	
	public void process(Object object, IVisitor visitor);
}
