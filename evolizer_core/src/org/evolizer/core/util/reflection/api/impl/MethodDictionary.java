package org.evolizer.core.util.reflection.api.impl;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import org.evolizer.core.util.collections.FilteringIterator;
import org.evolizer.core.util.collections.IPredicate;

public class MethodDictionary {
	private Map<Class<?>, Map<String, Method>> keyedByMethodNameCache;

	/**
	 * Constructor.
	 */
	public MethodDictionary() {
		keyedByMethodNameCache = new WeakHashMap<Class<?>, Map<String, Method>>();
	}
	
	public Iterator<Method> methodsFor(Class<?> type) {
		return buildMap(type).values().iterator();
	}
	
	public Iterator<Method> methodsFor(Class<?> type, IPredicate<Method> filterPredicate) {
		Iterator<Method> unfilteredIterator = buildMap(type).values().iterator();
		return new FilteringIterator<Method>(unfilteredIterator, filterPredicate);
	}
	
	public Method[] methodArrayFor(Class<?> type) {
		return buildMap(type).values().toArray(new Method[0]);
	}
	
	private Map<String, Method> buildMap(final Class<?> type) {
		Class<?> cls = type;
		if(!keyedByMethodNameCache.containsKey(type)) {
			// find all super classes
			final List<Class<?>> classHierarchy = new LinkedList<Class<?>>();
			while(!Object.class.equals(cls)) {
				// reverse order of inheritance: subclasses get an
				// index greater than the index of their superclasses.
				classHierarchy.add(0, cls);
				cls = cls.getSuperclass(); // first iteration stores class under investigation (=type)
			}

			// subclasses inherit all the methods of their superclasses. This Map will grow the deeper we descend the inheritance hierarchy
			Map<String, Method> lastKeyedByMethodName = Collections.emptyMap();

			// go through all methods of the classes in the hierarchy...
			for (Iterator<Class<?>> iter = classHierarchy.iterator(); iter.hasNext();) {
				Map<String, Method> keyedByMethodName = new HashMap<String, Method>(lastKeyedByMethodName);
				
				cls = iter.next();
				if(!keyedByMethodNameCache.containsKey(cls)) { // ... and if class was not cached previously ...
					Method[] methods = cls.getDeclaredMethods();
					for (Method method : methods) {
						Method existent = keyedByMethodName.get(method.getName()); 
						if (existent == null
							// do overwrite statics
							|| ((existent.getModifiers() & Modifier.STATIC) != 0)
							// overwrite non-statics with non-statics only
							|| (existent != null && ((method.getModifiers() & Modifier.STATIC) == 0))) {
							
							keyedByMethodName.put(method.getName(), method);
						}
					}
					keyedByMethodNameCache.put(cls, keyedByMethodName);
				}
				lastKeyedByMethodName = keyedByMethodNameCache.get(cls);
			}
		}
	
		Map<String, Method> result = keyedByMethodNameCache.get(type);
	
		return (result != null) ? result : new WeakHashMap<String, Method>();
	}
}
