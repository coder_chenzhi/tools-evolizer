package org.evolizer.core.util.reflection.api.impl;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;

import org.evolizer.core.util.reflection.api.IReflectionProvider;
import org.evolizer.core.util.reflection.api.impl.ClassRegistry;

/**
 * Facade for evolizer reflection api and provider of several convenience
 * methods for working with reflection;
 * 
 * TODO Refactor to factory or at least singleton.
 * 
 * @author wuersch
 *
 */
public class DefaultReflectionProvider implements IReflectionProvider {
	
	public void process(Object object, IVisitor visitor) {
		Class<?> declaringClass = object.getClass();
		visitor.visit(declaringClass, object);
		
		Iterator<Method> methods = ClassRegistry.methodsFor(declaringClass);
		while (methods.hasNext()) {
			Method method = methods.next();
			visitor.visit(method, declaringClass, object);
		}
		
		visitor.endVisit(declaringClass, object);
	}

	public static Iterator<Method> methodsFor(Object object) {
		Class<?> type = object.getClass();
		return ClassRegistry.methodsFor(type);
	}
	
	public static Method[] methodArrayFor(Object object) {
		Class<?> type = object.getClass();
		return ClassRegistry.methodArrayFor(type);
	}
	
	public static boolean isArray(Object object) {
		Class<?> type = object.getClass();
		return type.isArray();
	}
	
	public static Object invoke(Method method, Object instance, Object... args) {
		Object result = null;
		
		try {
			result = method.invoke(instance, args);
		} catch (IllegalAccessException ex) {
			
			try {
				method.setAccessible(true);
				result = method.invoke(instance, args);
				method.setAccessible(false);
			} catch (Exception unhandlable) {
				//ignore - can only happen if security manager disallows reflective invocations
			}
			
		} catch (Exception ex) {
			//ignore - result will be null. might happen if method signature does not exist or if instance is 'null'
		}
		
		return result;
	}
	
	public static Object invoke(String methodName, Object instance, Object... args) {
		Object result = null;
		
		Class<?> type = instance.getClass();
		
		try {
			Method method = type.getMethod(methodName);
			result = invoke(method, instance, args);
		} catch(NoSuchMethodException ex) { /* if method does not exist, result will remain 'null' */ }
		
		return result;
	}
	
	public static boolean isCollection(Object object) {
		return object instanceof Collection<?>;
	}
	
	//TODO does not return collections but rather their parameterized types (the first one), which is the intended behavior so far.
	public static Class<?> getReturnTypeFor(Method method) {
		Type gtype = method.getGenericReturnType();
		if(gtype instanceof ParameterizedType) {
			ParameterizedType ptype = (ParameterizedType)gtype;
			Type[] genericTypes = ptype.getActualTypeArguments();
			return (Class<?>)genericTypes[0];
		} else {
			return method.getReturnType();
		}
	}
}
