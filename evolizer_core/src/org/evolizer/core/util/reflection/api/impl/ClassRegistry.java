package org.evolizer.core.util.reflection.api.impl;

import java.lang.reflect.Method;
import java.util.Iterator;

import org.evolizer.core.util.reflection.api.impl.MethodDictionary;

/**
 * Static wrapper for MethodDictionary (and possibly other stuff later).
 * 
 * TODO What about fields and constructors?
 * 
 * @author wuersch
 *
 */
public class ClassRegistry {
	private static MethodDictionary methods;
	
	static {
		methods = new MethodDictionary();
	}
	
	public static Iterator<Method> methodsFor(Class<?> type) {
		return methods.methodsFor(type);
	}
	
	public static Iterator<Method> methodsFor(Object object) {
		Class<?> type = object.getClass();
		return methods.methodsFor(type);
	}
	
	public static Method[] methodArrayFor(Class<?> type) {
		return methods.methodArrayFor(type);
	}
	
	public static Method[] methodArrayFor(Object object) {
		Class<?> type = object.getClass();
		return methods.methodArrayFor(type);
	}
}
