/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.evolizer.core.logging.base.PluginLogManager;
import org.evolizer.core.logging.base.PluginLogManagerRegistry;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle.
 * 
 * @author wuersch
 */
public class EvolizerCorePlugin extends Plugin {

    /**
     * The plug-in ID
     */
    public static final String PLUGIN_ID = "org.evolizer.core";

    // The shared instance
    private static EvolizerCorePlugin sPlugin;

    // The path to the log4j.properties file
    private static final String LOG_PROPERTIES_FILE = "config/log4j.properties";

    // The log manager
    private PluginLogManager fLogManager;
  
    /**
     * The constructor.
     */
    public EvolizerCorePlugin() {
        sPlugin = this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        configure();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        sPlugin = null;

        if (fLogManager != null) {
            fLogManager.shutdown();
            fLogManager = null;
        }
        
        PluginLogManagerRegistry.getInstance().shutdown();

        super.stop(context);
    }

    /**
     * Returns the shared instance.
     * 
     * @return the shared instance
     */
    public static EvolizerCorePlugin getDefault() {
        return sPlugin;
    }
    
    /**
     * Opens a file located within the plug-in-bundle or one of its registered fragments.
     * 
     * @param relativeFSPath
     *            relative path of the file starting at the root of this plug-in
     * @return an InputStream reading the specified file
     * @throws IOException
     *             if file could not be opened
     */
	public static InputStream openFile(String relativeFSPath) throws IOException {
		return FileLocator.openStream(EvolizerCorePlugin.getDefault().getBundle(), new Path(relativeFSPath), false);
	}
    
	/**
	 * Converts a path relative to the bundle root to an absolute file system path.
	 * 
	 * @param relativeFSPath a path relative to the root of the current bundle.
	 * @return the absolute file system path for the specified relative one.
	 * @throws IOException if an error occurs during the resolution of the relative path.
	 */
	public static String getAbsoluteFSPath(String relativeFSPath) throws IOException {
		URL url = EvolizerCorePlugin.getURL(relativeFSPath);
		return FileLocator.resolve(url).getFile();
	}
	
	/**
	 * Converts a relative path to an {@link URL} object.
	 * 
	 * @param relativeFSPath a path relative to the root of the current bundle.
	 * @return a {@link URL} pointing to the file/folder specified by the bundle-relative path.
	 */
	public static URL getURL(String relativeFSPath) throws IOException {
		return FileLocator.find(EvolizerCorePlugin.getDefault().getBundle(), new Path(relativeFSPath), null);
	}

    /**
     * Returns the log manager.
     * 
     * @return the log manager
     */
    public static PluginLogManager getLogManager() {
        return getDefault().fLogManager;
    }

    // TODO refactor into a class?
    private void configure() {
        try {
            InputStream propertiesInputStream = openFile(LOG_PROPERTIES_FILE);

            if (propertiesInputStream != null) {
                Properties props = new Properties();
                props.load(propertiesInputStream);
                propertiesInputStream.close();

                fLogManager = new PluginLogManager(this, props);
            }
            propertiesInputStream.close();
        } catch (IOException e) {
            String message = "Error while initializing log properties." + e.getMessage();
            IStatus status =
                    new Status(IStatus.ERROR, getDefault().getBundle().getSymbolicName(), IStatus.ERROR, message, e);
            getLog().log(status);
            throw new RuntimeException("Error while initializing log properties.", e);
        }
    }
}
