package org.evolizer.core.logging.base;

import java.util.ArrayList;
import java.util.List;

public class PluginLogManagerRegistry {
	private static PluginLogManagerRegistry instance = new PluginLogManagerRegistry();
	
	// Keeps an instance of PluginLogManager for each plug-in that uses logging.
    private List<PluginLogManager> fLogManagers;
    
    private PluginLogManagerRegistry() {
    	fLogManagers = new ArrayList<PluginLogManager>();
    }
    
    public static PluginLogManagerRegistry getInstance() {
    	return instance;
    }
    
    /**
     * Adds a log manager object to the list of active log managers.
     * 
     * @param logManager
     *            the log manager to add
     */
    public void addLogManager(PluginLogManager logManager) {
        synchronized (fLogManagers) {
            if (logManager != null) {
                fLogManagers.add(logManager);
            }
        }
    }
    
    /**
     * Removes a log manager object from the list of active log managers.
     * 
     * @param logManager
     *            the log manager to remove
     */
    public void removeLogManager(PluginLogManager logManager) {
        synchronized (fLogManagers) {
            if (logManager != null) {
                fLogManagers.remove(logManager);
            }
        }
    }
    
    public void shutdown() {
    	synchronized (fLogManagers) {
            for (PluginLogManager logManager : fLogManagers) {
                logManager.internalShutdown();
            }

            fLogManagers.clear();
        }
    }
    
    
}
