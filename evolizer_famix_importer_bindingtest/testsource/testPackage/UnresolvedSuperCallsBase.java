package testPackage;

public class UnresolvedSuperCallsBase {

	public UnresolvedSuperCallsBase() {
	}
	
	public UnresolvedSuperCallsBase(NotDef2 nd2) {
	}
	
	public void x() {
	}
	
	public void x(NotDef notDef) {
		System.out.println("x()" + notDef.toString());
	}
	
}
