package testPackage;

import testPackage.Dummy;
import undef.Undef;
import undef.Undef2;

/**
 * Check class: org.eclipse.compare.CompareEditorInput
 * 
 * @author mpinzger@tudelft.net
 *
 */
public class UnresolvedInterface implements Undef, Undef2 {
    public UnresolvedInterface() {
    }
    
    public void aMethod(String name) {
        System.out.println(name);
    }
    
    public void foo(String name) {
        Dummy dummy = new Dummy();
        dummy.foo(name);
    }
}
