package testPackage;

import testPackage.UnresolvedTypeParameters;

public class UnresolvedCalls {
	int a;
	NotDef2 aND2;
	
	public UnresolvedCalls() {
		NotDef2 nd2 = new NotDef2();
		this(0, nd2);
	}
	
	public UnresolvedCalls(int a, NotDef2 nd2) {
		this.a = a;
		this.aND2 nd2;
	}
	
	private void undefClassInstanceCreation() {
		Dummy dummyOk = new Dummy();
		NotDef nd = new NotDef();
		Dummy dummy = new Dummy(nd);
		NotDef nd2 = new NotDef(nd);
	}
	
	private void undefTypeAndAttribute() {
		NotDef notDef = new NotDef();
		notDef.undefMethod(a, b, aND2);
	}
	String b = "";
	
	private void undefAttributeAfterMethod() {
		NotDef notDef = new NotDef();
//		NotDef3 aND3;
		notDef.undefMethod(aND2, aND3);
	}
	
	NotDef3 aND3;
	private void callOverwrittenVariable() {
		String aND3 = "";
		NotDef notDef = new NotDef();
		notDef.undefMethod(a, aND3);
	}
	
	private String undefParameter(NotDef notDef) {
		System.out.println("Private method with undefined parameter");
	}
	
	public void callUndefType() {
		NotDef notDef = new NotDef();
		notDef.undefMethod();
		if (notDef != null) {
			NotDef2 notDef2 = new NotDef2();
			notDef2.undefMethod();
		} else {
			NotDef2 notDef2 = new NotDef2();
			notDef2.undefMethod();
		}
	}
	
	public void callUndefType(NotDef notDef) {
		notDef.undefParameter();
	}
	
	public void callWitInnerClassAttribute() {
		DummySub dummy = new DummySub();
		inner innerClass = new inner();
		dummy.foo(innerClass.innerStr);
	}
	
	public class inner {
		public String innerStr = "hehe";
		public NotDef2 a = new NotDef2();
		
		// only to check allowed Java variable definition
		public void variableScope(int param) {
			NotDef notDef = new NotDef();
			notDef.undefMethod(a);
			
			int local = 0;
			if (local == 0) {
				int newLocal = a;
			} else {
				int newLocal;
			}
			int newLocal;
		}
	}
	
	public void nestedCall() {
		NotDef notDef = new NotDef();
		System.out.println("x()" + notDef.toString());
	}
	
	public void callStaticUndefParam() {
		NotDef nd = new NotDef();
		Dummy.staticFoo(nd);
	}
	
	public void unresolvedAnonymClass() {
		NotDef nd = new NotDef(3,"juhu") {
			private String fieldInAnonym = "ein String";
			public void inUndefAnonym(String a, NotDef2 nd2) {
			}
			public void inUndefAnonym(String a) {

			}
		};
	}
	
	public void unresolvedCallSequence() {
		Dummy d = new Dummy();
		d.foo(nd, 10).toString();
		d.createDummy().foo(nd, "haha");
	}
	
	// call unresolved method of this class
	public void undefCallWithinClass() {
		NotDef nd = new NotDef();
		Dummy d = new Dummy();

		undefParameter(nd);
		this.undefParameter(nd);
		d.foo(undefParameter(nd));
		d.foo(nd, undefParameter(nd).length);
	}

	// instantiate the sub type and call an unresolved method inherited from the base type
	public void findCallInBaseClass() {
		Dummy d = new DummySub();
		NotDef nd = new NotDef();
		d.foo(nd, 10);
	}
	
	public void handleMultipleMatches() {
		DummySub ds = new DummySub();
		NotDef nd = new NotDef();
		ds.foo(nd);
	}
	
	public void callWithTemplateParameter() {
	    NotDef nd = new NotDef();
	    NotDef2 nd2 = new NotDef2();
	    String str = new String();
	    UnresolvedTypeParameters<String> utp = new UnresolvedTypeParameters<String>();
	    utp.someMethod(str, nd2);
	}
}


