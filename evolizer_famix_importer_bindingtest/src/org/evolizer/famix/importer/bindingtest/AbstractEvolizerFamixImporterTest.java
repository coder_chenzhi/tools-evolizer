/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.famix.importer.bindingtest;

import java.io.InputStream;
import java.util.Properties;

import org.evolizer.famix.importer.FamixImporterPlugin;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public abstract class AbstractEvolizerFamixImporterTest {
	private static final String DB_CONFIG_FILE = "./config/db.properties";

	protected static String fDBUrl;

	protected static String fDBDialect;

	protected static String fDBDriverName;

	protected static String fDBUser;

	protected static String fDBPasswd;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream propertiesInputStream = FamixImporterPlugin.openBundledFile(DB_CONFIG_FILE);

		if (propertiesInputStream != null) {
			Properties props = new Properties();
			props.load(propertiesInputStream);
			propertiesInputStream.close();

			fDBUrl = props.getProperty("dbUrl");
			fDBDialect = props.getProperty("dbDialect");
			fDBDriverName = props.getProperty("dbDriverName");
			fDBUser = props.getProperty("dbUser");
			fDBPasswd = props.getProperty("dbPasswd");
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

}
