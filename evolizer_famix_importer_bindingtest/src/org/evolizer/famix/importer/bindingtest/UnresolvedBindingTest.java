/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.famix.importer.bindingtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.launching.JavaRuntime;
import org.evolizer.famix.importer.FamixImporterPlugin;
import org.evolizer.famix.importer.FamixModelFactory;
import org.evolizer.famix.importer.ProjectParser;
import org.evolizer.famix.model.entities.AbstractFamixEntity;
import org.evolizer.famix.model.entities.FamixAssociation;
import org.evolizer.famix.model.entities.FamixAttribute;
import org.evolizer.famix.model.entities.FamixClass;
import org.evolizer.famix.model.entities.FamixLocalVariable;
import org.evolizer.famix.model.entities.FamixMethod;
import org.evolizer.famix.model.entities.FamixModel;
import org.evolizer.famix.model.entities.FamixParameter;
import org.evolizer.famix.model.entities.SourceAnchor;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author pinzger
 *
 */
public class UnresolvedBindingTest {
	
	private static FamixModel fModel;
	private static FamixModelFactory fFactory = new FamixModelFactory();

	private static void setUpProject() throws CoreException, IOException{
		String name = "TestProject";
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root= workspace.getRoot();
		root.delete(true, false, null);
		IProject project= root.getProject(name);
		project.create(null);
		project.open(null);
		
		IProjectDescription desc = project.getDescription();
		desc.setNatureIds(new String[] {
				JavaCore.NATURE_ID});
		project.setDescription(desc, null);
		
		IJavaProject javaProj = JavaCore.create(project);
		IFolder binDir = project.getFolder("bin");
		IPath binPath = binDir.getFullPath();
		javaProj.setOutputLocation(binPath, null);
		
		IClasspathEntry cpe = JavaRuntime.getDefaultJREContainerEntry();
		javaProj.setRawClasspath(new IClasspathEntry[] {cpe},null);
		
		IFolder folder = project.getFolder("src");
		folder.create(true, true, null);
		IClasspathEntry entry = JavaCore.newSourceEntry(folder.getFullPath());
		IClasspathEntry[] entries = javaProj.getRawClasspath();
		IClasspathEntry[] newEntries = new IClasspathEntry[entries.length + 1];
		System.arraycopy(entries, 0, newEntries, 0, entries.length);
		newEntries[newEntries.length - 1] = entry;
		javaProj.setRawClasspath(newEntries, null);
		
		IPackageFragmentRoot javaRoot = javaProj.getPackageFragmentRoot(folder);
		javaRoot.createPackageFragment("testPackage", true, null);
		javaRoot = javaProj.getPackageFragmentRoot(folder);
		IPackageFragment packageFragment = javaRoot.getPackageFragment("testPackage");
		IFolder packageFolder = project.getFolder(packageFragment.getResource().getProjectRelativePath());
		IFile sourceFile = packageFolder.getFile("Test.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Test.java"), IResource.NONE, null);
		
		sourceFile = packageFolder.getFile("Test2.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Test2.java"), IResource.NONE, null);
		
		sourceFile = packageFolder.getFile("Test3.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Test3.java"), IResource.NONE, null);
		
		sourceFile = packageFolder.getFile("Test4.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Test4.java"), IResource.NONE, null);

		sourceFile = packageFolder.getFile("NestedCalls.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/NestedCalls.java"), IResource.NONE, null);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		setUpProject();
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IJavaModel model =  JavaCore.create(workspace.getRoot());
		IJavaProject project = model.getJavaProject("TestProject");
		List<IJavaElement> selection = new ArrayList<IJavaElement>();
		selection.add(project);
		ProjectParser parser = new ProjectParser(selection);
		parser.parse(null);
		fModel = parser.getModel();

//		 output all entities and source anchors
		for (AbstractFamixEntity entity : fModel.getFamixEntities()) {
			System.out.print(entity.getUniqueName());
			if (entity.getSourceAnchor() != null) {
				System.out.print(": " + entity.getSourceAnchor().toString());
			}
			System.out.println();
		}
	}
	
	@Test
	public void testUnresolvedInheritance() {
		FamixClass clazz = (FamixClass)fModel.getElement(fFactory.createClass("testPackage.Test",null));
		assertNotNull(clazz);
		FamixClass superClass = (FamixClass) fModel.getElement(fFactory.createClass("<undef>.SomeClass",null));
		assertNotNull(superClass);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(superClass);
		assertTrue("FamixClass " + superClass.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		int containsInheritsTo = TestHelper.containsRelationTo(superClass, lRelations);
		assertTrue("Missing inheritance relationship in base class" + clazz.getUniqueName() + " to " + superClass.getUniqueName(), containsInheritsTo > 0);
		
		lRelations = fModel.getAssociations(clazz);
		assertTrue("FamixClass " + clazz.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		containsInheritsTo = TestHelper.containsRelationTo(superClass, lRelations);
		assertTrue("Missing inheritance relationship in sub class " + clazz.getUniqueName() + " to " + superClass.getUniqueName(), containsInheritsTo > 0);
	}
	
	@Test
	public void testUnresolvedInterfaces(){
		FamixClass interfaceClass = (FamixClass) fModel.getElement(fFactory.createClass("<undef>.SomeInterface",null));
		FamixClass baseClass = (FamixClass) fModel.getElement(fFactory.createClass("testPackage.Test",null));
		assertNotNull(interfaceClass);
		assertNotNull(baseClass);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(baseClass);
		assertTrue("FamixClass " + baseClass.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		int containsInheritsTo = TestHelper.containsRelationTo(interfaceClass, lRelations);
		assertTrue("Missing inheritance relationship in class" + baseClass.getUniqueName() + " to " + interfaceClass.getUniqueName(), containsInheritsTo > 0);
		
		lRelations = fModel.getAssociations(interfaceClass);
		assertTrue("FamixClass " + interfaceClass.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		containsInheritsTo = TestHelper.containsRelationTo(interfaceClass, lRelations);
		assertTrue("Missing inheritance relationship in sub class " + baseClass.getUniqueName() + " to " + interfaceClass.getUniqueName(), containsInheritsTo > 0);
	}
	
	@Test
	public void testUnresolvedAttribute(){
		FamixClass clazz = (FamixClass)fModel.getElement(fFactory.createClass("<undef>.Warrior",null));
		FamixAttribute simpleAttribute = (FamixAttribute) fModel.getElement(fFactory.createAttribute("testPackage.Test.gigs",null));
		assertNotNull(clazz);
		assertNotNull(simpleAttribute);
		assertEquals(simpleAttribute.getDeclaredClass(), clazz);
		
		assertEquals("Type string of " + simpleAttribute.getUniqueName() + " must have modifier PRIVATE", 
				simpleAttribute.getModifiers() & Modifier.PRIVATE, Modifier.PRIVATE);
		assertEquals("Type string of " + simpleAttribute.getUniqueName() + " must have modifier STATIC", 
				simpleAttribute.getModifiers() & Modifier.STATIC, Modifier.STATIC);
		assertEquals("Type string of " + simpleAttribute.getUniqueName() + " must have modifier FINAL", 
				simpleAttribute.getModifiers() & Modifier.FINAL, Modifier.FINAL);
	}
	
	
	@Test
	public void testMethodParameters(){
		FamixMethod method = (FamixMethod)fModel.getElement(fFactory.createMethod("testPackage.Test.myMethod(int,double,<undef>.Warlock)",null));
		assertNotNull(method);
		assertEquals(3, method.getParameters().size());
		
		FamixParameter param = (FamixParameter) fModel.getElement(fFactory.createFormalParameter("testPackage.Test.myMethod(int,double,<undef>.Warlock).i",null,0));
		assertNotNull(param);
		assertEquals(new Integer(0), param.getParamIndex());
		
		FamixClass clazz = (FamixClass) fModel.getElement(fFactory.createClass("int",null));
		assertEquals(clazz, param.getDeclaredClass());
		assertTrue(method.getParameters().contains(param));
		assertEquals(method, param.getParent());
		
		param = (FamixParameter) fModel.getElement(fFactory.createFormalParameter("testPackage.Test.myMethod(int,double,<undef>.Warlock).d",null,1));
		clazz = (FamixClass) fModel.getElement(fFactory.createClass("double",null));
		assertEquals(clazz, param.getDeclaredClass());
		assertNotNull(param);
		assertTrue(method.getParameters().contains(param));
		assertEquals(method, param.getParent());
		assertEquals(new Integer(1), param.getParamIndex());
		
		param = (FamixParameter) fModel.getElement(fFactory.createFormalParameter("testPackage.Test.myMethod(int,double,<undef>.Warlock).zorf",null,2));
		clazz = (FamixClass) fModel.getElement(fFactory.createClass("<undef>.Warlock",null));
		assertEquals(clazz, param.getDeclaredClass());
		assertNotNull(param);
		assertTrue(method.getParameters().contains(param));
		assertEquals(method, param.getParent());
		assertEquals(new Integer(2), param.getParamIndex());
	}
	
	@Test
	public void testUnresolvedArrayParameter(){
		FamixMethod method = (FamixMethod)fModel.getElement(fFactory.createMethod("testPackage.Test.myMethod(<undef>.Hunter[])",null));
		assertNotNull(method);
		
		FamixParameter param = (FamixParameter) fModel.getElement(fFactory.createFormalParameter("testPackage.Test.myMethod(<undef>.Hunter[]).h",null,0));
		assertNotNull(param);
		assertEquals(new Integer(0), param.getParamIndex());
		FamixClass clazz = (FamixClass)fModel.getElement(fFactory.createClass(AbstractFamixEntity.ARRAY_TYPE_NAME,null));
		assertEquals(clazz, param.getDeclaredClass());
		assertTrue(method.getParameters().contains(param));
		assertEquals(method, param.getParent());
	}
	
	@Test
	public void testUnresolvedReturnParameter(){
		FamixClass clazz = (FamixClass) fModel.getElement(fFactory.createClass("<undef>.Dudu",null));
		assertNotNull(clazz);
		FamixMethod method = (FamixMethod)fModel.getElement(fFactory.createMethod("testPackage.Test.someMethod()",null));
		assertNotNull(method);
		assertEquals(clazz, method.getDeclaredReturnClass());
	}
	
	@Test
	public void testUnresolvedArrayReturnParameter(){
		FamixClass clazz = (FamixClass)fModel.getElement(fFactory.createClass(AbstractFamixEntity.ARRAY_TYPE_NAME,null));
		assertNotNull(clazz);
		FamixMethod method = (FamixMethod)fModel.getElement(fFactory.createMethod("testPackage.Test.method()",null));
		assertNotNull(method);
		assertEquals(method.getDeclaredReturnClass(), clazz);
	}
	
	@Test
	public void testUnresolvedArrayAttribute(){
		FamixClass clazz = (FamixClass)fModel.getElement(fFactory.createClass(AbstractFamixEntity.ARRAY_TYPE_NAME,null));
		assertNotNull(clazz);
		FamixAttribute simpleAttribute = (FamixAttribute) fModel.getElement(fFactory.createAttribute("testPackage.Test2.paladins",null));
		assertNotNull(simpleAttribute);
		assertEquals(clazz, simpleAttribute.getDeclaredClass());
	}
	
	@Test
	public void testMethodLocalVariableContainsSimple() {
		FamixMethod simpleMethod = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Test3.gather()",null));
		assertNotNull("FamixModel most contain method testPackage.Test3.gather()", simpleMethod);
		FamixLocalVariable simpleLocal = fFactory.createLocalVariable("testPackage.Test3.gather().a", null); 
		simpleLocal.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test3.java", 203, 204));
		simpleLocal = (FamixLocalVariable) fModel.getElement(simpleLocal);
		assertNotNull("FamixModel must contain local variable testPackage.Test3.gather().a", simpleLocal);
		
		assertTrue("FamixMethod must contain local variable", simpleMethod.getLocalVariables().size() > 0);
		boolean containsLocal = containsLocalVariable(simpleMethod, simpleLocal);
		assertTrue("FamixMethod must contain local variable simpleLocal", containsLocal);
		assertEquals("No or wrong parent method for local variable simpleLocal", simpleMethod, simpleLocal.getParent());
		
		FamixClass clazz = (FamixClass) fModel.getElement(fFactory.createClass("<undef>.EliteBoss",null));
		assertNotNull("FamixModel must contain class <undef>.EliteBoss", clazz);
		assertEquals("FamixClass must be equal", clazz, simpleLocal.getDeclaredClass());
	}
	
	@Test
	public void testMethodLocalVariableContainsMulti() {
		FamixMethod simpleMethod = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Test3.gather()",null));
		assertNotNull("FamixModel must contain method testPackage.Test3.gather()", simpleMethod);
		
		FamixLocalVariable multiLocal1 = fFactory.createLocalVariable("testPackage.Test3.gather().gigs1",simpleMethod);
		multiLocal1.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test3.java", 249, 254));
		multiLocal1 = (FamixLocalVariable) fModel.getElement(multiLocal1);
		FamixLocalVariable multiLocal2 = fFactory.createLocalVariable("testPackage.Test3.gather().gigs2",simpleMethod);
		multiLocal2.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test3.java", 257, 262));
		multiLocal2 = (FamixLocalVariable) fModel.getElement(multiLocal2);
		
		assertNotNull("FamixModel must contain local variable testPackage.Test3.gather().gigs1", multiLocal1);
		assertNotNull("FamixModel must contain local variable testPackage.Test3.gather().gigs2", multiLocal2);
		
		assertTrue("FamixMethod must contain local variable", simpleMethod.getLocalVariables().size() > 0);
		
		FamixClass clazz = (FamixClass)fModel.getElement(fFactory.createClass("<undef>.Gigs",null));
		assertNotNull("FamixModel must contain class <undef>.Gigs", clazz);
		
		boolean containsLocal1 = containsLocalVariable(simpleMethod, multiLocal1);
		assertTrue("FamixMethod must contain local variable multiLocal1", containsLocal1);
		assertEquals("No or wrong parent method for local variable multiLocal1", simpleMethod, multiLocal1.getParent());
		assertEquals("FamixClass must be equal", clazz, multiLocal1.getDeclaredClass());

		boolean containsLocal2 = containsLocalVariable(simpleMethod, multiLocal2);
		assertTrue("FamixMethod must contain local variable multiLocal2", containsLocal2);
		assertEquals("No or wrong parent method for local variable multiLocal2", simpleMethod, multiLocal2.getParent());
		assertEquals("FamixClass must be equla", clazz, multiLocal2.getDeclaredClass());
	}
	
	@Test
	public void testMethodLocalVariableContainsWithinFor() {
		FamixMethod simpleMethod = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Test4.iterate()",null));
		assertNotNull("FamixModel must contain method testPackage.Test4.iterate()", simpleMethod);
		
		FamixLocalVariable withinFor = fFactory.createLocalVariable("testPackage.Test4.iterate().iter",simpleMethod);
		withinFor.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test4.java", 152, 173));
		withinFor = (FamixLocalVariable) fModel.getElement(withinFor);
		assertNotNull("FamixModel must contain local variable testPackage.Test4.iterate().iter", withinFor);
		
		assertTrue("FamixMethod must contain local variable", simpleMethod.getLocalVariables().size() > 0);

		boolean containsLocal = containsLocalVariable(simpleMethod, withinFor);
		assertTrue("FamixMethod must contain local variable nrs within for loop", containsLocal);
		assertEquals("No or wrong parent method for local variable nrs within for loop", simpleMethod, withinFor.getParent());
		
		FamixClass clazz = (FamixClass) fModel.getElement(fFactory.createClass("<undef>.Iterator",null));
		assertNotNull("FamixModel must contain class <undef>.Iterator", clazz);
		
		assertEquals("FamixClass must be equal", clazz, withinFor.getDeclaredClass());
		
	}
	
	@Test
	public void testArrayLocalVariable(){
		FamixMethod simpleMethod = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Test3.gather()",null));
		assertNotNull("FamixModel must contain method testPackage.Test3.gather()", simpleMethod);
		FamixLocalVariable lVar = fFactory.createLocalVariable("testPackage.Test3.gather().items", simpleMethod);
		lVar.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test3.java", 216, 235));
		lVar = (FamixLocalVariable)fModel.getElement(lVar);
		assertNotNull("FamixModel must contain local variable testPackage.Test3.gather().items", lVar);
		FamixClass declClass = (FamixClass) fModel.getElement(fFactory.createClass(AbstractFamixEntity.ARRAY_TYPE_NAME, null));
		assertNotNull("FamixModel must contain class " + AbstractFamixEntity.ARRAY_TYPE_NAME, declClass);
		
		assertEquals("FamixClass must be equal", declClass, lVar.getDeclaredClass());
	}
	
	@Test
	public void testNestedCalls() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.NestedCalls.sum()",null));
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.NestedCalls.add10(int)",null));

		assertNotNull("FamixModel must contain the method testPackage.NestedCalls.sum()", caller);
		assertNotNull("FamixModel must contain the method testPackage.NestedCalls.add10(int)", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int containsInvocationTo = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 4, containsInvocationTo);
	}
	
	private boolean containsLocalVariable(FamixMethod simpleMethod, FamixLocalVariable simpleLocal) {
		boolean containsLocal = false;
		for (FamixLocalVariable lLocal : simpleMethod.getLocalVariables()) {
			if (lLocal.getUniqueName().equals(simpleLocal.getUniqueName())) {
				containsLocal = true;
				break;
			}
		}
		return containsLocal;
	}
}
