package org.evolizer.versioncontrol.git.importer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.git.importer.GitImporter;
import org.evolizer.versioncontrol.git.importer.exceptions.GitImporterException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * SVNImportTest
 * 
 * Tests the SVNImport class
 * 
 * @author ghezzi
 *
 */
public class GITImportTest {
	
    private static EvolizerSessionHandler fSessionHandler;
    private static IEvolizerSession fSession;
    private static String DB_CONFIG_FILE = "./config/db.properties";
    private static String fDBUrl;
    private static String fDBDialect;
    private static String fDBDriverName;
    private static String fDBUser;
    private static String fDBPasswd;
    private static GitImporter importer;
    
	@BeforeClass
	public static void setUp() throws Exception {
	    InputStream propertiesInputStream = Activator.openBundledFile(DB_CONFIG_FILE);

        if (propertiesInputStream != null) {
            Properties props = new Properties();
            props.load(propertiesInputStream);
            propertiesInputStream.close();

            fDBUrl = props.getProperty("dbUrl");
            fDBDialect = props.getProperty("dbDialect");
            fDBDriverName = props.getProperty("dbDriverName");
            fDBUser = props.getProperty("dbUser");
            fDBPasswd = props.getProperty("dbPasswd");
            
            fSessionHandler = EvolizerSessionHandler.getHandler();
            fSessionHandler.initSessionFactory(fDBUrl, fDBUser, fDBPasswd);
            fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
            fSession = fSessionHandler.getCurrentSession(fDBUrl);
        }
        
        propertiesInputStream.close();
        
        GitImporter exporter;
        try {
            importer = new GitImporter("/Users/Giacomo/Documents/facebooker", fSession); 
//            importer = new GitImporter("git://github.com/mmangino/facebooker.git", fSession);
            importer.importProject();
        } catch (GitImporterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}

	@AfterClass
	public static void tearDown() throws Exception {
	    fSession.close();
        fSessionHandler.cleanupHibernateSessions();
        fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
        fSessionHandler = null;
	}

	/**
	 * Tests the number and the names of the releases.
	 */
	@Test
	public void testReleases(){
	    List<Release> releases = fSession.query("from Release", Release.class);
	    assertEquals("Number of releases found is not what expected", 2, releases.size());
	    ArrayList<String> relNames = new ArrayList<String>();
	    relNames.add("REL_1.0.30");
	    relNames.add("REL_1.0.31");
	    for(String r : relNames){
	        assertEquals("Release " + r + " was not found", 1, fSession.query("from Release where name = '" + r + "'", Release.class).size());
	    }
	}
	
	@Test
	public void testSpecificRelease() throws ParseException{
	    Release release = fSession.uniqueResult("from Release where name = 'REL_1.0.30'", Release.class);
	    SimpleDateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy ZZZZ");
	    Date d = df.parse("Wed Apr 29 14:31:27 2009 -0700");
	    assertTrue(d.equals(release.getTimeStamp()));
	    List<Transaction> allTransactions = fSession.query("from Transaction", Transaction.class);
	    int count = 0;
	    for(Transaction t : allTransactions){
	        if(t.getInvolvedRevisions()!=null && t.getInvolvedRevisions().iterator().hasNext() && t.getInvolvedRevisions().iterator().next().getReleases().contains(release))
	            count++;
	    }
	    assertEquals(554, count);
	}
}