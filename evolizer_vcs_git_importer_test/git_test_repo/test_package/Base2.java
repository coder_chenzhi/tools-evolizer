package testPackage;

public class Base2 implements IBase {
	private int a=0;
	private int b=0;

	public Base() {
	}
	public Base(int a, int b) {
		this.a = a;
		this.b = b;
	}
	public Base(int a, int b, NotDef notDef) {
		this.a = a;
		this.b = b + notDef.getValue();
	}
	
	public abstract int compute();
	
	public int computeOther(String name) {
		return name.length();
	}
	
	public int computeOther(int offset, String name) {
		return computeOther(name) + compute();
	}
	
	public int getA() {
		return a;
	}
}
