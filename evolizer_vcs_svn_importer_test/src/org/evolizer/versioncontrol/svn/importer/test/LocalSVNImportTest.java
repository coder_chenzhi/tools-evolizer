/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.humans.Role;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.CommitterRole;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNImporter;
import org.evolizer.versioncontrol.svn.model.entities.SVNRelease;
import org.evolizer.versioncontrol.svn.model.entities.SVNRevision;
import org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * Tests the {@link EvolizerSVNImporter} class, fetching the history from a small local repository.
 * This test is obviously much faster then the remote ones.
 * 
 * @author ghezzi
 * 
 */
public class LocalSVNImportTest extends AbstractEvolizerSVNImporterTest {

    /**
     * 
     * @throws Exception If problems arise.
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        AbstractEvolizerSVNImporterTest.setUpBeforeClass();
        
        /***** Loading the test settings *****/
        /*
         *  This settings should be changed accordingly to where the SVN test
         *  repository has been installed, and if authentication has been set up. 
         */
        sSVNUrl = "file:///Users/Giacomo/localSVN/importer_test";
        sSVNUser = "";
        sSVNPwd = "";

        String [] repositoryData = new String [3];
        repositoryData[0] = sSVNUser;
        repositoryData[1] = sSVNPwd;
        repositoryData[2] = sSVNUrl;
        sImporter =
            new EvolizerSVNImporter(
                    repositoryData,
                    2,
                    false,
                    "",
                    null);
        sImporter.importProject(sSession, new NullProgressMonitor(), 0, 20, false);
        sImporter.importProject(sSession, new NullProgressMonitor(), 20, -1, true);
//        AbstractEvolizerSVNImporterTest.tearDownAfterClass();
        AbstractEvolizerSVNImporterTest.setUpBeforeClass2();
        ArrayList<String> rels = new ArrayList<String>();
        rels.add("project_0_1");
        sImporter.fetchRevisionRangeSource(sSession, ".txt", 0, 26, new NullProgressMonitor());
    }

    /**
     * 
     * @throws Exception If problems arise.
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
//        AbstractEvolizerSVNImporterTest.tearDownAfterClass();
    }

    /**
     * Tests the number and the names of the releases.
     */
    @Test
    public void testReleasesNumberAndNames() {
        ArrayList<String> releaseNames = new ArrayList<String>();        
        releaseNames.add("project_0_1");
        releaseNames.add("project_0_2");
        releaseNames.add("project_0_3");
        releaseNames.add("project_0_4");
        releaseNames.add("project_0_5");
        
        Collection<SVNRelease> releases = getReleases();
        // Testing the number of releases
        assertEquals(releaseNames.size(), releases.size());
        ArrayList<String> foundReleases = new ArrayList<String>();
        // testing the releases names
        for (SVNRelease rel : releases) {
            assertTrue("Imported SVN tag " + rel.getName() + " does not really exists", releaseNames.contains(rel
                    .getName()));
            assertFalse("Release " + rel.getName() + " was imported twice", foundReleases.contains(rel.getName()));
            foundReleases.add(rel.getName());
        }
    }

    /**
     * Tests a specific release.
     * 
     * @throws ParseException If problems arise.
     */
    @Test
    public void testRelease() throws ParseException {
        SVNRelease rel = getRelease("project_0_2");
        assertEquals("Release " + rel.getName() + " was not created with its real date",
        "Fri Oct 09 13:42:55 CEST 2009", rel.getTimeStamp().toString());
        Set<Revision> revisions = ((SVNRelease) rel).getReleaseRevisions();
        int versionsCount = 0;
        for (Revision tempVer : revisions) {
            Revision ver = ((SVNRevision) tempVer).getAncestor();
            if (ver.getState() == null) {
                versionsCount++;
            }
        }
        assertEquals("Wrong number of FileVersions for release " + rel.getName(), 6, versionsCount);
        String normFileName = "/importer_test/tags/project_0_2/TestFolder1/TestFile4.txt";
        String normFileAncestorName = "/importer_test/trunk/project/TestFolder1/TestFile4.txt";
        int totFilesTested = 0;
        for (Revision currRev : revisions) {

            if (currRev.getFile().getPath().compareTo(normFileName) == 0) {

                
                assertEquals("Wrong File Version", "15", currRev.getNumber());
                SVNRevision ancestor = ((SVNRevision) currRev).getAncestor();
                assertEquals("Wrong ancestor", normFileAncestorName, ancestor.getFile().getPath());

                ArrayList<String> versionNums = new ArrayList<String>();
                versionNums.add("13");
                versionNums.add("9");
                versionNums.add("8");
                versionNums.add("6");
                versionNums.add("5");
                versionNums.add("3");

                for (String version : versionNums) {
                    assertTrue("Wrong version number for file " + ancestor.getFile().getPath(), version.compareTo(ancestor.getNumber()) == 0);

                    if (version.compareTo("13") == 0) {
                        totFilesTested++;
                        assertTrue("Wrong commit message for version " + version + " of file " + normFileAncestorName + " for release "
                                + rel.getName(), ancestor.getReport().getCommitMessage().contains("modified all files"));
                        assertTrue("Wrong commit author for version " + version + " of file " + normFileAncestorName + " for release "
                                + rel.getName(), ancestor.getReport().getAuthor().getFirstName().compareTo("Giacomo") == 0);

                        for (Role r : ancestor.getReport().getAuthor().getRoles()) {
                            if (r instanceof CommitterRole) {
                                assertTrue("Author of " + ancestor.getFile().getName() + ":" + ancestor.getNumber() + " doesn't have it in its artifacts", ((CommitterRole) r).getArtifacts().contains(ancestor));
                                break;
                            }
                        }
                        assertTrue("Wrong commit date for version " + version + " of file " + normFileAncestorName + " for release "
                                + rel.getName(), "Fri Oct 09 13:40:12 CEST 2009".equals(ancestor.getReport().getCreationTime().toString()));
                    } else if (version.compareTo("3") == 0) {
                        totFilesTested++;
                        assertTrue("Wrong commit message for version " + version + " of file " + normFileAncestorName + " for release "
                                + rel.getName(), ancestor.getReport().getCommitMessage().contains(""));
                        assertTrue("Wrong commit author for version " + version + " of file " + normFileAncestorName + " for release "
                                + rel.getName(), ancestor.getReport().getAuthor().getFirstName().compareTo("Giacomo") == 0);

                        for (Role r : ancestor.getReport().getAuthor().getRoles()) {
                            if (r instanceof CommitterRole) {
                                assertTrue("Author of " + ancestor.getFile().getName() + ":" + ancestor.getNumber() + " doesn't have it in its artifacts", ((CommitterRole) r).getArtifacts().contains(ancestor));
                                break;
                            }
                        }
                        assertTrue("Wrong commit date for version " + version + " of file " + normFileAncestorName + " for release "
                                + rel.getName(), ancestor.getReport().getCreationTime().toString().equals("Fri Oct 09 11:41:31 CEST 2009"));
                     // assert ancestor has no more previous versions
                        assertNull("The number of versions for " + normFileAncestorName + " is more then expected", ancestor.getPreviousRevision());
                        
                    }
                    if (ancestor.getPreviousRevision() != null) {
                        assertEquals(
                                "Next and PreviousVersion of two successive FileVersions don't match for release "
                                + rel.getName(),
                                ancestor.getPreviousRevision().getNextRevision().getId(),
                                ancestor.getId());
                    }
                    if (ancestor.getPreviousRevision() != null) {
                        ancestor = sSession.load(SVNRevision.class, ancestor
                            .getPreviousRevision().getId());
                        }

                }
                break;
            }
        }
        assertEquals(
                "Didn't test all the versions I was supposed to test, I might have not found some of them for release "
                        + rel.getName(),
                2,
                totFilesTested);
    }

    /**
     * Tests a specific branch.
     */
    @Test
    public void testBranch() {
        Collection<Branch> branches = getBranches();
        assertEquals("The number of found branches are not what expected", branches.size(), 2);
        ArrayList<String> branchNames = new ArrayList<String>();
        branchNames.add("branch_1");
        branchNames.add("branch_0_2");
        ArrayList<String> foundBranches = new ArrayList<String>();
        for (Branch br : branches) {
            assertTrue("Imported SVN branch " + br.getName() + " does not really exists", branchNames.contains(br
                    .getName()));
            assertFalse("Branch " + br.getName() + " was imported twice", foundBranches.contains(br.getName()));
            foundBranches.add(br.getName());
        }
        Branch branch = getBranch("branch_1");

        Set<Revision> revisions = branch.getRevisions();
        int versionsCount = 0;
        int deletionsCount = 0;
        int movesFromCount = 0;
        for (Revision ver : revisions) {
            if (ver.getState() != null && ver.getState().compareTo("DELETED") == 0) {
                assertNull("The FileVersion has a newer version, even though it is marked as deleted", ver
                        .getNextRevision());
                deletionsCount++;
            } else if (ver.getState() != null && ver.getState().compareTo("MOVED") == 0) {
                assertNotNull("File " + ver.getFile()
                        + " was moved but it was not linked to the new file created from it", ((SVNVersionedFile) ver.getFile())
                        .getCopiedTo());
                assertEquals(
                        "CopiedTo and CopiedFrom don't match",
                        ((SVNVersionedFile) ((SVNVersionedFile) ver.getFile()).getCopiedTo()).getCopiedFrom(),
                        ver.getFile());
                movesFromCount++;
            } else {
                versionsCount++;
            }
        }
        assertEquals("Wrong number of FileVersions for branch " + branch.getName(), 6, versionsCount);
    }

    /**
     * Tests a specific ChangeSet.
     * 
     * @throws ParseException If problems arise.
     */
    @Test
    public void testChangeSet() throws ParseException {
        Transaction cs = getTransaction("2009-10-09 13:57:57 +0200");
        if (cs != null) {
            ArrayList<String> modified = new ArrayList<String>();
            ArrayList<String> added = new ArrayList<String>();
            ArrayList<String> moved = new ArrayList<String>();
            modified.add("/importer_test/trunk/project/.DS_Store");
            modified.add("/importer_test/trunk/project/TestFolder1/TestFile4.txt");
            modified.add("/importer_test/trunk/project/TestFolder1/TestFile5.txt");
            added.add("/importer_test/trunk/project/TestFolder1/TestFile6.txt");
            added.add("/importer_test/trunk/project/TestFolder2/TestFile1.txt");
            moved.add("/importer_test/trunk/project/TestFolder1/TestFile1.txt");
            assertEquals(
                    "Erroneous number of involved file versions for ChangeSet at date " + cs.getStarted(),
                    modified.size() + added.size() + moved.size(),
                    cs.getInvolvedRevisions().size());
            for (Revision ver : cs.getInvolvedRevisions()) {
                String fileVersionId = ver.getFile().getPath() + ":" + ver.getNumber();
                assertEquals(
                        "FileVersion " + fileVersionId + " has not the number of associated to the change set",
                        "20", ver.getNumber());
                assertEquals(
                        "ChangeSet and FileVersion is not symmetrical for FileVersion " + fileVersionId,
                        cs.getId(),
                        ((SVNRevision) ver).getChangeSet().getId());
                assertNotNull("FileVersion " + fileVersionId + " is not associated to any file", ver.getFile());
                if (modified.contains(ver.getFile().getPath())) {
                    assertNotNull("FileVersion " + fileVersionId
                            + " represents a modification, but it's not linked to a previous version", ver
                            .getPreviousRevision());
                    assertEquals("The previous FileVersion is not linked this one, " + fileVersionId, ver, ver
                            .getPreviousRevision().getNextRevision());
                    assertNull(ver.getState());
                } else if (added.contains(ver.getFile().getPath())) {
                    assertNull("FileVersion " + fileVersionId + " has a previous one, even though it's an add", ver
                            .getPreviousRevision());
                    if (((SVNVersionedFile) ver.getFile()).getCopiedFrom() != null) {
                        assertEquals(ver.getFile(), ((SVNVersionedFile) ((SVNVersionedFile) ver.getFile()).getCopiedFrom()).getCopiedTo());
                    }
                } else if (moved.contains(ver.getFile().getPath())) {
                    assertEquals("FileVersion " + fileVersionId
                            + " is not marked as a move action, even though it was", "MOVED", ver.getState());
                    assertNull("FileVersion " + fileVersionId + " has a next one, even though it's a move", ver
                            .getNextRevision());
                    assertEquals("The file to which this one, " + ver.getFile()
                            + ", was copied, is not linked to it", ver.getFile(), ((SVNVersionedFile) ((SVNVersionedFile) ver.getFile()).getCopiedTo())
                            .getCopiedFrom());
                }
            }
        }
    }

    /**
     * Tests a specific FileVersion over multiple releases.
     */
    @Test
    public void testFileVersionInMultipleReleases() {
        ArrayList<String> releases = new ArrayList<String>();
        releases.add("project_0_2");
        releases.add("project_0_3");
        releases.add("project_0_4");
        SVNRevision fileVersion = getSpecificFileVersion(8, "/importer_test/trunk/project/TestFolder1/TestFile4.txt");
        assertEquals("The number of releases associated to FileVersion /importer_test/trunk/project/TestFolder1/TestFile4.txt:8 is incorrect",
                releases.size(), fileVersion.getReleases().size());
        
        for (Release r : fileVersion.getReleases()) {
            assertTrue("Fileversion is linked to a wrong release", releases.contains(r.getName()));
            releases.remove(r.getName());
        }
        assertTrue("Some releases associated to FileVersion /importer_test/trunk/project/TestFolder1/TestFile4.txt:8 were not found", releases.isEmpty());
    }

    /**
     * Tests some specific directory structures.
     */
    @Test
    public void testDirectoryStructure() {
        ArrayList<String> folders = new ArrayList<String>();
        folders.add("/importer_test/trunk/project/TestFolder2");
        folders.add("/importer_test/trunk/project/TestFolder1");

        for (String path : folders) {
            assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
            String oldPath = path;
            int index = path.lastIndexOf("/");
            while (index > -1) {
                if (index == 0) {
                    path = "/";
                    assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
                    assertEquals(
                            "Father " + path + "and Child " + oldPath + "are not consistent",
                            getDirectory(oldPath).getParentDirectory(),
                            getDirectory(path));
                    assertTrue("Father " + path + "does not contain Child " + oldPath, getDirectory(path).getChildren()
                            .contains(getDirectory(oldPath)));
                    oldPath = path;
                    index = -1;
                } else {
                    path = path.substring(0, index);
                    assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
                    assertEquals(
                            "Father " + path + "and Child " + oldPath + "are not consistent",
                            getDirectory(oldPath).getParentDirectory(),
                            getDirectory(path));
                    assertTrue("Father " + path + "does not contain Child " + oldPath, getDirectory(path).getChildren()
                            .contains(getDirectory(oldPath)));
                    oldPath = path;
                    index = path.lastIndexOf("/");
                }
            }
        }
    }

    /**
     * Tests files that were moved and deleted
     */
    @Test
    public void testMoveAndDelete() {
        SVNVersionedFile newerFile = getFile("/importer_test/trunk/project/TestFile2.txt");
        assertNotNull(
                "File /importer_test/trunk/project/TestFile2.txt doesn't exist in the data extracted",
                newerFile);
        
            SVNVersionedFile f = getFile("/importer_test/trunk/project/TestFolder1/TestFile2.txt");
            assertNotNull("File /importer_test/trunk/project/TestFolder1/TestFile2.txt doesn't exist in the data extracted", f);
            assertEquals("The last version of File " + f.getPath()
                    + " is not the one expected (the one in which the move was made)", "23", f.getLatestRevision().getNumber());
            assertEquals("The first version of File " + newerFile.getPath()
                    + " is not the one expected (the one in which the move was made, and thus the file created)",
                    "23", newerFile.getRevisions().get(0).getNumber());
            assertEquals("The original file " + newerFile.getPath()
                    + " doesn't have a link to the moved file", newerFile, f.getCopiedTo());
            assertEquals(
                    "File " + newerFile.getPath() + " is not linked to the one it has been copied from",
                    newerFile.getCopiedFrom(),
                    f);
            assertEquals(((SVNRevision) newerFile.getRevisions().get(0)).getAncestor(), f.getLatestRevision());
            newerFile = f;
            f = getFile("/importer_test/trunk/project/TestFolder2/TestFile2.txt");
            assertNotNull("File /importer_test/trunk/project/TestFolder2/TestFile2.txt doesn't exist in the data extracted", f);
            assertEquals("The last version of File " + f.getPath()
                    + " is not the one expected (the one in which the move was made)", "11", f.getLatestRevision().getNumber());
            assertEquals("The first version of File " + newerFile.getPath()
                    + " is not the one expected (the one in which the move was made, and thus the file created)",
                    "11", getFirstRevision(newerFile).getNumber());
            assertEquals("The original file " + newerFile.getPath()
                    + " doesn't have a link to the moved file", newerFile, f.getCopiedTo());
            assertEquals(
                    "File " + newerFile.getPath() + " is not linked to the one it has been copied from",
                    newerFile.getCopiedFrom(),
                    f);
            assertEquals(((SVNRevision) getFirstRevision(newerFile)).getAncestor(), f.getLatestRevision());
            
            SVNVersionedFile deletedFile = getFile("/importer_test/trunk/project/TestFolder1/TestFile4.txt");
            assertNotNull(
                    "File /importer_test/trunk/project/TestFolder1/TestFile4.txt doesn't exist in the data extracted",
                    deletedFile);
            assertEquals("23", deletedFile.getLatestRevision().getNumber());
        
    }

    /**
     * Tests the content of a specific file.
     * 
     * @throws SecurityException If problems arise.
     * @throws IllegalArgumentException If problems arise.
     * @throws NoSuchFieldException If problems arise.
     * @throws IllegalAccessException If problems arise.
     */
    @Test
    public void testFileContent()
            throws SecurityException,
            IllegalArgumentException,
            NoSuchFieldException,
            IllegalAccessException {
        SVNVersionedFile f = getFile("/importer_test/trunk/project/TestFile2.txt");
        for (Revision ver : f.getRevisions()) {
            if (ver.getNumber().compareTo("23") == 0) {
                assertEquals(
                        "The file extracted in the import is not as the one residing in the repository",
                        fetchFile("/importer_test/trunk/project/TestFile2.txt", 23),
                        ver.getSourceCodeWrapper().getSource());
            }
        }
    }

    /**
     * Tests a specific author.
     */
    @Test
    public void testAuthors() {
        assertEquals(
                "The total number of authors found is not what it's supposed to be",
                1, getPersons().size());
        Person author = getSpecificPerson("Giacomo");
        assertNotNull("Author " + author.getFirstName() + " was not found during the import", author);
        List<Long> res =
            sSession.query(
                    "SELECT r.number FROM Revision as r, Person as p, CommitterRole as c WHERE c in elements(p.roles) and r in elements(c.artifacts) AND p.firstName='"
                    + author.getFirstName() + "' GROUP BY r.number",
                    Long.class);
        assertEquals(
                "Total commits found for author " + author.getFirstName() + " is not what expected ",
                17,
                res.size());
    }
}
