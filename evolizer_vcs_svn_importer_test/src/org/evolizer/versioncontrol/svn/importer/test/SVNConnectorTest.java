/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.evolizer.versioncontrol.svn.importer.EvolizerSVNPlugin;
import org.evolizer.versioncontrol.svn.importer.exceptions.SVNImporterException;
import org.evolizer.versioncontrol.svn.importer.util.SVNConnector;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNException;

/**
 * Tests the {@link SVNConnector}, the class used to connect and interact with an SVN server. 
 * It uses the same SVN repository used for {@link ShortRemoteSVNImportTest}.
 *
 * @author ghezzi
 *
 */
public class SVNConnectorTest {

    private static SVNConnector sConnector;
    private static String sUrl = "file:///Users/Giacomo/localSVN/";
    private static String sUser = "";
    private static String sPwd = "";
    
    /**
     * 
     * @throws Exception
     *             If problems arise.
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        sConnector = new SVNConnector(sUrl, sUser, sPwd, null);
        sConnector.connectSVN();
    }
    
    /**
     * 
     */
    @Test
    public void testConnection() {
        assertNotNull("No repository associated to connector", sConnector.getRepository());
        assertEquals("The svn user name is not what expected", sUser, sConnector.getSVNUserName());
        assertEquals("The svn pwd is not what expected", sPwd, sConnector.getSVNUserPassword());
    }
    
    /**
     * 
     */
    @Test
    public void testLinesChanged() {
        int [] lines = sConnector.getLinesChanged("/importer_test/trunk/project/TestFolder1/TestFile5.txt", 26, 14);
        assertEquals("Wrong number of lines added", 1, lines[0]);
        assertEquals("Wrong number of lines deleted", 9, lines[1]);
    }

    @Test
    public void testGetRevisions() throws SVNException {
        /*
         * The number of releases is actually 36, but the importer also fetches, with getAllRevisions, also
         * revision 0, which is never user created.
         */
        assertEquals("Wrong number of total revisions found", 37, sConnector.getAllRevisions().size());
        assertEquals("Wrong number of revisions found, while fetching a range of them", 3, sConnector.getRevisions(1, 3).size());
    }
    
    @Test
    public void testFileContentsFetch() throws SVNImporterException, IOException {
        String contents = sConnector.getFileContents("/importer_test/trunk/project/TestFolder1/TestFile5.txt", 14);
        
        BufferedReader read = new BufferedReader ( new InputStreamReader(EvolizerSVNPlugin.openFile("./config/TestFile5.txt")));
        String contents2 = "";
        String t = read.readLine();
        while(t != null) {
            contents2 = contents2 + t;
            t = read.readLine();
            if(t != null) {
                contents2 = contents2 + "\n";
            }
        }
        contents2 = contents2.substring(0, contents2.length());
        assertEquals(contents, contents2);
    }
    
    /**
     * 
     */
    @AfterClass
    public static void tearDownAfterClass(){
        sConnector.disconnectSVN();
    }
}
