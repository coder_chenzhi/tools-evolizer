package org.evolizer.versioncontrol.svn.importer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.CommitterRole;
import org.evolizer.versioncontrol.cvs.model.entities.ModificationReport;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.svn.importer.exceptions.SVNImporterException;
import org.evolizer.versioncontrol.svn.importer.mapper.SVNModelMapper;
import org.evolizer.versioncontrol.svn.model.entities.SVNRelease;
import org.evolizer.versioncontrol.svn.model.entities.SVNRevision;
import org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

@SuppressWarnings("unchecked")
public class ModelMapperTest extends AbstractEvolizerSVNImporterTest {

    private static ModificationReport sReport;
    private static Branch sBranch;
    private static SVNModelMapper sModelMapper;
    private static Person sAuthor;
    private static String sAuthorName = "Mister Author";
    private static String sAuthorEmail = "author@email.com";
    private static Transaction sTrans;
    private static SVNRevision sRev;
    private static SVNVersionedFile sFile;
    private static SVNRelease sRel;

    /**
     * 
     * @throws Exception
     *             If problems arise.
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        AbstractEvolizerSVNImporterTest.setUpBeforeClass();
        sModelMapper = new SVNModelMapper(sSession);
    }

    /**
     * Testing that a {@link Person} is created correctly, given its name.
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    @Test
    public void testPersonCreationByName()
    throws SecurityException,
    NoSuchFieldException,
    IllegalArgumentException,
    IllegalAccessException {
        sAuthor = sModelMapper.createPerson(sAuthorName);
        assertTrue("Author's name is not what is supposed to be", sAuthor.getFirstName().compareTo(sAuthorName) == 0);
        assertNull(sAuthor.getEmail());
        assertEquals("The number of the author's role is not what expected", 1, sAuthor.getRoles().size());
        assertTrue(sAuthor.getRoles().iterator().next() instanceof CommitterRole);

        // Check the author was saved in the right private field
        Field authorsField = SVNModelMapper.class.getDeclaredField("fPersons");
        authorsField.setAccessible(true);
        HashMap<String, Person> authors = (HashMap<String, Person>) authorsField.get(sModelMapper);
        assertEquals(1, authors.size());
        assertEquals(sAuthor, authors.get(sAuthorName));

        // Check no duplicates are created
        sModelMapper.createPerson(sAuthorName);
        assertEquals(1, authors.size());
    }

    /**
     * Testing that a {@link Person} is created correctly, given its email.
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    @Test
    public void testPersonCreationByEMail()
    throws SecurityException,
    NoSuchFieldException,
    IllegalArgumentException,
    IllegalAccessException {
        // First I check that one author already exists
        Field authorsField = SVNModelMapper.class.getDeclaredField("fPersons");
        authorsField.setAccessible(true);
        HashMap<String, Person> authors = (HashMap<String, Person>) authorsField.get(sModelMapper);
        assertEquals(1, authors.size());

        Person p = sModelMapper.createPerson(sAuthorEmail);
        assertTrue("Author's name is not what is supposed to be", p.getFirstName().compareTo(sAuthorEmail) == 0);
        assertTrue("Author's email is not what is supposed to be", p.getEmail().compareTo(sAuthorEmail) == 0);
        assertEquals("The number of the author's role is not what expected", 1, p.getRoles().size());
        assertTrue(p.getRoles().iterator().next() instanceof CommitterRole);

        // Now I check the number of tracked authors increased by one.
        assertEquals(2, authors.size());
        assertEquals(p, authors.get(sAuthorEmail));

        // Check no duplicates are created
        sModelMapper.createPerson(sAuthorEmail);
        assertEquals(2, authors.size());
    }

    /**
     * Testing that a {@link Transaction} is created correctly.
     */
    @Test
    public void testTransactionCreation() {
        Date d = new Date(1234563566);
        sTrans = sModelMapper.createTransaction(d);
        assertEquals(1234563566, sTrans.getStarted().getTime());
        assertEquals(1234563566, sTrans.getFinished().getTime());
    }

    /**
     * Testing that Files and Directories are created correctly.
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    @Test
    public void testFileDirCreation()
    throws SecurityException,
    NoSuchFieldException,
    IllegalArgumentException,
    IllegalAccessException {
        // Testing the creation of one file and all its parent folders and the links between them
        sFile = sModelMapper.createFile("/rootDir/dir/file.txt");
        assertTrue(sFile.getName().compareTo("file.txt") == 0);
        assertTrue(sFile.getPath().compareTo("/rootDir/dir/file.txt") == 0);
        assertTrue(sFile.getParentDirectory().getName().compareTo("dir") == 0);
        assertTrue(sFile.getParentDirectory().getPath().compareTo("/rootDir/dir") == 0);
        assertEquals(sFile, sFile.getParentDirectory().getChildren().iterator().next());
        assertTrue(sFile.getParentDirectory().getParentDirectory().getName().compareTo("rootDir") == 0);
        assertTrue(sFile.getParentDirectory().getParentDirectory().getPath().compareTo("/rootDir") == 0);
        assertEquals(sFile.getParentDirectory(), sFile.getParentDirectory().getParentDirectory().getChildren()
                .iterator().next());
        assertTrue(sFile.getParentDirectory().getParentDirectory().getParentDirectory().getName().compareTo("") == 0);
        assertTrue(sFile.getParentDirectory().getParentDirectory().getParentDirectory().getPath().compareTo("/") == 0);
        assertEquals(sFile.getParentDirectory().getParentDirectory(), sFile.getParentDirectory().getParentDirectory()
                .getParentDirectory().getChildren().iterator().next());
        assertNull(sFile.getParentDirectory().getParentDirectory().getParentDirectory().getParentDirectory());

        // Check the Files and Dirs were saved in the right private field
        Field dirsField = SVNModelMapper.class.getDeclaredField("fDirectories");
        dirsField.setAccessible(true);
        HashMap<String, Directory> dirs = (HashMap<String, Directory>) dirsField.get(sModelMapper);
        assertEquals(3, dirs.size());
        assertTrue(dirs.containsKey("/rootDir/dir"));
        assertEquals(sFile.getParentDirectory(), dirs.get("/rootDir/dir"));
        assertTrue(dirs.containsKey("/rootDir"));
        assertEquals(sFile.getParentDirectory().getParentDirectory(), dirs.get("/rootDir"));
        assertTrue(dirs.containsKey("/"));
        assertEquals(sFile.getParentDirectory().getParentDirectory().getParentDirectory(), dirs.get("/"));

        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertEquals(1, files.size());
        assertEquals(sFile, files.get("/rootDir/dir/file.txt"));

        // Testing for no duplicates
        sModelMapper.createFile("/rootDir/dir/file.txt");
        assertEquals(3, dirs.size());
        assertEquals(1, files.size());
        sModelMapper.createDirectory("/rootDir/dir");
        assertEquals(3, dirs.size());
        assertEquals(1, files.size());
        sModelMapper.createFile("/rootDir/dir/file2.txt");
        assertEquals(3, dirs.size());
        assertEquals(2, files.size());
    }

    /**
     * Testing that a {@link ModificationReport} is created correctly.
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    @Test
    public void testModificationReportCreation()
    throws SecurityException,
    NoSuchFieldException,
    IllegalArgumentException,
    IllegalAccessException {
        sReport = sModelMapper.createModificationReport(new Date(1243545656), "test message", sAuthorName);
        assertTrue("The report message is not what expected", sReport.getCommitMessage().compareTo("test message") == 0);
        assertEquals("The report date is not what expected", 1243545656, sReport.getCreationTime().getTime());
        assertEquals("The report author is not what expected", sAuthor, sReport.getAuthor());

        // Check the authors are still the same
        Field authorsField = SVNModelMapper.class.getDeclaredField("fPersons");
        authorsField.setAccessible(true);
        HashMap<String, Person> authors = (HashMap<String, Person>) authorsField.get(sModelMapper);
        assertEquals(2, authors.size());

        // Creating another report with a totally new author to check that the author entity is created in the process
        sModelMapper.createModificationReport(new Date(124568756), "test message2", "New Author");
        assertEquals(3, authors.size());
    }

    /**
     * Testing that a {@link Revision} is created correctly.
     * @throws SVNImporterException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws SecurityException
     * @throws NoSuchFieldException
     */
    @Test
    public void testRevisionCreation()
    throws SVNImporterException,
    IllegalArgumentException,
    IllegalAccessException,
    SecurityException,
    NoSuchFieldException {

        sRev = sModelMapper.createRevision(1, sFile, sTrans, sReport, null, false, "These are the contents");

        assertEquals("The revision report is not the one expected", sReport, sRev.getReport());
        assertEquals("The revision transaction is not the one expected", sTrans, sRev.getChangeSet());
        assertEquals("The test transaction does not contain the test revision", sRev, sTrans.getInvolvedRevisions()
                .iterator().next());
        assertEquals("The test revision is not the test file latest revision", sRev, sFile.getLatestRevision());
        assertEquals("The revision author is not the expected one", sAuthor, sRev.getAuthor());
        assertTrue("The test author does not have the test revision in its artifacts", ((CommitterRole) sAuthor
                .getRoles().iterator().next()).getArtifacts().contains(sRev));
        assertEquals("The revision file is not what expected", sFile, sRev.getFile());
        assertTrue(sRev.getSourceCodeWrapper().getSource().compareTo("These are the contents") == 0);

        // Check the revision was added to the related field
        Field revsField = SVNModelMapper.class.getDeclaredField("fRevisionElemsCache");
        revsField.setAccessible(true);
        ArrayList<?> revsAndFiles = (ArrayList<?>) revsField.get(sModelMapper);
        assertEquals(2, revsAndFiles.size());

        /*
         * Testing the branch revision creation when related to a new branch.
         * If the branch does not exists already (can happen if the project was moved around),
         * it is created on the fly.
         */

        Transaction t = sModelMapper.createTransaction(new Date(13143647));
        ModificationReport m = sModelMapper.createModificationReport(new Date(132323), "another message", sAuthorName);
        SVNVersionedFile f = sModelMapper.createFile("/file.txt");
        SVNRevision rev2 =
            sModelMapper.createBranchRevision(2, f, t, m, null, false, "These are the new contents", "branch");

        // Check the branch was created and was added to the related field
        Field branchesField = SVNModelMapper.class.getDeclaredField("fBranches");
        branchesField.setAccessible(true);
        HashMap<String, Branch> branches = (HashMap<String, Branch>) branchesField.get(sModelMapper);
        assertEquals(1, branches.size());

        assertTrue("The created branch does not exist", branches.get("branch") != null);
        assertEquals("Wrong creation time for the newly created branch", m.getCreationTime(), branches.get("branch")
                .getCreationDate());
        assertTrue("The branch has children, when it shoudn't", branches.get("branch").getChildren().isEmpty());
        assertEquals(rev2, branches.get("branch").getRevisions().iterator().next());
        assertEquals("Wrong number of revisions for test branch", 1, branches.get("branch").getRevisions().size());
        // Check that the revision and file were added to the related data structure
        assertEquals(4, revsAndFiles.size());
    }

    /**
     * Testing that a {@link Transaction} is correctly saved to the DB (finalized).
     */
    @Test
    public void testFinalizeTransaction() {
        //First I call the method
        sModelMapper.finalizeTransaction(sTrans, new ArrayList<String>());

        //Now I check that all the required entities were saved correctly
        List<Transaction> transactions = sSession.query("from Transaction", Transaction.class);
        Transaction toAnalyze = null;
        for (Transaction t : transactions) {
            if (t.getStarted().equals(sTrans.getStarted())) {
                toAnalyze = t;
            }
        }
        assertNotNull(toAnalyze);

        assertEquals("The number of involved revisions for the test transaction is not what expected", 1, toAnalyze.getInvolvedRevisions().size());
        assertEquals("The revision transaction is not the one expected", toAnalyze.getId(), sRev.getChangeSet().getId());
        assertEquals("The test transaction does not contain the test revision", sRev.getId(), toAnalyze.getInvolvedRevisions().iterator().next().getId());
        assertEquals("The test revision's author is not what expected", sRev.getAuthor().getId(), toAnalyze.getInvolvedRevisions().iterator().next().getAuthor().getId());

        Person p = sSession.uniqueResult("from Person as p where p.firstName ='" + sAuthor.getFirstName() + "'", Person.class);
        CommitterRole r = (CommitterRole) p.getRoles().iterator().next();
        SVNVersionedFile f = sSession.uniqueResult("from SVNVersionedFile where path= '/rootDir/dir/file.txt'", SVNVersionedFile.class);
        assertEquals(toAnalyze.getInvolvedRevisions().iterator().next().getId(), f.getRevisions().iterator().next().getId());
        assertEquals("The saved revision was not associated to its saved author", r.getArtifacts().iterator().next(), toAnalyze.getInvolvedRevisions().iterator().next());
    }

    /**
     * Testing that a {@link Release} is created correctly.
     * @throws Exception
     */
    @Test
    public void testReleaseCreation() throws Exception{
        Date d = new Date(1234563999);
        Transaction newTrans = sModelMapper.createTransaction(d);         
        sRel =
            sModelMapper.createRelease(
                    new Date(1234563999),
                    newTrans,
                    "Test release creation message",
                    sAuthorName,
                    "/releaseTag",
                    "/rootDir",
                    1,
                    2);
        assertEquals("The release timestamp is not what expected", 1234563999, sRel.getTimeStamp().getTime());
        assertEquals("The number of the test release revisions is not what expected", 1, sRel.getReleaseRevisions().size());
        assertTrue("The test release only revision number is not what expected", sRel.getReleaseRevisions().iterator().next().getNumber().compareTo("2") == 0);
        assertTrue("The release revision file is not what expected", sRel.getReleaseRevisions().iterator().next().getFile().getPath().compareTo("/releaseTag/dir/file.txt") == 0);
        assertEquals(sRev.getId(), ((SVNRevision) sRel.getReleaseRevisions().iterator().next()).getAncestor().getId());
        assertTrue("The release name is not what expected", sRel.getName().compareTo("releaseTag") == 0);

        //Testing also that directories and files were created for the release
        Field dirsField = SVNModelMapper.class.getDeclaredField("fDirectories");
        dirsField.setAccessible(true);
        HashMap<String, Directory> dirs = (HashMap<String, Directory>) dirsField.get(sModelMapper);
        assertNotNull(dirs.get("/releaseTag"));
        assertNotNull(dirs.get("/releaseTag/dir"));

        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertNotNull(files.get("/releaseTag/dir/file.txt"));
    }

    /**
     * Testing that a {@link Release} is correctly saves to the DB.
     * @throws Exception
     */
    @Test
    public void testFinalizeRelease() throws Exception{
        sModelMapper.finalizeRelease(sRel);
        SVNRelease rel = sSession.uniqueResult("from SVNRelease where name = 'releaseTag'" , SVNRelease.class);
        assertEquals("The number of revisions for the fetched test release is not what expected", 1, rel.getRevisions().size());
        assertTrue(rel.getRevisions().iterator().next().getFile().getPath().compareTo("/rootDir/dir/file.txt") == 0);

        //Testing also that the directories and files that were created for the release are removed from the SVNModelMapper
        Field dirsField = SVNModelMapper.class.getDeclaredField("fDirectories");
        dirsField.setAccessible(true);
        HashMap<String, Directory> dirs = (HashMap<String, Directory>) dirsField.get(sModelMapper);
        assertNull(dirs.get("/releaseTag"));
        assertNull(dirs.get("/releaseTag/dir"));

        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertNull(files.get("/releaseTag/dir/file.txt"));

    }

    /**
     * Testing that the mapper removes everything related to a {@link Release} from the DB and from its in memory data.
     */
    @Test
    public void testDeleteRelease(){
        Date d = new Date(1234565999);
        Transaction newTrans = sModelMapper.createTransaction(d);         
        sRel =
            sModelMapper.createRelease(
                    d,
                    newTrans,
                    "Test release creation message",
                    sAuthorName,
                    "/releaseTag2",
                    "/rootDir",
                    1,
                    3);

        sModelMapper.deleteRelease(1, sAuthorName, "/releaseTag2");

        SVNRevision rev = sSession.uniqueResult("from SVNRevision where id = " + sRev.getId(), SVNRevision.class);
        assertEquals(1, rev.getReleases().size());
        assertTrue(rev.getReleases().iterator().next().getName().compareTo("releaseTag") == 0);
    }

    /**
     * Testing that a {@link Branch} is created correctly.
     * @throws SecurityException
     * @throws NoSuchFieldException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    @Test
    public void testBranchCreation()
    throws SecurityException,
    NoSuchFieldException,
    IllegalArgumentException,
    IllegalAccessException {
        Transaction t = sModelMapper.createTransaction(new Date(13147));
        sBranch =
            sModelMapper.createBranch(
                    new Date(1324),
                    t,
                    "branch creation",
                    sAuthorName,
                    "branch_1",
                    null,
                    "/branchPath",
                    "/rootDir",
                    2,
                    3);

        // Check the branch was created and was added to the related field
        Field branchesField = SVNModelMapper.class.getDeclaredField("fBranches");
        branchesField.setAccessible(true);
        HashMap<String, Branch> branches = (HashMap<String, Branch>) branchesField.get(sModelMapper);
        assertEquals(2, branches.size());

        //Testing also that directories and files were created for the branch
        Field dirsField = SVNModelMapper.class.getDeclaredField("fDirectories");
        dirsField.setAccessible(true);
        HashMap<String, Directory> dirs = (HashMap<String, Directory>) dirsField.get(sModelMapper);
        assertNotNull(dirs.get("/branchPath"));
        assertNotNull(dirs.get("/branchPath/dir"));

        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertNotNull(files.get("/branchPath/dir/file.txt"));
        sModelMapper.finalizeTransaction(t, new ArrayList<String>());
    }

    /**
     * Testing that a {@link File} is correctly added to an existing {@link Branch}.
     * @throws Exception
     */
    @Test
    public void testAddToBranch() throws Exception {
        sModelMapper.createFile("/rootDir/additionalFile.txt");
        sModelMapper.addToBranch(sBranch, sModelMapper.createTransaction(new Date(13345)), new Date(2356), "add to branch", sAuthorName, "/branchPath/additionalFile.txt", "/rootDir/additionalFile.txt", 5, 4);
        
        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertNotNull(files.get("/branchPath/additionalFile.txt"));
        
        boolean found = false;
        for (Revision r : sBranch.getRevisions()) {
            if (r.getFile().getPath().compareTo("/branchPath/additionalFile.txt") == 0) {
                found = true;
            }
        }
        assertTrue("The new file added to the test branch was not found", found);
    }
    
    /**
     * Testing that a {@link File} is correctly added to an existing {@link Release}.
     * @throws Exception
     */
    @Test
    public void testAddToRelease() throws Exception {
        sModelMapper.addToRelease(sRel, sModelMapper.createTransaction(new Date(13389)), new Date(2590), "add to release", sAuthorName, "/releaseTag/additionalFile.txt", "/rootDir/additionalFile.txt", 5, 4);
        
        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertNotNull(files.get("/releaseTag/additionalFile.txt"));
        
        boolean found = false;
        for (Revision r : sRel.getReleaseRevisions()) {
            if (r.getFile().getPath().compareTo("/releaseTag/additionalFile.txt") == 0) {
                found = true;
            }
        }
        assertTrue("The new file added to the test release was not found", found);
    }
    
    /**
     * Testing that a {@link Revision} is correctly removed from the DB.
     * @throws Exception
     */
    @Test
    public void testDeleteRevision() throws Exception{
        Date d = new Date(1234563999);
        Transaction newTrans = sModelMapper.createTransaction(d);
        ModificationReport newReport = sModelMapper.createModificationReport(new Date(1243548890), "test message", sAuthorName);
        Long previousRevId = sFile.getLatestRevision().getId();
        sModelMapper.createRevision(6, sFile, newTrans, newReport, null, false, "These are the contents");
        sModelMapper.finalizeTransaction(newTrans, new ArrayList<String>());
        SVNRevision fetched = sSession.uniqueResult("from SVNRevision where number = " + 6, SVNRevision.class);
        SVNRevision previous = sSession.uniqueResult("from SVNRevision where id = " + previousRevId, SVNRevision.class);
        assertEquals("the two successive revisions of the test file are not linked together", fetched.getPreviousRevision().getId(), previous.getId());
        assertEquals("the two successive revisions of the test file are not linked together", fetched.getId(), previous.getNextRevision().getId());
        assertNotNull("The test revision was not saved", fetched);
        sModelMapper.deleteRevision(6, sAuthorName, null);
        assertTrue("The deleted revision still exists", sSession.query("from SVNRevision where number = " + 6, SVNRevision.class).isEmpty());
        previous = sSession.uniqueResult("from SVNRevision where id = " + previousRevId, SVNRevision.class);
        assertNull("The deleted revision is still around...", previous.getNextRevision());
    }

    /**
     * Testing that a {@link Branch} is correctly removed from the DB.
     * @throws Exception
     */
    @Test
    public void testDeleteBranch() throws Exception{

        Transaction t = sModelMapper.createTransaction(new Date(19847));
        Branch newBranch =
            sModelMapper.createBranch(
                    new Date(4562),
                    t,
                    "branch 2 creation",
                    sAuthorName,
                    "branch_2",
                    null,
                    "/branch2Path",
                    "/rootDir",
                    2,
                    5);

        sModelMapper.deleteBranch(5, sAuthorName, newBranch.getName());

        // Check the deleted branch was removed from to the related field
        Field branchesField = SVNModelMapper.class.getDeclaredField("fBranches");
        branchesField.setAccessible(true);
        HashMap<String, Branch> branches = (HashMap<String, Branch>) branchesField.get(sModelMapper);
        assertFalse("the test branch was deleted but is still around", branches.containsKey("branch_2"));

        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        int count = 0;
        for (String fileName : files.keySet()) {
            if (fileName.contains("/branch2Path")) {
                count++;
            }
        }
        assertEquals("Some files of the deleted files are still around", 0, count);
    }

    /**
     * Testing that an existing {@link Release} is correctly converted to a {@link Branch}.
     * @throws Exception
     */
    @Test
    public void testConvertReleaseToBranch() throws Exception{
        //First I need to create a release to then convert to a branch
        Transaction newTrans = sModelMapper.createTransaction(new Date(1576563999));         
        SVNRelease newRel =
            sModelMapper.createRelease(
                    new Date(1576563999),
                    newTrans,
                    "Test release creation message",
                    sAuthorName,
                    "/releaseTag3",
                    "/rootDir",
                    1,
                    20);       
        sModelMapper.finalizeRelease(newRel);
        sModelMapper.convertReleaseToBranch(newRel.getName());
        
        assertTrue(sSession.query("from SVNRelease where name = '" + newRel.getName() + "'", SVNRelease.class).isEmpty());
        
        List<SVNRevision> revs = sSession.query("from Revision", SVNRevision.class);
        for (SVNRevision r : revs) {
            for (Release rel : r.getReleases()) {
                assertTrue("revision " +r.getFile().getPath() + ":" + r.getNumber() + "has still a reference to the converted release", rel.getName().compareTo(newRel.getName()) != 0);
            }
        }
        // Check the branch was created and was added to the related field
        Field branchesField = SVNModelMapper.class.getDeclaredField("fBranches");
        branchesField.setAccessible(true);
        HashMap<String, Branch> branches = (HashMap<String, Branch>) branchesField.get(sModelMapper);
        assertTrue("The converted branch does not exist", branches.keySet().contains(newRel.getName()));
    }
    
    /**
     * Testing that a {@link Directory} is created correctly, only as a {@link Directory} and not as a {@link File}.
     */
    @Test
    public void testIsOnlyDirectory(){
        assertTrue("The test directory is not only a directory according to the ModelMapper", sModelMapper.isOnlyDirectory("/rootDir/dir"));
        assertFalse("The test file is a directory according to the ModelMapper", sModelMapper.isOnlyDirectory("/rootDir/dir/file2.txt"));
        assertFalse("A non existing directory is a proper existing directory according to the ModelMapper", sModelMapper.isOnlyDirectory("/rootDir4"));
        
    }
    
    /**
     * Testing that a {@link File} is created correctly, only as a {@link File} and not as a {@link Directory}.
     */
    @Test
    public void testIsOnlyFile(){
        assertTrue("The test file is not only a file according to the ModelMapper", sModelMapper.isOnlyFile("/rootDir/dir/file.txt"));
        assertFalse("The test directory is a file according to the ModelMapper", sModelMapper.isOnlyFile("/rootDir/dir"));
        assertFalse("A non existing file is a proper existing file according to the ModelMapper", sModelMapper.isOnlyDirectory("/rootDir/filewe.txt"));
        
    }
    
    /**
     * Testing that a {@link Directory} was correctly removed from the mapper memory.
     * @throws Exception
     */
    @Test
    public void testDeleteDirectory() throws Exception {
        sModelMapper.deleteDirectory("/rootDir", 7, sModelMapper.createTransaction(new Date(34143647)), new Date(134553647), "deleting stuff", sAuthorName, null);
    
        // Check the Files and Dirs were deleted from the right private field
        Field dirsField = SVNModelMapper.class.getDeclaredField("fDirectories");
        dirsField.setAccessible(true);
        HashMap<String, Directory> dirs = (HashMap<String, Directory>) dirsField.get(sModelMapper);
        assertFalse(dirs.containsKey("/rootDir/dir"));
        assertFalse(dirs.containsKey("/rootDir"));
        assertTrue(dirs.containsKey("/"));

        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        // Testing for no duplicates
        assertFalse(files.containsKey("/rootDir/dir/file.txt"));
        assertFalse(files.containsKey("/rootDir/dir/file2.txt"));
    }
   
    /**
     * Testing that the mapper fetches the correct previous {@link Revision}, of a given {@link Revision}.
     * @throws Exception
     */
    @Test
    public void testGetPreviousRevision() throws Exception {
        assertEquals("Wrong previous revision: there should be none", "-1" , sModelMapper.getPreviousRevision("/rootDir/dir/file.txt", 1));
        assertEquals("Wrong previous revision number for the test revision", "1", sModelMapper.getPreviousRevision("/rootDir/dir/file.txt", 2));
    }
    
    /**
     * Testing that a {@link File} is correctly removed from a {@link Release}.
     * @throws Exception
     */
    @Test
    public void testRemoveFromRelease() throws Exception{
        Transaction t = sModelMapper.createTransaction(new Date(97897899));
        ModificationReport m = sModelMapper.createModificationReport(new Date(155323), "another message", sAuthorName);
        SVNVersionedFile f = sModelMapper.createFile("/rootDir/dir/file2.txt");
        sModelMapper.createRevision(2, f, t, m, null, false, "These are the new contents");
        sModelMapper.finalizeTransaction(t, new ArrayList<String>());
        
        Transaction newTrans = sModelMapper.createTransaction(new Date(1576763378));
        SVNRelease newRel =
            sModelMapper.createRelease(
                    new Date(1576763378),
                    newTrans,
                    "Test release creation message",
                    sAuthorName,
                    "/releaseTag3",
                    "/rootDir",
                    2,
                    7);
        
        // Check the Files and Dirs were saved in the right private field
        Field dirsField = SVNModelMapper.class.getDeclaredField("fDirectories");
        dirsField.setAccessible(true);
        HashMap<String, Directory> dirs = (HashMap<String, Directory>) dirsField.get(sModelMapper);
        assertTrue(dirs.containsKey("/releaseTag3/dir"));

        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertTrue(files.containsKey("/releaseTag3/dir/file.txt"));
        
        boolean found = false;
        for (Revision r : newRel.getReleaseRevisions()) {
            if (r.getFile().getPath().compareTo("/releaseTag3/dir/file.txt") == 0) {
                found = true;
            }
        }
        assertTrue("The test file was not linked to the associated test release", found);
        
        sModelMapper.removeFromRelease(newRel, "/releaseTag3/dir/file.txt");
        assertTrue(dirs.containsKey("/releaseTag3/dir"));
        assertTrue(files.containsKey("/releaseTag3/dir/file2.txt"));
        assertFalse(files.containsKey("/releaseTag3/dir/file.txt"));
        
        found = false;
        for (Revision r : newRel.getReleaseRevisions()) {
            if (r.getFile().getPath().compareTo("/releaseTag3/dir/file.txt") == 0) {
                found = true;
            }
        }
        assertFalse("The deleted test file is still linked to the associated test release", found);
        
        sModelMapper.removeFromRelease(newRel, "/releaseTag3/dir");
        assertFalse(dirs.containsKey("/releaseTag3/dir"));
        assertFalse(files.containsKey("/releaseTag3/dir/file2.txt"));
    }

    /**
     * Testing that a {@link File} is correctly replaced in a {@link Release}. 
     * @throws Exception
     */
    @Test
    public void testReplaceInRelease() throws Exception{
        Transaction t = sModelMapper.createTransaction(new Date(97897899));
        ModificationReport m = sModelMapper.createModificationReport(new Date(155323), "another message", sAuthorName);
        SVNVersionedFile f = sModelMapper.createFile("/rootDir2/dir/file3.txt");
        sModelMapper.createRevision(2, f, t, m, null, false, "These are the new contents");
        sModelMapper.finalizeTransaction(t, new ArrayList<String>());
        
        
        Transaction newTrans = sModelMapper.createTransaction(new Date(1599997378));
        SVNRelease newRel =
            sModelMapper.createRelease(
                    new Date(1576763378),
                    newTrans,
                    "Test release creation message",
                    sAuthorName,
                    "/releaseTag4",
                    "/rootDir",
                    2,
                    8);
        
        sModelMapper.replaceInRelease(
                newRel, 
                newTrans,
                new Date(1576763378), 
                "Test release creation message",
                sAuthorName, 
                "/releaseTag4/dir",
                "/rootDir2/dir",
                2, 
                8);
        
        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertTrue(files.containsKey("/releaseTag4/dir/file3.txt"));
        
        boolean found = false;
        for (Revision rev : newRel.getReleaseRevisions()) {
            if (rev.getFile().getPath().compareTo("/releaseTag4/dir/file3.txt") == 0) {
                found = true;
            }
        }
        assertTrue("The replacement of the replace operation was not found", found);
    }
    
    /**
     * Testing that a {@link File} is correctly removed from a {@link Branch}.
     * @throws Exception
     */
    @Test
    public void testRemoveFromBranch() throws Exception {
        Transaction t = sModelMapper.createTransaction(new Date(97897899));
        ModificationReport m = sModelMapper.createModificationReport(new Date(155323), "another message", sAuthorName);
        SVNVersionedFile f = sModelMapper.createFile("/rootDir/dir/file2.txt");
        sModelMapper.createRevision(2, f, t, m, null, false, "These are the new contents");
        sModelMapper.finalizeTransaction(t, new ArrayList<String>());
        
        Transaction newTrans = sModelMapper.createTransaction(new Date(1576763378));
                
        Branch newBranch = sModelMapper.createBranch(
                new Date(1576763378), 
                newTrans, 
                "Test branch creation message",
                sAuthorName,
                "branch3",
                null,
                "/branch3",
                "/rootDir",
                2,
                11);
        // Check the Files and Dirs were saved in the right private field
        Field dirsField = SVNModelMapper.class.getDeclaredField("fDirectories");
        dirsField.setAccessible(true);
        HashMap<String, Directory> dirs = (HashMap<String, Directory>) dirsField.get(sModelMapper);
        assertTrue(dirs.containsKey("/branch3/dir"));

        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertTrue(files.containsKey("/branch3/dir/file.txt"));
        
        boolean found = false;
        for (Revision r : newBranch.getRevisions()) {
            if (r.getFile().getPath().compareTo("/branch3/dir/file.txt") == 0) {
                found = true;
            }
        }
        assertTrue("The test file was not linked to the associated test release", found);
        
        sModelMapper.removeFromBranch(newBranch, "/branch3/dir/file.txt");
        assertTrue(dirs.containsKey("/branch3/dir"));
        assertTrue(files.containsKey("/branch3/dir/file2.txt"));
        assertFalse(files.containsKey("/branch3/dir/file.txt"));
        
        found = false;
        for (Revision r : newBranch.getRevisions()) {
            if (r.getFile().getPath().compareTo("/branch3/dir/file.txt") == 0) {
                found = true;
            }
        }
        assertFalse("The deleted test file is still linked to the associated test release", found);
        
        sModelMapper.removeFromBranch(newBranch, "/branch3/dir");
        assertFalse(dirs.containsKey("/branch3/dir"));
        assertFalse(files.containsKey("/branch3/dir/file2.txt"));
    }
    
    /**
     * Testing that that a {@link File} is correctly replaced in a {@link Branch}.
     * @throws Exception
     */
    @Test
    public void testReplaceInBranch() throws Exception {
        Transaction newTrans = sModelMapper.createTransaction(new Date(1599997378));
        Branch newBranch = sModelMapper.createBranch(
                new Date(1576763378), 
                newTrans, 
                "Test branch creation message",
                sAuthorName,
                "branch4",
                null,
                "/branch4",
                "/rootDir",
                2,
                14);
        
        sModelMapper.replaceInBranch(
                newBranch, 
                newTrans,
                new Date(1576763378), 
                "Test branch creation message",
                sAuthorName, 
                "/branch4/dir",
                "/rootDir2/dir",
                2, 
                8);
        
        Field filesField = SVNModelMapper.class.getDeclaredField("fFiles");
        filesField.setAccessible(true);
        HashMap<String, SVNVersionedFile> files = (HashMap<String, SVNVersionedFile>) filesField.get(sModelMapper);
        assertTrue(files.containsKey("/branch4/dir/file.txt"));
        
        boolean found = false;
        for (Revision rev : newBranch.getRevisions()) {
            if (rev.getFile().getPath().compareTo("/branch4/dir/file3.txt") == 0) {
                found = true;
            }
        }
        assertTrue("The replacement of the replace operation was not found", found);
    }
    
    /**
     * @throws Exception
     * 
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        AbstractEvolizerSVNImporterTest.tearDownAfterClass();
    }
}
