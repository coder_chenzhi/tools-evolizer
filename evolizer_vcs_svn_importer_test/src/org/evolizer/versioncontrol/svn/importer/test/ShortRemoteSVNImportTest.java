/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.humans.Role;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.CommitterRole;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNImporter;
import org.evolizer.versioncontrol.svn.model.entities.SVNRelease;
import org.evolizer.versioncontrol.svn.model.entities.SVNRevision;
import org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * ShortRemoteSVNImportTest
 * 
 * Tests the {@link EvolizerSVNImporter} class, by extracting a small subset of Httpunit project from Sourceforge. SVN Url:
 * https://httpunit.svn.sourceforge.net/svnroot/httpunit
 * 
 * @author Giacomo Ghezzi
 * 
 */
public class ShortRemoteSVNImportTest extends AbstractEvolizerSVNImporterTest {

    protected static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
    /**
     * 
     * @throws Exception
     *             If problems arise.
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        AbstractEvolizerSVNImporterTest.setUpBeforeClass();

        sSVNUrl = "https://httpunit.svn.sourceforge.net/svnroot/httpunit";
        sSVNUser = "anonymous";
        sSVNPwd = "";

        String[] repositoryData = new String[3];
        repositoryData[0] = sSVNUser;
        repositoryData[1] = sSVNPwd;
        repositoryData[2] = sSVNUrl;
        sImporter = new EvolizerSVNImporter(repositoryData, 2, false, "", null);
        sImporter.importProject(sSession, new NullProgressMonitor(), 0, 50, false);
        sImporter.importProject(sSession, new NullProgressMonitor(), 50, 245, true);
    }

    /**
     * 
     * @throws Exception
     *             If problems arise.
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        AbstractEvolizerSVNImporterTest.tearDownAfterClass();
    }

    /**
     * Tests the number and the names of the releases.
     */
    @Test
    public void testReleasesNumberAndNames() {
        ArrayList<String> releaseNames = new ArrayList<String>();
        releaseNames.add("httpunit_0_9_0");
        releaseNames.add("httpunit_0_9_1");
        releaseNames.add("httpunit_0_9_5");
        releaseNames.add("httpunit_1_0");
        releaseNames.add("httpunit_1_1");
        releaseNames.add("httpunit_1_2");
        releaseNames.add("httpunit_1_2_1");
        releaseNames.add("httpunit_1_2_2");
        releaseNames.add("httpunit_1_2_3");
        releaseNames.add("httpunit_1_2_4");
        releaseNames.add("httpunit_1_2_5");
        releaseNames.add("httpunit_1_2_6");
        releaseNames.add("httpunit_1_2_7");
        releaseNames.add("httpunit_1_3");
        releaseNames.add("httpunit_1_4");
        releaseNames.add("start");

        ArrayList<String> foundReleases = new ArrayList<String>();
        Collection<SVNRelease> releases = getReleases();
        // Testing the number of releases
        assertEquals(releaseNames.size(), releases.size());
        // testing the releases names
        for (SVNRelease rel : releases) {
            assertTrue("Imported SVN tag " + rel.getName() + " does not really exists", releaseNames.contains(rel
                    .getName()));
            assertFalse("Release " + rel.getName() + " was imported twice", foundReleases.contains(rel.getName()));
            foundReleases.add(rel.getName());
        }
    }

    /**
     * Tests a specific release.
     * 
     * @throws ParseException
     *             If problems arise.
     */
    @Test
    public void testRelease() throws ParseException {
        SVNRelease rel = getRelease("httpunit_1_2_2");
        assertEquals("Release " + rel.getName() + " was not created with its real date", sDateFormat.parse(
        "2001-02-21 20:01:07 +0100").toString(), rel.getTimeStamp().toString());Set<Revision> revisions = ((SVNRelease) rel).getReleaseRevisions();
        int versionsCount = 0;
        for (Revision tempVer : revisions) {
            Revision ver = ((SVNRevision) tempVer).getAncestor();
            if (ver.getState() == null) {
                versionsCount++;
            }
        }
        assertEquals("Wrong number of FileVersions for release " + rel.getName(), 71, versionsCount);

        // /tags/httpunit_1_2_2/httpunit/src/com/meterware/httpunit/WebResponse.java
        int totFilesTested = 0;
        for (Revision currRev : revisions) {

            // Testing a normal file
            if (currRev.getFile().getPath().compareTo(
                    "/tags/httpunit_1_2_2/httpunit/src/com/meterware/httpunit/WebResponse.java") == 0) {

                totFilesTested++;
                assertEquals("Wrong File Version", "93", currRev.getNumber());
                SVNRevision ancestor = ((SVNRevision) currRev).getAncestor();
                assertEquals("Wrong ancestor", "/trunk/httpunit/src/com/meterware/httpunit/WebResponse.java", ancestor
                        .getFile().getPath());

                ArrayList<String> versionNums = new ArrayList<String>();
                versionNums.add("85");
                versionNums.add("83");
                versionNums.add("82");
                versionNums.add("79");
                versionNums.add("77");
                versionNums.add("75");
                versionNums.add("72");
                versionNums.add("69");
                versionNums.add("67");
                versionNums.add("60");
                versionNums.add("59");
                versionNums.add("58");
                versionNums.add("56");
                versionNums.add("54");
                versionNums.add("51");
                versionNums.add("48");
                versionNums.add("42");
                versionNums.add("40");
                versionNums.add("38");
                versionNums.add("37");
                versionNums.add("32");
                versionNums.add("29");
                versionNums.add("26");
                versionNums.add("22");
                versionNums.add("21");
                versionNums.add("18");
                versionNums.add("9");
                versionNums.add("8");
                versionNums.add("2");

                for (String version : versionNums) {
                    assertTrue("Wrong version number for file " + ancestor.getFile().getPath(), version
                            .compareTo(ancestor.getNumber()) == 0);

                    if (version.compareTo("37") == 0) {
                        assertTrue("Wrong commit message for version " + version
                                + " of file /trunk/httpunit/src/com/meterware/httpunit/WebResponse.java for release "
                                + rel.getName(), ancestor.getReport().getCommitMessage().contains(
                                "Added getTitle, getURLString"));
                        assertTrue(
                                "Wrong commit author for version "
                                        + version
                                        + " of file /trunk/httpunit/src/com/meterware/httpunit/WebResponse.java for release "
                                        + rel.getName(),
                                ancestor.getReport().getAuthor().getFirstName().compareTo("russgold") == 0);

                        for (Role r : ancestor.getReport().getAuthor().getRoles()) {
                            if (r instanceof CommitterRole) {
                                assertTrue("Author of " + ancestor.getFile().getName() + ":" + ancestor.getNumber()
                                        + " doesn't have it in its artifacts", ((CommitterRole) r).getArtifacts()
                                        .contains(ancestor));
                                break;
                            }
                        }
                        assertTrue("Wrong commit date for version " + version
                                + " of file /trunk/httpunit/src/com/meterware/httpunit/WebResponse.java for release "
                                + rel.getName(), sDateFormat.parse("2000-09-11 16:23:48 +0200").equals(
                                        ancestor.getReport().getCreationTime()));
                    }
                    if (ancestor.getPreviousRevision() != null) {
                        assertEquals("Next and PreviousVersion of two successive FileVersions don't match for release "
                                + rel.getName(), ancestor.getPreviousRevision().getNextRevision().getId(), ancestor.getId());
                    }
                    if (ancestor.getPreviousRevision() != null) {
                        ancestor = sSession.load(SVNRevision.class, ancestor
                                .getPreviousRevision().getId());
                    }

                }
            } else if (currRev.getFile().getPath().compareTo("/tags/httpunit_1_2_2/httpunit/examples/NearWords.java") == 0) {
                totFilesTested++;
                assertTrue("Wrong ancestor for moved file " + currRev.getFile().getPath() + " for release "
                        + rel.getName(), ((SVNRevision) currRev).getAncestor().getFile().getPath().compareTo(
                        "/branches/meterware/httpunit/examples/NearWords.java") == 0);
                assertEquals("Wrong revision number for the ancestor of file " + currRev.getFile().getPath()
                        + " for release " + rel.getName(), "3", ((SVNRevision) currRev).getAncestor().getNumber());
                assertTrue("Wrong commit message for the ancestor of file " + currRev.getFile().getPath()
                        + " for release " + rel.getName(), ((SVNRevision) currRev).getAncestor().getReport()
                        .getCommitMessage().compareTo(
                                "This commit was manufactured by cvs2svn to create branch 'meterware'.") == 0);
            }
        }
        assertEquals(
                "Didn't test all the versions I was supposed to test, I might have not found some of them for release "
                        + rel.getName(),
                2,
                totFilesTested);
    }

    /**
     * Tests a specific branch.
     */
    @Test
    public void testBranches() {
        Collection<Branch> branches = getBranches();
        assertEquals("The number of found branches are not what expected", branches.size(), 1);
        Branch branch = branches.iterator().next();
        assertEquals("Branch name is not what it's supposed to be", "meterware", branch.getName());
        assertEquals("Wrong number of FileVersions for branch " + branch.getName(), 15, branch.getRevisions().size());
    }

    /**
     * Tests a specific ChangeSet.
     * 
     * @throws ParseException
     *             If problems arise.
     */
    @Test
    public void testChangeSet() throws ParseException {
        Transaction cs = getTransaction("2000-10-31 20:54:15");
        assertNotNull("No transaction done at date 2000-10-31 20:54:15 +0100 was found", cs);
        assertEquals("Wrong author for transaction 2000-10-31 20:54:15 +0100", "russgold", cs.getInvolvedRevisions()
                .iterator().next().getAuthor().getFirstName());
        ArrayList<String> modified = new ArrayList<String>();
        ArrayList<String> added = new ArrayList<String>();

        modified.add("/trunk/httpunit/doc/release_notes.txt");
        modified.add("/trunk/httpunit/src/com/meterware/httpunit/GetMethodWebRequest.java");
        modified.add("/trunk/httpunit/src/com/meterware/httpunit/ParsedHTML.java");
        modified.add("/trunk/httpunit/src/com/meterware/httpunit/PostMethodWebRequest.java");
        modified.add("/trunk/httpunit/src/com/meterware/httpunit/WebForm.java");
        modified.add("/trunk/httpunit/src/com/meterware/httpunit/WebRequest.java");
        modified.add("/trunk/httpunit/src/com/meterware/httpunit/WebResponse.java");
        modified.add("/trunk/httpunit/src/com/meterware/httpunit/WebTable.java");
        modified.add("/trunk/httpunit/test/com/meterware/httpunit/FormParametersTest.java");
        modified.add("/trunk/httpunit/test/com/meterware/httpunit/FormSubmitTest.java");
        modified.add("/trunk/httpunit/test/com/meterware/httpunit/HtmlTablesTest.java");
        modified.add("/trunk/httpunit/test/com/meterware/httpunit/WebFormTest.java");

        added.add("/trunk/httpunit/src/com/meterware/httpunit/SubmitButton.java");

        assertEquals("Erroneous number of involved file versions for ChangeSet at date " + cs.getStarted(), modified
                .size()
                + added.size(), cs.getInvolvedRevisions().size());
        for (Revision ver : cs.getInvolvedRevisions()) {
            String fileVersionId = ver.getFile().getPath() + ":" + ver.getNumber();
            assertEquals(
                    "FileVersion " + fileVersionId + " has not the number of associated to the change set",
                    "67",
                    ver.getNumber());
            assertNotNull("FileVersion " + fileVersionId + " is not associated to any file", ver.getFile());
            if (modified.contains(ver.getFile().getPath())) {
                assertNotNull("FileVersion " + fileVersionId
                        + " represents a modification, but it's not linked to a previous version", ver
                        .getPreviousRevision());
                assertEquals("The previous FileVersion is not linked this one, " + fileVersionId, ver, ver
                        .getPreviousRevision().getNextRevision());
                assertNull(ver.getState());
            } else if (added.contains(ver.getFile().getPath())) {
                assertNull("FileVersion " + fileVersionId + " has a previous one, even though it's an add", ver
                        .getPreviousRevision());
                if (((SVNVersionedFile) ver.getFile()).getCopiedFrom() != null) {
                    assertEquals(ver.getFile(), ((SVNVersionedFile) ((SVNVersionedFile) ver.getFile()).getCopiedFrom())
                            .getCopiedTo());
                }
            }
        }
    }

    /**
     * Tests a specific FileVersion over multiple releases.
     */
    @Test
    public void testFileVersionInMultipleReleases() {
        String fileName = "/trunk/httpunit/src/com/meterware/httpunit/WebResponse.java";
        SVNRevision fileVersion = getSpecificFileVersion(48, fileName);

        ArrayList<String> releases = new ArrayList<String>();
        releases.add("httpunit_1_1");
        releases.add("httpunit_1_2");
        releases.add("httpunit_1_2_1");
        releases.add("httpunit_1_2_2");
        releases.add("httpunit_1_2_3");
        releases.add("httpunit_1_2_4");
        releases.add("httpunit_1_2_5");
        releases.add("httpunit_1_2_6");
        releases.add("httpunit_1_2_7");
        releases.add("httpunit_1_3");
        releases.add("httpunit_1_4");
        assertEquals(
                "The number of releases associated to FileVersion "
                        + "/trunk/httpunit/src/com/meterware/httpunit/WebResponse.java is incorrect",
                releases.size(),
                fileVersion.getReleases().size());

        for (Release r : fileVersion.getReleases()) {
            assertTrue("Fileversion is linked to a wrong release", releases.contains(r.getName()));
            releases.remove(r.getName());
        }
        assertTrue(
                "Some releases associated to FileVersion /trunk/httpunit/src/com/meterware/httpunit/WebResponse.java were not found",
                releases.isEmpty());
    }

    /**
     * Tests some specific directory structures.
     */
    @Test
    public void testDirectoryStructure() {
        ArrayList<String> folders = new ArrayList<String>();
        folders.add("/trunk/httpunit/doc/tutorial/src/tutorial/persistence");
        folders.add("/trunk/httpunit/examples");
        folders.add("/trunk/httpunit/jars");
        folders.add("/trunk/httpunit/src/com/meterware/httpunit");
        folders.add("/trunk/httpunit/test/com/meterware/httpunit");
        folders.add("/trunk/httpunit/test/com/meterware/servletunit");

        for (String path : folders) {
            assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
            String oldPath = path;
            int index = path.lastIndexOf("/");
            while (index > -1) {
                if (index == 0) {
                    path = "/";
                    assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
                    assertEquals(
                            "Father " + path + "and Child " + oldPath + "are not consistent",
                            getDirectory(oldPath).getParentDirectory(),
                            getDirectory(path));
                    assertTrue("Father " + path + "does not contain Child " + oldPath, getDirectory(path).getChildren()
                            .contains(getDirectory(oldPath)));
                    oldPath = path;
                    index = -1;
                } else {
                    path = path.substring(0, index);
                    assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
                    assertEquals(
                            "Father " + path + "and Child " + oldPath + "are not consistent",
                            getDirectory(oldPath).getParentDirectory(),
                            getDirectory(path));
                    assertTrue("Father " + path + "does not contain Child " + oldPath, getDirectory(path).getChildren()
                            .contains(getDirectory(oldPath)));
                    oldPath = path;
                    index = path.lastIndexOf("/");
                }
            }
        }
    }

    /**
     * Tests files that were moved and deleted
     */
    @Test
    public void testMoveAndDelete() {
        SVNVersionedFile newerFile = getFile("/trunk/httpunit/index.html");
        assertNotNull("File /trunk/httpunit/index.html doesn't exist in the data extracted", newerFile);

        SVNVersionedFile f = getFile("/trunk/httpunit/doc/index.html");
        assertNotNull("File /trunk/httpunit/doc/index.html doesn't exist in the data extracted", newerFile);
        assertEquals("The last version of File " + f.getPath()
                + " is not the one expected (the one in which the move was made)", "22", f.getLatestRevision()
                .getNumber());
        assertEquals(
                "The first version of File " + newerFile.getPath()
                        + " is not the one expected (the one in which the move was made, and thus the file created)",
                "22",
                getFirstRevision(newerFile).getNumber());
        assertEquals(
                "The original file, " + newerFile.getPath() + " is coming doesn't have a link to the moved file",
                newerFile,
                f.getCopiedTo());
        assertEquals("File " + newerFile.getPath() + " is not linked to the one it has been copied from", newerFile
                .getCopiedFrom(), f);
        assertEquals(((SVNRevision) getFirstRevision(newerFile)).getAncestor(), f.getLatestRevision());
        newerFile = f;

        SVNVersionedFile deletedFile =
                getFile("/trunk/httpunit/src/com/meterware/httpunit/UncheckedParameterCollection.java");
        assertNotNull(
                "File /trunk/httpunit/src/com/meterware/httpunit/UncheckedParameterCollection.java doesn't exist in the data extracted",
                deletedFile);
        assertEquals("209", deletedFile.getLatestRevision().getNumber());
    }

    /**
     * Tests a specific author.
     */
    @Test
    public void testAuthors() {
        assertEquals("The total number of authors found is not what it's supposed to be", 2, getPersons().size());
        Person author = getSpecificPerson("russgold");
        assertNotNull("Author " + author.getFirstName() + " was not found during the import", author);
        List<Long> res =
                sSession
                        .query(
                                "SELECT r.number FROM Revision as r, Person as p, CommitterRole as c WHERE c in elements(p.roles) and r in elements(c.artifacts) AND p.firstName='"
                                        + author.getFirstName() + "' GROUP BY r.number",
                                Long.class);
        assertEquals("Total commits found for author " + author.getFirstName() + " is not what expected ", 226, res
                .size());
    }
}
