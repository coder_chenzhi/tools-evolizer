/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.humans.Role;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.CommitterRole;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNImporter;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNPlugin;
import org.evolizer.versioncontrol.svn.model.entities.SVNRelease;
import org.evolizer.versioncontrol.svn.model.entities.SVNRevision;
import org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * Tests the {@link EvolizerSVNImporter} class, fetching the history of httpunit (up to revision 1022). Beware that, depending on the
 * Sourceforge SVN server response time, it might take quite sometime (at least 20 minutes).
 * 
 * @author ghezzi
 * 
 */
public class LongRemoteSVNImportTest extends AbstractEvolizerSVNImporterTest {

    private static String sConfig = "./config/httpunit_svntest_config.properties";
    private static String sSVNUrl;
    private static String sSVNUser;
    private static String sSVNPwd;
    private static int sTOTReleases = 0;
    private static int sTOTBranches = 0;
    private static Properties props;
    private static ArrayList<String> sReleaseNames = new ArrayList<String>();
    private static ArrayList<String> sBranchNames = new ArrayList<String>();
    private static ArrayList<String> sReleasesToTest = new ArrayList<String>();
    private static ArrayList<String> sBranchesToTest = new ArrayList<String>();
    private static SimpleDateFormat sDateFormat;

    /**
     * The SVNImporter to test
     */
    private static EvolizerSVNImporter sImporter;

    /**
     * 
     * @throws Exception
     *             If problems arise.
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        /***** Loading the test settings *****/
        props = new Properties();
        InputStream propertiesInputStream = EvolizerSVNPlugin.openFile(sConfig);
        if (propertiesInputStream != null) {
            props.load(propertiesInputStream);
        }
        propertiesInputStream.close();
        sDateFormat = new SimpleDateFormat(props.getProperty("dateFormat"));
        sSVNUrl = props.getProperty("url");
        sSVNUser = props.getProperty("user");
        sSVNPwd = props.getProperty("password");

        String releases = props.getProperty("releases");
        StringTokenizer tokenizer = new StringTokenizer(releases, ",");
        while (tokenizer.hasMoreTokens()) {
            sTOTReleases++;
            sReleaseNames.add(tokenizer.nextToken());
        }

        int toTest = Integer.parseInt(props.getProperty("testedReleasesNum"));
        for (int i = 1; i <= toTest; i++) {
            sReleasesToTest.add(props.getProperty("release" + i));
        }
        String branches = props.getProperty("branches");
        tokenizer = new StringTokenizer(branches, ",");
        while (tokenizer.hasMoreTokens()) {
            sTOTBranches++;
            sBranchNames.add(tokenizer.nextToken());
        }

        toTest = Integer.parseInt(props.getProperty("testedBranchesNum"));
        for (int i = 1; i <= toTest; i++) {
            sBranchesToTest.add(props.getProperty("branch" + i));
        }
        AbstractEvolizerSVNImporterTest.setUpBeforeClass();

        String[] repositoryData = new String[3];
        repositoryData[0] = sSVNUser;
        repositoryData[1] = sSVNPwd;
        repositoryData[2] = sSVNUrl;
        sImporter = new EvolizerSVNImporter(repositoryData, 2, false, "", null);
        sImporter.importProject(
                sSession,
                new NullProgressMonitor(),
                Integer.parseInt(props.getProperty("beginImport")),
                Integer.parseInt(props.getProperty("endImport")),
                false);
    }

    /**
     * 
     * @throws Exception
     *             If problems arise.
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        AbstractEvolizerSVNImporterTest.tearDownAfterClass();
    }

    /**
     * Tests the number and the names of the releases.
     */
    @Test
    public void testReleasesNumberAndNames() {
        Collection<SVNRelease> releases = getReleases();
        // Testing the number of releases
        assertEquals(sTOTReleases, releases.size());
        ArrayList<String> foundReleases = new ArrayList<String>();
        // testing the releases names
        for (SVNRelease rel : releases) {
            assertTrue("Imported SVN tag " + rel.getName() + " does not really exists", sReleaseNames.contains(rel
                    .getName()));
            assertFalse("Release " + rel.getName() + " was imported twice", foundReleases.contains(rel.getName()));
            foundReleases.add(rel.getName());
        }
    }

    /**
     * Tests some specific releases
     * 
     * @throws ParseException
     *             If problems arise.
     */
    @Test
    public void testReleases() throws ParseException {
        Collection<SVNRelease> releases = getReleases();
        int releasesCount = 0;
        // Iterating over the relases to be tested
        for (SVNRelease rel : releases) {
            int releaseIndex = sReleasesToTest.indexOf(rel.getName());
            if (releaseIndex >= 0) {
                releasesCount++;
                String releaseRef = "release" + (releaseIndex + 1);
                assertEquals("Release " + rel.getName() + " was not created with its real date", sDateFormat.parse(
                        props.getProperty(releaseRef + ".date")).toString(), rel.getTimeStamp().toString());
                Set<Revision> revisions = ((SVNRelease) rel).getReleaseRevisions();
                int versionsCount = 0;
                int deletionsCount = 0;
                for (Revision tempVer : revisions) {
                    Revision ver = ((SVNRevision) tempVer).getAncestor();
                    if (ver.getState() == null) {
                        versionsCount++;
                    }
                }
                assertEquals("Wrong number of FileVersions for release " + rel.getName(), props.getProperty(releaseRef
                        + ".totFileVersions"), versionsCount + "");

                if (props.getProperty(releaseRef + ".totDeletedFiles") != null) {
                    assertEquals("Wrong number of deleted Files for release " + rel.getName(), props
                            .getProperty(releaseRef + ".totDeletedFiles"), deletionsCount + "");
                }
                ArrayList<String> switchedFiles = new ArrayList<String>();
                if (props.getProperty(releaseRef + ".switchedFiles") != null
                        && props.getProperty(releaseRef + ".switchedFiles").compareTo("") != 0) {
                    int totMoved = Integer.parseInt(props.getProperty(releaseRef + ".switchedFiles"));
                    for (int i = 1; i <= totMoved; i++) {
                        switchedFiles.add(props.getProperty(releaseRef + ".switchedFile" + 1));
                    }
                }

                ArrayList<String> filesToTest = new ArrayList<String>();
                int totFilesToTest = Integer.parseInt(props.getProperty(releaseRef + ".totFilesToTest"));
                for (int i = 1; i <= totFilesToTest; i++) {
                    filesToTest.add(props.getProperty(releaseRef + ".file" + i));
                }
                // It could actually be that I decided to test no files for this release
                if (totFilesToTest > 0) {
                    int totFilesTested = 0;
                    for (Revision currRev : revisions) {
                        // Testing a normal file
                        int fileIndex = filesToTest.indexOf(currRev.getFile().getPath());
                        if (fileIndex >= 0) {
                            totFilesTested++;
                            String fileRef = releaseRef + ".file" + (fileIndex + 1);
                            assertEquals("Wrong File Version", (Long) Long.parseLong(props.getProperty(fileRef
                                    + ".releaseVersion")), (Long) Long.parseLong(currRev.getNumber()));
                            SVNRevision ancestor = ((SVNRevision) currRev).getAncestor();
                            assertEquals("Wrong ancestor", props.getProperty(fileRef + ".realFile"), ancestor.getFile()
                                    .getPath());
                            String versions = props.getProperty(fileRef + ".versions");
                            StringTokenizer tokenizer = new StringTokenizer(versions, ",");
                            ArrayList<String> versionNums = new ArrayList<String>();
                            while (tokenizer.hasMoreTokens()) {
                                versionNums.add(tokenizer.nextToken());
                            }
                            for (String version : versionNums) {
                                assertTrue("Wrong version number for file " + ancestor.getFile().getPath(), version
                                        .compareTo(ancestor.getNumber()) == 0);
                                if (props.getProperty(fileRef + version + ".message") != null) {
                                    assertTrue("Wrong commit message for version " + version + " of file "
                                            + props.getProperty(fileRef + ".realFile") + " for release "
                                            + rel.getName(), ancestor.getReport().getCommitMessage().contains(
                                            props.getProperty(fileRef + version + ".message")));
                                    assertTrue("Wrong commit author for version " + version + " of file "
                                            + props.getProperty(fileRef + ".realFile") + " for release "
                                            + rel.getName(), ancestor.getReport().getAuthor().getFirstName().compareTo(
                                            props.getProperty(fileRef + version + ".author")) == 0);

                                    for (Role r : ancestor.getReport().getAuthor().getRoles()) {
                                        if (r instanceof CommitterRole) {
                                            assertTrue(
                                                    "Author of " + ancestor.getFile().getName() + ":"
                                                            + ancestor.getNumber()
                                                            + " doesn't have it in its artifacts",
                                                    ((CommitterRole) r).getArtifacts().contains(ancestor));
                                            break;
                                        }
                                    }
                                    assertTrue("Wrong commit date for version " + version + " of file "
                                            + props.getProperty(fileRef + ".realFile") + " for release "
                                            + rel.getName(), sDateFormat.parse(
                                            props.getProperty(fileRef + version + ".date")).equals(
                                            ancestor.getReport().getCreationTime()));
                                }
                                if (ancestor.getPreviousRevision() != null) {
                                    assertEquals(
                                            "Next and PreviousVersion of two successive FileVersions don't match for release "
                                                    + rel.getName(),
                                            ancestor.getPreviousRevision().getNextRevision(),
                                            ancestor);
                                }
                                ancestor = (SVNRevision) ancestor.getPreviousRevision();

                            }
                            // assert ancestor has no more previous versions
                            assertNull("The number of versions for " + props.getProperty(fileRef + ".realFile")
                                    + " is more then expected", ancestor);
                        }
                        // Testing files that were replaced by others during the tag creation
                        fileIndex = switchedFiles.indexOf(currRev.getFile().getPath());
                        if (fileIndex >= 0) {
                            assertTrue("Wrong ancestor for moved file " + currRev.getFile().getPath() + " for release "
                                    + rel.getName(), ((SVNRevision) currRev).getAncestor().getFile().getPath()
                                    .compareTo(
                                            props.getProperty(releaseRef + ".switchedFile" + (fileIndex + 1)
                                                    + ".newParent")) == 0);
                            assertEquals("Wrong revision number for the ancestor of file "
                                    + currRev.getFile().getPath() + " for release " + rel.getName(), (Long) Long
                                    .parseLong(props.getProperty(releaseRef + ".switchedFile" + (fileIndex + 1)
                                            + ".newParentVersion")), (Long) Long.parseLong(((SVNRevision) currRev)
                                    .getAncestor().getNumber()));
                            assertTrue("Wrong commit message for the ancestor of file " + currRev.getFile().getPath()
                                    + " for release " + rel.getName(), ((SVNRevision) currRev).getAncestor()
                                    .getReport().getCommitMessage().compareTo(
                                            props.getProperty(releaseRef + ".switchedFile" + (fileIndex + 1)
                                                    + ".newParentCommitMessage")) == 0);
                        }
                    }
                    assertEquals(
                            "Didn't test all the versions I was supposed to test, I might have not found some of them for release "
                                    + rel.getName(),
                            totFilesToTest,
                            totFilesTested);
                }
            }
        }
        assertEquals("Didn't test all the releases I was supposed to test", sReleasesToTest.size(), releasesCount);
    }

    /**
     * Tests some specific branches
     */
    @Test
    public void testBranches() {
        Collection<Branch> branches = getBranches();
        assertEquals("The number of found branches are not what expected", branches.size(), sTOTBranches);
        ArrayList<String> foundBranches = new ArrayList<String>();
        for (Branch br : branches) {
            assertTrue("Imported SVN branch " + br.getName() + " does not really exists", sBranchNames.contains(br
                    .getName()));
            assertFalse("Branch " + br.getName() + " was imported twice", foundBranches.contains(br.getName()));
            foundBranches.add(br.getName());
        }
        for (Branch branch : branches) {
            int branchIndex = sBranchesToTest.indexOf(branch.getName());
            if (branchIndex >= 0) {
                String branchRef = "branch" + (branchIndex + 1);
                Set<Revision> revisions = branch.getRevisions();
                int versionsCount = 0;
                int deletionsCount = 0;
                int movesFromCount = 0;
                for (Revision ver : revisions) {
                    if (ver.getState() != null && ver.getState().compareTo("DELETED") == 0) {
                        assertNull("The FileVersion has a newer version, even though it is marked as deleted", ver
                                .getNextRevision());
                        deletionsCount++;
                    } else if (ver.getState() != null && ver.getState().compareTo("MOVED") == 0) {
                        assertNotNull(
                                "File " + ver.getFile()
                                        + " was moved but it was not linked to the new file created from it",
                                ((SVNVersionedFile) ver.getFile()).getCopiedTo());
                        assertEquals("CopiedTo and CopiedFrom don't match", ((SVNVersionedFile) ((SVNVersionedFile) ver
                                .getFile()).getCopiedTo()).getCopiedFrom(), ver.getFile());
                        movesFromCount++;
                    } else {
                        versionsCount++;
                    }
                }
                assertEquals("Wrong number of FileVersions for branch " + branch.getName(), props.getProperty(branchRef
                        + ".totFileVersions"), versionsCount + "");
            }
        }
    }

    /**
     * Tests some specific ChangeSets
     * 
     * @throws ParseException
     *             If problems arise.
     */
    @Test
    public void testChangeSets() throws ParseException {
        int totCs = Integer.parseInt(props.getProperty("testChangeSets"));
        for (int i = 1; i <= totCs; i++) {
            Transaction cs = getTransaction(props.getProperty("changeSet" + i + ".date"));
            if (cs != null) {
                ArrayList<String> modified = new ArrayList<String>();
                ArrayList<String> deleted = new ArrayList<String>();
                ArrayList<String> added = new ArrayList<String>();
                ArrayList<String> moved = new ArrayList<String>();

                if (props.getProperty("changeSet" + i + ".modifiedFiles") != null
                        && props.getProperty("changeSet" + i + ".modifiedFiles").compareTo("") != 0) {
                    StringTokenizer tokenizer =
                            new StringTokenizer(props.getProperty("changeSet" + i + ".modifiedFiles"), ",");
                    while (tokenizer.hasMoreTokens()) {
                        modified.add(tokenizer.nextToken());
                    }
                }
                if (props.getProperty("changeSet" + i + ".deletedFiles") != null
                        && props.getProperty("changeSet" + i + ".deletedFiles").compareTo("") != 0) {
                    StringTokenizer tokenizer =
                            new StringTokenizer(props.getProperty("changeSet" + i + ".deletedFiles"), ",");
                    while (tokenizer.hasMoreTokens()) {
                        deleted.add(tokenizer.nextToken());
                    }

                }
                if (props.getProperty("changeSet" + i + ".addedFiles") != null
                        && props.getProperty("changeSet" + i + ".addedFiles").compareTo("") != 0) {
                    StringTokenizer tokenizer =
                            new StringTokenizer(props.getProperty("changeSet" + i + ".addedFiles"), ",");
                    while (tokenizer.hasMoreTokens()) {
                        added.add(tokenizer.nextToken());
                    }
                }
                if (props.getProperty("changeSet" + i + ".movedFiles") != null
                        && props.getProperty("changeSet" + i + ".movedFiles").compareTo("") != 0) {
                    StringTokenizer tokenizer =
                            new StringTokenizer(props.getProperty("changeSet" + i + ".movedFiles"), ",");
                    while (tokenizer.hasMoreTokens()) {
                        moved.add(tokenizer.nextToken());
                    }
                }
                assertEquals(
                        "Erroneous number of involved file versions for ChangeSet at date " + cs.getStarted(),
                        modified.size() + deleted.size() + added.size() + moved.size(),
                        cs.getInvolvedRevisions().size());
                for (Revision ver : cs.getInvolvedRevisions()) {
                    String fileVersionId = ver.getFile().getPath() + ":" + ver.getNumber();
                    assertEquals(
                            "FileVersion " + fileVersionId + " has not the number of associated to the change set",
                            (Long) Long.parseLong(props.getProperty("changeSet" + i)),
                            (Long) Long.parseLong(ver.getNumber()));
                    assertEquals(
                            "ChangeSet and FileVersion is not symmetrical for FileVersion " + fileVersionId,
                            cs,
                            ((SVNRevision) ver).getChangeSet());
                    assertNotNull("FileVersion " + fileVersionId + " is not associated to any file", ver.getFile());
                    if (modified.contains(ver.getFile().getPath())) {
                        assertNotNull("FileVersion " + fileVersionId
                                + " represents a modification, but it's not linked to a previous version", ver
                                .getPreviousRevision());
                        assertEquals("The previous FileVersion is not linked this one, " + fileVersionId, ver, ver
                                .getPreviousRevision().getNextRevision());
                        assertNull(ver.getState());
                    } else if (deleted.contains(ver.getFile().getPath())) {
                        assertEquals("FileVersion " + fileVersionId
                                + " is not marked as a delete action, even though it was", "DELETED", ver.getState());
                        assertNotNull("FileVersion " + fileVersionId
                                + " is not linked to a previous version, even though it should", ver
                                .getPreviousRevision());
                        assertEquals("The previous FileVersion is not linked this one, " + fileVersionId, ver, ver
                                .getPreviousRevision().getNextRevision());
                        assertNull("FileVersion " + fileVersionId + " has a next one, even though it's a delete", ver
                                .getNextRevision());
                    } else if (added.contains(ver.getFile().getPath())) {
                        assertNull("FileVersion " + fileVersionId + " has a previous one, even though it's an add", ver
                                .getPreviousRevision());
                        if (((SVNVersionedFile) ver.getFile()).getCopiedFrom() != null) {
                            assertEquals(ver.getFile(), ((SVNVersionedFile) ((SVNVersionedFile) ver.getFile())
                                    .getCopiedFrom()).getCopiedTo());
                        }
                    } else if (moved.contains(ver.getFile().getPath())) {
                        assertEquals("FileVersion " + fileVersionId
                                + " is not marked as a move action, even though it was", "MOVED", ver.getState());
                        assertNull("FileVersion " + fileVersionId + " has a next one, even though it's a move", ver
                                .getNextRevision());
                        assertEquals(
                                "The file to which this one, " + ver.getFile() + ", was copied, is not linked to it",
                                ver.getFile(),
                                ((SVNVersionedFile) ((SVNVersionedFile) ver.getFile()).getCopiedTo()).getCopiedFrom());
                    }
                }
            }
        }
    }

    /**
     * Tests some specific FileVersions over multiple releases
     */
    @Test
    public void testFileVersionInMultipleReleases() {
        int toTest = Integer.parseInt(props.getProperty("crossReleaseFileVersions"));
        for (int i = 1; i <= toTest; i++) {
            Long verNum =
                    Long.parseLong(props.getProperty("crossReleaseFileVersion" + i).substring(
                            props.getProperty("crossReleaseFileVersion" + i).indexOf(":") + 1));
            String fileName =
                    props.getProperty("crossReleaseFileVersion" + i).substring(
                            0,
                            props.getProperty("crossReleaseFileVersion" + i).indexOf(":"));
            SVNRevision fileVersion = getSpecificFileVersion(verNum, fileName);
            StringTokenizer tokenizer =
                    new StringTokenizer(props.getProperty("crossReleaseFileVersion" + i + ".releases"), ",");
            assertEquals(
                    "The number of releases associated to FileVersion "
                            + props.getProperty("crossReleaseFileVersion" + i) + " is incorrect",
                    tokenizer.countTokens(),
                    fileVersion.getReleases().size());
            ArrayList<String> releases = new ArrayList<String>();
            while (tokenizer.hasMoreTokens()) {
                releases.add(tokenizer.nextToken());
            }
            for (Release r : fileVersion.getReleases()) {
                assertTrue("Fileversion is linked to a wrong release", releases.contains(r.getName()));
                releases.remove(r.getName());
            }
            assertTrue("Some releases associated to FileVersion " + props.getProperty("crossReleaseFileVersion" + i)
                    + " were not found", releases.isEmpty());
        }
    }

    /**
     * Tests some specific directory structures
     */
    @Test
    public void testDirectoryStructure() {
        StringTokenizer tokenizer = new StringTokenizer(props.getProperty("folders"), ",");
        while (tokenizer.hasMoreTokens()) {
            String path = tokenizer.nextToken();
            assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
            String oldPath = path;
            int index = path.lastIndexOf("/");
            while (index > -1) {
                if (index == 0) {
                    path = "/";
                    assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
                    assertEquals(
                            "Father " + path + "and Child " + oldPath + "are not consistent",
                            getDirectory(oldPath).getParentDirectory(),
                            getDirectory(path));
                    assertTrue("Father " + path + "does not contain Child " + oldPath, getDirectory(path).getChildren()
                            .contains(getDirectory(oldPath)));
                    oldPath = path;
                    index = -1;
                } else {
                    path = path.substring(0, index);
                    assertNotNull("The folder " + path + " was not created in the import", getDirectory(path));
                    assertEquals(
                            "Father " + path + "and Child " + oldPath + "are not consistent",
                            getDirectory(oldPath).getParentDirectory(),
                            getDirectory(path));
                    assertTrue("Father " + path + "does not contain Child " + oldPath, getDirectory(path).getChildren()
                            .contains(getDirectory(oldPath)));
                    oldPath = path;
                    index = path.lastIndexOf("/");
                }
            }
        }
    }

    /**
     * Tests files that were moved and deleted
     */
    @Test
    public void testMoveAndDelete() {
        if (props.getProperty("movedFileName") != null || props.getProperty("movedFileName").compareTo("") != 0) {
            SVNVersionedFile newerFile = getFile(props.getProperty("movedFileName"));
            assertNotNull(
                    "File " + props.getProperty("movedFileName") + " doesn't exist in the data extracted",
                    newerFile);
            for (int i = 1; i <= Integer.parseInt(props.getProperty("totMoves")); i++) {
                SVNVersionedFile f = getFile(props.getProperty("move" + i + ".source"));
                assertNotNull("File " + props.getProperty("move" + i + ".source")
                        + " doesn't exist in the data extracted", newerFile);
                assertEquals("The last version of File " + f.getPath()
                        + " is not the one expected (the one in which the move was made)", (Long) Long.parseLong(props
                        .getProperty("move" + i + ".revision")), (Long) Long.parseLong(f.getLatestRevision()
                        .getNumber()));
                assertEquals(
                        "The first version of File "
                                + newerFile.getPath()
                                + " is not the one expected (the one in which the move was made, and thus the file created)",
                        (Long) Long.parseLong(props.getProperty("move" + i + ".revision")),
                        (Long) Long.parseLong(newerFile.getRevisions().get(0).getNumber()));
                assertEquals("The original file, " + newerFile.getPath()
                        + " is coming doesn't have a link to the moved file", newerFile, f.getCopiedTo());
                assertEquals(
                        "File " + newerFile.getPath() + " is not linked to the one it has been copied from",
                        newerFile.getCopiedFrom(),
                        f);
                assertEquals(((SVNRevision) newerFile.getRevisions().get(0)).getAncestor(), f.getLatestRevision());
                newerFile = f;
            }
        } else {
            SVNVersionedFile deletedFile = getFile(props.getProperty("deletedFileName"));
            assertNotNull(
                    "File " + props.getProperty("deletedFileName") + " doesn't exist in the data extracted",
                    deletedFile);
            assertEquals((Long) Long.parseLong(props.getProperty("deleteRevision")), (Long) Long.parseLong(deletedFile
                    .getLatestRevision().getNumber()));
        }
    }

    /**
     * Tests some specific authors
     */
    @Test
    public void testAuthors() {
        StringTokenizer st = new StringTokenizer(props.getProperty("authors"), ",");
        assertEquals(
                "The total number of authors found is not what it's supposed to be",
                st.countTokens(),
                getPersons().size());
        while (st.hasMoreTokens()) {
            Person author = getSpecificPerson(st.nextToken());
            assertNotNull("Author " + author.getFirstName() + " was not found during the import", author);
            if (author.getFirstName().compareTo(props.getProperty("testedAuthor")) == 0) {
                int totalCommits = Integer.parseInt(props.getProperty("totalCommits"));

                List<Long> res =
                        sSession
                                .query(
                                        "SELECT r.number FROM Revision as r, Person as p, CommitterRole as c WHERE c in elements(p.roles) and r in elements(c.artifacts) AND p.firstName='"
                                                + author.getFirstName() + "' GROUP BY r.number",
                                        Long.class);
                assertEquals(
                        "Total commits found for author " + author.getFirstName() + " is not what expected ",
                        totalCommits,
                        res.size());
            }
        }
    }
}
