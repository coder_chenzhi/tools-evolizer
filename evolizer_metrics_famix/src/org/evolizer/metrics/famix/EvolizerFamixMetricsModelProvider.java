/**
 * 
 */
package org.evolizer.metrics.famix;

import org.evolizer.core.hibernate.model.api.IEvolizerModelProvider;
import org.evolizer.metrics.famix.model.FamixMeasurement;


/**
 *
 * @author pinzger
 *
 */
public class EvolizerFamixMetricsModelProvider implements IEvolizerModelProvider {

    /** 
     * {@inheritDoc}
     */
    public Class<?>[] getAnnotatedClasses() {
        Class<?>[] annotatedClasses =
        {
                FamixMeasurement.class,
        };

        return annotatedClasses;
    }
}
