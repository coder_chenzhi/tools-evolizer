/**
 * 
 */
package org.evolizer.metrics.famix.strategies.noa;

import org.evolizer.metrics.famix.strategies.AbstractFamixMetricStrategy;


/**
 * Abstract class for the number of static attributes strategies.
 *
 * @author pinzger
 */
public abstract class AbstractFamixNumberOfStaticAttributesStrategy extends AbstractFamixMetricStrategy {
    private static final String identifier = "NOSA";
    private static final String description = "Calculates the number of static attributes";

    /** 
     * {@inheritDoc}
     */
    public String getDescription() {
        return description;
    }

    /** 
     * {@inheritDoc}
     */
    public String getIdentifier() {
        return identifier;
    }

}
