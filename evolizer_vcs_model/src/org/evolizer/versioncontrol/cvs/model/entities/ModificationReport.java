/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;
import org.evolizer.model.resources.entities.humans.Person;

/**
 * Modification Reports are taken from a CVS or equal and contain information about the reasons, why changes have been
 * made to the software.
 * 
 * @author wuersch, jetter
 */
@Entity
public class ModificationReport implements IEvolizerModelEntity {

    private Long id;

    /**
     * The creation-time of the current modification report.
     */
    private Date creationTime;

    /**
     * The Person who submitted the modification report.
     */
    private Person author;

    /**
     * The nickName that the author has used to commit the revision.
     */
    private String authorNickName;

    /**
     * Number of lines added during the modification.
     */
    private int linesAdd;

    /**
     * Number of lines removed during the modification.
     */
    private int linesDel;

    /**
     * The author's message submitted when he commited the modification.
     */
    private String commitMessage;

    /**
     * Instantiates a new modification report.
     */
    public ModificationReport() {
        super();
    }

    /**
     * Instantiates a new modification report.
     * 
     * @param creationTime
     *            the creation time
     */
    public ModificationReport(Date creationTime) {
        this();
        this.creationTime = creationTime;
    }

    /**
     * The Constructor.
     * 
     * @param creationTime
     *            A String with the format "yyyy/MM/dd HH:mm:ss".
     * @throws ParseException
     *             if creationTimes's not in the format "yyyy/MM/dd HH:mm:ss".
     */
    public ModificationReport(String creationTime) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = df.parse(creationTime);

        this.creationTime = date;
    }

    /**
     * Returns the id.
     * 
     * @return the id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @SuppressWarnings(value = {"unused"})
    private void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the author.
     * 
     * @return the author.
     */
    @ManyToOne(cascade = {CascadeType.ALL})
    public Person getAuthor() {
        return author;
    }

    /**
     * Sets the author.
     * 
     * @param author
     *            The author to set.
     */
    public void setAuthor(Person author) {
        this.author = author;
    }

    /**
     * Returns the author nick name.
     * 
     * @return the author nick name
     */
    public String getAuthorNickName() {
        return authorNickName;
    }

    /**
     * Sets the author nick name.
     * 
     * @param authorNickName
     *            the new author nick name
     */
    public void setAuthorNickName(String authorNickName) {
        this.authorNickName = authorNickName;
    }

    /**
     * Returns the commit message.
     * 
     * @return the commitMessage.
     */
    @Lob
    public String getCommitMessage() {
        return commitMessage;
    }

    /**
     * Sets the commit message.
     * 
     * @param commitMessage
     *            The commitMessage to set.
     */
    public void setCommitMessage(String commitMessage) {
        this.commitMessage = commitMessage;
    }

    /**
     * Append to commit message.
     * 
     * @param messageLine
     *            the message line
     */
    public void appendToCommitMessage(String messageLine) {
        if (commitMessage == null) {
            commitMessage = messageLine;
        } else {
            commitMessage.concat(messageLine);
        }
    }

    /**
     * Returns the creation time.
     * 
     * @return the creationTime.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * Sets the creation time.
     * 
     * @param creationTime
     *            The creationTime to set.
     */
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * Sets the creation time.
     * 
     * @param creationTime
     *            A String with the format "yyyy/MM/dd HH:mm:ss"
     * @throws ParseException
     *             the parse exception
     */
    public void setCreationTime(String creationTime) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = df.parse(creationTime);

        this.creationTime = date;
    }

    /**
     * Returns the lines add.
     * 
     * @return the linesAdd.
     */
    public int getLinesAdd() {
        return linesAdd;
    }

    /**
     * Sets the lines add.
     * 
     * @param linesAdd
     *            The linesAdd to set.
     */
    public void setLinesAdd(int linesAdd) {
        this.linesAdd = linesAdd;
    }

    /**
     * Returns the lines del.
     * 
     * @return the linesDel.
     */
    public int getLinesDel() {
        return linesDel;
    }

    /**
     * Sets the lines del.
     * 
     * @param linesDel
     *            The linesDel to set.
     */
    public void setLinesDel(int linesDel) {
        this.linesDel = linesDel;
    }

    /**
     * Sets the authors email address.
     * 
     * @param email
     *            the new authors email address
     */
    public void setAuthorsEmailAddress(String email) {
        if (author == null) {
            author = new Person();
        }
        author.setEmail(email);
    }

    /**
     * Returns the authors email address.
     * 
     * @return the authors email address
     */
    public String getAuthorsEmailAddress() {
        return author.getEmail();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof ModificationReport) {
            ModificationReport other = (ModificationReport) object;
            return ((author == null ? other.getAuthor() == null : author.equals(other.getAuthor()))
                    && (commitMessage == null ? other.getCommitMessage() == null : commitMessage.equals(other
                            .getCommitMessage()))
                    && (creationTime == null ? other.getCreationTime() == null : creationTime.equals(other
                            .getCreationTime())) && (linesAdd == other.getLinesAdd()) && (linesDel == other
                    .getLinesDel()));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        final int PRIME = 31;

        int result = 1;
        result = PRIME * result + ((author == null) ? 0 : author.hashCode());
        result = PRIME * result + ((commitMessage == null) ? 0 : commitMessage.hashCode());
        result = PRIME * result + ((creationTime == null) ? 0 : creationTime.hashCode());
        result = PRIME * result + (new Integer(linesAdd)).hashCode();
        result = PRIME * result + (new Integer(linesDel)).hashCode();
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getLabel() {
        // TODO Need a more meaningful label
        return "Modification Report " + id;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getURI() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Adds the nick name to author.
     * 
     * @param name
     *            the name
     */
    public void addNickNameToAuthor(String name) {
        if (author == null) {
            author = new Person();
            author.addNickName(name);
        } else {
            author.addNickName(name);
        }
    }
}
