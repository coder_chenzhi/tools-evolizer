/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model.entities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.fs.File;

/**
 * {@link File} under version control that knows its {@link Revision}s (versions).
 * 
 * @author wuersch
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class VersionedFile extends File {

    private List<Revision> revisions;

    /**
     * Instantiates a new versioned file.
     * 
     * @param path
     *            the path
     * @param parent
     *            the parent
     */
    public VersionedFile(String path, Directory parent) {
        super(path, parent);
        revisions = new ArrayList<Revision>();
    }

    /**
     * A default constructor is needed for Hibernate to load instances; otherwise an exception is thrown
     * Shouldn't be used to create new instances of VersionedFile, use VersionedFile(String path, Directory parent) instead.
     */
    public VersionedFile() {
        super();
    }

    /**
     * Adds the revision.
     * 
     * @param revision
     *            the revision
     */
    public void addRevision(Revision revision) {
        revisions.add(revision);
    }

    /**
     * Returns the revisions.
     * 
     * @return the revisions
     */
    @OneToMany(cascade = CascadeType.ALL)
    public List<Revision> getRevisions() {
        return revisions;
    }

    /**
     * Sets the revisions.
     * 
     * @param revisions
     *            the new revisions
     */
    public void setRevisions(List<Revision> revisions) {
        this.revisions = revisions;
    }

    /**
     * Returns the latest revision.
     * 
     * @return the latest revision
     */
    @Transient
    public Revision getLatestRevision() {
        if (revisions.size() > 0) {
            Iterator<Revision> revs = revisions.iterator();
            Revision max = revs.next();
            while (revs.hasNext()) {
                Revision rev = revs.next();
                if (max.getCreationTime().before(rev.getCreationTime())) {
                    max = rev;
                }
            }
            return max;
        } else {
            return null;
        }
    }
}
