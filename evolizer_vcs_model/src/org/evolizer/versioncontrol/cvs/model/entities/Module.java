/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;
import org.evolizer.model.resources.entities.fs.File;

/**
 * This class represents the concept of a Module, e.g. a plug-in of Eclipse,
 * the client-part of a client-server application etc. Introducing this model entity
 * allows us to distinguish files that occur twice, e.g., once in plug-in A and once in plug-in B.
 * 
 * @author wuersch
 */
@Entity
public class Module implements IEvolizerModelEntity {

    private Long id;
    private String name;
    private Set<File> content = new HashSet<File>();

    /**
     * Creates a new module.
     */
    Module() {
        super();
    }

    /**
     * Instantiates a new module.
     * 
     * @param name
     *            the name
     */
    public Module(String name) {
        this();
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @SuppressWarnings("unused")
    private void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the content.
     * 
     * @return the content
     */
    @OneToMany(cascade = CascadeType.ALL)
    public Set<File> getContent() {
        return content;
    }

    /**
     * Sets the content.
     * 
     * @param content
     *            the new content
     */
    public void setContent(Set<File> content) {
        this.content = content;
    }

    /**
     * Adds the.
     * 
     * @param unit
     *            the unit
     */
    public void add(File unit) {
        content.add(unit);
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getLabel() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getURI() {
        // TODO Auto-generated method stub
        return null;
    }
}
