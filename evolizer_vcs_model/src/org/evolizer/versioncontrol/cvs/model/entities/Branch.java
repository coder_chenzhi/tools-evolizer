/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;

/**
 * Branches allow different development paths to be tried. They can be created from the main trunk, or from other
 * branches.
 * 
 * @author wuersch, jetter, giger
 */
@Entity
public class Branch implements IEvolizerModelEntity {

    private Date creationDate;
    /**
     * Unique ID, used by Hibernate.
     */
    private Long id;

    /**
     * A branch's descriptor. E.g. 1.2.1.1.
     */
    private String name;

    /**
     * Not implemented yet!!
     * 
     * The parent-branch from which the current one was derived. null if main trunk.
     */
    private Branch parent;

    /**
     * A <code>Set</code> of all child-branches derived from the current one.
     */
    private Set<Branch> children = new HashSet<Branch>();

    /**
     * A <code>Set</code> of all revisions belonging to the current branch.
     */
    private Set<Revision> revisions = new HashSet<Revision>();

    /**
     * A default constructor is needed for Hibernate to load instances; otherwise an exception is thrown. Since normally
     * no default constructor is needed its modifier is set private.
     */
    private Branch() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param name
     *            the name
     */
    public Branch(String name) {
        this();
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    
    /**
     * Returns the children.
     * 
     * @return the children.
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_branch_fk")
    public Set<Branch> getChildren() {
        return children;
    }

    /**
     * Sets the children.
     * 
     * @param children
     *            the children to set.
     */
    public void setChildren(Set<Branch> children) {
        this.children = children;
    }

    /**
     * Returns the id.
     * 
     * @return the id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @SuppressWarnings(value = {"unused"})
    private void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the name.
     * 
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the parent.
     * 
     * @return the parent.
     */
    @ManyToOne
    @JoinColumn(name = "parent_branch_fk")
    public Branch getParent() {
        return parent;
    }

    /**
     * Sets the parent.
     * 
     * @param parent
     *            the parent to set
     */
    public void setParent(Branch parent) {
        this.parent = parent;
    }

    /**
     * Adds the child.
     * 
     * @param branch
     *            the branch
     */
    public void addChild(Branch branch) {
        children.add(branch);
    }

    /**
     * Returns the revisions.
     * 
     * @return the revisions
     */
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "branch_fk")
    public Set<Revision> getRevisions() {
        return revisions;
    }

    /**
     * Sets the revisions.
     * 
     * @param revisions
     *            The revisions to set.
     */
    public void setRevisions(Set<Revision> revisions) {
        this.revisions = revisions;
    }

    /**
     * Adds the revision.
     * 
     * @param revision
     *            the revision
     */
    public void addRevision(Revision revision) {
        revisions.add(revision);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof Branch) {
            Branch other = (Branch) object;
            return ((children == null ? other.getChildren() == null : children.equals(other.getChildren()))
                    && (name == null ? other.getName() == null : name.equals(other.getName()))
                    && (parent == null ? other.getParent() == null : parent.equals(other.getParent())) && (revisions == null
                    ? other.getRevisions() == null
                    : revisions.equals(other.getRevisions())));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getLabel() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getURI() {
        // TODO Auto-generated method stub
        return null;
    }
}
