/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.humans.Role;

/**
 * Role that identifies a {@link Person} as a Committer.
 * 
 * @author wuersch
 */
@Entity
public class CommitterRole extends Role {

    private Set<Revision> artifacts;

    /**
     * Instantiates a new committer role.
     */
    public CommitterRole() {
        super("Commiter");
        artifacts = new HashSet<Revision>();
    }

    /**
     * Returns the artifacts.
     * 
     * @return the artifacts
     */
    @OneToMany
    public Set<Revision> getArtifacts() {
        return artifacts;
    }

    /**
     * Sets the artifacts.
     * 
     * @param artifacts
     *            the new artifacts
     */
    public void setArtifacts(Set<Revision> artifacts) {
        this.artifacts = artifacts;
    }

    /**
     * Adds the revision.
     * 
     * @param revision
     *            the revision
     */
    public void addRevision(Revision revision) {
        artifacts.add(revision);
    }
}
