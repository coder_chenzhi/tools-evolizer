/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;

/**
 * A software product can have several versions, called Releases. Releases are built out of one or many files having
 * also specific versions called Revisions.
 * 
 * @author wuersch, jetter
 */
@Entity
@Table(name = "release_entity")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Release implements IEvolizerModelEntity {

    /**
     * Unique ID, used by Hibernate.
     */
    private Long id;

    /**
     * A release's descriptor. E.g. 3.0.
     */
    private String name;

    /**
     * Date of the release.
     */
    private Date timeStamp;

    /**
     * A <code>Set</code> of all revisions contributing to the release.
     */
    private Set<Revision> revisions = new HashSet<Revision>();

    /**
     * A default constructor is needed for Hibernate to load instances; otherwise an exception is thrown
     * Shouldn't be used to create new instances of Release, use Release(String name) instead.
     */
    public Release() {
        super();
    }

    /**
     * Instantiates a new release.
     * 
     * @param name
     *            the name
     */
    public Release(String name) {
        this();
        this.name = name;
    }

    /**
     * Instantiates a new release.
     * 
     * @param name
     *            the name
     * @param timeStamp
     *            the time stamp
     */
    public Release(String name, Date timeStamp) {
        this(name);
        this.timeStamp = timeStamp;
    }

    /**
     * Returns the id.
     * 
     * @return the id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @SuppressWarnings(value = {"unused"})
    private void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the name.
     * 
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the revisions.
     * 
     * @return the revisions.
     */
    @ManyToMany(mappedBy = "releases", targetEntity = Revision.class, cascade = {CascadeType.ALL})
    public Set<Revision> getRevisions() {
        return revisions;
    }

    /**
     * Sets the revisions.
     * 
     * @param revisions
     *            The revisions to set.
     */
    public void setRevisions(Set<Revision> revisions) {
        this.revisions = revisions;
    }

    /**
     * Adds the revision.
     * 
     * @param revision
     *            the revision
     */
    public void addRevision(Revision revision) {
        revisions.add(revision);
    }

    /**
     * Returns the time stamp.
     * 
     * @return the timeStamp.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the time stamp.
     * 
     * @param timeStamp
     *            The timeStamp to set.
     */
    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof Release) {
            Release other = (Release) object;
            return ((name == null ? other.getName() == null : name.equals(other.getName()))
                    && (revisions == null ? other.getRevisions() == null : revisions.equals(other.getRevisions())) && (timeStamp == null
                    ? other.getTimeStamp() == null
                    : timeStamp.equals(other.getTimeStamp())));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        final int PRIME = 31;
        int result = 1;

        result = PRIME * result + ((name == null) ? 0 : name.hashCode());
        result = PRIME * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getLabel() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getURI() {
        // TODO Auto-generated method stub
        return null;
    }

}