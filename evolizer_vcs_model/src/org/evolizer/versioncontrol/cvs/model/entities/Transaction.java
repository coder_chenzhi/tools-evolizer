/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;

/**
 * A transaction is a set of files that were committed together to a CVS server (aka. Change Set).
 * CVS does not store transactions, but they can be more or less reconstructed using some heuristics.
 * 
 * @author wuersch, jetter
 */
@Entity
public class Transaction implements IEvolizerModelEntity {

    private Long id;
    private Date started;
    private Date finished;

    private Set<Revision> involvedRevisions = new HashSet<Revision>();

    /**
     * Returns the finished.
     * 
     * @return the finished
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getFinished() {
        return finished;
    }

    /**
     * Sets the finished.
     * 
     * @param finished
     *            the new finished
     */
    public void setFinished(Date finished) {
        this.finished = finished;
    }

    /**
     * {@inheritDoc}
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @SuppressWarnings(value = {"unused"})
    private void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the involved revisions.
     * 
     * @return the involved revisions
     */
    @OneToMany
    public Set<Revision> getInvolvedRevisions() {
        return involvedRevisions;
    }

    /**
     * Sets the involved revisions.
     * 
     * @param involvedRevisions
     *            the new involved revisions
     */
    public void setInvolvedRevisions(Set<Revision> involvedRevisions) {
        this.involvedRevisions = involvedRevisions;
    }

    /**
     * Returns the started.
     * 
     * @return the started
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getStarted() {
        return started;
    }

    /**
     * Sets the started.
     * 
     * @param started
     *            the new started
     */
    public void setStarted(Date started) {
        this.started = started;
    }

    /**
     * Adds the revision.
     * 
     * @param revision
     *            the revision
     */
    public void addRevision(Revision revision) {
        involvedRevisions.add(revision);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof Transaction) {
            Transaction other = (Transaction) object;
            return ((finished == null ? other.getFinished() == null : finished.equals(other.getFinished()))
                    && (involvedRevisions == null ? other.getInvolvedRevisions() == null : involvedRevisions
                            .equals(other.getInvolvedRevisions())) && (started == null
                    ? other.getStarted() == null
                    : started.equals(other.getStarted())));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {

        final int PRIME = 31;
        int result = 1;

        result = PRIME * result + ((finished == null) ? 0 : finished.hashCode());
        result = PRIME * result + ((started == null) ? 0 : started.hashCode());

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getLabel() {
        return "Transaction " + started + " until " + finished;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getURI() {
        // TODO Auto-generated method stub
        return null;
    }

}
