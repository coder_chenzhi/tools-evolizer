/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.exceptions;

/**
 * This Exception is used to report any error arising during the import of the versioning history of a SVN repository
 * 
 * @author ghezzi
 * 
 */
public class SVNImporterException extends Exception {

    /**
     * auto generated serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * 
     * @param message
     *            The message of the newly created exception.
     */
    public SVNImporterException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param cause
     *            The cause of the newly created exception.
     */
    public SVNImporterException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     * 
     * @param message
     *            The message of the newly created exception.
     * @param cause
     *            The cause of the newly created exception.
     */
    public SVNImporterException(String message, Throwable cause) {
        super(message, cause);
    }
}
