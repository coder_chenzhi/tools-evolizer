/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer;

import org.eclipse.osgi.util.NLS;

/**
 * Standard Eclipse String externalization class. So that the EclipseSVNImporter does not have hardcoded strings.
 * 
 * @author ghezzi
 * 
 */
public final class ImporterMessages extends NLS {

    private static final String BUNDLE_NAME = "org.evolizer.versioncontrol.svn.importer.messages";

    /*
     * SVN misc
     */
    public static String EvolizerSVNImporter_directoriesRegEx;
    public static String EvolizerSVNImporter_exportedBycvs2svn;
    /*
     * SVN exceptions
     */
    public static String EvolizerSVNImporter_svnReset;
    public static String EvolizerSVNImporter_svnTimeOut;
    public static String EvolizerSVNImporter_svnWaiting;
    /*
     * Error messages
     */
    public static String EvolizerSVNImporter_connectionProbs;
    public static String EvolizerSVNImporter_networkProbs;
    public static String EvolizerSVNImporter_importError;
    public static String EvolizerSVNImporter_problemResumingFromRevision;
    public static String EvolizerSVNImporter_branchDoesnotExist;
    public static String EvolizerSVNImporter_stopImport;
    public static String EvolizerSVNImporter_noRepresentationRetrieved;
    public static String EvolizerSVNImporter_noHistoryYet;
    public static String EvolizerSVNImporter_historyUpToDate;
    public static String EvolizerSVNImporter_invalidRevisionNumber;
    /*
     * Importer progress messages
     */
    public static String EvolizerSVNImporter_fetchingAllReleasesSource;
    public static String EvolizerSVNImporter_fetchingReleasesSource;
    public static String EvolizerSVNImporter_fetchingReleaseSource;
    public static String EvolizerSVNImporter_fetchingRevisionsSource;
    public static String EvolizerSVNImporter_fetchingRevisionSource;
    public static String EvolizerSVNImporter_possReleaseFound;
    public static String EvolizerSVNImporter_extractingFromRevision;
    public static String EvolizerSVNImporter_extractionTime;
    public static String EvolizerSVNImporter_performingRollback;
    public static String EvolizerSVNImporter_resumingImport;
    public static String EvolizerSVNImporter_sourceUnitSkipWarning;
    public static String EvolizerSVNImporter_dataExtracted;
    public static String EvolizerSVNImporter_startingImport;
    public static String EvolizerSVNImporter_startingUpdate;
    public static String EvolizerSVNImporter_finishedImport;
    public static String EvolizerSVNImporter_restartingImport;
    public static String EvolizerSVNImporter_resuming;
    public static String EvolizerSVNImporter_foundcvs2svnBranch;
    public static String EvolizerSVNImporter_foundBranch;
    public static String EvolizerSVNImporter_foundRelease;
    public static String EvolizerSVNImporter_foundRevision;
    public static String EvolizerSVNImporter_importing;
    public static String EvolizerSVNImporter_connected;
    public static String EvolizerSVNImporter_disconnected;
    public static String EvolizerSVNImporter_reconnected;
    public static String EvolizerSVNImporter_foundSubBranch;
    /*
     * Entity manipulation messages
     */
    public static String EvolizerSVNImporter_deleteFromBranch;
    public static String EvolizerSVNImporter_createdBranch;
    public static String EvolizerSVNImporter_deleteFromRelease;
    public static String EvolizerSVNImporter_createdRelease;
    public static String EvolizerSVNImporter_createdRevision;
    /*
     * Generic info messages
     */
    
    public static String EvolizerSVNImporter_cvs2svnBranch;
    public static String EvolizerSVNImporter_cvs2svnTag;
    
    
    public static String EvolizerSVNImporter_customDirSet;
    public static String EvolizerSVNImporter_revisionBranchSimilarity;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, ImporterMessages.class);
    }

    private ImporterMessages() {}
}
