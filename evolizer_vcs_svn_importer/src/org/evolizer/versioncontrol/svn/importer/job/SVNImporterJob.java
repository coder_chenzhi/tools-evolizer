/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.job;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.team.svn.core.SVNTeamProvider;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNImporter;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNPlugin;
import org.evolizer.versioncontrol.svn.importer.exceptions.SVNImporterException;
import org.tmatesoft.svn.core.SVNException;

/**
 * Job that wraps {@link EvolizerSVNImporter} to import the SVN information of a given {@link IProject} into the RHDB.
 * The import steps are: - getting the SVN log (mandatory). - parsing the log and extracting the history incrementally -
 * downloading the content of all files that match a given regular expression (optional).
 * 
 * @see EvolizerSVNImporter
 * @author ghezzi
 */
public class SVNImporterJob extends Job {

    /*
     * For logging purpose.
     */
    private static final Logger LOGGER =
            EvolizerSVNPlugin.getLogManager().getLogger(SVNImporterJob.class.getCanonicalName());
    /*
     * The project to import the history from.
     */
    private IProject project;
    /*
     * The file types to import (the extensions in a semi-colon separated list regular expression).
     */
    private String fileExtensionRegEx;
    /*
     * The trunk/tags/branches directories in the repository, in case the repository does not use the custom layout.
     */
    private String[] directories;
    /*
     * The revision to start the import from.
     */
    private long start;
    /*
     * The revision to stop the import at.
     */
    private long end;
    /*
     * The amount of SVN failures (usually timeouts) the import can endure before failing.
     */
    private int tries;
    /*
     * A valid user name to connect to the SVN repository.
     */
    private String user;
    /*
     * A valid password to connect to the SVN repository.
     */
    private String pwd;
    /*
     * Whether or not the import is an update of a pre-existing imported history.
     */
    private boolean update;
    /*
     * Whether or not to calculate the diff between successive revisions.
     */
    private boolean diff;

    /**
     * Constructor.
     * 
     * @param project
     *            The project to import the history from.
     * @param userInfo
     *            A two element array containing a valid SVN user name and its password.
     * @param fileExtensionRegEx
     *            The file types to import (the extensions in a semi-colon separated list regular expression).
     * @param dirs
     *            The trunk/tags/branches directories (if not using the standard structure) in a three elements array.
     * @param startRev
     *            The revision from which the importer will start. Can be an empty string. In that case, the importer
     *            will start from the BASE revision.
     * @param endRev
     *            The revision at which the importer will stop. Can be an empty string. In that case, the importer will
     *            stop at the HEAD revision.
     * @param tries
     *            the number of SVN/network failures that the importer can tolerate before failing.
     * @param diff
     *            If the importer needs to calculate the difference between each subsequent version of the files.
     * @param update
     *            If the import will actually just update the history of a project that was already imported.
     */
    public SVNImporterJob(
            IProject project,
            String[] userInfo,
            String fileExtensionRegEx,
            String[] dirs,
            String startRev,
            String endRev,
            int tries,
            boolean diff,
            boolean update) {
        super("Import SVN Version Control Information");
        if (update) {
            super.setName("Update SVN Version Control Information");
        }
        this.project = project;
        this.fileExtensionRegEx = fileExtensionRegEx;
        this.directories = dirs;
        this.tries = tries;
        this.user = userInfo[0];
        this.pwd = userInfo[1];
        this.diff = diff;

        if (startRev.compareTo("") != 0) {
            this.start = Long.parseLong(startRev);
        } else {
            this.start = 0;
        }
        if (endRev.compareTo("") != 0) {
            this.end = Long.parseLong(endRev);
        } else {
            this.end = -1;
        }
        this.update = update;
    }

    /**
     * Constructor.
     * 
     * @param project
     *            The project to import the history from.
     */
    public SVNImporterJob(IProject project) {
        super("Import SVN Version Control Information");
        this.project = project;
        this.fileExtensionRegEx = "*";
    }

    /**
     * Getter for the related project.
     * 
     * @return The project related to this import job.
     */
    public IProject getProject() {
        return this.project;
    }

    /**
     * Calculates the longest common prefix of the two given strings.
     * 
     * @param s
     *            The first string to be analyzed.
     * @param t
     *            The second string to be analyzed.
     * @return the longest common prefix found.
     */
    private String lcp(String s, String t) {
        int n = Math.min(s.length(), t.length());
        for (int i = 0; i < n; i++) {
            if (s.charAt(i) != t.charAt(i)) {
                return s.substring(0, i);
            }
        }
        return s.substring(0, n);
    }

    /**
     * Runs the SVN importer. {@inheritDoc}
     */
    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            if (this.update) {
                LOGGER.info("Starting update of project " + project.getName() + " history job");
            } else {
                LOGGER.info("Starting import of project " + project.getName() + " history job");
            }
            EvolizerSessionHandler handler = EvolizerSessionHandler.getHandler();
            handler.updateSchema(this.project);
            IEvolizerSession persistenceProvider = handler.getCurrentSession(this.project);

            String[] repositoryData = new String[3];
            repositoryData[0] = this.user;
            repositoryData[1] = this.pwd;

            if (this.directories != null) {
                repositoryData[2] = this.lcp(this.directories[0], this.lcp(this.directories[1], this.directories[2]));
                this.directories[0] = this.directories[0].replace(repositoryData[2], "");
                this.directories[1] = this.directories[1].replace(repositoryData[2], "");
                this.directories[2] = this.directories[2].replace(repositoryData[2], "");
            } else {
                String url =
                        ((SVNTeamProvider) RepositoryProvider.getProvider(project)).getRepositoryResource().getUrl();
                if (url.contains("/trunk")) {
                    repositoryData[2] = url.substring(0, url.indexOf("/trunk"));
                } else if (url.contains("/tags")) {
                    repositoryData[2] = url.substring(0, url.indexOf("/tags"));
                } else if (url.contains("/branches")) {
                    repositoryData[2] = url.substring(0, url.indexOf("/branches"));
                } else {
                    repositoryData[2] = url;
                }
            }
            EvolizerSVNImporter importer =
                    new EvolizerSVNImporter(
                            repositoryData,
                            this.tries,
                            this.diff,
                            this.fileExtensionRegEx,
                            this.directories);

            try {
                importer.importProject(persistenceProvider, monitor, this.start, this.end, this.update);
                LOGGER.info("Import Job of project " + project.getName() + " version history done");
            } catch (SVNException e) {
                LOGGER.error("Error while importing the version history for project " + project.getName(), e);
                System.err.println(e);
                return Status.CANCEL_STATUS;
            } finally {
                persistenceProvider.close();
            }

        } catch (SVNImporterException e) {
            if (e.getMessage().compareTo("The history is up to date") == 0) {
                monitor.subTask("The history is up to date");
                return Status.OK_STATUS;
            }
            LOGGER.error("Error while importing the version history for project " + project.getName(), e);
            System.err.println(e);
            return Status.CANCEL_STATUS;
        } catch (EvolizerException e) {
            LOGGER.error("Error while importing the version history for project " + project.getName(), e);
            System.err.println(e);
            return Status.CANCEL_STATUS;
        }
        return Status.OK_STATUS;
    }

    /**
     * Cleans up the job "workspace".
     */
    public void cleanUpJob() {
        try {
            IEvolizerSession session = EvolizerSessionHandler.getHandler().getCurrentSession(this.project);
            if ((session != null) && session.isOpen()) {
                session.clear();
                session.close();
            }
        } catch (EvolizerException e) {
            e.printStackTrace();
        }
    }

}
