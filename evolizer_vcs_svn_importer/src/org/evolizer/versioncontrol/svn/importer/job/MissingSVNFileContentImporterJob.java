/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.job;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.team.svn.core.SVNTeamProvider;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNImporter;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNPlugin;
import org.evolizer.versioncontrol.svn.importer.exceptions.SVNImporterException;

/**
 * The Class MissingFileContentImporterJob.
 * 
 * @author ghezzi
 */
public class MissingSVNFileContentImporterJob extends Job {

    /*
     * For logging purpose.
     */
    private static Logger sLogger =
            EvolizerSVNPlugin.getLogManager().getLogger(MissingSVNFileContentImporterJob.class.getName());
    /*
     * The project to import the historical file content from.
     */
    private IProject project;
    /*
     * The filetypes to import (their extensions in a semi-colon separated list regular expression).
     */
    private String fileExtensionRegEx;
    /*
     * The import type.
     */
    private int importType;
    /*
     * The start revision to start the fetching from.
     */
    private long start;
    /*
     * The final revision to end the fetching at.
     */
     private long end;
     /*
      * The names of the releases to fetch the content of. 
      */
     private List<String> releases;

    /**
     * Instantiates a new missing file content importer job.
     * 
     * @param name
     *            The name of the import job.
     * @param project
     *            The project to import the historical file content from.
     * @param fileExtensionRegEx
     *            The file types to import (use * to import all the files).
     * @param importType
     *            The import type: 0 for fetching the whole history file content, 1 for fetching the file content for
     *            all the releases, 2 for fetching the file content for a given revision range, 3 for fetching the file
     *            content for a given set of releases.
     * @param start
     *            The start revision, if fetching the file content for a given revision range. This parameter will be
     *            ignored for all the other cases.
     * @param end
     *            The end revision, if fetching the file content for a given revision range. This parameter will be
     *            ignored for all the other cases.
     * @param releases
     *            A {@link List} of the names of releases, if fetching the file content for a given set of releases.
     *            Leave null for all the other types of import.
     */
    public MissingSVNFileContentImporterJob(
            String name,
            IProject project,
            String fileExtensionRegEx,
            int importType,
            long start,
            long end,
            List<String> releases) {
        super(name);
        this.project = project;
        this.fileExtensionRegEx = fileExtensionRegEx;
        this.importType = importType;
        this.start = start;
        this.end = end;
        this.releases = releases;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected IStatus run(IProgressMonitor monitor) {
        try {
            EvolizerSessionHandler handler = EvolizerSessionHandler.getHandler();
            handler.updateSchema(this.project);
            IEvolizerSession persistenceProvider = handler.getCurrentSession(this.project);

            String[] repositoryData = new String[3];
            repositoryData[0] = "";
            repositoryData[1] = "";

            String url =
                    ((SVNTeamProvider) RepositoryProvider.getProvider(this.project)).getRepositoryResource().getUrl();
            if (url.contains("/trunk")) {
                repositoryData[2] = url.substring(0, url.indexOf("/trunk"));
            } else if (url.contains("/tags")) {
                repositoryData[2] = url.substring(0, url.indexOf("/tags"));
            } else if (url.contains("/branches")) {
                repositoryData[2] = url.substring(0, url.indexOf("/branches"));
            } else {
                repositoryData[2] = url;
            }

            EvolizerSVNImporter importer = new EvolizerSVNImporter(repositoryData);

            // Checking what type of file content import to run (there are 4 types of file content import)
            switch (this.importType) {
                // fetch file content for the whole history (all the existing revisions)
                case 0:
                    try {
                        importer.fetchRevisionRangeSource(persistenceProvider, this.fileExtensionRegEx, 0, -1, monitor);
                    } catch (SVNImporterException e) {
                        sLogger.error("Import of missing file content for the whole SVN history failed:", e);
                        return Status.CANCEL_STATUS;
                    } finally {
                        persistenceProvider.close();
                    }
                    break;
                // Fetch file content for all existing releases
                case 1:
                    try {
                        importer.fetchReleasesSource(persistenceProvider, this.fileExtensionRegEx, null, monitor);
                    } catch (SVNImporterException e) {
                        sLogger.error("Import of missing file content for all the existing releases failed:", e);
                        return Status.CANCEL_STATUS;
                    } finally {
                        persistenceProvider.close();
                    }
                    break;
                // Fetch file content for a revision range
                case 2:
                    try {
                        importer.fetchRevisionRangeSource(
                                persistenceProvider,
                                this.fileExtensionRegEx,
                                this.start,
                                this.end,
                                monitor);
                    } catch (SVNImporterException e) {
                        sLogger.error("Import of missing file content for the " + this.start + ":" + this.end + " revision range failed:", e);
                        return Status.CANCEL_STATUS;
                    } finally {
                        persistenceProvider.close();
                    }
                    break;
                // Fetch file content for selected releases
                case 3:
                    try {
                        importer.fetchReleasesSource(
                                persistenceProvider,
                                this.fileExtensionRegEx,
                                this.releases,
                                monitor);
                    } catch (SVNImporterException e) {
                        sLogger.error("Import of missing file content for a given set of releases failed:", e);
                        return Status.CANCEL_STATUS;
                    } finally {
                        persistenceProvider.close();
                    }
                    break;
                default:
                    break;
            }
            sLogger.debug("Import of missing file content completed");
        } catch (EvolizerException e) {
            sLogger.error("Import of missing file content failed:", e);
            return Status.CANCEL_STATUS;
        }
        return Status.OK_STATUS;
    }
}
