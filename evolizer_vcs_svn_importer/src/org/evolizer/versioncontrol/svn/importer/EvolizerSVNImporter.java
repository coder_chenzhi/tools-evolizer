/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.osgi.util.NLS;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.ModificationReport;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.svn.importer.exceptions.SVNImporterException;
import org.evolizer.versioncontrol.svn.importer.job.SVNImporterJob;
import org.evolizer.versioncontrol.svn.importer.mapper.SVNModelMapper;
import org.evolizer.versioncontrol.svn.importer.util.SVNConnector;
import org.evolizer.versioncontrol.svn.importer.util.SVNConnectorMessages;
import org.evolizer.versioncontrol.svn.model.entities.SVNRelease;
import org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;

/**
 * 
 * This class imports the SVN release history of a given {@link IProject}. It extracts and parses the desired
 * information from the log entries and passes it to the {@link SVNModelMapper} which creates a custom model
 * accordingly.
 * 
 * @see SVNModelMapper
 * @see SVNImporterJob
 * @author ghezzi
 * 
 */
public class EvolizerSVNImporter {

    /**
     * For logging purposes.
     */
    private static final Logger LOGGER =
            EvolizerSVNPlugin.getLogManager().getLogger(EvolizerSVNImporter.class.getCanonicalName());
    /**
     * Counting how many times the importer failed.
     */
    private int fFailureCount;
    /**
     * The maximum number of failures the importer can deal with, before just failing.
     */
    private int fMaxFailures;
    /**
     * SVN URL.
     */
    private String fSVNBaseURL;
    /**
     * SVN user name.
     */
    private String fSVNUserName;
    /**
     * SVN user password.
     */
    private String fSVNUserPassword;
    /**
     * Determines whether or not to calculate the line changes between file versions.
     */
    private boolean fCalculateLineChanges;
    /**
     * The SVNModelMapper instance.
     */
    private SVNModelMapper fModelMapper;
    /**
     * Standard SVN folders name. They are not hardcoded so that a user could actually set up the the importer to work
     * with custom folders, instead of the 3 standard ones, but behaving as it is importing a standard repository.
     */
    private String fTags = SVNConnectorMessages.SVNConnector_tags;
    private String fBranches = SVNConnectorMessages.SVNConnector_branches;
    private String fTrunk = SVNConnectorMessages.SVNConnector_trunk;
    /**
     * The last revision analyzed, used when the importer needs to be restarted.
     */
    private long fCurrRev;
    /**
     * The monitor to track and report the import progress.
     */
    private IProgressMonitor fMonitor;
    /**
     * The revision number from which to start the import.
     */
    private long fBeginImport;
    /**
     * The revision number of the last revision that should be imported (the default, -1 makes the importer stops when
     * the current revision is reached).
     */
    private long fEndImport = -1;
    /**
     * Instance of the class dealing with the connection with SVN.
     */
    private SVNConnector fConnector;
    /**
     * The wildcard representing any type of file extension. Used when the user wants to fetch the source for every type
     * of file.
     */
    private static final String FILE_EXTENSION_WILDCARD = "*";
    /**
     * Whether or not the importer will also fetch the actual contents of every file in the repository.
     */
    private boolean fFetchSrc;
    /**
     * The list of the file types for which the importer has to fetch the contents.
     */
    private ArrayList<String> fExtensions = new ArrayList<String>();
    private boolean usingCustomPaths = false;
    private Pattern fileRegExPattern = Pattern.compile("(\\w)+");

    /**
     * Constructor. It initializes several options of the importer.
     * 
     * @param repositoryData
     *            An array containing all the 3 pieces of information needed to connect to the SVN repository: (1) a
     *            valid SVN username for the repository, (2) a valid SVN password for the used username, (3) the URL of
     *            the repository to import. In this precise order.
     * @param maxFailures
     *            The maximum amount of failures (due to network/server related problems) the importer can tolerate.
     * @param diff
     *            Whether or the importer should calculate the number lines changed for each file committed.
     * @param fileExtensionRegex
     *            the file extension regex, telling the importer which files source to fetch. If null or empty, no file
     *            will be fetched.
     * @param customDirs
     *            The custom repositories directories that substitute trunk/tags/branches (in this specific order).
     */
    public EvolizerSVNImporter(
            String[] repositoryData,
            int maxFailures,
            boolean diff,
            String fileExtensionRegex,
            String[] customDirs) {
        // Set base URL without trailing '/'
        fSVNBaseURL =
                repositoryData[2].lastIndexOf("/") == repositoryData[2].length() - 1 ? repositoryData[2].substring(
                        0,
                        repositoryData[2].length()) : repositoryData[2];
        fSVNUserName = repositoryData[0];
        fSVNUserPassword = repositoryData[1];
        fCalculateLineChanges = diff;
        fMaxFailures = maxFailures;

        this.processFileExtensionRegEx(fileExtensionRegex);

        if (customDirs != null) {
            this.usingCustomPaths = true;
            fTrunk = customDirs[0];
            fTags = customDirs[1];
            fBranches = customDirs[2];
            LOGGER.info(NLS.bind(ImporterMessages.EvolizerSVNImporter_customDirSet, new String[]{
                    fTrunk,
                    fTags,
                    fBranches}));
            fConnector =
                    new SVNConnector(
                            fSVNBaseURL,
                            fSVNUserName,
                            fSVNUserPassword,
                            new String[]{fTrunk, fTags, fBranches});
        } else {
            fConnector = new SVNConnector(fSVNBaseURL, fSVNUserName, fSVNUserPassword, null);
        }
    }

    /**
     * Constructor. It initializes several options of the importer. This constructor should only be used when setting up
     * an SVNImporter to fetch the file contents of an already extracted SVN history.
     * 
     * @param repositoryData
     *            An array containing all the 3 pieces of information needed to connect to the SVN repository: (1) a
     *            valid SVN username for the repository, (2) a valid SVN password for the used username, (3) the URL of
     *            the repository to import. In this precise order.
     */
    public EvolizerSVNImporter(String[] repositoryData) {
        // Set base URL without trailing '/'
        fSVNBaseURL =
                repositoryData[2].lastIndexOf("/") == repositoryData[2].length() - 1 ? repositoryData[2].substring(
                        0,
                        repositoryData[2].length()) : repositoryData[2];
        fSVNUserName = repositoryData[0];
        fSVNUserPassword = repositoryData[1];
        fConnector = new SVNConnector(fSVNBaseURL, fSVNUserName, fSVNUserPassword, null);
    }

    /**
     * Does the actual importing.
     * 
     * @param persistenceProvider
     *            The IEvolizerSession to be used to save the project.
     * @param monitor
     *            The monitor to report the progress of the import.
     * @param beginImport
     *            The revision to start the import from (0 if the importer should start from the creation of the
     *            repository).
     * @param endImport
     *            The revision to end the import at (-1 if the importer should import up to the most recent revision).
     * @param isContinuation
     *            If the import is actually an update of one that was previously done.
     * @throws SVNImporterException
     *             if some problems extracting/recreating SVN historical data are encountered.
     * @throws SVNException
     *             If some problems with the SVN Repository are encountered.
     */
    public void importProject(
            IEvolizerSession persistenceProvider,
            IProgressMonitor monitor,
            long beginImport,
            long endImport,
            boolean isContinuation) throws SVNException, SVNImporterException {

        fBeginImport = beginImport;
        fEndImport = endImport;
        fMonitor = monitor;
        fModelMapper = new SVNModelMapper(persistenceProvider);

        // First I connect to SVN repository
        fConnector.connectSVN();
        LOGGER.info(ImporterMessages.EvolizerSVNImporter_connected);

        if (isContinuation) {
            LOGGER.info(ImporterMessages.EvolizerSVNImporter_startingUpdate);
            fMonitor.subTask(ImporterMessages.EvolizerSVNImporter_startingUpdate);
            fBeginImport = fModelMapper.getLastRevisionNum();
            if (fBeginImport == -1) {
                throw new SVNImporterException(ImporterMessages.EvolizerSVNImporter_noHistoryYet);
            }
            if (fConnector.getLatestRevision() == fBeginImport) {
                throw new SVNImporterException(ImporterMessages.EvolizerSVNImporter_historyUpToDate);
            }
            fModelMapper.restoreContents(fTags);
        } else {
            LOGGER.info(ImporterMessages.EvolizerSVNImporter_startingImport);
            fMonitor.subTask(ImporterMessages.EvolizerSVNImporter_startingImport);
        }
        // Now I create the representation
        // I try to import the history and save into an instance of a Version Control model
        try {
            createRepresentation(isContinuation);
        } catch (SVNException e) {
            // If it fails, if it was due to some common network problems, I try to resume the import for as many times
            // as the user requested
            if (e.getMessage().contains(ImporterMessages.EvolizerSVNImporter_svnReset)
                    || e.getMessage().contains(ImporterMessages.EvolizerSVNImporter_svnTimeOut)
                    || e.getMessage().contains(ImporterMessages.EvolizerSVNImporter_svnWaiting)
                    && fFailureCount <= fMaxFailures) {
                while (++fFailureCount <= fMaxFailures) {
                    LOGGER.error(NLS.bind(ImporterMessages.EvolizerSVNImporter_importError, fCurrRev, fSVNBaseURL)
                            + NLS.bind(ImporterMessages.EvolizerSVNImporter_resuming, fFailureCount, fMaxFailures) + e);
                    try {
                        this.resumeImport();
                        break;
                    } catch (SVNException e2) {
                        if (e2.getMessage().contains(ImporterMessages.EvolizerSVNImporter_svnReset)
                                || e2.getMessage().contains(ImporterMessages.EvolizerSVNImporter_svnTimeOut)
                                || e2.getMessage().contains(ImporterMessages.EvolizerSVNImporter_svnWaiting)) {
                            if (fFailureCount + 1 > fMaxFailures) {
                                LOGGER.error(NLS.bind(
                                        ImporterMessages.EvolizerSVNImporter_importError,
                                        fCurrRev,
                                        fSVNBaseURL)
                                        + ImporterMessages.EvolizerSVNImporter_stopImport, e2);
                                throw new SVNImporterException(NLS.bind(
                                        ImporterMessages.EvolizerSVNImporter_networkProbs,
                                        fSVNBaseURL,
                                        fCurrRev), e2);
                            }
                        } else {
                            throw e2;
                        }
                    }
                }
            } else {
                throw new SVNImporterException(NLS.bind(
                        ImporterMessages.EvolizerSVNImporter_networkProbs,
                        fSVNBaseURL,
                        fCurrRev), e);
            }

        }

        fMonitor.subTask(ImporterMessages.EvolizerSVNImporter_dataExtracted);
        LOGGER.info(ImporterMessages.EvolizerSVNImporter_dataExtracted);

        // Eventually I save the representation
        fModelMapper.saveRepresentation();
        fMonitor.subTask(ImporterMessages.EvolizerSVNImporter_finishedImport);

        LOGGER.info(ImporterMessages.EvolizerSVNImporter_finishedImport);
        fConnector.disconnectSVN();
    }

    /**
     * Resumes the import from the repository it was working on, starting from the latest revision encountered by the
     * importer. Usually resuming is necessary due to network failures or problems with the SVN server itself. The data
     * that was already collected from that revision is actually thrown away to avoid any inconsistencies and the whole
     * revision is reanalyzed.
     * 
     * @throws SVNImporterException
     *             if some problems extracting/recreating SVN historical data are encountered.
     * @throws SVNException
     *             If some problems with the SVN Repository are encountered.
     */
    @SuppressWarnings("unchecked")
    private void resumeImport() throws SVNImporterException, SVNException {
        // First I reconnect to SVN repository
        fConnector.connectSVN();
        LOGGER.info(ImporterMessages.EvolizerSVNImporter_reconnected);
        fMonitor.subTask(NLS.bind(ImporterMessages.EvolizerSVNImporter_performingRollback, fCurrRev));
        SVNLogEntry entry = null;
        try {
            entry = fConnector.getRevision(fCurrRev);
        } catch (NoSuchElementException e) {
            throw new SVNImporterException(NLS.bind(
                    ImporterMessages.EvolizerSVNImporter_problemResumingFromRevision,
                    fCurrRev));
        }
        /*
         * I proceed to clean up the data of the last revision found, so I don't run into inconsistencies.
         * The way I do it depends on whether the repository is a standard one or not.
         */
        // Sometimes log entries can be empty, I will just skip them
        if (!entry.getChangedPaths().isEmpty()) {
            // Check if it was a standard release
            if (isRelease(entry)) {
                LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundRelease, entry.getRevision()));
                TreeMap<String, SVNLogEntryPath> sortedPaths =
                        new TreeMap<String, SVNLogEntryPath>(entry.getChangedPaths());
                SVNLogEntryPath tagPath = sortedPaths.values().iterator().next();
                fModelMapper.deleteRelease(entry.getRevision(), entry.getAuthor(), tagPath.getPath());

            } else if (isBranch(entry, true) || isSubBranch(entry, true)) {
                LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundBranch, entry.getRevision()));
                Map<String, SVNLogEntryPath> sortedPaths =
                        new TreeMap<String, SVNLogEntryPath>(entry.getChangedPaths());
                SVNLogEntryPath tagPath = sortedPaths.values().iterator().next();
                fModelMapper.deleteBranch(entry.getRevision(), entry.getAuthor(), getBranchName(entry, tagPath));
            } else {
                LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundRevision, entry.getRevision()));
                String path = null;
                if (this.isInBranch(((SVNLogEntryPath) entry.getChangedPaths().values().iterator().next()).getPath())
                        && (this.fConnector.isStandardSVNLayout() || this.usingCustomPaths)) {
                    path = ((SVNLogEntryPath) entry.getChangedPaths().values().iterator().next()).getPath();
                }
                fModelMapper.deleteRevision(entry.getRevision(), entry.getAuthor(), path);
            }
        }

        fBeginImport = fCurrRev;
        // Then I create a representation
        LOGGER.info(ImporterMessages.EvolizerSVNImporter_restartingImport);
        fMonitor.subTask(ImporterMessages.EvolizerSVNImporter_restartingImport);
        createRepresentation(false);
    }

    /**
     * Retrieves the whole history log of the associated SVN repository (which was set through the constructor).
     * 
     * TODO add the merge from the branch back to the trunk or to the parent branch.
     * 
     * @param isContinuation
     *            If the representation being created is going to be an update (with new additional historical data) of
     *            an existing one . *
     * @throws SVNImporterException
     *             if some problems extracting/recreating SVN historical data are encountered.
     * @throws SVNException
     *             If some problems with the SVN Repository are encountered.
     * @see #SVNImporter(String, String, String, String, int, boolean, boolean, boolean, String[])
     */
    private void createRepresentation(boolean isContinuation) throws SVNImporterException, SVNException {
        try {
            Collection<SVNLogEntry> logEntries;
            // Check whether the whole history is going to be extracted, or only a range (in case of a importing resume)
            if (fBeginImport == 0) {
                // Get all the revisions (svn history)
                if (fEndImport == -1) {
                    logEntries = fConnector.getAllRevisions();
                } else {
                    logEntries = fConnector.getRevisions(fBeginImport, fEndImport);
                }
            } else {
                logEntries = fConnector.getRevisions(fBeginImport, fEndImport);
            }

            if (isContinuation) {
                logEntries.remove(logEntries.iterator().next());
            }
            //
            // // If it has no standard layout at all, i'll just abort the import for now
            // if (!fConnector.isStandardSVNLayout()) {
            // throw new SVNImporterException(SVNConnectorMessages.SVNConnector_noStandard);
            // }
            // Loop through all the revisions and extract all the information I need
            int revisionCount = 1;

            fMonitor
                    .beginTask(NLS.bind(ImporterMessages.EvolizerSVNImporter_importing, fSVNBaseURL), logEntries.size());
            long start = System.currentTimeMillis();
            long previousTime = 0L;
            long prevision = 0L;
            double hrs = 0;
            double mins = 0;
            String previsionResult = "";
            for (SVNLogEntry logEntry : logEntries) {

                if (revisionCount % 100 == 0) {
                    if (previousTime != 0) {
                        previousTime = ((System.currentTimeMillis() - start) + previousTime) / 2;
                    } else {
                        previousTime = System.currentTimeMillis() - start;
                    }
                    start = System.currentTimeMillis();
                    prevision = previousTime * ((logEntries.size() - revisionCount) / 100);
                    hrs = prevision / 3600000;
                    mins = prevision / 60000;
                    if (mins < 60) {
                        previsionResult = mins + " minutes";
                    } else {
                        DecimalFormat df = new DecimalFormat("#.##");
                        previsionResult = df.format(hrs) + " hour(s)";
                    }
                }

                if (revisionCount % 500 == 0) {
                    System.gc();
                }
                if (prevision != 0) {
                    fMonitor.subTask(NLS.bind(
                            ImporterMessages.EvolizerSVNImporter_extractingFromRevision,
                            new Object[]{logEntry.getRevision(), revisionCount, logEntries.size()})
                            + "\n" + NLS.bind(ImporterMessages.EvolizerSVNImporter_extractionTime, previsionResult));
                } else {
                    fMonitor.subTask(NLS.bind(
                            ImporterMessages.EvolizerSVNImporter_extractingFromRevision,
                            new Object[]{logEntry.getRevision(), revisionCount, logEntries.size()}));
                }

                fCurrRev = logEntry.getRevision();

                // If it has no standard layout at all, i'll just abort the import for now
                if (!fConnector.isStandardSVNLayout() && !usingCustomPaths) {
                    if (!logEntry.getChangedPaths().isEmpty()) {
                        LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundRevision, logEntry
                                .getRevision()));
                        handleRevision(logEntry);
                    }
                } else {
                    // Sometimes log entries can be empty, I will just skip them
                    if (!logEntry.getChangedPaths().isEmpty()) {
                        // Check if it was a standard release
                        if (isRelease(logEntry)) {
                            LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundRelease, logEntry
                                    .getRevision()));
                            handleRelease(logEntry);
                        } else if (isBranch(logEntry, false)) { // Check it it was a branch creation (this would only
                            // work
                            // with standard svn repositories).
                            LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundBranch, logEntry
                                    .getRevision()));
                            handleBranch(logEntry, false);
                        } else if (isSubBranch(logEntry, false)) { // Check it it was a subBranch creation
                            LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundSubBranch, logEntry
                                    .getRevision()));
                            handleBranch(logEntry, true);
                        } else {
                            LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundRevision, logEntry
                                    .getRevision()));
                            handleRevision(logEntry);
                        }
                    }
                }
                fMonitor.worked(1);
                revisionCount++;
            }
            fMonitor.done();

        } catch (SVNException e) {
            LOGGER.error(ImporterMessages.EvolizerSVNImporter_noRepresentationRetrieved, e);
            throw e;
        } finally {
            fMonitor.done();
        }
    }

    /**
     * Adds a specific revision of the contents contained in SVN path (specified by a SVNLogEntryPath) to the given
     * Branch. This has to be used only for additions that are done in the same revision of the Branch creation (common
     * thing in cvs2svn repositories), otherwise handleRevision() should be used.
     * 
     * @param addEntry
     *            The SVN path of the entry to be added.
     * @param logEntry
     *            The log entry related to the SVN revision to be added.
     * @param branch
     *            The branch to which the contents need to be added.
     * @param changeSet
     *            The SVN change set in which this operation was done.
     * @throws SVNImporterException
     *             if some problems extracting/recreating SVN historical data are encountered.
     * @throws SVNException
     *             If some problems with the SVN Repository are encountered.
     * @see org.tmatesoft.svn.core.SVNLogEntry
     * @see org.tmatesoft.svn.core.SVNLogEntryPath
     */
    private void addToBranch(SVNLogEntryPath addEntry, SVNLogEntry logEntry, Branch branch, Transaction changeSet)
            throws SVNException,
            SVNImporterException {
        // Check if path points to a directory or a file on current revision
        SVNNodeKind nodeKind = getEntryType(addEntry, logEntry);
        // Path points to a File
        if (nodeKind == SVNNodeKind.FILE) {
            if (addEntry.getCopyPath() == null) {
                SVNVersionedFile file = (SVNVersionedFile) fModelMapper.createFile(addEntry.getPath());
                ModificationReport report =
                        fModelMapper.createModificationReport(logEntry.getDate(), logEntry.getMessage(), logEntry
                                .getAuthor());
                fModelMapper.createBranchRevision(
                        logEntry.getRevision(),
                        file,
                        changeSet,
                        report,
                        null,
                        false,
                        null,
                        branch.getName());
            } else {
                fModelMapper.addToBranch(branch, changeSet, logEntry.getDate(), logEntry.getMessage(), logEntry
                        .getAuthor(), addEntry.getPath(), addEntry.getCopyPath(), logEntry.getRevision(), addEntry
                        .getCopyRevision());
            }
        } else if (nodeKind == SVNNodeKind.DIR) { // Path points to a Directory
            // If the copyPath is null, it means that an empty directory was added, and the content might follow later
            // in other adds ==> so I just create a new Directory
            if (addEntry.getCopyPath() == null) {
                fModelMapper.createDirectory(addEntry.getPath());
            } else {
                // The copy path is not null, so I need to copy all the directory contents to the branch
                fModelMapper.addToBranch(branch, changeSet, logEntry.getDate(), logEntry.getMessage(), logEntry
                        .getAuthor(), addEntry.getPath(), addEntry.getCopyPath(), logEntry.getRevision(), addEntry
                        .getCopyRevision());
            }
        } else { // Path is something else
            LOGGER.error(NLS.bind(ImporterMessages.EvolizerSVNImporter_sourceUnitSkipWarning, new Object[]{
                    addEntry.getType(),
                    nodeKind,
                    addEntry.getPath()}));
        }
    }

    /**
     * Adds a specific revision of the contents contained in SVN path (specified by a SVNLogEntryPath) to the given
     * Release. This has to be used only for additions that are done in the same revision of the Release creation
     * (common thing in cvs2svn repositories), otherwise handleRevision() should be used.
     * 
     * @param addEntry
     *            The SVN path of the entry to be added.
     * @param logEntry
     *            The log entry related to the SVN revision to be added.
     * @param release
     *            The branch to which the contents need to be added.
     * @param changeSet
     *            The SVN change set in which this operation was done.
     * @throws SVNImporterException
     *             if some problems extracting/recreating SVN historical data are encountered.
     * @throws SVNException
     *             If some problems with the SVN Repository are encountered.
     * @see org.tmatesoft.svn.core.SVNLogEntryPath
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    private void addToRelease(SVNLogEntryPath addEntry, SVNLogEntry logEntry, SVNRelease release, Transaction changeSet)
            throws SVNException,
            SVNImporterException {
        // Check if path points to a directory or a file on current revision
        SVNNodeKind nodeKind = getEntryType(addEntry, logEntry);
        // Path points to a File
        if (nodeKind == SVNNodeKind.FILE) {
            if (addEntry.getCopyPath() == null) {
                // This is a file that has been added just to the release, without being in the repository
                // (sometimes it happens for READMEs and build files)
                SVNVersionedFile file = (SVNVersionedFile) fModelMapper.createFile(addEntry.getPath());
                ModificationReport report =
                        fModelMapper.createModificationReport(logEntry.getDate(), logEntry.getMessage(), logEntry
                                .getAuthor());
                fModelMapper.createRevision(logEntry.getRevision(), file, changeSet, report, null, false, null);
            } else {
                fModelMapper.addToRelease(release, changeSet, logEntry.getDate(), logEntry.getMessage(), logEntry
                        .getAuthor(), addEntry.getPath(), addEntry.getCopyPath(), logEntry.getRevision(), addEntry
                        .getCopyRevision());
            }
        } else if (nodeKind == SVNNodeKind.DIR) {
            /*
             * Path points to a Directory
             * If the copyPath is null, it means that an empty directory was added, 
             * and the content might follow later in other adds ==> so I just create a new Directory
             */
            if (addEntry.getCopyPath() == null) {
                fModelMapper.createDirectory(addEntry.getPath());
            } else {
                // The copy path is not null, so I need to copy all the directory contents to the branch
                fModelMapper.addToRelease(release, changeSet, logEntry.getDate(), logEntry.getMessage(), logEntry
                        .getAuthor(), addEntry.getPath(), addEntry.getCopyPath(), logEntry.getRevision(), addEntry
                        .getCopyRevision());
            }
        } else { // Path is something else

            LOGGER.error(NLS.bind(ImporterMessages.EvolizerSVNImporter_sourceUnitSkipWarning, new Object[]{
                    addEntry.getType(),
                    nodeKind,
                    addEntry.getPath()}));
        }
    }

    /**
     * Extracts the name of the involved branch from an SVN path and checks if it refers to an already existing branch.
     * 
     * @param path
     *            The path to check.
     * @return The name of the branch.
     * @throws SVNImporterException
     *             If the name extracted doesn't belong to any existing branch.
     */
    private String extractBranchName(String path) throws SVNImporterException {
        int branchesIn = path.lastIndexOf(fBranches + "/") + fBranches.length() + 1;
        int branchNameIn = path.indexOf("/", branchesIn);
        String extractedName;
        if (branchNameIn == -1) {
            extractedName = path.substring(branchesIn);
        } else {
            extractedName = path.substring(branchesIn, branchNameIn);
        }
        if (!fModelMapper.getBranches().containsKey(extractedName) && branchNameIn != -1) {
            extractedName = path.substring(branchesIn, branchNameIn);
            StringTokenizer t = new StringTokenizer(path.substring(branchNameIn + 1), "/");
            boolean found = false;
            while (t.hasMoreTokens()) {
                extractedName = extractedName + "/" + t.nextToken();
                if (fModelMapper.getBranches().containsKey(extractedName)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                extractedName = path.substring(branchesIn, branchNameIn);
            }
        }
        return extractedName;
    }

    /**
     * It extracts the most likely branch name from a branch creation commit. If the branch was created by cvs2svn, then
     * the branch naming can be 100% accurate.
     * 
     * @param logEntry
     *            The SVNLogEntry of that commit (useful when the branch was created with cvs2svn).
     * @param tagPath
     *            The repository path of the branch.
     * @return The branch name found.
     */
    private String getBranchName(SVNLogEntry logEntry, SVNLogEntryPath tagPath) {
        String tag;
        if (fConnector.isCvsToSvn()
                && logEntry.getMessage().contains(ImporterMessages.EvolizerSVNImporter_cvs2svnBranch)) {
            // If the branch was created by cvs2svn, I can be more accurate in its naming
            tag =
                    logEntry.getMessage().substring(
                            logEntry.getMessage().indexOf("'") + 1,
                            logEntry.getMessage().lastIndexOf("'"));
        } else {
            tag = tagPath.getPath().substring(tagPath.getPath().lastIndexOf("/") + 1);
        }
        return tag;
    }

    /**
     * Processes the file extension regular expression, extracting all the file extension defined in it.
     * 
     * @param fileExtensionRegex
     *            The string containing the regular expressions to process.
     */
    private void processFileExtensionRegEx(String fileExtensionRegex) {
        if (fileExtensionRegex != null && !fileExtensionRegex.equals("")) {
            fFetchSrc = true;
            if (fileExtensionRegex.equals(FILE_EXTENSION_WILDCARD)) {
                fExtensions.add(fileExtensionRegex);
            } else {
                StringTokenizer t = new StringTokenizer(fileExtensionRegex, ";");
                while (t.hasMoreTokens()) {
                    fExtensions.add(t.nextToken());
                }
            }
        }
    }

    /**
     * Fetches the content of all the file revisions inside a given range and saves that to the DB. Use -1 as the last
     * one if you want to fetch all the revisions' content up to the latest one.
     * 
     * @param persistenceProvider
     *            The session used to save the content found.
     * @param fileExtensionRegex
     *            The regular expression containing all the file extensions of the files that need to be imported.
     * @param start
     *            The starting revision.
     * @param end
     *            The last revision.
     * @param monitor
     *            The monitor to report the progress of the import.
     * @throws SVNImporterException
     *             If problems occur while fetching the content.
     */
    public void fetchRevisionRangeSource(
            IEvolizerSession persistenceProvider,
            String fileExtensionRegex,
            long start,
            long end,
            IProgressMonitor monitor) throws SVNImporterException {
        fMonitor = monitor;
        try {
            // First I connect to SVN repository
            fConnector.connectSVN();
            LOGGER.info(ImporterMessages.EvolizerSVNImporter_connected);
            this.processFileExtensionRegEx(fileExtensionRegex);

            if (end == -1) {
                Transaction t =
                        persistenceProvider.query("from Transaction order by finished DESC", Transaction.class).get(0);
                end = Long.parseLong(t.getInvolvedRevisions().iterator().next().getNumber());
            }

            if (start == 0) {
                for (Transaction t : persistenceProvider.query(
                        "from Transaction order by finished ASC",
                        Transaction.class)) {
                    if (t.getInvolvedRevisions() != null && !t.getInvolvedRevisions().isEmpty()) {
                        start = Long.parseLong(t.getInvolvedRevisions().iterator().next().getNumber());
                        break;
                    }
                }
            }
            /*
             *  To limit the number of useless queries to the DB, I first get a
             *  list of all the existing revision numbers in the requested range.
             */
            List<String> revNums =
                    persistenceProvider.query(
                            "select number from Revision where CAST(number as int) >= " + start
                                    + " AND CAST(number as int) <= " + end
                                    + " group by number order by CAST(number as int)",
                            String.class);

            fMonitor.beginTask(
                    NLS.bind(ImporterMessages.EvolizerSVNImporter_fetchingRevisionsSource, start, end),
                    (int) revNums.size());

            for (String revNum : revNums) {
                fMonitor.subTask(NLS.bind(ImporterMessages.EvolizerSVNImporter_fetchingRevisionSource, revNum));
                List<Revision> revs =
                        persistenceProvider.query("from Revision where number = '" + revNum + "'", Revision.class);
                /*
                 * This is a trick to try to be as light as possible on memory consumption
                 */
                Revision[] revisionsArray = revs.toArray(new Revision[]{});
                revs.clear();
                revs = null;

                persistenceProvider.startTransaction();
                int i = 0;
                for (Revision r : revisionsArray) {
                    r = persistenceProvider.load(Revision.class, r.getId());
                    try {
                        if (r.getSource() == null && r.getState() == null) {
                            String contents = getFileContents(r.getFile().getPath(), Long.parseLong(r.getNumber()));
                            if (contents != null) {
                                r.setSource(contents);
                                persistenceProvider.update(r);
                            }
                        }
                    } catch (NumberFormatException e) {
                        throw new SVNImporterException(NLS.bind(
                                ImporterMessages.EvolizerSVNImporter_invalidRevisionNumber,
                                r.getNumber()));
                    }
                    /*
                     *  now we can release the revision to the garbage
                     *  collector, since it was made persistent.
                     */
                    revisionsArray[i] = null;

                    if ((i % 24 == 0) && (i != 0)) {
                        persistenceProvider.flush();
                        persistenceProvider.clear();
                    }
                    i++;
                }
                persistenceProvider.flush();
                persistenceProvider.clear();
                persistenceProvider.endTransaction();
                fMonitor.worked(1);
            }

            fConnector.disconnectSVN();
            LOGGER.info(ImporterMessages.EvolizerSVNImporter_disconnected);
        } catch (SVNException e) {
            throw new SVNImporterException(ImporterMessages.EvolizerSVNImporter_connectionProbs, e);
        }
        fMonitor.done();
    }

    /**
     * Fetches the content of all the files making up a list of releases.
     * 
     * @param persistenceProvider
     *            The session used to save the content found.
     * @param fileExtensionRegex
     *            The regular expression containing all the file extensions of the files that need to be imported.
     * @param relsList
     *            A list of all the releases whose content need to be imported.
     * @param monitor
     *            The monitor to report the progress of the import.
     * @throws SVNImporterException
     *             If problems occur while fetching the content.
     */
    public void fetchReleasesSource(
            IEvolizerSession persistenceProvider,
            String fileExtensionRegex,
            List<String> relsList,
            IProgressMonitor monitor) throws SVNImporterException {
        fMonitor = monitor;
        try {
            // First I connect to SVN repository
            fConnector.connectSVN();
            LOGGER.info(ImporterMessages.EvolizerSVNImporter_connected);
            this.processFileExtensionRegEx(fileExtensionRegex);
            List<SVNRelease> releases;
            if (relsList == null) {
                releases = persistenceProvider.query("from SVNRelease", SVNRelease.class);
                fMonitor.beginTask(ImporterMessages.EvolizerSVNImporter_fetchingAllReleasesSource, releases.size());
            } else {
                String query = "";
                for (String n : relsList) {
                    query = query + "name = '" + n + "' or ";
                }
                query = query.substring(0, query.length() - 4);
                releases = persistenceProvider.query("from SVNRelease where " + query, SVNRelease.class);
                fMonitor.beginTask(ImporterMessages.EvolizerSVNImporter_fetchingReleasesSource, releases.size());
            }

            int j = 0;
            SVNRelease[] releasesArray = releases.toArray(new SVNRelease[]{});
            releases.clear();
            releases = null;

            for (SVNRelease r : releasesArray) {
                fMonitor.subTask(NLS.bind(ImporterMessages.EvolizerSVNImporter_fetchingReleaseSource, r.getName()));

                Revision[] revisionsArray = r.getRevisions().toArray(new Revision[]{});
                int i = 0;
                persistenceProvider.startTransaction();

                for (Revision rev : revisionsArray) {
                    rev = persistenceProvider.load(Revision.class, rev.getId());
                    try {
                        if (rev.getSource() == null) {
                            String contents = getFileContents(rev.getFile().getPath(), Long.parseLong(rev.getNumber()));
                            if (contents != null) {
                                rev.setSource(contents);
                            }
                        }
                    } catch (NumberFormatException e) {
                        throw new SVNImporterException(NLS.bind(
                                ImporterMessages.EvolizerSVNImporter_invalidRevisionNumber,
                                rev.getNumber()));
                    }

                    // now we can release the revision to the garbage
                    // collector,
                    // since it was made persistent.
                    revisionsArray[i] = null;

                    if ((i % 24 == 0) && (i != 0)) {
                        persistenceProvider.flush();
                        persistenceProvider.clear();
                    }
                    i++;
                }
                persistenceProvider.update(r);
                persistenceProvider.flush();
                fMonitor.worked(1);

                persistenceProvider.endTransaction();
                releasesArray[j] = null;
                j++;
            }

            fConnector.disconnectSVN();
            LOGGER.info(ImporterMessages.EvolizerSVNImporter_disconnected);
        } catch (SVNException e) {
            throw new SVNImporterException(ImporterMessages.EvolizerSVNImporter_connectionProbs, e);
        }
        fMonitor.done();
    }

    /**
     * Fetches the contents of a specific revision of a file, only if it belongs to one of the file types that need to
     * be exported.
     * 
     * @param filePath
     *            The repository path to the entity to be extracted.
     * @param revisionNumber
     *            The revision number of that entity.
     * @return A string with all the contents of that entity.
     * @throws SVNImporterException
     *             If problems arise while fetching the contents.
     */
    private String getFileContents(String filePath, long revisionNumber) throws SVNImporterException {
        String content = null;
        int index = filePath.lastIndexOf(".");
        if (fExtensions.contains(FILE_EXTENSION_WILDCARD)
                || (index != -1 && fExtensions.contains(filePath.substring(index)))) {
            content = fConnector.getFileContents(filePath, revisionNumber);
        }
        return content;
    }

    /**
     * Getter for the associated {@link SVNModelMapper}, which does the actual creation of the SVN model.
     * 
     * @return the modelMapper.
     * @see SVNModelMapper
     */
    public SVNModelMapper getModelMapper() {
        return fModelMapper;
    }

    /**
     * Returns the type of an SVN entry at a given path (e.g. file or directory).
     * 
     * @param entryPath
     *            the entry path.
     * @param logEntry
     *            the log entry associated.
     * @return The {@link SVNNodeKind} with the required information.
     * @throws SVNException
     *             It some problems with the repository arise while fetching that information.
     * @see {@link SVNNodeKind}
     */
    @SuppressWarnings("unchecked")
    private SVNNodeKind getEntryType(SVNLogEntryPath entryPath, SVNLogEntry logEntry) throws SVNException {
        /*
         * Since checking the SVN repository for the entry type is slow, we do a
         * couple of checks before enquiring the SVN server. First we check if
         * we've already imported the entry. In fact, if the entry was modified
         * or deleted we should have already imported the file/dir so we can
         * bypass the SVN server. If this check fails we check if the entry is a
         * file with a very simple heuristic: it has a . in its name. At last,
             * we check if the entry path is contained in any other path in the same log entry,
             * as this would mean that we are dealing with a directory.
             * If all checks fail, we ask the SVN server.
         */
        if ((entryPath.getType() == SVNLogEntryPath.TYPE_MODIFIED)
                || (entryPath.getType() == SVNLogEntryPath.TYPE_DELETED)) {
            // Check for file type of imported file
            if (this.fModelMapper.isOnlyFile(entryPath.getPath())) {
                return SVNNodeKind.FILE;
            } else if (this.fModelMapper.isOnlyDirectory(entryPath.getPath())) {
                return SVNNodeKind.DIR;
            }
        }
        int i = entryPath.getPath().lastIndexOf("/");
        String name;
        if (i == -1) {
            name = entryPath.getPath();
        } else {
            name = entryPath.getPath().substring(i);
        }
        if (name.contains(".") && this.fileRegExPattern.matcher(name.substring(name.lastIndexOf(".") + 1)).matches()) {
            return SVNNodeKind.FILE;
        } else {
            Map<String, SVNLogEntryPath> tempChangedPaths = logEntry.getChangedPaths();
            for (SVNLogEntryPath logEntryPath : tempChangedPaths.values()) {
                if (logEntryPath != entryPath && logEntryPath.getPath().contains(entryPath.getPath() + "/")) {
                    return SVNNodeKind.DIR;
                }
            }

        }
        return this.fConnector.getEntryType(entryPath.getPath(), logEntry.getRevision(), entryPath.getType());
    }

    /**
     * Handles a detected branch. It extracts all the needed information from the given log entry before passing it to
     * SVNModelMapper (the one actually creating a representation of the found branch corresponding to our versioning
     * data model). Its internal logic is extremely similar to handleRelease.
     * 
     * @param logEntry
     *            The SVNLogEntry of the detected tag/release.
     * @param isSubBranch
     *            true if the branch found is a sub branch of an already existing branch.
     * @throws SVNException
     *             In case of SVN related problems while fetching the revisions.
     * @throws SVNImporterException
     *             In case of some problems related to the Importer itself happen.
     * @see #handleRelease(SVNLogEntry)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#createBranch(Date, String, String,
     *      String, String, String, long, long, boolean)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#addToBranch(SVNBranch, Date,
     *      SVNModificationReport, String, String, long, long)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#replaceInBranch(SVNBranch, String,
     *      String, long, long)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#removeFromBranch(SVNBranch,
     *      String)
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    @SuppressWarnings("unchecked")
    private void handleBranch(SVNLogEntry logEntry, boolean isSubBranch) throws SVNException, SVNImporterException {
        Map<String, SVNLogEntryPath> sortedPaths = new TreeMap<String, SVNLogEntryPath>(logEntry.getChangedPaths());

        SVNLogEntryPath tagPath = sortedPaths.values().iterator().next();
        String tag = getBranchName(logEntry, tagPath);
        String parentTag = null;
        if (isSubBranch) {
            parentTag = tagPath.getCopyPath().substring(tagPath.getCopyPath().lastIndexOf("/") + 1);
        }
        // Create the ChangeSet related to the branch creation
        Transaction changeSet = fModelMapper.createTransaction(logEntry.getDate());
        // Here I don't check whether the logEntryPath points to a directory as It should already be done in isBranch.
        Branch branch =
                fModelMapper.createBranch(
                        logEntry.getDate(),
                        changeSet,
                        logEntry.getMessage(),
                        logEntry.getAuthor(),
                        tag,
                        parentTag,
                        tagPath.getPath(),
                        tagPath.getCopyPath(),
                        tagPath.getCopyRevision(),
                        logEntry.getRevision());

        // I iterate over remaining log entry paths, as there might be some adds or deletes
        sortedPaths.remove(tagPath.getPath());
        ArrayList<SVNLogEntryPath> delete = new ArrayList<SVNLogEntryPath>();
        TreeMap<String, SVNLogEntryPath> add = new TreeMap<String, SVNLogEntryPath>();
        TreeMap<String, SVNLogEntryPath> replace = new TreeMap<String, SVNLogEntryPath>();
        ArrayList<SVNLogEntryPath> alreadyAdded = new ArrayList<SVNLogEntryPath>();
        for (SVNLogEntryPath logEntryPath : sortedPaths.values()) {
            if (logEntryPath.getType() == SVNLogEntryPath.TYPE_DELETED) {
                delete.add(logEntryPath);
            } else if (logEntryPath.getType() == SVNLogEntryPath.TYPE_ADDED) {
                add.put(logEntryPath.getPath().replace(tagPath.getPath(), ""), logEntryPath);
            } else if (logEntryPath.getType() == SVNLogEntryPath.TYPE_REPLACED) {
                replace.put(logEntryPath.getPath().replace(tagPath.getPath(), ""), logEntryPath);
            }
        }
        for (SVNLogEntryPath replaceEntry : replace.values()) {
            for (SVNLogEntryPath addEntry : add.values()) {
                if (replaceEntry.getPath().contains(addEntry.getPath())) {
                    if (!alreadyAdded.contains(addEntry)) {
                        alreadyAdded.add(addEntry);
                        addToBranch(addEntry, logEntry, branch, changeSet);
                    }
                }
            }
            // do the replace
            fModelMapper.replaceInBranch(
                    branch,
                    changeSet,
                    logEntry.getDate(),
                    logEntry.getMessage(),
                    logEntry.getAuthor(),
                    replaceEntry.getPath(),
                    replaceEntry.getCopyPath(),
                    replaceEntry.getCopyRevision(),
                    logEntry.getRevision());
        }

        for (SVNLogEntryPath addEntry : add.values()) {
            if (!alreadyAdded.contains(addEntry)) {
                addToBranch(addEntry, logEntry, branch, changeSet);
            }
        }

        // At last I take care of deletes, after I did all the replacement and additions in the current log
        for (SVNLogEntryPath deletedEntry : delete) {
            LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_deleteFromBranch, deletedEntry.getPath(), branch
                    .getName()));
            fModelMapper.removeFromBranch(branch, deletedEntry.getPath());
        }
        ArrayList<String> toDelete = new ArrayList<String>();
        for (SVNLogEntryPath deleteEntry : delete) {
            toDelete.add(deleteEntry.getPath());
        }
        fModelMapper.finalizeTransaction(changeSet, toDelete);
        LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_createdBranch, branch.getName()));
    }

    /**
     * Handles the creation of a revision of a directory, extracting all the needed information from the given log entry
     * before passing it to SVNModelMapper. It offers the same functionalities of handleFileRevision, but tailored for
     * directories.
     * 
     * @param logEntryPath
     *            The repository path to the directory.
     * @param logEntry
     *            The log entry associated to the revision being analyzed.
     * @param changeSet
     *            The change set containing this revision.
     * @throws SVNImporterException
     *             If serious problems arise while extracting and mapping the data found.
     * @see org.tmatesoft.svn.core.SVNLogEntry
     * @see org.tmatesoft.svn.core.SVNLogEntryPath
     */
    private void handleDirectoryRevision(SVNLogEntryPath logEntryPath, SVNLogEntry logEntry, Transaction changeSet)
            throws SVNImporterException {
        if (logEntryPath.getType() == SVNLogEntryPath.TYPE_DELETED) {
            // If the directory was deleted, I need to mark all the files inside as deleted too,
            // and create a new FileVersion for each of them, as I do also when single files are deleted.
            if (this.isInBranch(logEntryPath.getPath())
                    && (this.fConnector.isStandardSVNLayout() || this.usingCustomPaths)) {
                // If it's a deletion from a branch, I'll attach the new file version to it right away.
                fModelMapper.deleteDirectory(logEntryPath.getPath(), logEntry.getRevision(), changeSet, logEntry
                        .getDate(), logEntry.getMessage(), logEntry.getAuthor(), this.extractBranchName(logEntryPath
                        .getPath()));
            } else {
                fModelMapper.deleteDirectory(logEntryPath.getPath(), logEntry.getRevision(), changeSet, logEntry
                        .getDate(), logEntry.getMessage(), logEntry.getAuthor(), null);
            }
        } else if ((logEntryPath.getType() == SVNLogEntryPath.TYPE_ADDED) && (logEntryPath.getCopyPath() != null)
                && (logEntryPath.getCopyPath().compareTo("") != 0)) {
            // If the directory was moved, I need to move all its content (files and dirs) and
            // create a new FileVersion for each of the files inside, as I do it also when single files are
            // moved.
            if (this.isInBranch(logEntryPath.getPath())
                    && (this.fConnector.isStandardSVNLayout() || this.usingCustomPaths)) {
                // If it's a move inside a branch, I'll attach the new file version to it right away.
                fModelMapper.copyDirectory(
                        logEntryPath.getCopyPath(),
                        logEntryPath.getPath(),
                        logEntryPath.getCopyPath(),
                        logEntry.getRevision(),
                        changeSet,
                        logEntry.getDate(),
                        logEntry.getMessage(),
                        logEntry.getAuthor(),
                        this.extractBranchName(logEntryPath.getPath()));
            } else {
                fModelMapper.copyDirectory(
                        logEntryPath.getCopyPath(),
                        logEntryPath.getPath(),
                        logEntryPath.getCopyPath(),
                        logEntry.getRevision(),
                        changeSet,
                        logEntry.getDate(),
                        logEntry.getMessage(),
                        logEntry.getAuthor(),
                        null);
            }
        } else {
            // Create new Directory
            fModelMapper.createDirectory(logEntryPath.getPath());
        }
    }

    /**
     * Handles the creation of a revision of a file. It extracts all the needed information from the given log entry
     * before passing it to SVNModelMapper. It offers the same functionalities of handleDirectoryRevision, but tailored
     * for files.
     * 
     * @param logEntryPath
     *            The repository path to the directory.
     * @param logEntry
     *            The log entry associated to the revision being analyzed.
     * @param changeSet
     *            The change set containing this revision.
     * @param movedFromPath
     *            The repository path from which the file is being moved (null if the file was not moved).
     * @throws SVNImporterException
     *             If serious problems arise while extracting and mapping the data found.
     */
    private void handleFileRevision(
            SVNLogEntryPath logEntryPath,
            SVNLogEntry logEntry,
            Transaction changeSet,
            String movedFromPath) throws SVNImporterException {

        SVNVersionedFile file = (SVNVersionedFile) fModelMapper.createFile(logEntryPath.getPath());
        ModificationReport report;
        if ((logEntryPath.getType() == SVNLogEntryPath.TYPE_MODIFIED) && fCalculateLineChanges) {
            // Calculate line changes depending on user request and create the ModificationReport accordingly
            long prevRevisionNumber =
                    Long.parseLong(fModelMapper.getPreviousRevision(logEntryPath.getPath(), logEntry.getRevision()));
            int[] linesChanged =
                    fConnector.getLinesChanged(logEntryPath.getPath(), logEntry.getRevision(), prevRevisionNumber);
            report =
                    fModelMapper.createModificationReport(logEntry.getDate(), logEntry.getMessage(), logEntry
                            .getAuthor(), linesChanged[0], linesChanged[1]);
        } else {
            // Create ModificationReport without line change information
            report =
                    fModelMapper.createModificationReport(logEntry.getDate(), logEntry.getMessage(), logEntry
                            .getAuthor());
        }

        // I check whether the File was deleted (in this case I will mark its state as removed) or it was moved
        // from another location in this revision.
        String copyPath = null;
        boolean deleted = false;
        if (logEntryPath.getType() == SVNLogEntryPath.TYPE_DELETED) {
            deleted = true;
        } else if ((logEntryPath.getType() == SVNLogEntryPath.TYPE_ADDED) && (logEntryPath.getCopyPath() != null)
                && (logEntryPath.getCopyPath().compareTo("") != 0)) {
            copyPath = logEntryPath.getCopyPath();
        } else if (movedFromPath != null && !movedFromPath.equals("")) {
            copyPath = movedFromPath;
        }

        String fileContents = null;
        // If the current operation is not a deleted, it checks if the file content needs to be fetched and dos it if so
        if (!deleted && fFetchSrc) {
            fileContents = this.getFileContents(logEntryPath.getPath(), logEntry.getRevision());
        }
        // Proceeding to create the FileVersion
        if (this.isInBranch(logEntryPath.getPath()) && (this.fConnector.isStandardSVNLayout() || this.usingCustomPaths)) {
            // If it's a revision of a branch, I'll attach the new file version to it right away.
            fModelMapper.createBranchRevision(
                    logEntry.getRevision(),
                    file,
                    changeSet,
                    report,
                    copyPath,
                    deleted,
                    fileContents,
                    this.extractBranchName(logEntryPath.getPath()));
        } else {
            // Create FileVersion, with no branch to attach to, as it's in the trunk.
            fModelMapper.createRevision(
                    logEntry.getRevision(),
                    file,
                    changeSet,
                    report,
                    copyPath,
                    deleted,
                    fileContents);
        }
    }

    /**
     * Handles a detected release. It extracts all the needed information from the given log entry before passing it to
     * SVNModelMapper (the one actually creating a representation of the found release corresponding to our versioning
     * data model).
     * 
     * @param logEntry
     *            The SVNLogEntry of the detected tag/release.
     * @throws SVNException
     *             In case of SVN related problems while fetching the revisions.
     * @throws SVNImporterException
     *             In case of some problems related to the Importer itself happen
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#createRelease(Date, String,
     *      String, String, String, long, long)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#addToRelease(SVNRelease, Date,
     *      String, String, long, long)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#replaceInRelease(SVNRelease,
     *      String, String, long, long)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#removeFromRelease(SVNRelease,
     *      String)
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    @SuppressWarnings("unchecked")
    private void handleRelease(SVNLogEntry logEntry) throws SVNException, SVNImporterException {
        /*
         *  Releases can consist in a first creation  of the release (a copy from trunk to tags) followed by some replacement/additions/deletion.
         *  This happens almost for any cvs2svn repository, but also for standard ones, as many times, additional files 
         *  (ant build files or readmes) are added at release time.
         *  Because of this combination of operations, it is important process the entries in a 
         *  specific order to avoid inconsistent data.
         *  This is why I need to convert the SVN log entry paths Map into a TreeMap, so I get the entries sorted.
         *  In this way I am sure that the first item is the release creation (as it is the shortest path).
         */
        TreeMap<String, SVNLogEntryPath> sortedPaths = new TreeMap<String, SVNLogEntryPath>(logEntry.getChangedPaths());
        Transaction changeSet = fModelMapper.createTransaction(logEntry.getDate());
        // I get the first logEntryPath, the one containing the creation of the tag folder and I create a release with
        // that
        SVNLogEntryPath tagPath = sortedPaths.values().iterator().next();
        SVNRelease release =
                fModelMapper.createRelease(
                        logEntry.getDate(),
                        changeSet,
                        logEntry.getMessage(),
                        logEntry.getAuthor(),
                        tagPath.getPath(),
                        tagPath.getCopyPath(),
                        tagPath.getCopyRevision(),
                        logEntry.getRevision());

        // I iterate over remaining log entry paths
        sortedPaths.remove(tagPath.getPath());
        ArrayList<SVNLogEntryPath> delete = new ArrayList<SVNLogEntryPath>();
        TreeMap<String, SVNLogEntryPath> add = new TreeMap<String, SVNLogEntryPath>();
        TreeMap<String, SVNLogEntryPath> replace = new TreeMap<String, SVNLogEntryPath>();
        ArrayList<SVNLogEntryPath> alreadyAdded = new ArrayList<SVNLogEntryPath>();

        // I get all the single log entry paths and classify them as delete, add or replace
        for (SVNLogEntryPath logEntryPath : sortedPaths.values()) {
            if (logEntryPath.getType() == SVNLogEntryPath.TYPE_DELETED) {
                delete.add(logEntryPath);
            } else if (logEntryPath.getType() == SVNLogEntryPath.TYPE_ADDED) {
                add.put(logEntryPath.getPath().replace(tagPath.getPath(), ""), logEntryPath);
            } else if (logEntryPath.getType() == SVNLogEntryPath.TYPE_REPLACED) {
                replace.put(logEntryPath.getPath().replace(tagPath.getPath(), ""), logEntryPath);
            }
        }

        /*
         * I iterate over the replaces of the log entry
         * Since they are stored in a TreeMap, they are sorted by their names ==> which also means by their path length.
         * In this way if there are chains of recursive replacements inside some directories, 
         * I will always replace the father before the child, thus always having consistent data!
         */
        for (SVNLogEntryPath replaceEntry : replace.values()) {
            // for each replace I check whether there is any parent directory involved in an ADD operation.
            // If so I first take care of those related ADD operations
            for (SVNLogEntryPath addEntry : add.values()) {
                if (replaceEntry.getPath().contains(addEntry.getPath())) {
                    /*
                     * I also keep track of the ADDs that I already took care of, as
                     * multiple replacements might share a common directory that has been added and thus
                     * I need to be sure that I add it to the release only once!
                     */
                    if (!alreadyAdded.contains(addEntry)) {
                        alreadyAdded.add(addEntry);
                        // add to release
                        addToRelease(addEntry, logEntry, release, changeSet);
                    }
                }
            }
            // eventually I do the replace
            fModelMapper.replaceInRelease(
                    release,
                    changeSet,
                    logEntry.getDate(),
                    logEntry.getMessage(),
                    logEntry.getAuthor(),
                    replaceEntry.getPath(),
                    replaceEntry.getCopyPath(),
                    replaceEntry.getCopyRevision(),
                    logEntry.getRevision());
        }

        for (SVNLogEntryPath addEntry : add.values()) {
            if (!alreadyAdded.contains(addEntry)) {
                addToRelease(addEntry, logEntry, release, changeSet);
            }
        }

        // At last I take care of deletes, after I did all the replacement and additions in the current log
        for (SVNLogEntryPath deleteEntry : delete) {
            LOGGER.debug(NLS.bind(
                    ImporterMessages.EvolizerSVNImporter_deleteFromRelease,
                    deleteEntry.getPath(),
                    release.getName()));
            fModelMapper.removeFromRelease(release, deleteEntry.getPath());
        }
        ArrayList<String> toDelete = new ArrayList<String>();
        for (SVNLogEntryPath deleteEntry : delete) {
            toDelete.add(deleteEntry.getPath());
        }
        fModelMapper.finalizeTransaction(changeSet, toDelete);
        fModelMapper.finalizeRelease(release);
        LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_createdRelease, release.getName()));
    }

    /**
     * Handles a detected revision. It extracts all the needed information from the given log entry before passing it to
     * SVNModelMapper (the one actually creating a representation of the found revision corresponding to our versioning
     * data model).
     * 
     * @param logEntry
     *            The SVNLogEntry of the detected revision.
     * @throws SVNException
     *             In case of SVN related problems while fetching the revisions.
     * @throws SVNImporterException
     *             In case of some problems related to the Importer itself happen.
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#createChangeSet(Date)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#createFile(String)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#createModificationReport(Date,
     *      String, String)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#createModificationReport(Date,
     *      String, String, int, int)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#createFileVersion(long, SVNFile,
     *      ChangeSet, SVNModificationReport, String, boolean, String)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#createDirectory(String)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#copyDirectory(String, String,
     *      String, long, ChangeSet, Date, String, String, String)
     * @see org.evolizer.versioncontrol.svn.importer.mapper.servis.svn.SVNModelMapper#deleteDirectory(String, long,
     *      ChangeSet, Date, String, String, String)
     */
    @SuppressWarnings("unchecked")
    private void handleRevision(SVNLogEntry logEntry) throws SVNException, SVNImporterException {
        Map<String, SVNLogEntryPath> tempChangedPaths = logEntry.getChangedPaths();
        /*
         * I analyze all the changedPaths in the current log entry to extract the adds, deletes and the other operations.
         * This is extremely useful as in this way I'm able to order all the operations according to their type.
         * In particular I can process the deletes before the add and I can easily find out all the moves
         * that were done (not just the explicit ones).
         * In fact, whenever some moves were done, the corresponding SVN log contains a delete of the old file or directory 
         * and a copy of the old contents to the new file/directory.
         * This can be an explicit copy, an add with as copyPath the old file/directory that is being copied,
         * or just an add of the same file that was deleted (this happens often in cvs2svn repositories).
         * Since the logEntryPath don't follow a specific folder, as they are into HashMaps,
         * I would know whether the delete or the add come first, so if I treat the delete after the add I need to
         * make sure that I don't set the state to deleted.
         * All this can be solved with this pre-processing.
         */
        ArrayList<SVNLogEntryPath> delete = new ArrayList<SVNLogEntryPath>();
        ArrayList<SVNLogEntryPath> add = new ArrayList<SVNLogEntryPath>();
        ArrayList<SVNLogEntryPath> rest = new ArrayList<SVNLogEntryPath>();
        Map<SVNLogEntryPath, SVNLogEntryPath> moved = new HashMap<SVNLogEntryPath, SVNLogEntryPath>();
        for (SVNLogEntryPath logEntryPath : tempChangedPaths.values()) {
            if (logEntryPath.getType() == SVNLogEntryPath.TYPE_DELETED) {
                delete.add(logEntryPath);
            } else if (logEntryPath.getType() == SVNLogEntryPath.TYPE_ADDED) {
                add.add(logEntryPath);
            } else {
                rest.add(logEntryPath);
            }
        }
        ArrayList<SVNLogEntryPath> changedPaths = new ArrayList<SVNLogEntryPath>();
        changedPaths.addAll(delete);
        changedPaths.addAll(add);
        changedPaths.addAll(rest);
        for (SVNLogEntryPath addEntry : add) {
            for (SVNLogEntryPath deleteEntry : delete) {
                if (addEntry.getPath().substring(addEntry.getPath().lastIndexOf("/") + 1).equals(
                        deleteEntry.getPath().substring(deleteEntry.getPath().lastIndexOf("/") + 1))) {
                    moved.put(addEntry, deleteEntry);
                    break;
                }
            }
        }
        // Now I proceed to the actual revision handling/creation
        Date date = logEntry.getDate();
        Transaction changeSet = fModelMapper.createTransaction(date);

        for (SVNLogEntryPath logEntryPath : changedPaths) {

            // Check if path points to a directory or a file on current revision
            SVNNodeKind nodeKind = getEntryType(logEntryPath, logEntry);
            if (nodeKind == SVNNodeKind.FILE) {
                // Path points to a File
                String movedFromPath = null;
                if (moved.containsKey(logEntryPath)) {
                    movedFromPath = moved.get(logEntryPath).getPath();
                }
                this.handleFileRevision(logEntryPath, logEntry, changeSet, movedFromPath);
            } else if (nodeKind == SVNNodeKind.DIR) {
                // Path points to a Directory
                this.handleDirectoryRevision(logEntryPath, logEntry, changeSet);
            } else { // Path is something else
                LOGGER.error(NLS.bind(ImporterMessages.EvolizerSVNImporter_sourceUnitSkipWarning, new Object[]{
                        logEntryPath.getType(),
                        nodeKind,
                        logEntryPath.getPath()}));
                continue;
            }
        }
        // Extract the list of paths of deleted files and dirs so I can remove them from the Model Mapper
        ArrayList<String> toDelete = new ArrayList<String>();
        for (SVNLogEntryPath deleteEntry : delete) {
            toDelete.add(deleteEntry.getPath());
        }
        fModelMapper.finalizeTransaction(changeSet, toDelete);
        LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_createdRevision, logEntry.getRevision()));
    }

    /**
     * Checks if an SVNLogEntry represents a branch creation.
     * 
     * @param logEntry
     *            The log entry to check.
     * @param isResume
     *            If i'm currently resuming an import.
     * @return true if the log entry is a branch creation.
     * @throws SVNException
     *             If some problems with the SVN Repository are encountered.
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    @SuppressWarnings("unchecked")
    private boolean isBranch(SVNLogEntry logEntry, boolean isResume) throws SVNException {
        /*
         * It works with standard SVN repositories, as it checks if a copy 
         * of an entire directory from the branches to the branches folder has been done.
         * It also checks that the ADD operation does not target an existing branch,
         * otherwise, also file/directory moves inside a branch could end up counting as sub branch creations.
         * Look at isRelease for an explanation on why I use a TreeMap
         */
        Map<String, SVNLogEntryPath> sortedMap = new TreeMap<String, SVNLogEntryPath>(logEntry.getChangedPaths());
        Iterator<SVNLogEntryPath> logEntryPathIterator = sortedMap.values().iterator();
        SVNLogEntryPath firstLogEntryPath = logEntryPathIterator.next();

        if ((logEntry.getMessage() != null)
                && logEntry.getMessage().contains(ImporterMessages.EvolizerSVNImporter_cvs2svnBranch)
                && !isContainedInBranch(firstLogEntryPath.getPath())) {
            SVNNodeKind nodeKind = getEntryType(firstLogEntryPath, logEntry);
            if (nodeKind == SVNNodeKind.DIR) {
                int beginIndex;
                int endIndex;
                beginIndex = logEntry.getMessage().indexOf("'");
                endIndex = logEntry.getMessage().lastIndexOf("'");
                LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundcvs2svnBranch, logEntry.getMessage()
                        .substring(beginIndex + 1, endIndex)));
                return true;
            } else {
                return false;
            }
        } else {
            // Checking that CopyPath and CopyRevision are set and the copy is done from the trunk to the branches
            // folder
            if ((firstLogEntryPath.getType() == SVNLogEntryPath.TYPE_ADDED)
                    && (firstLogEntryPath.getCopyPath() != null) && this.isInTrunk(firstLogEntryPath.getCopyPath())
                    && (firstLogEntryPath.getCopyRevision() != -1) && this.isInBranch(firstLogEntryPath.getPath())) {
                /*
                 * I check that all the paths that are being added are children of the first found (which is thus the root folder for this branch)
                 * In this way I know that the first entry is a directory and that I'm dealing with a real branch creation,
                 * as there could actually be some commits, in which different branches are modified and that would otherwise end up as a branch creation.
                 */
                while (logEntryPathIterator.hasNext()) {
                    SVNLogEntryPath entry = logEntryPathIterator.next();
                    // To do so I just need to check that all their paths start with the supposed release path
                    if (!entry.getPath().startsWith(firstLogEntryPath.getPath())) {
                        LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_revisionBranchSimilarity, logEntry
                                .getRevision()));
                        return false;
                    }
                }

                if (isResume) {
                    Branch b = null;
                    for (String branchName : fModelMapper.getBranches().keySet()) {
                        if (firstLogEntryPath.getPath().contains(branchName)) {
                            b = fModelMapper.getBranches().get(branchName);
                            break;
                        }
                    }
                    if (b != null) {
                        return b.getCreationDate().compareTo(logEntry.getDate()) == 0;
                    } else {
                        return true;
                    }
                } else if (!isContainedInBranch(firstLogEntryPath.getPath())) { // I check that It's not just an add to
                    // an existing branch
                    LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundBranch, firstLogEntryPath.getPath()
                            .substring(firstLogEntryPath.getPath().lastIndexOf("/") + 1)));
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Checks if a SVNLogEntry represents a sub branch creation.
     * 
     * @param logEntry
     *            The log entry to check.
     * @param isResume
     *            If i'm currently resuming an import.
     * @return true if the log entry is a sub branch creation.
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    @SuppressWarnings("unchecked")
    private boolean isSubBranch(SVNLogEntry logEntry, boolean isResume) {
        /*
         * It works with standard SVN repositories, as it checks if the a copy of an entire directory 
         * from the branches to the branches folder has been done.
         * It also checks that the ADD operation does not target an existing branch,
         * otherwise, also file/directory moves inside a branch could end up counting as sub branch creations.
         */
        Map<String, SVNLogEntryPath> sortedMap = new TreeMap<String, SVNLogEntryPath>(logEntry.getChangedPaths());
        Iterator<SVNLogEntryPath> logEntryPathIterator = sortedMap.values().iterator();
        SVNLogEntryPath firstLogEntryPath = logEntryPathIterator.next();
        if ((firstLogEntryPath.getType() == SVNLogEntryPath.TYPE_ADDED)
        // CopyPath is set
                && (firstLogEntryPath.getCopyPath() != null)
                // The copy is done from the branches folder
                && this.isInBranch(firstLogEntryPath.getCopyPath())
                // CopyRevision is set
                && (firstLogEntryPath.getCopyRevision() != -1)
                // and to the branches folder
                && this.isInBranch(firstLogEntryPath.getPath())) {
            /* 
             * I check that all the paths that are being added are children of the first found (which is thus the root folder for this branch).
             * In this way I know that the first entry is a directory and that I'm dealing with a real branch creation,
             * as there could actually be some commits, in which different branches are modified that would otherwise end up as a branch creation.
             */
            while (logEntryPathIterator.hasNext()) {
                SVNLogEntryPath entry = logEntryPathIterator.next();
                // To do so I just need to check that all their paths start with the supposed release path
                if (!entry.getPath().startsWith(firstLogEntryPath.getPath())) {
                    LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_revisionBranchSimilarity, logEntry
                            .getRevision()));
                    return false;
                }
            }

            if (isResume) {
                Branch b = null;
                for (String branchName : fModelMapper.getBranches().keySet()) {
                    if (firstLogEntryPath.getPath().contains(branchName)) {
                        b = fModelMapper.getBranches().get(branchName);
                        break;
                    }
                }
                if (b != null) {
                    return b.getCreationDate().compareTo(logEntry.getDate()) == 0;
                } else {
                    return true;
                }
            } else if (!isContainedInBranch(firstLogEntryPath.getPath())) { // So I check that I'm not adding to an
                // already existing branch.
                LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundSubBranch, firstLogEntryPath.getPath()
                        .substring(firstLogEntryPath.getPath().lastIndexOf("/") + 1)));
                return true;
            } else {
                // It's an operation involving some already existing branches
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Checks if the path passed as input refers to an existing branch.
     * 
     * @param path
     *            The path to check.
     * @return true if the path actually refers to an existing branch, otherwise it return false.
     */
    private boolean isContainedInBranch(String path) {
        for (String branchName : fModelMapper.getBranches().keySet()) {
            if (path.contains(branchName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the repository path passed as input belong to an SVN branch folder sub directory.
     * 
     * @param path
     *            The string containing the path to check.
     * @return true if it belongs to it.
     */
    private boolean isInBranch(String path) {
        return Pattern.matches(ImporterMessages.EvolizerSVNImporter_directoriesRegEx + "/" + fBranches
                + ImporterMessages.EvolizerSVNImporter_directoriesRegEx, path);
    }

    /**
     * Checks if the repository path passed as input belong to an SVN tag folder sub directory.
     * 
     * @param path
     *            The string containing the path to check.
     * @return true if it belongs to it.
     */
    private boolean isInTags(String path) {
        return Pattern.matches(ImporterMessages.EvolizerSVNImporter_directoriesRegEx + "/" + fTags
                + ImporterMessages.EvolizerSVNImporter_directoriesRegEx, path);
    }

    /**
     * Checks if the repository path passed as input belong to an SVN trunk folder sub directory.
     * 
     * @param path
     *            The string containing the path to check.
     * @return true if it belongs to it.
     */
    private boolean isInTrunk(String path) {
        return Pattern.matches(ImporterMessages.EvolizerSVNImporter_directoriesRegEx + "/" + fTrunk
                + ImporterMessages.EvolizerSVNImporter_directoriesRegEx, path);
    }

    /**
     * Checks if an {@link SVNLogEntry} represents a tag/release creation.
     * 
     * @param logEntry
     *            The SVNLogEntry toc check.
     * @return true if the logEntry represents a tag/release creation.
     * @throws SVNException
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    @SuppressWarnings("unchecked")
    private boolean isRelease(SVNLogEntry logEntry) throws SVNException {
        /*
         * I need to convert the Map into a TreeMap, so I get the entries sorted. In this way I am sure that the
         * first item is the release creation (as it is the shortest path).
         * With an HashMap, the values are placed randomly, so it would not be possible.
         */
        Map<String, SVNLogEntryPath> sortedMap = new TreeMap<String, SVNLogEntryPath>(logEntry.getChangedPaths());
        Iterator<SVNLogEntryPath> logEntryPathIterator = sortedMap.values().iterator();
        // it first checks if it's a cvs2svn release creation, as this would allow to make specific assumptions and be
        // more precise.
        if ((logEntry.getMessage() != null)
                && logEntry.getMessage().contains(ImporterMessages.EvolizerSVNImporter_cvs2svnTag)) {
            int beginIndex;
            int endIndex;
            beginIndex = logEntry.getMessage().indexOf("'");
            endIndex = logEntry.getMessage().lastIndexOf("'");
            String releaseTag = logEntry.getMessage().substring(beginIndex + 1, endIndex);
            LOGGER.debug(NLS.bind(ImporterMessages.EvolizerSVNImporter_foundRelease, releaseTag)
                    + ImporterMessages.EvolizerSVNImporter_exportedBycvs2svn);
            return true;
        } else {
            SVNLogEntryPath firstLogEntryPath = logEntryPathIterator.next();
            /*
             * A log entry, in order to be a standard release has to be made up of just one type of action:
             * an ADD from ONE folder to another of a directory (not of a file) and some optional addition of files to
             * that newly created directory.
             */
            if ((firstLogEntryPath.getType() == SVNLogEntryPath.TYPE_ADDED)
                    && (firstLogEntryPath.getCopyPath() != null) && (firstLogEntryPath.getCopyRevision() != -1)) {

                // I check whether all the additional logEntries are just additions of some additional files to the
                // newly created release.
                while (logEntryPathIterator.hasNext()) {
                    SVNLogEntryPath entry = logEntryPathIterator.next();
                    // To do so I just need to check that all their paths start with the supposed release path
                    if (!entry.getPath().startsWith(firstLogEntryPath.getPath())) {
                        return false;
                    }
                }
                /*
                 * If the project follows the standard SVN layout, I can be stricter and check that the ADD was from trunk to tags.
                 * In fact, if the standards are followed, also the tag/release creation should follow them.
                 */
                return (this.isInTrunk(firstLogEntryPath.getCopyPath()) || this.isInBranch(firstLogEntryPath
                        .getCopyPath()))
                        && this.isInTags(firstLogEntryPath.getPath());
            } else {
                return false;
            }
        }
    }
}
