/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.osgi.util.NLS;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNPlugin;
import org.evolizer.versioncontrol.svn.importer.exceptions.SVNImporterException;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNProperty;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNDiffClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.SVNLogEntry;

/**
 * Class used to connect and interact (e.g. connect, disconnect and get data) with an {@link SVNRepository}. In this way
 * all the interaction with SVN is done only in one class, decoupling the rest of the application from it.
 * 
 * @see SVNRepository
 * @author ghezzi
 * 
 */
public class SVNConnector {

    /**
     * SVN URL
     */
    private String fSVNBaseURL;
    /**
     * SVN user name
     */
    private String fSVNUserName;
    /**
     * SVN user password
     */
    private String fSVNUserPassword;
    /**
     * Whether or not the repository follows the standard SVN layout.
     */
    private boolean fStandardSVNLayout;
    /**
     * Whether or not the repository was imported from CVS using cvs2svn.
     */
    private boolean fCvsToSvn;
    /**
     * The trunk, tags, branches paths. Can be null if the repository is standard. ==> it has at its those three folders
     * at the root.
     */
    private String[] fPaths;
    /**
     * SVN repository associated
     */
    private SVNRepository fRepository;
    /**
     * The standard logger
     */
    private static final Logger LOGGER =
            EvolizerSVNPlugin.getLogManager().getLogger(SVNConnector.class.getCanonicalName());

    /**
     * Standard SVN folders name. They are not hardcoded so that a user could actually set up the the importer to work
     * with custom folders, instead of the 3 standard ones, but behaving as it is importing a standard repository.
     */
    private String fTags = SVNConnectorMessages.SVNConnector_tags;
    private String fBranches = SVNConnectorMessages.SVNConnector_branches;
    private String fTrunk = SVNConnectorMessages.SVNConnector_trunk;

    /**
     * Constructor.
     * 
     * @param svnBaseUrl
     *            The URL on which the SVN repository resides.
     * @param svnUserName
     *            The user name to use to access the repository.
     * @param svnUserPassword
     *            The password associated to that username.
     * @param paths
     *            The trunk/branches/tags paths if using non standard structure, otherwise can be null.
     */
    public SVNConnector(String svnBaseUrl, String svnUserName, String svnUserPassword, String[] paths) {
        fSVNBaseURL = svnBaseUrl;
        fSVNUserName = svnUserName;
        fSVNUserPassword = svnUserPassword;
        fPaths = paths;
        if (paths != null) {
            fTrunk = paths[0];
            fTags = paths[1];
            fBranches = paths[2];
        }
    }

    /**
     * Returns whether the associated SVN repository was imported from CVS with cvs2svn or not.
     * 
     * @return true if the repository was imported from CVS with cvs2svn.
     */
    public boolean isCvsToSvn() {
        return fCvsToSvn;
    }

    /**
     * Returns whether the associated SVN repository follows the standard structure or not.
     * 
     * @return true if it follows the standard structure
     */
    public boolean isStandardSVNLayout() {
        return fStandardSVNLayout;
    }

    /**
     * Getter for the SVN url to the SVNConnector is associated to
     * 
     * @return A string containing the URL.
     */
    public String getSVNBaseURL() {
        return fSVNBaseURL;
    }

    /**
     * Getter for the SVN username for the SVN repository the SVNConnector is associated to.
     * 
     * @return A string containing the username.
     */
    public String getSVNUserName() {
        return fSVNUserName;
    }

    /**
     * Getter for the SVN password for the SVN repository the SVNConnector is associated to.
     * 
     * @return A string containing the password.
     */
    public String getSVNUserPassword() {
        return fSVNUserPassword;
    }

    /**
     * Getter for the {@link SVNRepository} associated to this SVNConnector.
     * 
     * @return The {@link SVNRepository}
     */
    public SVNRepository getRepository() {
        return fRepository;
    }

    /**
     * Connects the importer to its associated SVN Server (which was set through the constructor).
     * 
     * @throws SVNException
     *             if some SVN related problems arose at connection time.
     * @throws SVNImporterException
     *             if the the URL is somehow faulty.
     */

    public void connectSVN() throws SVNImporterException, SVNException {
        try {

            if ((fSVNBaseURL == null) || fSVNBaseURL.equals("")) {
                throw new SVNImporterException(SVNConnectorMessages.SVNConnector_noURL);
            }

            // initialize the library (it must be done before ever using the library itself)
            setupLibrary();
            fRepository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(fSVNBaseURL));

        } catch (SVNException e) {
            LOGGER.error(NLS.bind(SVNConnectorMessages.SVNConnector_repoCreationError, fSVNBaseURL), e);
            throw e;
        }

        // Create Authentication manager
        ISVNAuthenticationManager authManager;
        if ((fSVNUserName == null) && (fSVNUserPassword == null)) {
            authManager = SVNWCUtil.createDefaultAuthenticationManager();
        } else {
            authManager = SVNWCUtil.createDefaultAuthenticationManager(fSVNUserName, fSVNUserPassword);
        }
        fRepository.setAuthenticationManager(authManager);

        try {
            /*
             * Checks up if the specified path/to/repository part of the URL
             * really corresponds to a directory.
             */
            SVNNodeKind nodeKind = fRepository.checkPath("", -1); // -1 corresponds to latest revision
            if (nodeKind == SVNNodeKind.NONE) {
                throw new SVNImporterException(NLS.bind(SVNConnectorMessages.SVNConnector_noEntry, fSVNBaseURL));
            } else if (nodeKind == SVNNodeKind.FILE) {
                throw new SVNImporterException(NLS.bind(SVNConnectorMessages.SVNConnector_unexpectedEntry, fSVNBaseURL));
            }
        } catch (SVNImporterException e) {
            LOGGER.error(SVNConnectorMessages.SVNConnector_connectionError, e);
            throw e;
        }

    }

    /**
     * Initializes the library to work with a repository using different protocols.
     * 
     * @see org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory#setup()
     * @see org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl#setup()
     * @see org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory#setup()
     */
    private void setupLibrary() {
        // For using over http:// and https://
        DAVRepositoryFactory.setup();
        // For using over svn:// and svn+xxx://
        SVNRepositoryFactoryImpl.setup();
        // For using over file:///
        FSRepositoryFactory.setup();
    }

    /**
     * Retrieves the all SVN Revisions inside a given range.
     * 
     * @param startRevision
     *            the lower bound of the range.
     * @param endRevision
     *            the upper bound of the range.
     * @return a collection containing all the log entries belonging to the revision range.
     * @throws SVNException
     *             In case of SVN related problems while fetching the revisions.
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    @SuppressWarnings("unchecked")
    public Collection<SVNLogEntry> getRevisions(long startRevision, long endRevision) throws SVNException {
        Collection<SVNLogEntry> entries;
        LOGGER.debug(NLS.bind(SVNConnectorMessages.SVNConnector_retrievingRevisions, startRevision, (endRevision != -1 ? endRevision : "head")));
        if (fPaths != null) {
            entries = fRepository.log(fPaths, null, startRevision, endRevision, true, true);
        } else {
            entries = fRepository.log(new String[]{""}, null, startRevision, endRevision, true, true);
        }
        /*
         * I detect if the repository is a cvs2svn one and/or if it has a standard SVN layout (trunk/tags/branches)
         * This allows the importer to make more specific assumptions and recreate a more precise SVN history.
         * In fact, if the repository has the standard layout, tags and branches can be tracked with almost a 100% precision.
         * I skip this check if I already know it does (it was set by the user).
         */
        detectProperties(entries);
        if (isCvsToSvn()) {
            LOGGER.info(SVNConnectorMessages.SVNConnector_isStandard);
        }
        if (isStandardSVNLayout()) {
            LOGGER.info(SVNConnectorMessages.SVNConnector_hasStandard);
        } else {
            LOGGER.error(SVNConnectorMessages.SVNConnector_noStandard);
        }
        return entries;
    }

    /**
     * Retrieves a specific SVN Revision.
     * 
     * @param revision
     *            The revision to fetch.
     * @return The SVNLogEntry containing the data of the specific revision.
     * @throws SVNException
     *             In case of SVN related problems while fetching the revision.
     */
    public SVNLogEntry getRevision(long revision) throws SVNException {

        SVNLogEntry entry;
        LOGGER.debug(NLS.bind(SVNConnectorMessages.SVNConnector_retrievingSingleRevision, revision));
        if (fPaths != null) {
            entry = (SVNLogEntry) fRepository.log(fPaths, null, revision, revision, true, true).iterator().next();
        } else {
            entry =
                    (SVNLogEntry) fRepository.log(new String[]{""}, null, revision, revision, true, true).iterator()
                            .next();
        }

        return entry;
    }

    public long getLatestRevision() throws SVNException {
        return fRepository.getLatestRevision();
    }
    /**
     * Retrieves all the SVN Revisions (the whole SVN repository history).
     * 
     * @return Collection containing LogEntry objects.
     * @throws SVNException
     *             In case of SVN related problems while fetching the revisions
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    public Collection<SVNLogEntry> getAllRevisions() throws SVNException {
        return getRevisions(0, -1);
    }

    /**
     * Returns the type of the entry at a given path (e.g. file or directory) by querying the SVN repository.
     * 
     * @param path
     *            the path to the entry.
     * @param revisionNumber
     *            the revision number of the entry.
     * @param logType
     *            if the entry was added, deleted, modified, etc.
     * @return The {@link SVNNodeKind} representing entry type found.
     * 
     * @throws SVNException
     *             If problems arise while fetching the information.
     */
    public SVNNodeKind getEntryType(String path, long revisionNumber, char logType) throws SVNException {
        LOGGER.debug(NLS.bind(SVNConnectorMessages.SVNConnector_retrievingNodeKind, path));

        // Change comparing revision to one down if entry was deleted in current revision
        long compareNumber = revisionNumber;
        if (logType == SVNLogEntryPath.TYPE_DELETED) {
            compareNumber -= 1;
        }

        // Get node kind from repository
        return fRepository.checkPath(path, compareNumber);
    }

    /**
     * Fetches the contents of a specific revision of an SVN entity (file/directory). It basically does an svn export of
     * that file.
     * 
     * @param filePath
     *            The repository path to the entity to be extracted.
     * @param revisionNumber
     *            The revision number of that entity.
     * @return A string containing all the contents of that entity.
     * @throws SVNImporterException
     *             If problems arise while fetching the contents.
     */
    public String getFileContents(String filePath, long revisionNumber) throws SVNImporterException {
        String content = null;

        SVNProperties fileProperties = new SVNProperties();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            fRepository.getFile(filePath, revisionNumber, fileProperties, baos);
            String mimeType = fileProperties.getStringValue(SVNProperty.MIME_TYPE);
            boolean isTextType = SVNProperty.isTextMimeType(mimeType);
            if (isTextType) {
                try {
                    content = baos.toString();
                    baos.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            } else {
                LOGGER.debug(NLS.bind(SVNConnectorMessages.SVNConnector_noTextFile, filePath));
            }
        } catch (SVNException e) {
            SVNImporterException e1 =
                    new SVNImporterException(NLS.bind(SVNConnectorMessages.SVNConnector_fetchFileError, filePath, revisionNumber));
            e1.setStackTrace(e.getStackTrace());
            throw e1;
        }
        return content;
    }

    /**
     * Calculates the the number of lines added and deleted between two specific file versions, using the SVN diff
     * algorithm.
     * 
     * @param changedPath
     *            path to the file to examine.
     * @param currRevisionNumber
     *            The most recent revision to examine.
     * @param prevRevisionNumber
     *            The least recent revision to examine.
     * @return array with [0]: lines added, [1]: lines deleted.
     */
    public int[] getLinesChanged(String changedPath, long currRevisionNumber, long prevRevisionNumber) {
        int[] linesChanged = {-1, -1};

        // If there's no previous version, we don't need to calculate the diff
        if (prevRevisionNumber == -1) {
            return linesChanged;
        }

        LOGGER.debug(NLS.bind(SVNConnectorMessages.SVNConnector_calculatingChangeLines, new Object [] {changedPath, prevRevisionNumber, currRevisionNumber}));

        try {
            // Create SVNURL from changed file
            SVNURL svnURLChangedPath =
                    SVNURL.parseURIDecoded(fRepository.getRepositoryRoot(true).toDecodedString() + changedPath);

            // Create SVNRevisions from respective numbers
            SVNRevision currentRevision = SVNRevision.create(currRevisionNumber);
            SVNRevision previousRevision = SVNRevision.create(prevRevisionNumber);

            // Do the diff
            SVNDiffClient diffClient = SVNClientManager.newInstance().getDiffClient();
            ByteArrayOutputStream diffBuffer = new ByteArrayOutputStream();
            diffClient.doDiff(
                    svnURLChangedPath,
                    previousRevision,
                    svnURLChangedPath,
                    currentRevision,
                    SVNDepth.EMPTY,
                    false,
                    diffBuffer);

            // Transform diff output to buffered reader
            String uniDiffOutput = diffBuffer.toString();
            BufferedReader uniDiffReader = new BufferedReader(new StringReader(uniDiffOutput));

            // Parse diff output line by line and gather number of lines added and deleted
            // by counting the number of '-' and '+' at the beginning of a line
            String curLine;
            // Init with -1, since first two lines always contain a '-' and '+' when the file changed (independently of
            // the actual number of changed lines)
            int linesAdded = -1;
            int linesDeleted = -1;
            while ((curLine = uniDiffReader.readLine()) != null) {
                if (curLine.indexOf('+') == 0) {
                    linesAdded++;
                } else if (curLine.indexOf('-') == 0) {
                    linesDeleted++;
                }
            }
            // If lines added and lines deleted are still -1, the file was not changed at all (no lines in diff)
            if ((linesAdded == -1) && (linesDeleted == -1)) {
                linesAdded = 0;
                linesDeleted = 0;
            }
            uniDiffReader.close();

            // Set return value
            linesChanged[0] = linesAdded;
            linesChanged[1] = linesDeleted;

        } catch (SVNException e) {
            LOGGER.error(NLS.bind(SVNConnectorMessages.SVNConnector_cannotDiff, new Object [] {prevRevisionNumber, currRevisionNumber, changedPath}), e);
        } catch (IOException ioe) {
            LOGGER.error(NLS.bind(SVNConnectorMessages.SVNConnector_cannotDiff, new Object [] {prevRevisionNumber, currRevisionNumber, changedPath}), ioe);
        }

        return linesChanged;
    }

    /**
     * It automatically detects if the SVN repository to be analyzed has been exported from CVS, using cvs2svn and if it
     * follows the standard SVN layout (trunk/tags/branches).
     * 
     * @param entries
     *            The SVNLogEntries making up the repository log.
     * @throws SVNException
     *             If some problems with the SVN Repository are encountered.
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    @SuppressWarnings("unchecked")
    private void detectProperties(Collection<SVNLogEntry> entries) throws SVNException {
        /*
         * It works both if the SVN layout was created in one revision, or if its creation is spread through several
         * successive revisions. Once the first files are added to the repository is found, the algorithm stops.
         */
        String trunkPath = null;
        String tagsPath = null;
        String branchesPath = null;

        /*
         * I check right away that the trunk,tags,branches folder exist at the HEAD revision.
         * In this case I can be already 100% sure that the repository is SVN standard, 
         * otherwise I analyze the SVN log. This makes it also more tolerant to repositories 
         * that were moved from another location (e.g. the Apache incubation policy)
         */
        String repoRoot = fRepository.getRepositoryRoot(true).toString();
        String repoUrl = fRepository.getLocation().toString();
        SVNNodeKind trunk =
                getEntryType(repoUrl.substring(repoUrl.indexOf(repoRoot) + repoRoot.length()) + "/"
                        + SVNConnectorMessages.SVNConnector_trunk, -1, 'M');
        SVNNodeKind tags =
                getEntryType(repoUrl.substring(repoUrl.indexOf(repoRoot) + repoRoot.length()) + "/"
                        + SVNConnectorMessages.SVNConnector_tags, -1, 'M');
        SVNNodeKind branches =
                getEntryType(repoUrl.substring(repoUrl.indexOf(repoRoot) + repoRoot.length()) + "/"
                        + SVNConnectorMessages.SVNConnector_branches, -1, 'M');
        if (!trunk.equals(SVNNodeKind.NONE) && !tags.equals(SVNNodeKind.NONE) && !branches.equals(SVNNodeKind.NONE)) {
            fStandardSVNLayout = true;
        } else {
            for (SVNLogEntry logEntry : entries) {
                Map<String, SVNLogEntryPath> changedPaths = logEntry.getChangedPaths();
                if (!fCvsToSvn) {
                    fCvsToSvn = checkCvsToSvn(logEntry);
                }
                if ((changedPaths != null) && (changedPaths.values().size() > 0)) {
                    /*
                     * As soon as I find a log entry that has some changed paths (some initial ones can
                     * be empty) I check if it's the creation of the known trunk/tags/branches structure, or parts of it.
                     */
                    for (SVNLogEntryPath entry : ((Map<String, SVNLogEntryPath>) logEntry.getChangedPaths()).values()) {
                        if (entry.getType() == SVNLogEntryPath.TYPE_ADDED) {
                            if (getEntryType(entry.getPath(), logEntry.getRevision(), entry.getType()) == SVNNodeKind.FILE) {
                                /* 
                                 * A file add has been found, this means that the initial repository setup is over for sure (at least according to our assumptions).
                                 * This means that I'll stop looking for the trunk/tags/branches folders and assume that the project uses its own customs structure.
                                 */
                                boolean res = (trunkPath != null) && (tagsPath != null) && (branchesPath != null);
                                if (res && ((trunkPath != null) || (tagsPath != null) || (branchesPath != null))) {
                                    LOGGER.info(SVNConnectorMessages.SVNConnector_partialStructure);
                                }
                                fStandardSVNLayout = res;
                                return;
                            }
                            int index = entry.getPath().lastIndexOf("/") + 1;
                            String t = entry.getPath().substring(index);
                            if (t.compareTo(fTags) == 0) {
                                tagsPath = entry.getPath().substring(0, index);
                            } else if (t.compareTo(fTrunk) == 0) {
                                trunkPath = entry.getPath().substring(0, index);
                            } else if (t.compareTo(fBranches) == 0) {
                                branchesPath = entry.getPath().substring(0, index);
                            }
                        }
                    }
                }
                // Check if all the three key folder have been found already.
                if ((trunkPath != null) && (tagsPath != null) && (branchesPath != null)) {
                    fStandardSVNLayout = true;
                    return;
                }
            }
            fStandardSVNLayout = false;
        }
    }

    /**
     * Checks if a given SVNLogEntry represents a cvs2svn repository initialization.
     * 
     * @param logEntry
     *            The SVNLogEntry to check.
     * @return true if the the SVNLogEntry contains a cvs2svn repository initialization.
     * @see org.tmatesoft.svn.core.SVNLogEntry
     */
    private boolean checkCvsToSvn(SVNLogEntry logEntry) {
        if (logEntry.getMessage() == null) {
            return false;
        }
        return logEntry.getMessage().contains(SVNConnectorMessages.SVNConnector_cvs2svnInitialized)
                && (logEntry.getAuthor() == null);
    }

    /**
     * Closes connection to SVN
     */
    public void disconnectSVN() {
        fRepository.closeSession();
    }
}
