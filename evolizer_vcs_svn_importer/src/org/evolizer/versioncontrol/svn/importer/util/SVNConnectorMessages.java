/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.util;

import org.eclipse.osgi.util.NLS;


public class SVNConnectorMessages extends NLS {

    private static final String BUNDLE_NAME = "org.evolizer.versioncontrol.svn.importer.util.messages";
    
    
    public static String SVNConnector_noURL;
    public static String SVNConnector_tags;
    public static String SVNConnector_branches;
    public static String SVNConnector_trunk;
    public static String SVNConnector_cvs2svnInitialized;
    public static String SVNConnector_partialStructure;
    public static String SVNConnector_noStandard;
    public static String SVNConnector_isStandard;
    public static String SVNConnector_hasStandard;
    public static String SVNConnector_repoCreationError;
    public static String SVNConnector_noEntry;
    public static String SVNConnector_unexpectedEntry;
    public static String SVNConnector_connectionError;
    public static String SVNConnector_retrievingRevisions;
    public static String SVNConnector_retrievingSingleRevision;
    public static String SVNConnector_retrievingNodeKind;
    public static String SVNConnector_noTextFile;
    public static String SVNConnector_fetchFileError;
    public static String SVNConnector_calculatingChangeLines;
    public static String SVNConnector_cannotDiff;
    
    static {
        NLS.initializeMessages(BUNDLE_NAME, SVNConnectorMessages.class);
    }
    
    private SVNConnectorMessages(){}
}
