/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.eclipse.osgi.util.NLS;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.fs.File;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.humans.Role;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.CommitterRole;
import org.evolizer.versioncontrol.cvs.model.entities.ModificationReport;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNPlugin;
import org.evolizer.versioncontrol.svn.importer.exceptions.SVNImporterException;
import org.evolizer.versioncontrol.svn.model.entities.SVNRelease;
import org.evolizer.versioncontrol.svn.model.entities.SVNRevision;
import org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile;

/**
 * SVNModelMapper
 * 
 * Maps the SVN structure found by the SVNImporter to an SVN representation using the CVS/SVN entities from
 * {@link versioning.model.entities}.
 * 
 * @author ghezzi
 * 
 */
public class SVNModelMapper {

    /**
     * A cache containing all the SVNVersionedFiles created during a revision and the Revision element itself. In this
     * way, I can saved them all at once to reduce DB accesses.
     */
    private ArrayList<Object> fRevisionElemsCache = new ArrayList<Object>();
    /**
     * A cache containing all the files created during a revision/release/branch creation. In this way, I can save them
     * all at once to reduce DB accesses and improve performance substantially.
     */
    private ArrayList<File> fFilesCache = new ArrayList<File>();
    /**
     * The current person (the author of the current revision/release/branch).
     */
    private Person fCurrPerson;
    private CommitterRole fCurrRole;
    private Directory fCachedRoot;
    /**
     * The standard logger.
     */
    private static final Logger LOGGER =
            EvolizerSVNPlugin.getLogManager().getLogger(SVNModelMapper.class.getCanonicalName());
    /**
     * Hibernate Session providing the connection to the database.
     */
    private IEvolizerSession fPersistenceProvider;
    /**
     * The persons (authors) found.
     */
    private HashMap<String, Person> fPersons = new HashMap<String, Person>();
    /**
     * The directories found.
     */
    private HashMap<String, Directory> fDirectories = new HashMap<String, Directory>();
    /**
     * The files found.
     */
    private HashMap<String, SVNVersionedFile> fFiles = new HashMap<String, SVNVersionedFile>();
    /**
     * The branches found.
     */
    private HashMap<String, Branch> fBranches = new HashMap<String, Branch>();
    /**
     * The branch associated to the current revision (if there's any).
     */
    private ArrayList<Branch> fCurrBranches = new ArrayList<Branch>();

    private Pattern wordPattern = Pattern.compile("(\\w)+");

    /**
     * Standard Constructor.
     * 
     * @param persistenceProvider
     *            The {@link IEvolizerSession} that will be used to save the model extracted.
     */
    public SVNModelMapper(IEvolizerSession persistenceProvider) {
        this.fPersistenceProvider = persistenceProvider;
    }

    /**
     * Adds some given content (File or a Directory) to a given Branch.
     * 
     * @param branch
     *            The target Branch.
     * @param changeSet
     *            The change set (transaction in generic terms) in which this addition is taking place.
     * @param date
     *            The date of the addition.
     * @param message
     *            The commit message of this addition.
     * @param author
     *            The author of this addition.
     * @param toPath
     *            The repository path to which the content are being added.
     * @param fromPath
     *            The repository path from which the content is being copied.
     * @param toRevNum
     *            The revision number of this addition.
     * @param fromRevNum
     *            The revision number of the content being added.
     * @throws SVNImporterException
     *             If fromPath doesn't point to an existing file or directory.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     */
    public void addToBranch(
            Branch branch,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            String toPath,
            String fromPath,
            long toRevNum,
            long fromRevNum) throws SVNImporterException {
        addToBranchOrRelease(branch, null, changeSet, date, message, author, toPath, fromPath, toRevNum, fromRevNum);
    }

    /**
     * Adds some given content (File or a Directory) to a given Branch or Release.
     * 
     * @param branch
     *            The target Branch, can be null if the content is to be added to a Release.
     * @param release
     *            The target Release, can be null if the content is to be added to a Branch.
     * @param changeSet
     *            The change set (transaction in generic terms) in which this addition is taking place.
     * @param date
     *            The date of the addition.
     * @param message
     *            The commit message of this addition.
     * @param author
     *            The author of this addition.
     * @param toPath
     *            The repository path to which the content are being added.
     * @param fromPath
     *            The repository path from which the content is being copied.
     * @param toRevNum
     *            The revision number of this addition.
     * @param fromRevNum
     *            The revision number of the content being added.
     * @throws SVNImporterException
     *             If both branch and release parameters are null or have valid values.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     * @see java.util.Date
     */
    private void addToBranchOrRelease(
            Branch branch,
            SVNRelease release,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            String toPath,
            String fromPath,
            long toRevNum,
            long fromRevNum) throws SVNImporterException {

        // I check whether I'm just adding a File (much faster) or a whole directory
        SVNVersionedFile fromFile = fFiles.get(fromPath);
        if (fromFile != null) {
            SVNVersionedFile newFile = (SVNVersionedFile) createFile(toPath);
            addFile(branch, release, changeSet, date, fromFile, newFile, message, author, toRevNum, fromRevNum);
        } else {
            // If it's not a file that is being added, it must be a directory, otherwise I throw an exception
            Directory dir = fDirectories.get(fromPath);
            if (dir != null) {
                /*
                 * If it's a Directory I need to get all the Files belonging to
                 * it and to all its sub directories. First though I check if
                 * I'm adding contents directly to the folder I'm copying from.
                 */
                if (fromPath.compareTo("/") == 0
                        || this.wordPattern.matcher(toPath.replace(fromPath + "/", "")).matches()) {
                    addDirectory(
                            branch,
                            release,
                            changeSet,
                            date,
                            fromPath,
                            fromPath,
                            toPath,
                            message,
                            author,
                            toRevNum,
                            fromRevNum,
                            this.createDirectory(toPath));
                } else {
                    addDirectory(
                            branch,
                            release,
                            changeSet,
                            date,
                            fromPath,
                            fromPath,
                            toPath,
                            message,
                            author,
                            toRevNum,
                            fromRevNum,
                            this.createDirectory(toPath));
                }
            } else {
                throw new SVNImporterException(NLS.bind(
                        MapperMessages.SVNModelMapper_copiedPathNotExists,
                        fromPath,
                        toPath));
            }
        }
    }

    /**
     * Adds a File to a given Release or Branch.
     * 
     * @param branch
     *            The target Branch, can be null, if the content is to be added to a Release.
     * @param release
     *            The target Release, can be null, if the content is to be added to a Branch.
     * @param changeSet
     *            The ChangeSet (transaction in generic terms) in which this addition is taking place.
     * @param date
     *            The date of the addition.
     * @param fromFile
     *            The file being copied.
     * @param newFile
     *            The destination file.
     * @param message
     *            The commit message of this addition.
     * @param author
     *            The author of this addition.
     * @param toRevNum
     *            The revision number of this addition.
     * @param fromRevNum
     *            The revision number of the content being added.
     * @throws SVNImporterException
     *             If both branch and release parameters are null or have valid values
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile
     */
    private void addFile(
            Branch branch,
            SVNRelease release,
            Transaction changeSet,
            Date date,
            SVNVersionedFile fromFile,
            SVNVersionedFile newFile,
            String message,
            String author,
            long toRevNum,
            long fromRevNum) throws SVNImporterException {
        /*
         * I create the new revision which will have as a revision number the one of the release creation
         * Then I link it the modification report (with the branch creation message) and the other related objects, and I link those
         * objects too (ex. Person to ModReport, etc.)
         */
        SVNRevision revision = getClosestRevision(fromFile, fromRevNum);
        SVNRevision newRevision = new SVNRevision("" + toRevNum);
        newRevision.setChangeSet(changeSet);
        changeSet.addRevision(newRevision); // Transaction -> SVNRevision
        ModificationReport report = this.createModificationReport(date, message, author);

        // Getting the committer role and adding this new revision to it
        for (Role r : report.getAuthor().getRoles()) {
            if (r instanceof CommitterRole) {
                ((CommitterRole) r).addRevision(newRevision);
                break;
            }
        }

        newRevision.setReport(report); // SVNRevision -> ModificationReport
        newRevision.setAncestor(revision);
        newFile.addRevision(newRevision); // File -> SVNRevision
        newRevision.setFile(newFile); // SVNRevision ->File
        if ((release != null) && (branch == null)) {
            release.addReleaseRevision(newRevision);
        } else if ((branch != null) && (release == null)) {
            branch.addRevision(newRevision); // Branch -> SVNRevision
        } else {
            throw new SVNImporterException(MapperMessages.SVNModelMapper_addFileMisuse);
        }
    }

    /**
     * Adds a Directory to a given Release or Branch.
     * 
     * @param branch
     *            The target Branch, can be null, if the content is to be added to a Release.
     * @param release
     *            The target Release, can be null, if the content is to be added to a Branch.
     * @param changeSet
     *            The change set (transaction in generic terms) in which this addition is taking place.
     * @param date
     *            The date of the addition.
     * @param currDirPath
     *            The repository path of the Directory to be copied.
     * @param fromPath
     *            The source repository path.
     * @param toPath
     *            The target repository path.
     * @param message
     *            The commit message of this addition.
     * @param author
     *            The author of this addition.
     * @param toRevNum
     *            The revision number of this addition.
     * @param fromRevNum
     *            The revision number of the content being added.
     * @param toIgnore
     *            A directory that needs to be ignored in this operation (can be null). This is used in case I'm adding
     *            exactly to the folder I'm copying from. In this way I avoid concurrent modification exceptions.
     * @throws SVNImporterException
     *             If both branch and release parameters are null or have valid values.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     * 
     */
    private void addDirectory(
            Branch branch,
            SVNRelease release,
            Transaction changeSet,
            Date date,
            String currDirPath,
            String fromPath,
            String toPath,
            String message,
            String author,
            long toRevNum,
            long fromRevNum,
            Directory toIgnore) throws SVNImporterException {
        Directory directory = fDirectories.get(currDirPath);
        if (directory != null) {
            for (File unit : directory.getChildren()) {
                if (unit instanceof SVNVersionedFile) {
                    SVNVersionedFile newFile = (SVNVersionedFile) createFile(unit.getPath().replace(fromPath, toPath));
                    addFile(
                            branch,
                            release,
                            changeSet,
                            date,
                            (SVNVersionedFile) unit,
                            newFile,
                            message,
                            author,
                            toRevNum,
                            fromRevNum);
                } else if (unit instanceof Directory) {
                    // I dig down in the sub directories, as I need to add them too
                    if (toIgnore == null || unit.getPath().compareTo(toIgnore.getPath()) != 0) {
                        addDirectory(
                                branch,
                                release,
                                changeSet,
                                date,
                                unit.getPath(),
                                fromPath,
                                toPath,
                                message,
                                author,
                                toRevNum,
                                fromRevNum,
                                toIgnore);
                    }
                }
            }
        }
    }

    /**
     * Adds some new content (File and Directories) to a specific Release.
     * 
     * @param release
     *            The target release.
     * @param changeSet
     *            The change set (transaction in generic terms) in which this addition is taking place.
     * @param date
     *            The date of the addition.
     * @param message
     *            The commit message of this addition.
     * @param author
     *            The author of this addition.
     * @param toPath
     *            The repository path to which the content are being added.
     * @param fromPath
     *            The repository path from which the content is being copied.
     * @param toRevNum
     *            The revision number of this addition.
     * @param fromRevNum
     *            The revision number of the content being added.
     * @throws SVNImporterException
     *             If fromPath doesn't point to an existing file or directory.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     */
    public void addToRelease(
            SVNRelease release,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            String toPath,
            String fromPath,
            long toRevNum,
            long fromRevNum) throws SVNImporterException {
        addToBranchOrRelease(null, release, changeSet, date, message, author, toPath, fromPath, toRevNum, fromRevNum);
    }

    /**
     * Connects a given Release or Branch to its Revisions. It works for both as, Releases and Branches are basically
     * the same in SVN, except minor things that I can easily take care of.
     * 
     * @param creationTime
     *            The Release/Branch creation time.
     * @param changeSet
     *            The change set (transaction in generic terms) related to this Release/Branch creation.
     * @param commitMessage
     *            The Release/Branch creation commit message.
     * @param author
     *            The author of the commit.
     * @param branch
     *            The Branch to connect (null if connecting a Release).
     * @param release
     *            The Release to connect (null if connecting a Release).
     * @param toPath
     *            The repository path from which this Release/Branch is being copied.
     * @param fromPath
     *            The repository path to which this Release/branch is being created
     * @param copyRevision
     *            The revision number to copy from.
     * @param revisionNum
     *            The revision number associated to the Branch/Release creation.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     * @see org.evolizer.model.resources.entities.humans.Person
     */
    private void connectRevisionToBranchOrRelease(
            Date creationTime,
            Transaction changeSet,
            String commitMessage,
            String author,
            Branch branch,
            SVNRelease release,
            String toPath,
            String fromPath,
            long copyRevision,
            long revisionNum) {
        List<Revision> revisions =
                fPersistenceProvider.query("FROM SVNRevision as f WHERE f.file.path LIKE '" + fromPath
                        + "%' ORDER BY f.file.path", Revision.class);
        HashMap<String, SVNRevision> m = new HashMap<String, SVNRevision>();
        for (Revision rev : revisions) {
            SVNRevision r = (SVNRevision) rev;
            SVNRevision found = m.get(r.getFile().getPath());
            if (found == null) {
                if (Long.parseLong(r.getNumber()) <= copyRevision) {
                    m.put(r.getFile().getPath(), r);
                }
            } else if (Long.parseLong(r.getNumber()) <= copyRevision
                    && Long.parseLong(r.getNumber()) > Long.parseLong(found.getNumber())) {
                m.put(r.getFile().getPath(), r);
            }
        }
        for (String key : m.keySet()) {
            SVNRevision oldRevision = m.get(key);
            String oldRevisionFileName = oldRevision.getFile().getPath();
            String newRevisionFileName = oldRevisionFileName.replaceFirst(fromPath, toPath);
            if (oldRevision.getState() == null) {
                // I create the new SVNRevision which will have as a revision number the one of the branch/release
                // creation
                SVNRevision newRevision = new SVNRevision(revisionNum + "");
                ModificationReport report = this.createModificationReport(creationTime, commitMessage, author);
                newRevision.setReport(report);

                // I also need to create a new File, as it is actually done by the SVN
                // It's the same of the old one, but with a different path
                SVNVersionedFile newFile = (SVNVersionedFile) createFile(newRevisionFileName);
                newRevision.setChangeSet(changeSet);
                changeSet.addRevision(newRevision); // Transaction -> SVNRevision
                newFile.addRevision(newRevision); // File -> SVNRevision
                newRevision.setFile(newFile); // SVNRevision -> File
                // Keep track of a new file ancestor
                newRevision.setAncestor(oldRevision);
                // I check whether I'm connecting the new SVNRevision to a Branch or to a Release
                if ((branch != null) && (release == null)) {
                    // Now I add the newly created file version to the Branch and viceversa
                    branch.addRevision(newRevision); // Branch -> SVNRevision
                    LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_addedRevisionToBranch, new String[]{
                            newRevision.getFile().getPath(),
                            newRevision.getNumber(),
                            branch.getName(),
                            oldRevision.getFile().getPath(),
                            oldRevision.getNumber()}));
                } else if ((branch == null) && (release != null)) {
                    // I add the newly created file version to the Release and viceversa
                    release.addReleaseRevision(newRevision);

                    LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_addedRevisionToRelease, new String[]{
                            newRevision.getFile().getPath(),
                            newRevision.getNumber(),
                            release.getName(),
                            oldRevision.getFile().getPath(),
                            oldRevision.getNumber()}));
                }
            }
        }
    }

    /**
     * Copies a Directory and its contents to a specific Branch.
     * 
     * @param currDirPath
     *            The repository path of the Directory to be copied.
     * @param toPath
     *            The target repository path.
     * @param copyPath
     *            The source repository path.
     * @param revNum
     *            The revision number of the Directory to be copied.
     * @param changeSet
     *            The change set (transaction in generic terms) related to this copy operation.
     * @param date
     *            The data of this copy.
     * @param message
     *            The commit message associated to this copy.
     * @param author
     *            The author of this copy operation.
     * @param branch
     *            The name of the branch involved in this operation.
     * @throws SVNImporterException
     *             If copyPath is not null but it does not point to any existing file.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     */
    public void copyDirectory(
            String currDirPath,
            String toPath,
            String copyPath,
            long revNum,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            String branch) throws SVNImporterException {
        if (toPath.compareTo(copyPath) == 0) {
            //Kinda dirty
            return;
        }
        Directory copyDir = fDirectories.get(copyPath);
        /*
         * I check if I'm adding contents directly to the folder I'm copying from.
         * In that case I need to be careful not to run into ConcurrentModification exceptions
         * 
         */
        if (toPath.contains(copyPath)) {
            String initialToPath = "";
            if (copyPath.compareTo("/") == 0) {
                int i = toPath.substring(1).indexOf("/");

                if (i == -1) {
                    initialToPath = toPath;
                } else {
                    initialToPath = toPath.substring(0, i + 1);
                }

            } else {
                StringTokenizer copyPathTk = new StringTokenizer(copyPath, "/");
                StringTokenizer toPathTk = new StringTokenizer(toPath, "/");
                String partialCopyPath = "";
                String partialToPath = "";
                boolean differentlastToken = false;
                while (copyPathTk.hasMoreTokens()) {
                    if (toPathTk.hasMoreTokens()) {
                        partialCopyPath = copyPathTk.nextToken();
                        partialToPath = toPathTk.nextToken();
                        if (partialCopyPath.compareTo(partialToPath) == 0) {
                            initialToPath = initialToPath + "/" + partialCopyPath;
                        } else {
                            differentlastToken = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!differentlastToken) {
                    partialToPath = toPathTk.nextToken();
                }
                if (!initialToPath.isEmpty() && !partialToPath.isEmpty()) {
                    initialToPath = initialToPath + "/" + partialToPath;
                }
            }
            if (initialToPath.isEmpty()) {
                this.internalCopyDirectory(
                        copyDir,
                        toPath,
                        copyPath,
                        revNum,
                        changeSet,
                        date,
                        message,
                        author,
                        branch,
                        null);
            } else {
                this.internalCopyDirectory(
                        copyDir,
                        toPath,
                        copyPath,
                        revNum,
                        changeSet,
                        date,
                        message,
                        author,
                        branch,
                        this.createDirectory(initialToPath));
            }

        } else {

            this.internalCopyDirectory(
                    copyDir,
                    toPath,
                    copyPath,
                    revNum,
                    changeSet,
                    date,
                    message,
                    author,
                    branch,
                    null);
        }
    }

    private void internalCopyDirectory(
            Directory currDir,
            String toPath,
            String copyPath,
            long revNum,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            String branch,
            Directory toIgnore) throws SVNImporterException {
        if (currDir != null) {
            for (File unit : currDir.getChildren()) {
                if (unit instanceof SVNVersionedFile) {

                    SVNVersionedFile newFile;
                    if (copyPath.compareTo("/") == 0) {
                        newFile = createFile(toPath + unit.getPath());
                    } else {
                        newFile = createFile(unit.getPath().replace(copyPath, toPath));
                    }

                    createRevision(
                            revNum,
                            newFile,
                            changeSet,
                            this.createModificationReport(date, message, author),
                            unit.getPath(),
                            false,
                            null,
                            branch);
                } else if (unit instanceof Directory) {
                    // I dig down in the subdirectories, as I need to "copy" them too
                    if (toIgnore == null || unit.getPath().compareTo(toIgnore.getPath()) != 0) {
                        internalCopyDirectory(
                                ((Directory) unit),
                                toPath,
                                copyPath,
                                revNum,
                                changeSet,
                                date,
                                message,
                                author,
                                branch,
                                toIgnore);
                    }
                }
            }
        } else {
            createDirectory(toPath);
        }
    }

    /**
     * Creates a Branch object.
     * 
     * @param creationTime
     *            The Branch creation time.
     * @param changeSet
     *            The change set (transaction in generic terms) associated to the Branch creation.
     * @param message
     *            The commit message associated to the Branch creation.
     * @param author
     *            The author of the commit.
     * @param tag
     *            The name (tag in SVN terminology) of the Branch to be created.
     * @param toPath
     *            The repository path from which this Branch was copied from.
     * @param fromPath
     *            The repository path where this Branch is saved.
     * @param copyRevisionNum
     *            The revision number of the copy.
     * @param revisionNum
     *            The revision number associated to the Branch creation.
     * @param isSubBranch
     *            true if the Branch is actually a Sub Branch.
     * @return The Branch created.
     * @throws SVNImporterException
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     */
    public Branch createBranch(
            Date creationTime,
            Transaction changeSet,
            String message,
            String author,
            String tag,
            String parentTag,
            String toPath,
            String fromPath,
            long copyRevisionNum,
            long revisionNum) {

        // Create the Branch
        Branch branch = new Branch(tag);
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_createdBranch, branch.getName()));
        branch.setCreationDate(creationTime);
        fBranches.put(tag, branch);
        fCurrBranches.add(branch);
        // Connect it to its related SVNRevisions
        connectRevisionToBranchOrRelease(
                creationTime,
                changeSet,
                message,
                author,
                branch,
                null,
                toPath,
                fromPath,
                copyRevisionNum,
                revisionNum);
        if (parentTag != null) {
            // If it's a sub branch, I link it to its parent and vice versa
            Branch parentBranch = fBranches.get(parentTag);
            parentBranch.addChild(branch);
            branch.setParent(parentBranch);
            LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_isSubBranch, tag, parentBranch.getName()));
        }
        return branch;
    }

    /**
     * Creates a Branch, given just its creation date and name. It is used to create branches whose original creation
     * was not found in the svn log.So the creation date is when it was actually encountered for the first time
     * 
     * @param creationTime
     *            The date of the Branch creation (when it was first encountered in the log).
     * @param tag
     *            The name of the branch.
     * @return the created branch
     */
    private Branch createSpecialBranch(Date creationTime, String tag) {
        // Create the Branch
        Branch branch = new Branch(tag);
        branch.setCreationDate(creationTime);
        fBranches.put(tag, branch);
        return branch;
    }

    /**
     * Creates a Transaction object.
     * 
     * @param date
     *            The Transaction creation date.
     * @return The Transaction created.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     */
    public Transaction createTransaction(Date date) {
        Transaction changeSet = new Transaction();
        changeSet.setStarted(date); // start and end date are the same
        changeSet.setFinished(date);
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_createdTransaction, date));
        return changeSet;
    }

    /**
     * Deletes a specific release from the Version Control model found by the importer.
     * 
     * @param revNum
     *            The commit on which the release was created.
     * @param author
     *            The author who committed the release.
     * @param releasePath
     *            The repository path pointing to the release to be deleted.
     */
    public void deleteRelease(long revNum, String author, String releasePath) {
        fRevisionElemsCache.clear();
        cleanUpCommitter(revNum, author);
        for (String k : fFiles.keySet()) {
            if (k.contains(releasePath)) {
                List<Revision> rv = fFiles.get(k).getRevisions();
                rv.clear();
                fFiles.get(k).setRevisions(rv);
                if (fFiles.get(k).getId() != null && fFiles.get(k).getId() != -1) {
                    fPersistenceProvider.update(fFiles.get(k));
                }
            }
        }
        fPersistenceProvider.flush();
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_deletedRelease, releasePath));
    }

    /**
     * Deletes a specific Branch from the Version Control model found by the importer.
     * 
     * @param revNum
     *            The commit on which the branch was created.
     * @param author
     *            The author who committed the branch.
     * @param branchName
     *            The name of the branch.
     */
    public void deleteBranch(long revNum, String author, String branchName) {
        fRevisionElemsCache.clear();
        fCurrBranches = new ArrayList<Branch>();

        Branch branch = fBranches.get(branchName);
        if (branch != null) {
            fPersistenceProvider.startTransaction();
            fPersistenceProvider.saveObject(branch);
            cleanUpCommitter(revNum, author);

            if (branch.getParent() != null) {
                Set<Branch> children = branch.getParent().getChildren();
                children.remove(branch);
                branch.getParent().setChildren(children);
                if (branch.getParent().getId() != null) {
                    fPersistenceProvider.update(branch.getParent());
                }
            }
            ((SVNRevision) branch.getRevisions().iterator().next()).getChangeSet().setInvolvedRevisions(null);

            for (Revision rev : branch.getRevisions()) {
                if (((SVNRevision) rev).getAncestor() != null) {
                    ((SVNVersionedFile) ((SVNRevision) rev).getAncestor().getFile()).setCopiedTo(null);
                }
                ((SVNRevision) rev).setAncestor(null);
                List<Revision> rvs = ((SVNVersionedFile) rev.getFile()).getRevisions();
                rvs.clear();
                ((SVNVersionedFile) rev.getFile()).setRevisions(rvs);
                if (((SVNVersionedFile) rev.getFile()).getCopiedFrom() != null) {
                    ((SVNVersionedFile) rev.getFile()).getCopiedFrom().setCopiedTo(null);
                    fPersistenceProvider.update(((SVNVersionedFile) rev.getFile()).getCopiedFrom());
                }
                if (rev.getFile().getId() != null) {
                    fPersistenceProvider.update(rev.getFile());
                }
                fFiles.remove(rev.getFile().getPath());
                rev.setReport(null);
            }
            fPersistenceProvider.delete(branch);
            fBranches.remove(branchName);
            fPersistenceProvider.endTransaction();
            fPersistenceProvider.flush();
            LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_deletedBranch, branch.getName()));
        }
    }

    /**
     * Deletes a specific SVNRevision from the Version Control model found by the importer.
     * 
     * @param revNum
     *            The SVNRevision number.
     * @param author
     *            The author who committed the revision.
     * @param branchPath
     *            The path to the branch involved in this revision. Can be null if there was no branch involved.
     */
    public void deleteRevision(long revNum, String author, String branchPath) {
        fRevisionElemsCache.clear();
        cleanUpCommitter(revNum, author);
        List<SVNRevision> revisions =
                fPersistenceProvider.query("from SVNRevision as fv where fv.number = " + revNum, SVNRevision.class);

        Branch branch = null;
        if (branchPath != null) {
            Set<String> keys = fBranches.keySet();
            for (String k : keys) {
                if (branchPath.contains(k)) {
                    branch = fBranches.get(k);
                }
            }
        }
        fPersistenceProvider.startTransaction();
        for (SVNRevision fv : revisions) {

            if (fv.getPreviousRevision() != null) {
                fv.getPreviousRevision().setNextRevision(null);
                fPersistenceProvider.update(fv.getPreviousRevision());
            }
            SVNVersionedFile f = (SVNVersionedFile) fv.getFile();
            if (f.getCopiedFrom() != null) {
                ((SVNVersionedFile) f.getCopiedFrom()).setCopiedTo(null);
                fPersistenceProvider.update((SVNVersionedFile) f.getCopiedFrom());
            }
            Revision rev = f.getLatestRevision();
            if (Long.parseLong(rev.getNumber()) == revNum) {
                List<Revision> revs = f.getRevisions();
                revs.remove(f.getLatestRevision());
                f.setRevisions(revs);
                fPersistenceProvider.update(f);
            }
            if (branch != null) {
                Set<Revision> rs = branch.getRevisions();
                rs.remove(fv);
                branch.setRevisions(rs);
            }
            if (fv.getPreviousRevision() != null) {
                fv.getPreviousRevision().setNextRevision(null);
            }
            fv.setReport(null);
            fPersistenceProvider.delete(fv);
        }
        fPersistenceProvider.endTransaction();
        fPersistenceProvider.flush();
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_deletedRevision, revNum));
    }

    /**
     * Removes all the file versions involved a revision from the committer's artifacts.
     * 
     * @param revNum
     *            The revision number of the files to be removed.
     * @param author
     *            The author to modify.
     */
    private void cleanUpCommitter(long revNum, String author) {
        Person authPerson = fPersons.get(author);
        if (authPerson != null) {
            Set<Role> roles = authPerson.getRoles();
            boolean needUpdate = false;
            for (Role r : roles) {
                if (r instanceof CommitterRole) {
                    ArrayList<Revision> toRemove = new ArrayList<Revision>();
                    for (Revision rev : ((CommitterRole) r).getArtifacts()) {
                        if (Long.parseLong(rev.getNumber()) == revNum) {
                            needUpdate = true;
                            toRemove.add(rev);
                        }
                    }
                    for (Revision rev : toRemove) {
                        ((CommitterRole) r).getArtifacts().remove(rev);
                    }
                    break;
                }
            }
            if (needUpdate) {
                fPersistenceProvider.update(authPerson);
                LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_removedArtifacts, revNum, author));
            }
        }
    }

    /**
     * It does the actual Directory creation described in createDirectory.
     * 
     * @param fullPath
     *            The full path of the Directory to be created.
     * @return The Directory created, or the one found (if it exists already).
     * @see org.evolizer.model.resources.entities.fs.Directory
     * @see org.evolizer.versioncontrol.svn.importer.mapper.SVNModelMapper.createDirectory
     */
    private Directory internalCreateDirectory(String fullPath) {
        Directory directory = fDirectories.get(fullPath);
        if (directory == null) {
            directory = new Directory(fullPath, null);
            fCachedRoot = directory; // I cache the deepest, non saved parent.
            Directory parent = null;
            if (!fullPath.equals("/")) {
                String parentPath = getParentDirectoryPath(fullPath);
                parent = fDirectories.get(parentPath);
                if (parent == null) {
                    parent = internalCreateDirectory(parentPath);
                }
                directory.setParentDirectory(parent); // SourceUnit -> Directory
                parent.add(directory); // Directory -> SourceUnit
            }
            fDirectories.put(fullPath, directory);
            LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_createdDirectory, fullPath));
        }
        return directory;
    }

    /**
     * Creates a Directory object, only if it hasn't been already created. It also recursively creates all parent
     * folders, if the do not exist yet, until the root is found.
     * 
     * @param fullPath
     *            The full path of the Directory to be created.
     * @return The Directory created, or the one found (if it exists already).
     * @see org.evolizer.model.resources.entities.fs.Directory
     */
    public Directory createDirectory(String fullPath) {
        Directory dir = internalCreateDirectory(fullPath);
        if (fCachedRoot != null) {
            /*
             * Thanks to cascading I just need to add to the 
             * file cache only the "deepest" non saved parent.
             * In fact, all its children will
             * then be automatically saved.
             */
            fFilesCache.add(fCachedRoot);
            fCachedRoot = null;
        }
        return dir;
    }

    /**
     * Creates a File object, only if it hasn't been already created. It also recursively creates all parent folders, if
     * the do not exist yet, until the root is found.
     * 
     * @param fullPath
     *            The full path of the File to be created.
     * @return The File created, or the one found (if it exists already).
     * @see org.evolizer.model.resources.entities.fs.File
     * @see #createDirectory(String)
     */
    public SVNVersionedFile createFile(String fullPath) {
        SVNVersionedFile file = fFiles.get(fullPath);
        if (file == null) {
            file = new SVNVersionedFile(fullPath, null);
            String parentPath = getParentDirectoryPath(fullPath);
            Directory parent = fDirectories.get(parentPath);
            if (parent == null) {
                parent = createDirectory(parentPath);
            }
            file.setParentDirectory(parent); // SourceUnit -> Directory
            parent.add(file); // Directory -> SourceUnit
            if (fCachedRoot == null) {
                fFilesCache.add(file);
            }
            fFiles.put(fullPath, file);
            LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_createdFile, fullPath));
        }
        return file;
    }

    /**
     * Creates a SVNRevision of a given File.
     * 
     * @param number
     *            The revision number.
     * @param file
     *            The File associated to the SVNRevision.
     * @param changeSet
     *            The Transaction associated to the SVNRevision (the one during which this SVNRevision was committed).
     * @param report
     *            The ModificationReport associated to the SVNRevision.
     * @param copyPath
     *            The path of original File from which the File associated to this SVNRevision was copied from, only in
     *            case the SVNRevision to be created is actually a move, otherwise it has to be null.
     * @param deleted
     *            <code>true</code> if the SVNRevision to be created represent a deletion of a file otherwise
     *            <code>false</code>.
     * @param contents
     *            The contents of the file to add (can be null).
     * @return The SVNRevision created.
     * @throws SVNImporterException
     *             If the parameter deleted is true and copyPath is not null OR if copyPath is not null but it does not
     *             point to any existing file. In the first case, this would mean that I'm trying to create a
     *             SVNRevision of a File that has been deleted and copied from another File at the same time. In the
     *             second case it would mean that I'm copying a file that does not exist.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRevision
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.ModificationReport
     * @see #createRevision(long, SVNVersionedFile, Transaction, ModificationReport, String, boolean, String, String)
     */
    public SVNRevision createRevision(
            long number,
            SVNVersionedFile file,
            Transaction changeSet,
            ModificationReport report,
            String copyPath,
            boolean deleted,
            String contents) throws SVNImporterException {
        return this.createRevision(number, file, changeSet, report, copyPath, deleted, contents, null);
    }

    /**
     * Creates a SVNRevision of a given File and attaches it to its associated Branch.
     * 
     * @param number
     *            The revision number.
     * @param file
     *            The File associated to the SVNRevision.
     * @param changeSet
     *            The Transaction associated to the SVNRevision (the one during which this SVNRevision was committed).
     * @param report
     *            The ModificationReport associated to the SVNRevision.
     * @param copyPath
     *            The path of original File from which the File associated to this SVNRevision was copied from, only in
     *            case the SVNRevision to be created is actually a move, otherwise it has to be null.
     * @param deleted
     *            <code>true</code> if the SVNRevision to be created represent a deletion of a file otherwise
     *            <code>false</code>.
     * @param contents
     *            The contents of the file to add (can be null).
     * @param branchName
     *            The name of the branch to which this SVNRevision belongs to, if any. Otherwise, it should be null.
     * @return The SVNRevision created.
     * @throws SVNImporterException
     *             If the parameter deleted is true and copyPath is not null OR if copyPath is not null but it does not
     *             point to any existing file. In the first case, this would mean that I'm trying to create a
     *             SVNRevision of a File that has been deleted and copied from another File at the same time. In the
     *             second case it would mean that I'm copying a file that does not exist.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRevision
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.ModificationReport
     * @see #createRevision(long, SVNVersionedFile, Transaction, ModificationReport, String, boolean, String, String)
     */
    public SVNRevision createBranchRevision(
            long number,
            SVNVersionedFile file,
            Transaction changeSet,
            ModificationReport report,
            String copyPath,
            boolean deleted,
            String contents,
            String branchName) throws SVNImporterException {
        return this.createRevision(number, file, changeSet, report, copyPath, deleted, contents, branchName);
    }

    /**
     * Creates a SVNRevision of a given File and attaches it to its associated Branch, if there's any.
     * 
     * @param number
     *            The revision number.
     * @param file
     *            The File associated to the SVNRevision.
     * @param changeSet
     *            The Transaction (SVN change set) associated to the SVNRevision (the one during which this SVNRevision
     *            was committed).
     * @param report
     *            The ModificationReport associated to the SVNRevision.
     * @param copyPath
     *            The path of original File from which the File associated to this SVNRevision was copied from, only in
     *            case the SVNRevision to be created is actually a move, otherwise it has to be null.
     * @param deleted
     *            <code>true</code> if the SVNRevision to be created represent a deletion of a file otherwise
     *            <code>false</code>.
     * @param contents
     *            The contents of the file to add (can be null).
     * @param branchName
     *            The name of the branch to which this SVNRevision belongs to, if any. Otherwise, it should be null.
     * @return The SVNRevision created.
     * @throws SVNImporterException
     *             If the parameter deleted is true and copyPath is not null OR if copyPath is not null but it does not
     *             point to any existing file. In the first case, this would mean that I'm trying to create a
     *             SVNRevision of a File that has been deleted and copied from another File at the same time. In the
     *             second case it would mean that I'm copying a file that does not exist.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRevision
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.ModificationReport
     */
    private SVNRevision createRevision(
            long number,
            SVNVersionedFile file,
            Transaction changeSet,
            ModificationReport report,
            String copyPath,
            boolean deleted,
            String contents,
            String branchName) throws SVNImporterException {

        SVNRevision newRevision = new SVNRevision(number + "");
        SVNRevision oldRevision = (SVNRevision) file.getLatestRevision();
        
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_createdRevision, file.getPath(), number));
        // Check if I'm actually modifying a revision that I was already created handling the same transaction.
        if (oldRevision != null && oldRevision.getNumber().compareTo(number + "") == 0) {
            newRevision = oldRevision;
            oldRevision = (SVNRevision) oldRevision.getPreviousRevision();
        } else {
            // Transaction -> SVNRevision
            changeSet.addRevision(newRevision);
            if (oldRevision != null) {
                // Older SVNRevision -> New one
                oldRevision.setNextRevision(newRevision);
                // New SVNRevision -> Old one
                newRevision.setPreviousRevision(oldRevision); 
            }
            // File -> SVNRevision
            file.addRevision(newRevision);
        }
        
        newRevision.setChangeSet(changeSet);        
        newRevision.setFile(file); // SVNRevision -> File
        newRevision.setReport(report); // SVNRevision -> ModificationReport
        
        if (deleted && (copyPath != null)) {
            // If this SVNRevision is actually the deletion of the related file, I set its state as deleted
            throw new SVNImporterException(MapperMessages.SVNModelMapper_cannotCreateRevision);
        } else if (deleted) {
            newRevision.setState(MapperMessages.SVNModelMapper_DELETED);
        } else if (copyPath != null) {
            // If copy path is not null, it means that the current file version is a copy from a preexisting file
            SVNVersionedFile originalFile = fFiles.get(copyPath);
            if (originalFile != null) {
                file.setCopiedFrom(originalFile);
                originalFile.setCopiedTo(file);
                // I get the latest version of the file we are copying from so that I can link to it
                // with the "ancestor" property
                newRevision.setAncestor((SVNRevision) originalFile.getLatestRevision());
                originalFile.getLatestRevision().setState(MapperMessages.SVNModelMapper_MOVED);
                fRevisionElemsCache.add(originalFile.getLatestRevision());
            } else {
                LOGGER.warn(NLS.bind(MapperMessages.SVNModelMapper_fileNotExists, file.getPath(), copyPath));
            }
        }
        // If branchName is not null, it means that the SVNRevision I'm creating is being committed to a branch, which
        // thus I'll attach this to it right away.
        if (branchName != null) {
            Branch branch = fBranches.get(branchName);

            if (branch == null) {
                /*
                 * If no branch exists, it is probably due to the fact that the whole
                 * project was moved into its current repository somewhere in time.
                 * E.g. The apache incubation policy.
                 * So i create the branch the first time I encounter it.
                 */
                branch = createSpecialBranch(report.getCreationTime(), branchName);
                LOGGER.warn(NLS.bind(MapperMessages.SVNModelMapper_revisionDoesNotBelong, branchName));
            } else {
                // A branch, as a release, has only the latest revision directly attached to it,
                // I need to replace the old one with the new one I found now (if it exists).
                if (newRevision.getPreviousRevision() != null) {
                    branch.getRevisions().remove(newRevision.getPreviousRevision());
                }
                LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_createdBranchRevision, new String[]{
                        newRevision.getFile().getPath(),
                        newRevision.getNumber(),
                        branch.getName()}));
            }
            // Attaching the newRevision to the branch, so matter what kind of a branch i'm dealing with
            branch.addRevision(newRevision);
            fCurrBranches.add(branch);
        }
        if (!deleted && contents != null && !contents.equals("")) {
            newRevision.setSource(contents);
        }
        // Getting the committer role and adding this new revision to it
        for (Role r : report.getAuthor().getRoles()) {
            if (r instanceof CommitterRole) {
                ((CommitterRole) r).addRevision(newRevision);
                break;
            }
        }
        fRevisionElemsCache.add(file);
        fRevisionElemsCache.add(newRevision);
        return newRevision;
    }

    /**
     * Creates a ModificationReport without lines added and deleted information.
     * 
     * @param creationTime
     *            The report creation time.
     * @param message
     *            The report message.
     * @param person
     *            The name of the report author.
     * @return The ModificationReport created.
     * @see org.evolizer.versioncontrol.cvs.model.entities.ModificationReport
     */
    public ModificationReport createModificationReport(Date creationTime, String message, String person) {
        return this.createModificationReport(creationTime, message, person, -1, -1);
    }

    /**
     * Creates a ModificationReport object.
     * 
     * @param creationTime
     *            The report creation time.
     * @param message
     *            The report message.
     * @param author
     *            The name of the report author.
     * @param linesAdded
     *            The lines added in the changes related to the report.
     * @param linesDeleted
     *            The lines deleted in the changes related to the report.
     * @return The ModificationReport created
     * @see org.evolizer.versioncontrol.cvs.model.entities.ModificationReport
     */
    public ModificationReport createModificationReport(
            Date creationTime,
            String message,
            String author,
            int linesAdded,
            int linesDeleted) {

        String commitMessage = message;
        if ((commitMessage == null) || (commitMessage.compareTo("") == 0)) {
            commitMessage = MapperMessages.SVNModelMapper_emptyMessage;
        }

        // Create Person
        Person authorPerson = createPerson(author);

        ModificationReport report = new ModificationReport();
        report.setCreationTime(creationTime);
        report.setCommitMessage(commitMessage);
        report.setLinesAdd(linesAdded);
        report.setLinesDel(linesDeleted);
        report.setAuthor(authorPerson);

        return report;
    }

    /**
     * Creates a Person object, only if it hasn't been already created.
     * 
     * @param name
     *            The name of the Person to create.
     * @return The created Person created, or the one found if it exists already.
     * @see org.evolizer.model.resources.entities.humans.Person
     */
    public Person createPerson(String name) {
        String authorName = name;
        if ((authorName == null) || (authorName.compareTo("") == 0)) {
            authorName = MapperMessages.SVNModelMapper_noAuthor;
        }

        Person person = fPersons.get(authorName);
        if (person == null) {
            person = new Person();

            if (isEmail(authorName)) {
                person.setEmail(authorName);
            }
            // TODO should we be more precise?
            person.setFirstName(authorName);

            CommitterRole role = new CommitterRole();
            person.addRole(role);
            fCurrPerson = person;
            fCurrRole = role;
            fPersons.put(authorName, person);
            LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_createdPerson, name));
        }
        return person;
    }

    /**
     * Creates a Release object.
     * 
     * @param date
     *            The creation date of the Release.
     * @param changeSet
     *            The Transaction associated to the Release creation.
     * @param message
     *            The commit message associated to the Release.
     * @param author
     *            The creator of the Release.
     * @param toPath
     *            The path on which the release/tag is created.
     * @param fromPath
     *            The path from where the release/tag was copied.
     * @param copyRevisionNum
     *            The SVNRevision number from where the release is being copied.
     * @param revisionNum
     *            The SVNRevision number of the release creation.
     * @return The Release created.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     */
    public SVNRelease createRelease(
            Date date,
            Transaction changeSet,
            String message,
            String author,
            String toPath,
            String fromPath,
            long copyRevisionNum,
            long revisionNum) {

        // Creates the release
        SVNRelease release = new SVNRelease(getFolderName(toPath));
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_createdRelease, release.getName()));
        release.setUrl(toPath);
        release.setTimeStamp(date);
        // Connect the Release to its SVNRevisions
        connectRevisionToBranchOrRelease(
                date,
                changeSet,
                message,
                author,
                null,
                release,
                toPath,
                fromPath,
                copyRevisionNum,
                revisionNum);
        return release;
    }

    /**
     * Converts a specific Release to a Branch. It should be used when dealing with non-standard repositories, as I that
     * case is really hard to discriminate between branches and releases. So they should all be at first considered
     * releases, and then converted to branches whenever they are involved in some SVN operation (add, del, etc.) after
     * their creation (As releases are, on the other hand never touched again after their creation).
     * 
     * @param releaseName
     *            The name of the release to be converted.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     */
    public void convertReleaseToBranch(String releaseName) {
        SVNRelease release =
                fPersistenceProvider.uniqueResult(
                        "from SVNRelease as r where r.name = '" + releaseName + "'",
                        SVNRelease.class);
        Branch branch = new Branch(releaseName);

        // Remove the release reference from all the existing revisions
        for (Revision version : release.getRevisions()) {
            version.getReleases().remove(release);
        }
        for (Revision version : release.getReleaseRevisions()) {
            version.getReleases().remove(release);
            branch.addRevision(version);
        }
        branch.setCreationDate(release.getTimeStamp());
        fBranches.put(releaseName, branch);
        release.setReleaseRevisions(null);
        release.setRevisions(null);
        fPersistenceProvider.delete(release);
        fPersistenceProvider.flush();
    }

    /**
     * Deletes a Directory, and all its content(subDirs and Files) from the SVN model that is being created.
     * 
     * @param fullPath
     *            The repository path to the Directory to be deleted.
     * @param revNum
     *            The SVNRevision number on which the delete is taking place.
     * @param changeSet
     *            The Transaction associated to this deletion.
     * @param date
     *            The Date of this deletion.
     * @param message
     *            The commit message related to this deletion.
     * @param author
     *            The author of the deletion.
     * @param branch
     *            The branch to which the directory belongs, it can be null if the directory does not belong to any
     *            branch.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     */
    public void deleteDirectory(
            String fullPath,
            long revNum,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            String branch) {
        Directory directory = fDirectories.get(fullPath);
        if (directory != null) {
            for (File unit : directory.getChildren()) {
                if (unit instanceof SVNVersionedFile) {
                    try {
                        createRevision(revNum, (SVNVersionedFile) unit, changeSet, this.createModificationReport(
                                date,
                                message,
                                author), null, true, null, branch);
                    } catch (SVNImporterException e) {
                        // The exception is actually never thrown in this case
                        e.printStackTrace();
                    }
                } else if (unit instanceof Directory) {
                    // I dig down in the subdirectories, as I need to "delete" them too
                    deleteDirectory(((Directory) unit).getPath(), revNum, changeSet, date, message, author, branch);
                }
            }
//            fDirectories.remove(directory.getPath());
        }
    }

    /**
     * Returns the mapped Branches.
     * 
     * @return the Branches.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branches
     */
    public HashMap<String, Branch> getBranches() {
        return fBranches;
    }

    /**
     * Returns the mapped Directories.
     * 
     * @return the directories.
     * @see org.evolizer.model.resources.entities.fs.Directory
     */
    public HashMap<String, Directory> getDirectories() {
        return fDirectories;
    }

    /**
     * Returns the mapped Files.
     * 
     * @return the files.
     * @see org.evolizer.model.resources.entities.fs.File
     */
    public HashMap<String, SVNVersionedFile> getFiles() {
        return fFiles;
    }

    /**
     * Extracts the folder name from a given path.
     * 
     * @param path
     *            The path to extract the folder name from.
     * @return The folder name.
     */
    private String getFolderName(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    /**
     * Given a List of SVNRevisions, it returns the one closest to a specific SVNRevision number.
     * 
     * @param SVNRevisions
     *            The SVNRevision List from which to extract the one closest to the SVNRevision number.
     * @param revNum
     *            The SVNRevision number.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRevision
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile
     */
    private SVNRevision getClosestRevision(SVNVersionedFile f, long revNum) {
        /*
         * SVNRevisions is a list order by number of SVNRevision (from the earliest to the oldest).
         * So I'll go from the last element down. So, when a SVNRevision that has a SVNRevision
         * number smaller or equal to the wanted SVNRevision number if found, it's for sure the closest. 
         */
        for (int i = f.getRevisions().size() - 1; i >= 0; i--) {
            SVNRevision revision = (SVNRevision) f.getRevisions().get(i);
            if (Long.parseLong(revision.getNumber()) <= revNum) {
                return revision;
            }
        }
        return null;
    }

    /**
     * Extracts parent directory path from given path.
     * 
     * @param path
     *            The path from which to extract the parent directory from.
     * @return The parent directory found.
     */
    private String getParentDirectoryPath(String path) {
        return path.substring(0, path.lastIndexOf("/") <= 0 ? 1 : path.lastIndexOf("/")); //$NON-NLS-2$
    }

    /**
     * Returns the mapped Persons.
     * 
     * @return the persons.
     * @see org.evolizer.model.resources.entities.humans.Person
     */
    public HashMap<String, Person> getPersons() {
        return fPersons;
    }

    /**
     * Returns the previous revision number of a given File SVNRevision, -1 if it does not exist.
     * 
     * @param changedPath
     *            The path of the File.
     * @param currSVNRevision
     *            The revision number of that File.
     * @return The previous revision number.
     */
    public String getPreviousRevision(String changedPath, long currSVNRevision) {
        SVNRevision revision = null;
        long currRevision = currSVNRevision;
        while (revision == null && currRevision >= 0) {
            revision =
                    fPersistenceProvider.uniqueResult("FROM SVNRevision as f WHERE f.file.path='" + changedPath
                            + "' AND f.number = '" + (--currRevision) + "' ORDER BY(f.number) DESC", SVNRevision.class);
        }
        if ((revision != null)) {
            return revision.getNumber();
        } else {
            return "-1";
        }
    }

    /**
     * Checks if a given String contains an email address.
     * 
     * @param input
     *            The string to check.
     * @return if an email address was found.
     */
    private boolean isEmail(String input) {
        Pattern p = Pattern.compile(MapperMessages.SVNModelMapper_emailRegEx); // name@subdomain.domain.suffix
        Matcher m = p.matcher(input);
        if (m.find()) {
            return true;
        }

        return false;
    }

    /**
     * Checks if given path was found as a directory and only as directory.
     * 
     * @param directoryPath
     *            The path to check.
     * @return true if the given path refers to a directory.
     */
    public boolean isOnlyDirectory(String directoryPath) {
        return fDirectories.get(directoryPath) != null && fFiles.get(directoryPath) == null;
    }

    /**
     * Checks if given path refers to a file and only a file (not a directory).
     * 
     * @param filePath
     *            the path to check.
     * @return true if the given path refers to a file.
     */
    public boolean isOnlyFile(String filePath) {
        return fFiles.get(filePath) != null && fDirectories.get(filePath) == null;
    }

    /**
     * Deletes recursively all the contents of a Directory belonging to a specific Release.
     * 
     * @param toRemove
     *            A list containing all the Directories that have been so far removed from the Release.
     * @param dir
     *            The Directory to remove.
     * @param rel
     *            The Release to modify.
     * @see org.evolizer.model.resources.entities.fs.Directory
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     */
    private void removeDirContentFromRelease(List<Directory> toRemove, Directory dir, SVNRelease rel) {
        toRemove.add(dir);
        for (File unit : dir.getChildren()) {
            if (unit instanceof SVNVersionedFile) {
                // If it's a File I'll just remove it
                removeRevisionFromRelease((SVNVersionedFile) unit, rel);
                fFiles.remove(unit.getPath());
            } else if (unit instanceof Directory) {
                // If it's a Directory, I keep on recursing.
                removeDirContentFromRelease(toRemove, (Directory) unit, rel);
            }
        }
    }

    /**
     * Deletes recursively all the contents of a Directory belonging to a specific Branch.
     * 
     * @param dir
     *            The Directory to remove.
     * @param branch
     *            The Branch to modify.
     * @see org.evolizer.model.resources.entities.fs.Directory
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     */
    private void removeDirContentFromBranch(Directory dir, Branch branch) {
        for (File unit : dir.getChildren()) {
            if (unit instanceof SVNVersionedFile) {
                // If it's a File I'll just remove it
                removeRevisionFromBranch((SVNVersionedFile) unit, branch);
//                fFiles.remove(unit.getPath());
            } else if (unit instanceof Directory) {
                // If it's a Directory, I keep on recursing.
                removeDirContentFromBranch((Directory) unit, branch);
            }
        }
//        fDirectories.remove(dir.getPath());
    }

    /**
     * Removes the last SVNRevision of a File belonging to a specific Release.
     * 
     * @param file
     *            The File to which the SVNRevision to remove belongs to
     * @param release
     *            The Release to modify.
     * @see org.evolizer.model.resources.entities.fs.File
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     */
    private void removeRevisionFromRelease(SVNVersionedFile file, SVNRelease release) {
        SVNRevision toRemove = (SVNRevision) file.getLatestRevision();
        release.removeReleaseRevision(toRemove);
        toRemove.getReleases().remove(release); // SVNRevision ->Release
        toRemove.getChangeSet().getInvolvedRevisions().remove(toRemove);
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_removedRevisionFromRelease, new String[]{
                file.getPath(),
                file.getLatestRevision().getNumber(),
                release.getName()}));
    }

    /**
     * Removes the last SVNRevision of a specific File belonging to a specific Branch.
     * 
     * @param file
     *            The File to which the SVNRevision to remove belongs to.
     * @param branch
     *            The Branch to modify.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     */
    private void removeRevisionFromBranch(SVNVersionedFile file, Branch branch) {
        // Straight forward. A Branch has always a reference to just the latest SVNRevision of
        // all the files it is made of.
        // So I'll just get the latest SVNRevision of the File and remove the references.
        SVNRevision toRemove = (SVNRevision) file.getLatestRevision();
        branch.getRevisions().remove(toRemove); // Release -> SVNRevision
        toRemove.getChangeSet().getInvolvedRevisions().remove(toRemove);
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_removedRevisionFromBranch, new String[]{
                file.getPath(),
                file.getLatestRevision().getNumber(),
                branch.getName()}));
    }

    /**
     * Removes some content (file and directories) from a specific Release.
     * 
     * @param release
     *            The Release to modify.
     * @param path
     *            The release repository path of the contents to be removed.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     */
    public void removeFromRelease(SVNRelease release, String path) {
        Directory directory = fDirectories.get(path);
        // Check if the content to be removed is a Directory
        if (directory != null) {
            // If so, I need to dig down into the directory structure and remove all the content I find

            List<Directory> toRemove = new ArrayList<Directory>();
            removeDirContentFromRelease(toRemove, directory, release);
            // Remove the dir recursively to make some space. It's not really needed, but it could be useful.
            for (Directory dir : toRemove) {
                fDirectories.remove(dir.getPath());
            }
            toRemove = null;
        } else {
            SVNVersionedFile file = fFiles.get(path);
            if (file != null) {
                // If it's a File I'll just proceed with its removal.
                removeRevisionFromRelease(file, release);
                // Remove the file to make some space
                fFiles.remove(path);
            }
        }
    }

    /**
     * Removes some content (file and directories) from a specific Branch.
     * 
     * @param branch
     *            The Branch to modify.
     * @param path
     *            The branch repository path of the contents to be removed.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     */
    public void removeFromBranch(Branch branch, String path) {
        Directory directory = fDirectories.get(path);
        // Check if the content to be removed is a Directory
        if (directory != null) {
            // If so, I need to dig down into the directory structure and remove all the content I find
            removeDirContentFromBranch(directory, branch);
//            fDirectories.remove(path);
        } else {
            SVNVersionedFile file = fFiles.get(path);
            if (file != null) {
                // If it's a File I'll just proceed with its removal.
                removeRevisionFromBranch(file, branch);
                // Remove the file to make some space
//                fFiles.remove(path);
            }
        }
    }

    /**
     * Replaces recursively all the contents of a Directory belonging to a specific Release or Branch, with the contents
     * of another Directory.
     * 
     * @param dir
     *            The Directory to be replaced.
     * @param changeSet
     *            The change set associated to the replacement.
     * @param date
     *            The date of the replacement.
     * @param message
     *            The commit message of the replacement.
     * @param author
     *            The author of the replacement.
     * @param release
     *            The Release to be modified, has to be null if the replacement is done on a branch.
     * @param branch
     *            The Branch to be modified, has to be null if the replacement is done on a release.
     * @param releasePath
     *            The repository path to the Release.
     * @param replacementPath
     *            The path to the replacement directory.
     * @param replacementRevisionNum
     *            The revision number of the replacement directory.
     * @param releaseRevisionNum
     *            The revision number associated to the release.
     * @throws SVNImporterException
     *             If both branch and release fields are either null or with valid values.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.model.resources.entities.fs.Directory
     */
    private void replaceDirContent(
            Directory dir,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            SVNRelease release,
            Branch branch,
            String releasePath,
            String replacementPath,
            long replacementRevisionNum,
            long releaseRevisionNum) throws SVNImporterException {
        for (File unit : dir.getChildren()) {
            if (unit instanceof SVNVersionedFile) {
                // If it's a File, I replace it right away
                replaceRevision((SVNVersionedFile) unit, changeSet, date, message, author, release, branch, unit
                        .getPath().replace(replacementPath, releasePath), replacementRevisionNum, releaseRevisionNum);
            } else if (unit instanceof Directory) {
                // If it's a Directory I keep on iterating
                replaceDirContent(
                        (Directory) unit,
                        changeSet,
                        date,
                        message,
                        author,
                        release,
                        branch,
                        releasePath,
                        replacementPath,
                        replacementRevisionNum,
                        releaseRevisionNum);
            }
        }
    }

    /**
     * Replaces a SVNRevision belonging to a specific Release or Branch, with another SVNRevision.
     * 
     * @param toCopy
     *            The File to which the SVNRevision to be replaced belongs to.
     * @param changeSet
     *            The change set associated to the replacement.
     * @param date
     *            The date of the replacement.
     * @param message
     *            The commit message of the replacement.
     * @param author
     *            The author of the replacement.
     * @param release
     *            The Release to be modified.
     * @param branch
     *            The Branch to be modified.
     * @param path
     *            The path to the Release/Branch in the repository.
     * @param replacementRevisionNum
     *            The revision number of the replacement SVNRevision.
     * @param currentRevisionNum
     *            The revision number associated to the release/branch.
     * @throws SVNImporterException
     *             If both branch and release fields are either null or with valid values.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     */
    private void replaceRevision(
            SVNVersionedFile toCopy,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            SVNRelease release,
            Branch branch,
            String path,
            long replacementRevisionNum,
            long currentRevisionNum) throws SVNImporterException {
        SVNVersionedFile toUpdate = fFiles.get(path);
        /*
         * Now I need to check whether the file I'm updating exists or not.
         * In fact in an update there could be files and sub directories that don't exist
         * in the directory that is being updated. In that case, I need to create them.
         */
        if (toUpdate != null) {
            // that file actually exists
            SVNRevision revision = getClosestRevision(toCopy, replacementRevisionNum);
            if (revision != null) {
                ((SVNRevision) toUpdate.getLatestRevision()).setAncestor(revision);
            }
        } else {
            // That file does not exist yet
            toUpdate = (SVNVersionedFile) createFile(path);
            toUpdate.setCopiedFrom(toCopy);
            toCopy.setCopiedTo(toUpdate);
            SVNRevision revision = getClosestRevision(toCopy, replacementRevisionNum);
            ModificationReport report = this.createModificationReport(date, message, author);

            SVNRevision newRevision = new SVNRevision("" + currentRevisionNum);
            // Usual SVNRevision initialization
            changeSet.addRevision(newRevision); // Transaction -> SVNRevision
            newRevision.setChangeSet(changeSet); // SVNRevision -> Transaction
            newRevision.setAncestor(revision);
            toUpdate.addRevision(newRevision); // File -> SVNRevision
            newRevision.setFile(toUpdate); // SVNRevision -> File
            newRevision.setReport(report); // SVNRevision -> ModificationReport
            if (release != null) {
                release.addReleaseRevision(newRevision); // Release -> SVNRevision
                LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_addedFileToRelease, new String[]{
                        toUpdate.getPath(),
                        toUpdate.getLatestRevision().getNumber(),
                        release.getName()}));
            } else if (branch != null) {
                branch.addRevision(newRevision); // Release -> SVNRevision

                LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_addedFileToBranch, new String[]{
                        toUpdate.getPath(),
                        toUpdate.getLatestRevision().getNumber(),
                        branch.getName()}));
            } else {
                throw new SVNImporterException(MapperMessages.SVNModelMapper_misuse);
            }
        }
    }

    /**
     * Replaces some contents (directories and files) of a Release.
     * 
     * @param release
     *            The Release to modify.
     * @param changeSet
     *            The Transaction associated to the replacement.
     * @param date
     *            The date of the replacement.
     * @param message
     *            The commit message associated to the replacement.
     * @param author
     *            The author of the replacement.
     * @param releasePath
     *            The repository path to the release.
     * @param replacementPath
     *            The repository path to the replacement.
     * @param replacementRevisionNum
     *            The revision number of the replacement SVNRevision.
     * @param releaseRevisionNum
     *            The revision number associated to the release.
     * @throws SVNImporterException
     *             If the replacementPath doesn't point to a Directory nor a File.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     */
    public void replaceInRelease(
            SVNRelease release,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            String releasePath,
            String replacementPath,
            long replacementRevisionNum,
            long releaseRevisionNum) throws SVNImporterException {
        // I check whether a directory with the replacementPath (copy path of SVN) exists.
        Directory directory = fDirectories.get(replacementPath);
        if (directory != null) {
            // If it exists I iterate on its children
            replaceDirContent(
                    directory,
                    changeSet,
                    date,
                    message,
                    author,
                    release,
                    null,
                    releasePath,
                    replacementPath,
                    replacementRevisionNum,
                    releaseRevisionNum);
        } else {
            // If a directory with that path doesn't exist a File must. Otherwise I throw an exception
            // as apparently I'm trying to replace a dir or a file with something that is neither a file nor a dir!!
            SVNVersionedFile toCopy = fFiles.get(replacementPath);
            if (toCopy != null) {
                replaceRevision(
                        toCopy,
                        changeSet,
                        date,
                        message,
                        author,
                        release,
                        null,
                        releasePath,
                        replacementRevisionNum,
                        releaseRevisionNum);
            } else {
                LOGGER.error(NLS.bind(MapperMessages.SVNModelMapper_neitherDirectoryNorFile, replacementPath));
            }
        }
    }

    /**
     * Replaces some contents (directories and files) of a Branch.
     * 
     * @param branch
     *            The Branch to modify.
     * @param changeSet
     *            The Transaction associated to the replacement.
     * @param date
     *            The date of the replacement.
     * @param message
     *            The commit message associated to the replacement.
     * @param author
     *            The author of the replacement.
     * @param branchPath
     *            The repository path to the branch.
     * @param replacementPath
     *            The repository path to the replacement.
     * @param replacementRevisionNum
     *            The revision number of the replacement SVNRevision.
     * @param branchRevisionNum
     *            The revision number associated to the branch.
     * @throws SVNImporterException
     *             If the replacementPath doesnn't point to a Directory nor a File.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     * @see org.evolizer.versioncontrol.cvs.model.entities.Branch
     */
    public void replaceInBranch(
            Branch branch,
            Transaction changeSet,
            Date date,
            String message,
            String author,
            String branchPath,
            String replacementPath,
            long replacementRevisionNum,
            long branchRevisionNum) throws SVNImporterException {
        // I check whether a directory with the replacementPath (copy path of SVN) exists.
        Directory directory = fDirectories.get(replacementPath);
        if (directory != null) {
            // If it exists I iterate on its children
            replaceDirContent(
                    directory,
                    changeSet,
                    date,
                    message,
                    author,
                    null,
                    branch,
                    branchPath,
                    replacementPath,
                    replacementRevisionNum,
                    branchRevisionNum);
        } else {
            // If a directory with that path doesn't exist a File must. Otherwise I throw an exception
            // as apparently I'm trying to replace a dir or a file with something that is neither a file nor a dir!!
            SVNVersionedFile toCopy = fFiles.get(replacementPath);
            if (toCopy != null) {
                replaceRevision(
                        toCopy,
                        changeSet,
                        date,
                        message,
                        author,
                        null,
                        branch,
                        branchPath,
                        replacementRevisionNum,
                        branchRevisionNum);
            } else {
                throw new SVNImporterException(NLS.bind(
                        MapperMessages.SVNModelMapper_neitherDirectoryNorFile,
                        replacementPath));
            }
        }
    }

    /**
     * Saves the currently available representation to database.
     */
    public void saveRepresentation() {
        fPersistenceProvider.startTransaction();
        for (Person p : fPersons.values()) {
            fPersistenceProvider.saveOrUpdate(p);
        }
        for (Branch b : fBranches.values()) {
            fPersistenceProvider.saveOrUpdate(b);
        }
        fPersistenceProvider.endTransaction();
        LOGGER.debug(MapperMessages.SVNModelMapper_savedModel);
    }

    /**
     * Connects all the SVNRevisions found so far to a specific release.
     * 
     * @param rel
     *            The release to attach the SVNRevisions to.
     * @see org.evolizer.versioncontrol.svn.model.entities.SVNRelease
     */
    public void finalizeRelease(SVNRelease rel) {
        /*
         * Trying to free up a bit of space.
         * In fact I don't need to keep all the dirs and files
         * associated to the release, as they won't be used anymore.
         * 
         */
        Directory dir = fDirectories.get(rel.getUrl());
        if (dir != null) {
            recurseDelete(fDirectories.get(rel.getUrl()));
        }
        for (Revision tmp : rel.getReleaseRevisions()) {
            fFiles.remove(tmp.getFile().getPath());
            SVNRevision f = ((SVNRevision) tmp).getAncestor();
            if (f != null) {
                rel.addRevision(f);
                // I check that the file version doesn't refer to a deleted file
                if (f.getState() == null) {
                    while (f != null) {
                        f.addRelease(rel);
                        f = (SVNRevision) f.getPreviousRevision();
                    }
                }
            } else {
                rel.addRevision(tmp);
            }
        }
        fPersistenceProvider.saveOrUpdate(rel);
        fPersistenceProvider.flush();
        LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_savedRelease, rel.getName()));
    }

    /**
     * Deletes a given Directory and all its children.
     * 
     * @param dir
     *            The Directory to delete
     */
    private void recurseDelete(Directory dir) {
        fDirectories.remove(dir.getPath());
        for (File f : dir.getChildren()) {
            if (f instanceof Directory) {
                fDirectories.remove(((Directory) f).getPath());
                recurseDelete((Directory) f);
            } else {
                fFiles.remove(f.getPath());
            }
        }
    }

    /**
     * Loads from the DB all the entities needed to continue an import or update the data, with other, newer revisions.
     * 
     * @param tagsPath
     *            The repository path to the tags folder.
     */
    public void restoreContents(String tagsPath) {
        List<Person> persons = fPersistenceProvider.query("from Person", Person.class);
        for (Person p : persons) {
            fPersons.put(p.getFirstName(), p);
        }
        // I get the tags directory as I don't need to load files and dirs from the tags location.
        String tagsDir =
                fPersistenceProvider.query(
                        "from Directory as d where d.path LIKE '%" + tagsPath + "%' order by d.path ASC",
                        Directory.class).iterator().next().getPath();
        List<SVNVersionedFile> files =
                fPersistenceProvider.query(
                        "from SVNVersionedFile as f where f.path NOT LIKE '" + tagsDir + "%'",
                        SVNVersionedFile.class);
        for (SVNVersionedFile f : files) {
            fFiles.put(f.getPath(), f);
        }
        List<Directory> dirs =
                fPersistenceProvider.query(
                        "from Directory as d where d.path NOT LIKE '" + tagsDir + "%'",
                        Directory.class);
        for (Directory d : dirs) {
            fDirectories.put(d.getPath(), d);
        }
        List<Branch> branches = fPersistenceProvider.query("from Branch", Branch.class);
        for (Branch b : branches) {
            fBranches.put(b.getName(), b);
        }
    }

    /**
     * Get the number of the newest revision in the DB for the current project.
     * 
     * @return The revision number, -1 if no revision was found.
     */
    public long getLastRevisionNum() {
        List<Revision> rev = fPersistenceProvider.query("from Revision order by id DESC", Revision.class);
        if (rev != null && !rev.isEmpty()) {
            return Long.parseLong(rev.iterator().next().getNumber());
        } else {
            return -1;
        }
    }

    /**
     * Cleans up the model after a change set has been found and extracted. Removing all the files and directories that
     * were actually deleted from the repository during the change set.
     * 
     * @param cs
     *            The extracted Transaction.
     * @param toDelete
     *            A list of names of the deleted directories and files that need to be removed.
     * @see org.evolizer.versioncontrol.cvs.model.entities.Transaction
     */
    public void finalizeTransaction(Transaction cs, ArrayList<String> toDelete) {
        /*
         * Freeing up some memory.
         * If some directory were deleted in this revision, 
         * there is no need to keep track of them anymore.
         */
//        for (String d : toDelete) {
//            if (fDirectories.remove(d) == null) {
//                fFiles.remove(d);
//            }
//        }
        // Proceeding to save all the entities found in this Transaction
        fPersistenceProvider.startTransaction();
        // First I save the Files and Dirs
        for (File f : fFilesCache) {
            fPersistenceProvider.saveOrUpdate(f);
        }
        // If a new person was found, I save it together with its committer role
        if (fCurrPerson != null) {
            fPersistenceProvider.saveOrUpdate(fCurrPerson);
            fPersistenceProvider.saveOrUpdate(fCurrRole);
            fCurrPerson = null;
            fCurrRole = null;
        }
        // Saving the SVNVersionedFiles found and the Revision element.
        for (Object o : fRevisionElemsCache) {
            fPersistenceProvider.saveOrUpdate(o);
        }
        // Finally I save the Transaction
        fPersistenceProvider.saveOrUpdate(cs);
        // If the transaction involved a branch, I update the Branch too.
        // In this case if the importer crashes I can resume it with no inconsistencies
        for (Branch b : fCurrBranches) {
            fPersistenceProvider.saveOrUpdate(b);
        }
        fPersistenceProvider.endTransaction();
        fRevisionElemsCache.clear();
        fFilesCache.clear();
        if (cs.getInvolvedRevisions() != null && !cs.getInvolvedRevisions().isEmpty()) {
            LOGGER.debug(NLS.bind(MapperMessages.SVNModelMapper_savedTransaction, cs.getInvolvedRevisions().iterator()
                    .next().getNumber()));
        }
    }
}
