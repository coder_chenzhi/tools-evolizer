package org.evolizer.core.ui;

import org.eclipse.jface.resource.ImageDescriptor;

/**
 * Interface that holds constants for image descriptors that are used in Evolizer.
 *  
 * @author wuersch
 *
 */
public interface ISharedImages {
	/**
	 * Evolizer default icon. 16x16px.
	 */
	public final static ImageDescriptor EVOLIZER_ICON = EvolizerUIPlugin.getDefault().getImageDescriptor("icons/evolizer_icon.png");

}
