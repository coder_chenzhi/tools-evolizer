package org.evolizer.ontology.util;


import static org.junit.Assert.*;

import org.evolizer.ontology.util.Statement;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestStatement {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testToString() {
		Statement<String, String, String> statement = new Statement<String, String, String>("Subject", "Predicate", "Object");
		assertEquals("the toString() method returned an unexpected value.", "<Subject (:String), Predicate (:String), Object (:String)>", statement.toString());
	}
}
