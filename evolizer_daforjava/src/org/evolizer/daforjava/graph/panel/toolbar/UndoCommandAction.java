/*
 * Copyright 2009 Martin Pinzger, Delft University of Technology,
 * and University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.daforjava.graph.panel.toolbar;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.evolizer.daforjava.commands.CommandController;
import org.evolizer.daforjava.graph.panel.DAForJavaGraphPanel;

/**
 * Undoes the last executed command.
 * 
 * @author pinzger
 */
public class UndoCommandAction extends AbstractAction {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3658216913013877601L;
    
    /** The hierarchic graph panel. */
    private final DAForJavaGraphPanel fGraphPanel;

    /**
     * The Constructor.
     * 
     * @param graphPanel the graph panel
     */
    public UndoCommandAction(DAForJavaGraphPanel graphPanel) {
        fGraphPanel = graphPanel;
    }

    /** 
     * {@inheritDoc}
     */
    public void actionPerformed(ActionEvent e) {
        CommandController controller = fGraphPanel.getCommandController();
        controller.undoCommand();
    }
}
