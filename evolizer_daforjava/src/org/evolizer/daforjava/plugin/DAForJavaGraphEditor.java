/*
 * Copyright 2009 Martin Pinzger, Delft University of Technology,
 * and University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.daforjava.plugin;


import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.EditorPart;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.daforjava.commands.AbstractGraphEditCommand;
import org.evolizer.daforjava.commands.additions.AddEntitiesCommand;
import org.evolizer.daforjava.graph.data.GraphLoader;
import org.evolizer.daforjava.graph.panel.DAForJavaGraphPanel;
import org.evolizer.daforjava.graph.panel.MyDropTargetAdapter;
import org.evolizer.daforjava.plugin.configuration.DAForJavaPreferences;
import org.evolizer.daforjava.plugin.selectionhandler.AbstractSelectionHandler;
import org.evolizer.daforjava.plugin.selectionhandler.SelectionHandlerFactory;
import org.evolizer.famix.importer.jobs.FamixParserJob;
import org.evolizer.famix.importer.jobs.FamixStoreModelJob;
import org.evolizer.famix.model.entities.AbstractFamixEntity;
import org.evolizer.famix.model.utils.SnapshotAnalyzer;

/**
 * GraphEditor to display graphs in the eclipse editor area. The content, a
 * {@link java.awt.Frame}, is embedded using the SWT_AWT bridge.
 * 
 * @author Martin Pinzger, Katja Graefenhain
 */
public class DAForJavaGraphEditor extends EditorPart {

    /** The ID for the GraphEditor. */
    public static final String DAFORJAVA_GRAPH_EDITOR = "org.evolizer.daforjava.view.graph.panel.DAForJavaGraphEditor";

    /** The title of the Graph editor which has the default value: Hierarchic Dependency Graph. */
    private String fEditorTitle = "Hierarchic Dependency Graph";

    /** The GraphPanel which displays the DependencyGraph and the toolbar. */
    private DAForJavaGraphPanel fGraphPanel;

    /**
     * Initializes the editor part with a site and input. Initializes drag&drop support.
     * 
     * @param site the site
     * @param input the input
     * 
     * @throws PartInitException the part init exception
     */
    @Override
    public void init(IEditorSite site, IEditorInput input) throws PartInitException {
        if (!(input instanceof DAForJavaEditorInput)) {
            throw new PartInitException("Invalid Input");
        }

        setSite(site);
        setInput(input);

        DAForJavaEditorInput eInput = (DAForJavaEditorInput) input;
        AbstractSelectionHandler selectionHandler = SelectionHandlerFactory.getInstance().getSelectionHandler(eInput.getSelection());
        setPartName(selectionHandler.getEditorTitle());

        try {
            // initialize db connection
            IJavaProject selectedProject = selectionHandler.getSelectedProject();
            String dbUrl = initDatabaseConfiguration(selectedProject.getProject());

            IEvolizerSession session = EvolizerSessionHandler.getHandler().getCurrentSession(dbUrl);
            SnapshotAnalyzer snapshotAnalyzer = new SnapshotAnalyzer(session);
            fGraphPanel = new DAForJavaGraphPanel(new GraphLoader(snapshotAnalyzer));
            fGraphPanel.initGraphPanel();

//            site.getWorkbenchWindow().getActivePage().addPartListener(fPartListener);
            initDragAndDrop();
//            setPartName(selectionHandler.getEditorTitle());

            List<AbstractFamixEntity> entities = selectionHandler.getSelectedEntities(snapshotAnalyzer);
            if (entities.size() == 0) {
                IProgressMonitor pm = Job.getJobManager().createProgressGroup();
                pm.beginTask("Extracting and storing the FAMIX model for further use ...", 10);

                List<IJavaElement> selection = new ArrayList<IJavaElement>();
                selection.add(selectedProject);
                FamixParserJob parseJob = new FamixParserJob(selection);
                parseJob.setUser(true);
                parseJob.setProgressGroup(pm, 5);
                parseJob.schedule();
                parseJob.join();

                Job storeModelJob = new FamixStoreModelJob(dbUrl, parseJob.getFamixModel());
                storeModelJob.setUser(true);
                storeModelJob.setProgressGroup(pm, 5);
                storeModelJob.schedule();
                storeModelJob.join();

                entities = selectionHandler.getSelectedEntities(snapshotAnalyzer);
            }
            
            AbstractGraphEditCommand command = new AddEntitiesCommand(entities, fGraphPanel.getGraphLoader(), fGraphPanel.getEdgeGrouper());
            fGraphPanel.getCommandController().executeCommand(command);

            openDependencyAnalyzerPerspective();
        } catch (InterruptedException ie) {
            throw new PartInitException(ie.getMessage(), ie);
        } catch (EvolizerException ee) {
            throw new PartInitException(ee.getMessage(), ee);
        } catch (CoreException ce) {
            throw new PartInitException(ce.getMessage(), ce);
        }
    }

    /**
     * Inits the database configuration.
     * 
     * @param selectedProject the selected project
     * 
     * @return the string
     * 
     * @throws CoreException the core exception
     * @throws EvolizerException the evolizer exception
     */
    private String initDatabaseConfiguration(IProject selectedProject) throws CoreException, EvolizerException {
        Boolean isInMemoryDBEnabled = false;
        if (selectedProject.getPersistentProperty(DAForJavaPreferences.DB_USE_HSQLDB_INMEMORY) != null) {
            isInMemoryDBEnabled = Boolean.valueOf(selectedProject.getPersistentProperty(DAForJavaPreferences.DB_USE_HSQLDB_INMEMORY));
        }

        String dbUrl = null;
        if (isInMemoryDBEnabled) {
            Properties dbProperties = EvolizerSessionHandler.getDefaultHsqldbInMemoryConfig(selectedProject.getName());
            EvolizerSessionHandler.getHandler().initSessionFactory(dbProperties);
            dbUrl = dbProperties.getProperty("hibernate.connection.url");
            if (dbUrl.startsWith("jdbc:")) {
                dbUrl = dbUrl.substring("jdbc:".length());
            }
        } else {
            EvolizerSessionHandler.getHandler().initSessionFactory(selectedProject);
            dbUrl = EvolizerSessionHandler.getDBUrl(selectedProject);
        }
        return dbUrl;
    }

    /**
     * Open dependency analyzer perspective.
     */
    private void openDependencyAnalyzerPerspective() {
        try {
            getSite().getWorkbenchWindow().getWorkbench().showPerspective(DAForJavaPerspective.PERSPECTIVE_ID, getSite().getWorkbenchWindow());
        } catch (WorkbenchException e) {
            e.printStackTrace();
        }
    }

    /**
     * Inits the drag and drop.
     */
    private void initDragAndDrop() {
        PlatformUI.getWorkbench().getDisplay().addFilter(SWT.DragDetect, new Listener() {
            public void handleEvent(Event event) {
                if (!(event.widget instanceof Tree)) {
                    return;
                }
            }
        });
    }

    /**
     * Returns the panel.
     * 
     * @return The {@link DAForJavaGraphPanel} embedded in this editor.
     */
    public DAForJavaGraphPanel getPanel() {
        return fGraphPanel;
    }

    /**
     * Hides the corresponding filter view when the editor is closed.
     */
    @Override
    public void dispose() {
        super.dispose();
    }

    /**
     * Implements the user interface by embedding a {@link java.awt.Frame}.
     * 
     * @param parent The parent
     */
    @Override
    public void createPartControl(Composite parent) {
        Composite composite = new Composite(parent, SWT.EMBEDDED);
        Frame frame = SWT_AWT.new_Frame(composite);
        Panel panel = new Panel(new BorderLayout());
        panel.add(fGraphPanel);
        frame.add(panel);

    }

    /**
     * Initializes the drag and drop support for the given control based on
     * provided adapter for drop target listeners.
     * 
     * @param control The control
     */
    protected void initializeDragAndDrop(Control control) {
        MyDropTargetAdapter listener = new MyDropTargetAdapter(fGraphPanel);

        DropTarget dropTarget = new DropTarget(control, DND.DROP_COPY | DND.DROP_MOVE);
        dropTarget.setTransfer(listener.getTransfers());
        dropTarget.addDropListener(listener);
    }

    /** 
     * {@inheritDoc}
     */
    public String getTitle() {
        return fEditorTitle;
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        // not implemented
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public void doSaveAs() {
        // not implemented
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public boolean isDirty() {
        // not implemented
        return false;
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public boolean isSaveAsAllowed() {
        // not implemented
        return false;
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public void setFocus() {
        // not implemented
    }
}
